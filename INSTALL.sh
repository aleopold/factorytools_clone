### This INSTALL.sh is auto-generated upon commit. Don\'t touch manually!\n
mkdir WorkArea; cd WorkArea; #!
mkdir build; #!
mkdir src; #!
mkdir run; #!
cd src; #!
setupATLAS; #!
lsetup git; #!
git clone --recursive https://:@gitlab.cern.ch:8443/atlas-phys-susy-wg/Common/FactoryTools.git #!
cd FactoryTools; #!
source util/dependencyHacks.sh; #!!
cp util/CMakeLists.txt.TopLevel ../CMakeLists.txt; #!!
source util/setup.sh; #!!
cd ../../build/; #!!
cmake ../src/; #!
make; #!
source x*-*gcc*-opt/setup.sh  #!!  # (wildcards since os and gcc versions may differ)
