#ifndef REGION_VARIABLE_CALCULATOR_COSMICS_H
#define REGION_VARIABLE_CALCULATOR_COSMICS_H
//author : Karri DiPetrillo 
//date   : October 2016

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AsgTools/ToolHandle.h"
#include <AsgTools/AnaToolHandle.h>
#include "TrackVertexAssociationTool/LooseTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/TightTrackVertexAssociationTool.h"

//#include "xAODTracking/TrackingPrimitives.h"
//#include "TrkVertexFitterInterfaces/IVertexFitter.h"
//#include "TrkV0Fitter/TrkV0VertexFitter.h"
//#include "TrkVertexAnalysisUtils/V0Tools.h"
//#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
//#include "TrkExInterfaces/IExtrapolator.h"
//#include "TrkToolInterfaces/ITrackSelectorTool.h"
//#include "InDetConversionFinderTools/VertexPointEstimator.h"
//#include "InDetConversionFinderTools/InDetConversionFinderTools.h"
//#include "InDetConversionFinderTools/ConversionFinderUtils.h"

#include <map>
#include <iostream>

class RegionVarCalculator_cosmics : public RegionVarCalculator {

public :

private :
  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doSR0LCalculations (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doSR2LCalculations (std::map<std::string, anytype>& /* vars */ );

  CP::LooseTrackVertexAssociationTool *m_trackToVertexToolLoose;
  CP::TightTrackVertexAssociationTool *m_trackToVertexToolTight;

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_cosmics, 1);
  float m_id_d0_min = -1;
  bool m_saveIDtracks = true;
  bool m_saveSegments = false;

};

#endif //REGION_VARIABLE_CALCULATOR_COSMICS_H

//  LocalWords:  ifndef
