#ifndef REGION_VARIABLE_CALCULATOR_ZL_H
#define REGION_VARIABLE_CALCULATOR_ZL_H
//author : Russell Smith
//date   : January 2015
//

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "PATInterfaces/SystematicSet.h"
#include <TLorentzVector.h>
#include <TH1.h>
#include <TMatrixDSym.h>
#include <TMatrixDSymEigen.h>
#include <TVectorD.h>

#include <map>
#include <iostream>

class RegionVarCalculator_zl : public RegionVarCalculator {

public :

private :
  //todo probably clean this up
  virtual EL::StatusCode doInitialize(EL::IWorker * worker);
  
  EL::StatusCode doCalculate         (std::map<std::string, anytype>& /* vars */);
  EL::StatusCode doAllCalculations   (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doSRCalculations    (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doCR1LCalculations  (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doCR2LCalculations  (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doCRYCalculations   (std::map<std::string, anytype>& /* vars */ );

  float calcdPhi(std::vector<xAOD::IParticle*> jetVec, xAOD::MissingET * met, int startJetIndex = 0, int endJetIndex = -1);

  double CalcAplanarity(std::vector<xAOD::IParticle*> jetVec, double Aplanarity);

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_zl, 1);

};

#endif //REGION_VARIABLE_CALCULATOR_ZL_H

//  LocalWords:  ifndef
