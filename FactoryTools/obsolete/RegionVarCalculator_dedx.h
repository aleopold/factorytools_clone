#ifndef REGION_VARIABLE_CALCULATOR_DEDX_H
#define REGION_VARIABLE_CALCULATOR_DEDX_H
//author : Lawrence Lee
//date   : October 2016

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AssociationUtils/OverlapLinkHelper.h"
//#include "xAODTracking/TrackingPrimitives.h"
//#include "TrkVertexFitterInterfaces/IVertexFitter.h"
//#include "TrkV0Fitter/TrkV0VertexFitter.h"
//#include "TrkVertexAnalysisUtils/V0Tools.h"
//#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
//#include "TrkExInterfaces/IExtrapolator.h"
//#include "TrkToolInterfaces/ITrackSelectorTool.h"
//#include "InDetConversionFinderTools/VertexPointEstimator.h"
//#include "InDetConversionFinderTools/InDetConversionFinderTools.h"
//#include "InDetConversionFinderTools/ConversionFinderUtils.h"

#include <iostream>
#include <map>
#include <string>
#include <vector>

class RegionVarCalculator_dedx : public RegionVarCalculator {

public :

private :
  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
  // EL::StatusCode doOtherSRCalculations (std::map<std::string, anytype>& /* vars */ );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_dedx, 1);
  float m_id_d0_min = -1;
  float m_id_pt_min = 1;
  int m_id_prescale = 100;
  bool m_saveIDtracks = true;
  bool m_saveSegments = false;
  std::unique_ptr<ORUtils::OverlapLinkHelper> m_overlapLinkHelper;

};

#endif //REGION_VARIABLE_CALCULATOR_DV_H

//  LocalWords:  ifndef
