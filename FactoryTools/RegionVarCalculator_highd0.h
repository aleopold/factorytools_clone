#ifndef REGION_VARIABLE_CALCULATOR_HIGHD0_H
#define REGION_VARIABLE_CALCULATOR_HIGHD0_H
//author : Lawrence Lee
//date   : October 2016

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

//#include "xAODTracking/TrackingPrimitives.h"
//#include "TrkVertexFitterInterfaces/IVertexFitter.h"
//#include "TrkV0Fitter/TrkV0VertexFitter.h"
//#include "TrkVertexAnalysisUtils/V0Tools.h"
//#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
//#include "TrkExInterfaces/IExtrapolator.h"
//#include "TrkToolInterfaces/ITrackSelectorTool.h"
//#include "InDetConversionFinderTools/VertexPointEstimator.h"
//#include "InDetConversionFinderTools/InDetConversionFinderTools.h"
//#include "InDetConversionFinderTools/ConversionFinderUtils.h"

#include <map>
#include <iostream>

class RegionVarCalculator_highd0 : public RegionVarCalculator {

public :

private :
  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doSR0LCalculations (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doSR2LCalculations (std::map<std::string, anytype>& /* vars */ );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_highd0, 1);
  float m_id_d0_min = -1;
  float m_id_pt_min = 1;
  int m_id_prescale = 100;
  bool m_saveIDtracks = true;
  bool m_saveSegments = false;

};

#endif //REGION_VARIABLE_CALCULATOR_HIGHD0_H

//  LocalWords:  ifndef
