#ifndef FACTORYTOOLS_JetMakerTool_H
#define FACTORYTOOLS_JetMakerTool_H

// making it more like a tool
#include "AsgTools/AsgTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "JetInterface/IJetGroomer.h"
#include "JetInterface/IJetFromPseudojet.h"
#include "JetRec/JetFromPseudojet.h"

//#include "fastjet/contrib/SoftDrop.hh"
#include "fastjet/PseudoJet.hh"
#include "fastjet/JetDefinition.hh"

#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/JetContainer.h"
#include "JetInterface/IJetExecuteTool.h"

#include <EventLoop/Algorithm.h>
#include "JetRec/JetRecTool.h"
#include "JetRec/PseudoJetGetter.h"
#include "JetRec/JetToolRunner.h"
#include "JetRec/PseudoJetGetter.h"
#include "xAODRootAccess/Init.h"
#include "JetInterface/IJetModifier.h"
#include "JetInterface/IJetExecuteTool.h"
#include "JetRec/PseudoJetGetter.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetRec/JetFinder.h"
#include "JetRec/JetSplitter.h"
#include "JetRec/JetRecTool.h"
#include "JetRec/JetDumper.h"
#include "JetRec/JetToolRunner.h"


// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

#include <memory>
#include <vector>

using namespace std;


class JetMakerTool : public xAH::Algorithm{

public:
  xAOD::TEvent *m_event;  //!
  xAOD::TStore *m_store;  //!

  PseudoJetGetter* plcget; //!
  JetFromPseudojet* pbuild; //!
  JetFinder* pfind; //!
  JetRecTool* pjrf; //!
  ToolHandleArray<IPseudoJetGetter> hgets; //!
  ToolHandleArray<IJetExecuteTool> hrecs; //!
  JetToolRunner * jrun; //!

  TString OutputContainer; 
  TString InputClusters;
  TString InputLabel; 
  float JetRadius; 
  float PtMin; 
  TString JetAlgorithm;
  bool onlyPV;
  int minNTracks;
  float minSumPtTracks;
  bool SkipNegativeEnergy; //!

private:

	const xAOD::JetContainer *m_jets;//!

public:
	JetMakerTool ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
	virtual EL::StatusCode histFinalize ();


	ClassDef(JetMakerTool, 1);
};

#endif
