//=================================
// name: RegionVarCalculator_DRAWSelection
// type: Region-based variable calculator
// responsible: rcarney@lbl.gov
// description:
// 	Places uncalib. jets and
// 	'trackless' jets in StoreGate.
// 	Definition of 'trackless' jets is here:
// 	https://gitlab.cern.ch/atlas/athena/blob/master/PhysicsAnalysis/SUSYPhys/LongLivedParticleDPDMaker/python/DVFlags.py
//
//  last updated: Feb. 2018
//=================================
#ifndef REGION_VARIABLE_CALCULATOR_DRAW_SELECTION
#define REGION_VARIABLE_CALCULATOR_DRAW_SELECTION

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AsgTools/ToolHandle.h"
#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/MessageCheck.h>

#include <map>
#include <iostream>
#include <vector>
#include <string>

class RegionVarCalculator_DRAWSelection : public RegionVarCalculator {

public :
  std::vector<std::string> m_triggerNames; 

private :
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode storeDRAWFilterAttributes(std::map<std::string, anytype>& /* vars */ );

public :
  ClassDef(RegionVarCalculator_DRAWSelection, 1);

};

#endif //REGION_VARIABLE_CALCULATOR_DRAW_SELECTION

