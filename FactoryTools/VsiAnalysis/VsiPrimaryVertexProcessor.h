#ifndef VsiAnalysis_VsiPrimaryVertexProcessor_H
#define VsiAnalysis_VsiPrimaryVertexProcessor_H

#include "FactoryTools/ProcessorBase.h"

class VsiPrimaryVertexProcessor : public ProcessorBase {
 public:
  VsiPrimaryVertexProcessor( std::string name = "PrimaryVertex" );
  virtual ~VsiPrimaryVertexProcessor();

  virtual void registerVariables() override;
  virtual EL::StatusCode processDetail( xAOD::TEvent* /*event*/, xAOD::TStore* /*store*/ ) override;
};

#endif /* VsiAnalysis_VsiPrimaryVertexProcessor_H */
