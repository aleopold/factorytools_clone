#ifndef VsiAnalysis_VsiEventDisplay_H
#define VsiAnalysis_VsiEventDisplay_H

#include "FactoryTools/ProcessorBase.h"

class VsiEventDisplay : public ProcessorBase {
 private:

  class Impl;
  std::unique_ptr<Impl> m_impl; //!

 public:

  enum class Client { kVsiAnaExample };

  VsiEventDisplay( const std::string /*name*/ );
  virtual ~VsiEventDisplay();

  void registerVariables() override final;
  EL::StatusCode processDetail( xAOD::TEvent* /*event*/, xAOD::TStore* /*store*/ ) override final;

  void initialize();

  void setDoPrint( bool /* flag */ ) const;
  void setDoBatch( bool /* flag */ ) const;
  void setMaxEvents( const unsigned /* nmax */ ) const;
};

#endif /* VsiAnalysis_VsiEventDisplay_H */
