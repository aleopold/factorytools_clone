#ifndef __VsiAnalysis_VsiTruthHelper__
#define __VsiAnalysis_VsiTruthHelper__

#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTracking/TrackParticle.h>
#include <xAODTracking/TrackParticleContainer.h>

namespace VsiTruthHelper {
  bool selectRhadron    (const xAOD::TruthVertex*);
  bool selectBmeson     (const xAOD::TruthVertex*);
  bool selectDarkPhoton (const xAOD::TruthVertex*);
  bool selectHNL        (const xAOD::TruthVertex*);
  bool selectKshort     (const xAOD::TruthVertex*);
  bool selectHadInt     (const xAOD::TruthVertex*);

  const xAOD::TruthParticle* getTruthParticle       ( const xAOD::TrackParticle* trk );
  const xAOD::TruthParticle* getParentTruthParticle ( const xAOD::TrackParticle* trk );
  const xAOD::TruthVertex*   getProdVtx             ( const xAOD::TrackParticle* trk );

  bool isReconstructed( const xAOD::TruthParticle*, const xAOD::TrackParticleContainer* );
}

#endif
