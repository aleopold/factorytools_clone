/*###############################################
 name: DRAW_DVJETS_filters 
 type: C++ namespace header
 responsible: rcarney@lbl.gov
 
 description:
    Contains a struct, FilterFlags, used to store
    DRAWFilter decisions. Contains flag-setting
    functions and filter logic functions. 

 last updated: May 2018
###############################################*/

#ifndef DRAW_FILTERS_h
#define DRAW_FILTERS_h

#include <xAODJet/JetContainer.h>
#include "EventLoop/StatusCode.h"

#include <TH1F.h>

#include <iostream>
#include <vector>
#include <algorithm>    // std::all_of

namespace DRAW_DVJETS_filters{

        //Bit field for filter decisions
        struct FilterFlags{
            
            //Top level
            unsigned char pass_DRAW : 1;
            unsigned char pass_TracklessPrescale : 1;
            unsigned char pass_earlyRun2    : 1;
            unsigned char pass_lowPtJetMult : 1;
            unsigned char pass_highPtJetMult : 1;

            unsigned char pass_triggerFlags : 1;
            unsigned char pass_singleTracklessFlag : 1;
            unsigned char pass_doubleTracklessFlag : 1;

            //Jet multiplicity
            unsigned char pass_2_jetMultFlag : 1;
            unsigned char pass_3_jetMultFlag : 1;
            unsigned char pass_2AND3_jetMultFlag : 1;
            unsigned char pass_4_jetMultFlag : 1;
            unsigned char pass_5_jetMultFlag : 1;
            unsigned char pass_6_jetMultFlag : 1;
            unsigned char pass_7_jetMultFlag : 1;

            //Jet highpT multiplicity
            unsigned char pass_2_highpT_jetMultFlag : 1;
            unsigned char pass_3_highpT_jetMultFlag : 1;
            unsigned char pass_2AND3_highpT_jetMultFlag : 1;
            unsigned char pass_4_highpT_jetMultFlag : 1;
            unsigned char pass_5_highpT_jetMultFlag : 1;
            unsigned char pass_6_highpT_jetMultFlag : 1;
            unsigned char pass_7_highpT_jetMultFlag : 1;

            //Default value is 0
            FilterFlags(): 
                pass_DRAW(0),
                pass_TracklessPrescale(0),
                pass_earlyRun2(0),
                pass_lowPtJetMult(0),
                pass_highPtJetMult(0),
            
                pass_triggerFlags(0),
                pass_singleTracklessFlag(0),
                pass_doubleTracklessFlag(0),
                
                pass_2_jetMultFlag(0),
                pass_3_jetMultFlag(0),
                pass_2AND3_jetMultFlag(0),
                pass_4_jetMultFlag(0),
                pass_5_jetMultFlag(0),
                pass_6_jetMultFlag(0),
                pass_7_jetMultFlag(0),
                
                pass_2_highpT_jetMultFlag(0),
                pass_3_highpT_jetMultFlag(0),
                pass_2AND3_highpT_jetMultFlag(0),
                pass_4_highpT_jetMultFlag(0),
                pass_5_highpT_jetMultFlag(0),
                pass_6_highpT_jetMultFlag(0),
                pass_7_highpT_jetMultFlag(0){}

            //Reset bits to 0
            void reset(){
                pass_DRAW = 0;
                pass_TracklessPrescale = 0;
                pass_earlyRun2 = 0;
                pass_lowPtJetMult = 0;
                pass_highPtJetMult = 0;
                
                pass_triggerFlags = 0;
                pass_singleTracklessFlag = 0;
                pass_doubleTracklessFlag = 0;
                
                pass_2_jetMultFlag = 0;
                pass_3_jetMultFlag = 0;
                pass_2AND3_jetMultFlag = 0;
                pass_4_jetMultFlag = 0;
                pass_5_jetMultFlag = 0;
                pass_6_jetMultFlag = 0;
                pass_7_jetMultFlag = 0;
                
                pass_2_highpT_jetMultFlag = 0;
                pass_3_highpT_jetMultFlag = 0;
                pass_2AND3_highpT_jetMultFlag = 0;
                pass_4_highpT_jetMultFlag = 0;
                pass_5_highpT_jetMultFlag = 0;
                pass_6_highpT_jetMultFlag = 0;
                pass_7_highpT_jetMultFlag = 0;
            }
        };
            
        //Jet multiplicity flags
        bool DV_2JetFilterFlags( float jetPt );
        bool DV_3JetFilterFlags( float jetPt );
        bool DV_4JetFilterFlags( float jetPt );
        bool DV_5JetFilterFlags( float jetPt );
        bool DV_6JetFilterFlags( float jetPt );
        bool DV_7JetFilterFlags( float jetPt );
   
        //Trackless jet flags
        bool DV_SingleTracklessJetFlag( float jetPt, float jetEta, float sumtrkPt );
        bool DV_DoubleTracklessJetsFlag( float jetPt, float jetEta, float sumTrkPt );
      
        //High pT jet multiplicity flags
        bool DV_2JetFilterFlags_HighPtCut( float jetPt );
        bool DV_3JetFilterFlags_HighPtCut( float jetPt );
        bool DV_4JetFilterFlags_HighPtCut( float jetPt );
        bool DV_5JetFilterFlags_HighPtCut( float jetPt );
        bool DV_6JetFilterFlags_HighPtCut( float jetPt );
        bool DV_7JetFilterFlags_HighPtCut( float jetPt );

        //Check triggers
        bool Pass_triggerFlags( std::vector<int>& trigDecisions );

        //Use functions above to set flags
        void setFilterFlags( const xAOD::JetContainer* uncalibJets, std::vector<int>& trigDecisions, FilterFlags& filterFlags );
        
        //Filter logic
        EL::StatusCode filterEvents( const xAOD::JetContainer* uncalibJets, std::vector<int>& trigDecisions, FilterFlags& filterFlags );


}
#endif

