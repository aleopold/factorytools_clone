#ifndef ProcessorBase_H
#define ProcessorBase_H

#include "FactoryTools/TreeManager.h"
#include "FactoryTools/AnyType.h"
#include "FactoryTools/AlgConsts.h"
#include "FactoryTools/strongErrorCheck.h"

#include <EventLoop/Algorithm.h>
#include <EventLoop/StatusCode.h>
#include <xAODTracking/Vertex.h>

#include <map>
#include <string>
#include <limits>

class ProcessorBase {
 protected:

  static bool s_isMC;
  static xAOD::Vertex* s_thePV;

  std::string m_name;
  std::map<std::string, anytype>& m_vars;

 public:
  ProcessorBase( std::string name );
  virtual ~ProcessorBase();

  inline static bool isMC() { return s_isMC; }
  inline static void setMC( bool flag ) { s_isMC = flag; }
  inline static xAOD::Vertex* getPV() { return s_thePV; }

  virtual void registerVariables() = 0;
  EL::StatusCode process( xAOD::TEvent* /*event*/, xAOD::TStore* store = nullptr );
  void processCommon( xAOD::TEvent* /*event*/, xAOD::TStore* store = nullptr );
  virtual EL::StatusCode processDetail( xAOD::TEvent* /*event*/, xAOD::TStore* /*store*/ ) = 0;

};

#define DEBUG() std::cout << __PRETTY_FUNCTION__ << ": L" << __LINE__ << std::endl;
#define DEBUGW( x ) std::cout << __PRETTY_FUNCTION__ << ": L" << __LINE__ << ":: " << x << std::endl;

#define REFVAR( NAME, TYPE )                             \
  if( typeid(TYPE) != m_vars.at(#NAME).type() ) {        \
    throw std::runtime_error( #NAME " type mismatch" );    \
  }                                                      \
  auto& r_##NAME = m_vars.at(#NAME).getVar<TYPE>()       \

#define AUXDYN( obj, type, varname ) ( obj->isAvailable<type>(varname)? obj->auxdataConst<type>(varname) : std::numeric_limits<type>::quiet_NaN() )

#endif /* ProcessorBase_H */
