#ifndef FactoryTools_CalculateRegionVars_H
#define FactoryTools_CalculateRegionVars_H

#include <EventLoop/Algorithm.h>
#include <FactoryTools/RegionVarCalculator.h>

class CalculateRegionVars : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

private :

public:

  RegionVarCalculator  * m_calculator = nullptr;


  bool writeDebugVars = false;
  std::vector<std::string> listOfDetailedObjects;

  std::vector<std::string> triggerBranches;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)

  // this is a standard constructor
  CalculateRegionVars ();
  // CalculateRegionVars (RegionVarCalculator* calculator=nullptr);

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(CalculateRegionVars, 1);
};

#endif
