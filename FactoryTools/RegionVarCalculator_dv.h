#ifndef REGION_VARIABLE_CALCULATOR_DV_H
#define REGION_VARIABLE_CALCULATOR_DV_H
//author : Lawrence Lee
//date   : October 2016

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AssociationUtils/OverlapLinkHelper.h"
//#include "xAODTracking/TrackingPrimitives.h"
//#include "TrkVertexFitterInterfaces/IVertexFitter.h"
//#include "TrkV0Fitter/TrkV0VertexFitter.h"
//#include "TrkVertexAnalysisUtils/V0Tools.h"
//#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
//#include "TrkExInterfaces/IExtrapolator.h"
//#include "TrkToolInterfaces/ITrackSelectorTool.h"
//#include "InDetConversionFinderTools/VertexPointEstimator.h"
//#include "InDetConversionFinderTools/InDetConversionFinderTools.h"
//#include "InDetConversionFinderTools/ConversionFinderUtils.h"

#include <iostream>
#include <map>
#include <string>
#include <vector>

class RegionVarCalculator_dv : public RegionVarCalculator {

public :
  EL::StatusCode findLLPChildren(const xAOD::TruthParticle* parent);
  void fillChildMap( std::map< std::pair<int,int>, const xAOD::TruthParticle* >* childMap, const xAOD::TruthParticle* parent);

private :
  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
  // EL::StatusCode doOtherSRCalculations (std::map<std::string, anytype>& /* vars */ );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_dv, 1);
  float m_id_d0_min = -1;
  float m_id_pt_min = 1;
  int m_id_prescale = 1;
  bool m_saveIDtracks = true;
  bool m_saveSegments = false;
  std::unique_ptr<ORUtils::OverlapLinkHelper> m_overlapLinkHelper; //!
  std::map< std::pair<int,int>, std::pair< const xAOD::TruthParticle*, std::vector< const xAOD::TruthParticle*> > > m_llpChildren; //Might not get filled depending on detailedList
  std::vector< const xAOD::TruthParticle* > m_goodTrack_truthParticles; //TruthParticles with status==1, pT>1GeV, charge>0
  std::string m_dvContainerSuffix = "_fixedExtrapolator"; // plan is to make this configurable, see https://gitlab.cern.ch/atlas-phys-susy-wg/Common/FactoryTools/-/issues/49 - commented out for now! /CO
  // Below are for track jets
  bool onlyPVtrackjets = false;
  int minNTracks = 2;
  float minSumPtTracks = 15.;

};

#endif //REGION_VARIABLE_CALCULATOR_DV_H

//  LocalWords:  ifndef
