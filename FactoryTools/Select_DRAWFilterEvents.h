//=================================
// name: Select_DRAWFilterEvents
// type: Selection tool (header)
// responsible: rcarney@lbl.gov
// description:
// 	Applies no special selections,
// 	simply a placeholder for now.
// 	Necessary for WriteNtupleTool
// 	to retrieve RegionName from aux store.
//
//  last updated: Feb. 2018
//=================================
#ifndef FactoryTools_Select_DRAWFilterEvents_H
#define FactoryTools_Select_DRAWFilterEvents_H

#include <EventLoop/Algorithm.h>

class Select_DRAWFilterEvents : public EL::Algorithm
{
public:

  // this is a standard constructor
  Select_DRAWFilterEvents ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(Select_DRAWFilterEvents, 1);
};

#endif
