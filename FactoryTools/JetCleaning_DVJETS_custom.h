/*###############################################
 name: JetCleaning_DVJETS_custom 
 type: C++ namespace 
 responsible: rcarney@lbl.gov
 
 description:
    Contains a struct, JetCleaningFlags, used to store
    DRAWFilter decisions. Contains flag-setting
    functions and jet cleaning code copied out of
    standard JetSelectorTools package, 
    commit: 39cf36fea45c5677a531fd95c1615fa2964a4f94

 last updated: Jul. 2018
###############################################*/

#ifndef JET_CLEANING_DVJETS_CUSTOM_h
#define JET_CLEANING_DVJETS_CUSTOM_h

#include <xAODJet/JetContainer.h>
#include "EventLoop/StatusCode.h"

// STL includes
#include <iostream>
#include <cmath>
#include <cfloat>

namespace JetCleaning_DVJETS{

        //Bit field for filter decisions
        struct JetCleaningFlags{
            
            //Flags
            unsigned char fail              : 1;
            unsigned char fail_jetpT        : 1;
            unsigned char fail_ugly         : 1;
            unsigned char fail_NCB_lowEta   : 1;
            unsigned char fail_NCB_highEta  : 1;
            unsigned char fail_fmaxCut      : 1;
            unsigned char fail_QF           : 1;
            unsigned char fail_EMCaloNoise  : 1;
            unsigned char fail_LLPNegE      : 1;
            unsigned char fail_fmaxMin      : 1;
            unsigned char fail_NCB_monoJet  : 1;             

            //Default value is 0
            JetCleaningFlags(): 
                fail(0),
                fail_jetpT(0),
                fail_ugly(0),
                fail_NCB_lowEta(0),
                fail_NCB_highEta(0),
                fail_fmaxCut(0),
                fail_QF(0),
                fail_EMCaloNoise(0),
                fail_LLPNegE(0),
                fail_fmaxMin(0),
                fail_NCB_monoJet(0){}

            //Reset bits to 0
            void reset(){
                fail                = 0;
                fail_jetpT          = 0;
                fail_ugly           = 0;
                fail_NCB_lowEta     = 0;
                fail_NCB_highEta    = 0;
                fail_fmaxCut        = 0;
                fail_QF             = 0;
                fail_EMCaloNoise    = 0;
                fail_LLPNegE        = 0;
                fail_fmaxMin        = 0;
                fail_NCB_monoJet    = 0;
            }
        };

    //Body of jet cleaning logic, where flags are set
    EL::StatusCode checkCleanJets( const xAOD::Jet_v1& jet, bool doUgly, bool doLLP, JetCleaningFlags& flags );
    
    //Get jet cleaning variables from xAODjet
    EL::StatusCode getJetVars( const xAOD::Jet_v1& jet, bool doUgly, double& sumTrkPt, double& chf, int& FracSamplingMaxIndex, float& FracSamplingMax, float& EMFrac, float& HECFrac, float& LArQuality, float& HECQuality, float& NegativeE, float& AverageLArQF, float& jetPt); 

}
#endif


