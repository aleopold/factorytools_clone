#ifndef FactoryTools_StoreMuonDVObjects_H
#define FactoryTools_StoreMuonDVObjects_H

#include <EventLoop/Algorithm.h>
#include <TH2F.h>

class StoreMuonDVObjects : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  const std::string& notSetString() {
    static std::string const notSetString = "notSet";
    return notSetString;
  }

  // for saving muon information
  std::string segmentMapFileName;
  std::string InnerMapHistName;
  std::string MiddleMapHistName;
  std::string OuterMapHistName;
  TH2F* m_InnerMap     =0;
  TH2F* m_MiddleMap    =0;
  TH2F* m_OuterMap     =0;

  float m_sumEta   = 0.05;
  float m_deltaPhi = 0.22;
  float m_minEta   = 2.5;
  float m_minPt    = 25.;
  float m_minD0    = 1.5;
  float m_maxD0    = 300;
  float m_trackIso = 0.15;
  float m_caloIso  = 0.30;

  float m_muon = 0.1056583745; // GeV

  // this is a standard constructor
  StoreMuonDVObjects ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  virtual TLorentzVector seg_p4( xAOD::MuonSegment *seg);
  virtual float eta_corr_R( xAOD::Muon *mu, float detector_r);
  virtual float eta_corr( xAOD::Muon *mu, xAOD::MuonSegment *seg);
  virtual bool  isMatchedToSegment( xAOD::Muon *mu, const xAOD::MuonSegmentContainer* segments);
  virtual bool  passSegmentAcceptance( xAOD::Muon *mu,int run );
  virtual float dilepMass( std::vector<xAOD::Muon*> muons );
  virtual float zMass( xAOD::IParticleContainer *muons );

  // this is needed to distribute the algorithm to the workers
  ClassDef(StoreMuonDVObjects, 1);
};
#endif
