#ifndef REGION_VARIABLE_CALCULATOR_H
#define REGION_VARIABLE_CALCULATOR_H
//author : Russell Smith
//date   : January 2015
//update : Hide Oide, 2018-MAR-12

#include "FactoryTools/AnyType.h"

#include "EventLoop/StatusCode.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODMissingET/MissingET.h"
#include "xAODTruth/TruthParticle.h"

#include <map>
#include <iostream>

namespace EL{
  class IWorker;
}

class RegionVarCalculator {

public :
  virtual ~RegionVarCalculator() = 0;


public :
  EL::StatusCode initialize(EL::IWorker * worker){return doInitialize( worker );}
  //to be used per event
  EL::StatusCode calculate( std::map<std::string, anytype>& vars ) { return doCalculate( vars ); }
  EL::StatusCode doGeneralCalculations( std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode storeTriggerDecisions( std::map<std::string, anytype>& /* vars */, const std::vector<std::string>& /* triggerList */);
  const xAOD::TruthParticle* getGeneratorParent(const xAOD::TruthParticle* p);

  bool inListOfDetailedObjects(std::string object);

private :
  //todo probably clean this up
  virtual EL::StatusCode doInitialize(EL::IWorker * /*worker*/) = 0;
  virtual EL::StatusCode doCalculate( std::map<std::string, anytype>& ) = 0;

protected :
  RegionVarCalculator() : m_worker(nullptr) {};//this is to set this to nullptr in the constructor for the derived class
  EL::IWorker * m_worker; //!
  //we need to give the Worker to the calculator for flexibility.
  //This should be pointed to the EventLoop Worker in your alg which fills your map

  template <class Proxy>
  static bool PtOrder(const Proxy* left, const Proxy* right){ return left->pt() > right->pt() ; }


  std::vector<xAOD::IParticle*> sortByPt(xAOD::IParticleContainer* inputIParticleContainer);
  std::vector<xAOD::IParticle*> sortByPt(std::vector<xAOD::IParticle*>* inputVec);
  std::vector<xAOD::IParticle*> sortByPt(std::vector<xAOD::IParticle*> inputVec);


  template<typename T = float>
  EL::StatusCode addToVectorBranch(std::map<std::string, anytype>&, std::string, const T& );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator, 1);
  //TODO should decide if this is the best way to pass the trigger list to DRAW
  std::vector<int> m_event_triggerDecisions;

};

inline RegionVarCalculator::~RegionVarCalculator() { }


template<typename T>
inline EL::StatusCode RegionVarCalculator::addToVectorBranch( std::map<std::string, anytype>& vars, std::string branchName, const T& value )
{
  // If this doesn't exist in the vector yet!
  if( vars.find( branchName ) == vars.end() ){
    vars[branchName] = std::vector<T>{};
  }

  auto& var = vars[branchName];

  // type check
  if( var.type() != typeid( std::vector<T> ) ) {
    std::cerr << __PRETTY_FUNCTION__ << ": the specified variable " << branchName << " is not a type of " << demangle( typeid( std::vector<T> ).name() ) << std::endl;
    throw;
  }
  var.getVar< std::vector<T> >().emplace_back( value );

  return EL::StatusCode::SUCCESS;
}


#endif //REGION_VARIABLE_CALCULATOR_H

//  LocalWords:  ifndef
