#ifndef REGION_VARIABLE_CALCULATOR_MEDIUMD0_H
#define REGION_VARIABLE_CALCULATOR_MEDIUMD0_H

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AssociationUtils/OverlapLinkHelper.h"
#include <AnaAlgorithm/AnaAlgorithm.h>

#include <AsgTools/AnaToolHandle.h>
#include <IsolationSelection/IIsolationSelectionTool.h>
#include "PMGAnalysisInterfaces/IPMGCrossSectionTool.h"
#include "PMGTools/PMGCrossSectionTool.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/Vertex.h"
#include "Math/Vector3D.h"



#include <iostream>
#include <map>
#include <string>
#include <vector>

class RegionVarCalculator_mediumd0 : public RegionVarCalculator {

public :
    RegionVarCalculator_mediumd0();

private :

  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
  int hadronType(int pdgid) const;

  std::string sampleType(int &dsid) const;
  std::string sampleTypeSignalORsm(int &dsid) const;

  void findAllParents(std::vector<const xAOD::TruthParticle* > &parentVec,
		      const xAOD::TruthParticle* part) const;

  static double ctau(const xAOD::TruthParticle* particle,
		     const xAOD::TruthVertex *firstvertex) noexcept;

  static double tau(const xAOD::TruthParticle* particle,
		    const xAOD::TruthVertex *firstvertex) noexcept;

  static double ctau(const xAOD::TruthParticle* particle)noexcept;

  static double tau(const xAOD::TruthParticle* particle) noexcept;

  double getPdgidTau(int pdgidHadron);


public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_mediumd0, 1);
  std::unique_ptr<ORUtils::OverlapLinkHelper> m_overlapLinkHelper; //!
  int datasetid    = -999;
  int muon_pdgid   = 13;
  int tau_pdgid    = 15;
  int tquark_pdgid = 6;
  int bquark_pdgid = 5;
  int cquark_pdgid = 4;
  int zboson_pdgid = 23;
  int wboson_pdgid = 24;

  //left and right handed pdgids of sleptons
  int LHselectron_pdgid = 1000011;
  int RHselectron_pdgid = 2000011;
  int LHsmuon_pdgid = 1000013;
  int RHsmuon_pdgid = 2000013;
  int LHstau_pdgid  = 1000015;
  int RHstau_pdgid  = 2000015;
  int gravitino_pdgid = 1000039;
  std::vector<int> TruthMuonsFromSlepBarcodeVec;
  std::vector<int> TruthMuonsFromStauBarcodeVec;
  std::vector<int> truthMuonBarcodeVec;

  //dummy values
  //TODO FIX properly
  double dummy_d         = -999;
  unsigned long dummy_ul = -999;
  float dummy_f          = -999;
  int dummy_i            = -999;


  std::string m_PMGToolFiles = "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/PMGTools";

  // IsolationTool
  asg::AnaToolHandle<CP::IIsolationSelectionTool> m_iso; //!
  asg::AnaToolHandle<PMGTools::IPMGCrossSectionTool> m_PMGCrossSectionTool; //!

};

#endif //REGION_VARIABLE_CALCULATOR_MEDIUMD0_H

//  LocalWords:  ifndef
