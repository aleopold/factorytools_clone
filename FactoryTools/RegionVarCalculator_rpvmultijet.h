#ifndef REGION_VARIABLE_CALCULATOR_RPVMULTIJET_H
#define REGION_VARIABLE_CALCULATOR_RPVMULTIJET_H
//author : Anthony Badea
//date   : May 2020
//based on: RegionVarCalculator_dv written by Larry Lee in October 2016

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AssociationUtils/OverlapLinkHelper.h"

#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"

#include <iostream>
#include <map>
#include <string>
#include <vector>

class RegionVarCalculator_rpvmultijet : public RegionVarCalculator {

public :
  EL::StatusCode findLLPChildren(const xAOD::TruthParticle* parent, int barcode, int pdgID );
  void fillChildMap( std::map< std::pair<int,int>, const xAOD::TruthParticle* >* childMap, const xAOD::TruthParticle* parent);

private :
  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
  // EL::StatusCode doOtherSRCalculations (std::map<std::string, anytype>& /* vars */ );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_rpvmultijet, 1);
  float m_id_d0_min = -1;
  float m_id_pt_min = 1;
  int m_id_prescale = 1;
  bool m_saveIDtracks = true;
  bool m_saveSegments = false;

  float CalcMassMinMax(int jetdiff=10, int btagdiff=10);


  std::string fatJetContainerName;
  const std::string& notSetString() {
    static std::string const notSetString = "notSet";
    return notSetString;
  }

  asg::AnaToolHandle<ST::ISUSYObjDef_xAODTool> m_objTool;

  std::unique_ptr<ORUtils::OverlapLinkHelper> m_overlapLinkHelper;
  std::map< std::pair<int,int>, std::vector< const xAOD::TruthParticle*> > m_llpChildren; //Might not get filled depending on detailedList
  std::vector< const xAOD::TruthParticle* > m_goodTrack_truthParticles; //TruthParticles with status==1, pT>1GeV, charge>0

};

#endif //REGION_VARIABLE_CALCULATOR_RPVMULTIJET_H

//  LocalWords:  ifndef
