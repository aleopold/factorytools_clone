#ifndef FactoryTools_SelectRPVMultijetEvents_H
#define FactoryTools_SelectRPVMultijetEvents_H

#include <EventLoop/Algorithm.h>

enum selection {
  none = 0,
  trig = 1,
  trig_pt5 = 2
};


class SelectRPVMultijetEvents : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
 private:

  xAOD::TEvent* m_event; //!
  xAOD::TStore *m_store;  //!


  // float cutValue;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
 public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  
  // this is a standard constructor
  SelectRPVMultijetEvents ();

  int m_baselineSelection;

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  bool keepEvent ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(SelectRPVMultijetEvents, 1);
};

#endif
