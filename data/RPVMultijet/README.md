# RPV Multijet Full Run 2 Analysis 

## Data
- data15_SUSY4_p4238.txt
- data16_SUSY4_p4238.txt
- data17_SUSY4_p4238.txt
- data18_SUSY4_p4238.txt

## Monte Carlo

### QCD Dijet samples 
- JZWithSW: mc16a_JZWithSW_p4237.txt, mc16d_JZWithSW_p4237.txt, mc16e_JZWithSW_p4237.txt
- JZW: mc16a_JZW_p4237.txt, mc16d_JZW_p4237.txt, mc16e_JZW_p4237.txt

### $`t\bar{t}`$ samples
- ttbar.txt

### Signal samples
JIRA ticket for the signal production: https://its.cern.ch/jira/browse/ATLMCPROD-8842
- $`\tilde{g} \rightarrow qqq \; \mathrm{democratic}`$: GG_rpv_ALL.mc16a.txt, GG_rpv_ALL.mc16d.txt, GG_rpv_ALL.mc16e.txt
- $`\tilde{g} \rightarrow udb`$: GG_rpv_UDB.mc16a.txt, GG_rpv_UDB.mc16d.txt, GG_rpv_UDB.mc16e.txt
- $`\tilde{g} \rightarrow uds`$: GG_rpv_UDS.mc16a.txt, GG_rpv_UDS.mc16d.txt, GG_rpv_UDS.mc16e.txt