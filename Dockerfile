
FROM atlas/analysisbase:21.2.173
ADD . /analysis/src/FactoryTools/
WORKDIR /analysis/
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /analysis && \
    sudo usermod -aG root atlas && \
    ls && \
    mkdir build; mkdir run;\
    source src/FactoryTools/util/dependencyHacks.sh;\
    cp src/FactoryTools/util/CMakeLists.txt.TopLevel src/CMakeLists.txt;\
    cd build/; \
    cmake ../src/; \
    make; \
    source x86*/setup.sh
