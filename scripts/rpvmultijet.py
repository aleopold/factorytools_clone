#!/usr/bin/env python

import os,sys
import glob
import scriptHelperFunctions

#
# Setup for the job
#

nEvents = sys.argv[1]
inputDS = sys.argv[2]

scriptHelperFunctions.setupEnvironment()

#
# Actually run the job to produce the ntuple
#

os.system(
    """

    pwd;ls

    cd {buildDir}
    source */setup.sh
    cd {runDir}

    run_rpvmultijet.py --doOverwrite --verbosity error --nevents {nEvents} --inputDS {inputDS}

    pwd;ls

    """.format(
buildDir = os.environ["buildDir"],
runDir = os.environ["runDir"],
nEvents = nEvents,
inputDS = inputDS
    )
)

print("*"*60)
print("*"*5 + "  Done running ntuple production job")
print("*"*60)

#
# Run a script on the output ntuple
#

listOfFiles = glob.glob(os.environ["runDir"]+"/submit_dir/data-trees/*.root")

cuts = []

scriptHelperFunctions.printSummary(listOfFiles[0],"trees_SRRPV_",cuts,inputDSName=inputDS, scriptName=os.path.basename(__file__) )
