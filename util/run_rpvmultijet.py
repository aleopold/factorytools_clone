#!/usr/bin/env python

#*************** 
# S E T U P
#****************
import ROOT, logging, collections, commonOptions, os.path
ROOT.PyConfig.StartGuiThread = False #Workaround to fix threadlock issues with GUI

parser = commonOptions.parseCommonOptions()
(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)
logging.info(">>> Creating algorithms")
outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#*******************
#   R U N  A L G S
#*******************
#here we add the algorithms we want to run over
algsToRun = collections.OrderedDict()

#--------------------------
# Basic Event Selection
#--------------------------
algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" )
setattr(algsToRun["basicEventSelection"], "m_applyTriggerCut" , False )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , True ) # Changed this from False
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/rpvll_DV.prw.root") #TODO surely we want to make a different PRW file for DV_Jets samples? Or add?
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , True) #TODO check that the tool is getting passed GRL!
setattr(algsToRun["basicEventSelection"], "m_derivationName"  , "AllExecutedEvents" )

#--------------------------
# Calibrate SUSYTOOLS
#--------------------------
algsToRun["calibrateST"]               = ROOT.CalibrateST()
algsToRun["calibrateST"].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_rpvmultijet.conf"
algsToRun["calibrateST"].doPRW                    = algsToRun["basicEventSelection"].m_doPUreweighting;
algsToRun["calibrateST"].PRWConfigFileNames       = algsToRun["basicEventSelection"].m_PRWFileNames
algsToRun["calibrateST"].PRWLumiCalcFileNames     = algsToRun["basicEventSelection"].m_lumiCalcFileNames #TODO: what about actualMu files?
algsToRun["calibrateST"].fatJetContainerName      = "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"
setattr(algsToRun["calibrateST"], "useLLPMuons", 1)

#--------------------------
# Select RPV Multijet Events
#--------------------------
# Main selection and sorting algorithm
algsToRun["selectRPVMultijetEvents"]        = ROOT.SelectRPVMultijetEvents()
# Selections defined in FactoryTools/SelectRPVMultijetEvents.h
# 0: none, 1: trigger, 2: trigger and pT(5)>50 GeV
algsToRun["selectRPVMultijetEvents"].m_baselineSelection = 2
#--------------------------
# Calculate region var
#--------------------------
# These are the calculators that calculate various derived quantities
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].m_calculator         = ROOT.RegionVarCalculator_rpvmultijet()
algsToRun["calculateRegionVars"].triggerBranches      = ROOT.std.vector(ROOT.std.string)() #necessary, don't delete

#%%%%%%%%%%%%%%%%%%%%%%%
#Get triggers from list
#%%%%%%%%%%%%%%%%%%%%%%%
algsToRun["calculateRegionVars"].triggerBranches      += ["HLT_mu60_0eta105_msonly","HLT_xe100_mht_L1XE50", "HLT_xe110_mht_L1XE50","HLT_xe110_mht_L1XE50_AND_xe70_L1XE50","HLT_xe110_pufit_L1XE60"]

# 2018 
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_3j200","HLT_4j120","HLT_4j85_gsc115_boffperf_split"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_6j45_gsc55_boffperf_split_0eta240_L14J15"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_6j50_gsc70_boffperf_split_L14J15","HLT_6j55_0eta240_L14J15"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_7j35_gsc45_boffperf_split_L14J15","HLT_7j45_L14J15","HLT_10j40_L14J15"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_j420","HLT_j225_gsc420_boffperf_split","HLT_j260_320eta490"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_j460_a10r_L1SC111","HLT_j460_a10r_L1J100","HLT_j460_a10_lcw_subjes_L1SC111","HLT_j460_a10_lcw_subjes_L1J100","HLT_j460_a10t_lcw_jes_L1SC111","HLT_j460_a10t_lcw_jes_L1J100"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_ht1000_L1J100"]
# 2017 
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_3j200","HLT_4j120","HLT_4j60_gsc115_boffperf_split"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_6j45_gsc60_boffperf_split","HLT_6j45_gsc60_boffperf_split_L14J150ETA25","HLT_6j50_gsc70_boffperf_split","HLT_6j50_gsc70_boffperf_split_L14J150ETA25"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_7j45_L14J15","HLT_7j25_gsc45_boffperf_split_L14J20","HLT_7j25_gsc45_boffperf_split_L14J150ETA25","HLT_10j40_L14J15"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_j420","HLT_j225_gsc440_boffperf_split"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_j440_a10t_lcw_jes_L1J100","HLT_j460_a10t_lcw_jes_L1J100"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_j440_a10r_L1J100","HLT_j460_a10r_L1J100"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_ht1000_L1J100"]
# 2016
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_j380","HLT_j420_a10_lcw_L1J100","HLT_j420_a10r_L1J100","HLT_j260_320eta490"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_3j200","HLT_4j100","HLT_7j45_L14J15","HLT_10j40_L14J15"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_ht1000_L1J100"]
# 2015
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_j260_320eta490","HLT_j360"]#,"j360/360_a10r_L1J100/360_a10_sub_L1J100"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_3j175","HLT_4j85"]
algsToRun["calculateRegionVars"].triggerBranches    += ["HLT_ht700_L1J75"]


#%%%%%%%%%%%%%%%%%%%%%%%%%%
# list of detailed objects
#%%%%%%%%%%%%%%%%%%%%%%%%%%
# The ntuple writer will write out additional information about the following objects - should not be run in this run script, only in tests
#algsToRun["calculateRegionVars"].listOfDetailedObjects      = ROOT.std.vector(ROOT.std.string)() 
#algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["uncalibJets","DVJETS_DRAW_flags","dvTracks","baselineDVJETS","LLP_recoTracks","LLP_truth"] 
#algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["dvTracks","muons","msTracks","msSegments"]

#--------------
#  W R I T E 
#--------------
# These correspond to writing out the various trees used in the analysis
#for regionName in ["SRMET","SRLEP"]:
print "Writing out to ntuple: "
for regionName in ["SRRPV"]:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple


if options.doSystematics :
	algsToRun = commonOptions.doSystematics(algsToRun,fullChainOnWeightSysts = 0, excludeStrings = ["JET_Rtrk_","TAUS_"])

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options)

#--------------
# Monitoring
#--------------
if options.nevents > 0 :
    logging.info("Running " + str(options.nevents) + " events")
    job.options().setDouble (ROOT.EL.Job.optMaxEvents, float(options.nevents));

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
# OLD commonOptions.submitJob         ( job , options.driver , options.submitDir , options.gridUser , options.gridTag, options.nevents)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents, extraSubmitFlags = options.extraSubmitFlags, keepTags=options.keepTags)
