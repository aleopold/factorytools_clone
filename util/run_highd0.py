#!/usr/bin/env python

import ROOT, logging, collections, commonOptions
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
#parser.add_option('--verbosity', help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")

(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)


######################################################################
##


logging.info("creating algorithms")

outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#here we add the algorithms we want to run over
import collections
algsToRun = collections.OrderedDict()

algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] ) 
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" )
setattr(algsToRun["basicEventSelection"], "m_lumiCalcFileNames", "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root,GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root,GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root")



algsToRun["calibrateST"]                           = ROOT.CalibrateST()
algsToRun["calibrateST" ].SUSYToolsConfigFileName  = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_highd0.conf"
algsToRun["calibrateST" ].doPRW                    = algsToRun["basicEventSelection"].m_doPUreweighting;
algsToRun["calibrateST" ].PRWConfigFileNames       = algsToRun["basicEventSelection"].m_PRWFileNames
algsToRun["calibrateST" ].PRWLumiCalcFileNames     = algsToRun["basicEventSelection"].m_lumiCalcFileNames
setattr(algsToRun["calibrateST"], "useLLPMuons", 1)



# Main selection and sorting algorithm
algsToRun["selectHighd0"]        = ROOT.SelectHighd0Events()

# These are the calculators that calculate various derived quantities
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].m_calculator         = ROOT.RegionVarCalculator_highd0()

# The ntuple writer will automatically write-out the trig decisions for the triggers listed below
algsToRun["calculateRegionVars"].triggerBranches      = ROOT.std.vector(ROOT.std.string)() #stupid root... can't initialize the vector here...
algsToRun["calculateRegionVars"].triggerBranches      += ["HLT_g140_loose","HLT_mu60_0eta105_msonly","HLT_2g50_loose_L12EM20VH", "HLT_xe110_pufit_xe70_L1XE50", "HLT_xe120_pufit_L1XE50", "HLT_xe110_pufit_xe65_L1XE50", "HLT_xe110_pufit_xe70_L1XE50", "HLT_xe120_pufit_L1XE50", "HLT_xe110_pufit_xe65_L1XE50", "HLT_xe110_pufit_xe70_L1XE50", "HLT_mu80_msonly_3layersEC","HLT_g35_loose_g25_loose","HLT_2g50_loose"]
algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["muons","msTracks","msSegments"]

# These correspond to writing out the various trees used in the analysis
#for regionName in ["SRMET","SRLEP"]:
for regionName in ["SR_highd0"]:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    # tmpWriteOutputNtuple.systVar            = 0
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple



##
######################################################################




if options.doSystematics :
	algsToRun = commonOptions.doSystematics(algsToRun,fullChainOnWeightSysts = 0, excludeStrings = ["JET_Rtrk_","TAUS_"])

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents)
