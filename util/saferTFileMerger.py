#!/usr/bin/env python

# Script for more robust merging of ROOT files, to be used on the grid via --mergeScript="SaferTFileMerger.py --outputFile %OUT --inputFiles %IN"
# Written by Christian Ohm in 2021 to solve https://gitlab.cern.ch/atlas-phys-susy-wg/Common/FactoryTools/-/issues/42

import sys
import ROOT
import argparse

# main function
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = "Merge ROOT files containing histograms and TTrees, making sure that the union of all branches that exist in the full set of input files is created at the start to avoid that the set of branches present in the output depends on the order of the input files")

    parser.add_argument("--outputFile", default="MergerOutput.root", help="name of the output file containing the merged contents of the input file")
    parser.add_argument("--inputFiles", default="", help="comma-separated list of files to be merged, i.e. 'file1.root,file2.root'")
    parser.add_argument("--treeName", default="trees_SRDV_", help="name of TTree in the main output files (from e.g. EventLoop-based algs) expected in the files")

    args = parser.parse_args()

    # make a list of input files
    #inputFiles = [ROOT.TFile(input, "READ") for input in args.inputFiles.split(',')]

    # create the output file
    #outputFile = ROOT.TFile(args.outputFile, "RECREATE")
        
    print(args.inputFiles)

    # tree name
    treeName = args.treeName

    # open all the input files
    inputFiles = [ROOT.TFile(input, "READ") for input in args.inputFiles.split(',')]
        
    # check that all files have a TTree, if we expect them to (default is we do if they have "tree" in their name)
    treeExpected = False
    if all("trees" in fName for fName in args.inputFiles.split(',')):
        treeExpected = True
        inputFilesClean = [f for f in inputFiles if (f.Get(treeName).ClassName() == "TTree")]
        print("Cleaned list of files from files not containing a TTree (%d --> %d files)" % (len(inputFiles), len(inputFilesClean)))
        if (len(inputFiles) != len(inputFilesClean)):
            print("PROBLEM: some input files didn't have a valid TTree, will fail")
            sys.exit(1)
        inputFiles = inputFilesClean # using inputFiles only from here on
    # check if there's somehow a mix of file types fed into the merging job
    elif any("trees" in fName for fName in args.inputFiles.split(',')):
        print("PROBLEM: some input files had 'trees' in their name, others do not! Unexpected, will fail.")
        for fName in args.inputFiles.split(','):
            print(fName)
        sys.exit(1)
    
    # EL also writes out trees with run and event numbers for duplicate events, and these are stored in files with different names
    elif all("duplicates_tree" in fName for fName in args.inputFiles.split(',')):
        treeExpected = True
        treeName = "duplicates"

    # if trees are expected, special care needs to be taken with the order of the files to ensure to get the complete set of branches
    if treeExpected:
        # keep track of how many entries the trees in the input files had
        nEntriesPerFile = []
        for f in inputFiles:
            nEntriesPerFile.append(f.Get(treeName).GetEntries())
        
        # sort the files by number of branches, most first
        inputFiles.sort(key = lambda x: x.Get(treeName).GetNbranches(), reverse=True)
        print("Sorted the list of input files --> new order:")
        for i, f in enumerate(inputFiles):
            print("  %d: %s (%d branches, %d bytes)" % (i, f.GetName(), f.Get(treeName).GetNbranches(), f.GetSize()))

        # TODO: Maybe also check that no branches exist in the other files that's not in the one placed first? Should fail if that's the case, to catch rare cases of dynamically created sets of branches when running on few events. For the future, maybe..

    # create TFileMerger handling actual merging
    merger = ROOT.TFileMerger()
    #outputFile = ROOT.TFile(args.outputFile, "RECREATE")
    merger.OutputFile(args.outputFile, "RECREATE")
    print("TFileMerger::GetOutputFileName():", merger.GetOutputFileName())
    for f in inputFiles:
        merger.AddFile(f)
        print("Added file to be merged: %s" % f)
    if merger.Merge(): # returns True on success
        print("Merge successful")
    else:
        print("Problem merging, TFile::Merge() returned false - will fail")
        sys.exit(2)

    # check the output file if there were trees in the input files (reopen since closed after the merge is done)
    if treeExpected:
        outputFile = ROOT.TFile(merger.GetOutputFileName(), "READ")
        nEntriesMerged = outputFile.Get(treeName).GetEntries()
        if nEntriesMerged == sum(nEntriesPerFile):
            print("Done merging files with TTrees: output has TTree %s with %d entries (matching sum of input files)" % (treeName, nEntriesMerged))
        else:
            print("PROBLEM: Input files had a total of %d events - does not agree with output which has %d, will fail" % (sum(nEntriesPerFile), nEntriesMerged))
            print(nEntriesPerFile)
            sys.exit(3)
        outputFile.Close()

    else:
        print("Done with merging of TTree-less input files.")
