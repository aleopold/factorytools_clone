#!/usr/bin/env python

#***************
# S E T U P
#****************
import ROOT, logging, collections, commonOptions, os.path, re
ROOT.PyConfig.StartGuiThread = False #Workaround to fix threadlock issues with GUI

parser = commonOptions.parseCommonOptions()
(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)
logging.info(">>> Creating algorithms")
outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#*******************
#   R U N  A L G S
#*******************
#here we add the algorithms we want to run over
algsToRun = collections.OrderedDict()

#------------------------------------
# Identify data/MC (for skimming)
#------------------------------------
isData = False
if len(options.inputDS)>0:
  if re.search(re.compile('data'), options.inputDS[0]): isData = True

#--------------------------
# Basic Event Selection
#--------------------------
algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" )
setattr(algsToRun["basicEventSelection"], "m_applyTriggerCut" , False )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/rpvll_DV.prw.root") #TODO surely we want to make a different PRW file for DV_Jets samples? Or add?
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , True)

#-------------------------
# Get triggers from list
#------------------------
fname = os.path.expandvars('$FactoryToolsWrapper_DIR/data/FactoryTools/DV/DVPlusJetsFilterTriggerList.conf')
print "Adding triggers to calculateRegionVars.triggerBranches from: ", fname
with open(fname) as f:
	trigList = f.readlines()
trigList = [x.strip('\n') for x in trigList]
print "!!!  Trig list:\n ", trigList


#------------------------
# Apply skimming to data
#------------------------
if (isData): 
  skimList = trigList + ["HLT_g140_loose"]
  print "Skimming with triggers: ", '|'.join(skimList)
  setattr(algsToRun["basicEventSelection"], "m_applyTriggerCut" , True )
  setattr(algsToRun["basicEventSelection"], "m_triggerSelection", '|'.join(skimList) )

#--------------------------
# Calibrate SUSYTOOLS
#--------------------------
algsToRun["calibrateST"]               = ROOT.CalibrateST()
algsToRun["calibrateST"].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_dvjets.conf"
algsToRun["calibrateST"].doPRW                    = algsToRun["basicEventSelection"].m_doPUreweighting;
algsToRun["calibrateST"].PRWConfigFileNames       = algsToRun["basicEventSelection"].m_PRWFileNames
algsToRun["calibrateST"].PRWLumiCalcFileNames     = algsToRun["basicEventSelection"].m_lumiCalcFileNames #TODO: what about actualMu files?
#setattr(algsToRun["calibrateST"], "useLLPMuons", 1)

#--------------------------
# Store DV objects
#--------------------------
algsToRun["storeDVObjects"]               = ROOT.StoreDVObjects()
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_FileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/MaterialMap_v3.2_Inner.root")
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_HistName", "FinalMap_inner")
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_MatrixName", "FoldingInfo")
setattr(algsToRun["storeDVObjects"], "materialMap_Outer_FileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/MaterialMap_v3_Outer.root")
setattr(algsToRun["storeDVObjects"], "materialMap_Outer_HistName", "matmap_outer")
setattr(algsToRun["storeDVObjects"], "m_DVntrkMin", 5)
#setattr(algsToRun["storeDVObjects"], "m_dvContainerSuffix", "fixedExtrapolator") # default is "2" # See Issue #49

#--------------------------
# Select DV
#--------------------------
# Main selection and sorting algorithm
algsToRun["selectDV"]        = ROOT.SelectDVEvents()

#-----------------------------------
# Make track collection without LRT
#-----------------------------------
onlyPV = False
algsToRun["TracksForJets"] = ROOT.TracksForJets()
setattr(algsToRun["TracksForJets"], "NewContainerName", "InDetTrackParticles_noLRT")
setattr(algsToRun["TracksForJets"], "onlyPV", onlyPV)
setattr(algsToRun["TracksForJets"], "track_d0Sel", 2.0)
setattr(algsToRun["TracksForJets"], "do_z0sigCut", True)
setattr(algsToRun["TracksForJets"], "value_z0sigCut", 3.0)
setattr(algsToRun["TracksForJets"], "do_secondPass", True)
setattr(algsToRun["TracksForJets"], "value_secondPassCut", 0.5)

#--------------------------
# Make track jet collection
#--------------------------

algsToRun["JetMakerToolTrack"] = ROOT.JetMakerTool()
setattr(algsToRun["JetMakerToolTrack"], "JetRadius", 0.4)
setattr(algsToRun["JetMakerToolTrack"], "InputClusters", "InDetTrackParticles_noLRT")
setattr(algsToRun["JetMakerToolTrack"], "InputLabel", "Track")
setattr(algsToRun["JetMakerToolTrack"], "OutputContainer", "MyAntiKt4TrackJets")
setattr(algsToRun["JetMakerToolTrack"], "JetRadius", 0.4)
setattr(algsToRun["JetMakerToolTrack"], "PtMin", 1000)
setattr(algsToRun["JetMakerToolTrack"], "onlyPV", onlyPV)
# Choices below need to match values in RegionVarCalculator_dv.h
setattr(algsToRun["JetMakerToolTrack"], "minNTracks", 2)
setattr(algsToRun["JetMakerToolTrack"], "minSumPtTracks", 15.)

#--------------------------
# Store Muon DV Objects
#--------------------------
# Adds information about displaced muons
# Must run after selectDV and before calculateRegionVars
algsToRun["storeMuonDVObjects"]               = ROOT.StoreMuonDVObjects()
setattr(algsToRun["storeMuonDVObjects"], "segmentMapFileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/segmentMap2D_Run2_v4.root")
setattr(algsToRun["storeMuonDVObjects"], "InnerMapHistName", "BI_mask")
setattr(algsToRun["storeMuonDVObjects"], "MiddleMapHistName", "BM_mask")
setattr(algsToRun["storeMuonDVObjects"], "OuterMapHistName", "BO_mask")
setattr(algsToRun["storeMuonDVObjects"], "m_sumEta", 0.05)
setattr(algsToRun["storeMuonDVObjects"], "m_deltaPhi", 0.22)
setattr(algsToRun["storeMuonDVObjects"], "m_minEta", 2.5)
setattr(algsToRun["storeMuonDVObjects"], "m_minPt", 25.)
setattr(algsToRun["storeMuonDVObjects"], "m_minD0", 1.5)
setattr(algsToRun["storeMuonDVObjects"], "m_maxD0", 300)
setattr(algsToRun["storeMuonDVObjects"], "m_trackIso", 0.06)
setattr(algsToRun["storeMuonDVObjects"], "m_caloIso", 0.06)

#--------------------------
# Calculate region var
#--------------------------
# These are the calculators that calculate various derived quantities
# A name needs to be handed to the constructor since this now inherits from EL::AnaAlgorithm
algsToRun["calculateRegionVars"]                 = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].m_calculator    = ROOT.RegionVarCalculator_dv()
algsToRun["calculateRegionVars"].triggerBranches = ROOT.std.vector(ROOT.std.string)() #necessary, don't delete
algsToRun["calculateRegionVars"].triggerBranches      += trigList

#%%%%%%%%%%%%%%%%%%%%%%%%%%
# list of detailed objects
#%%%%%%%%%%%%%%%%%%%%%%%%%%
# The ntuple writer will write out additional information about the following objects - should not be run in this run script, only in tests
# description of the various collections, which might need to be added to the default set for dedicated studies:
# uncalibJets: adds info about the uncalibrated jets
# hadInt: adds truth info needed for studies of bg for hadronic interactions
# prescaled_idTracks: track variables for *all* tracks (not only those from DVs - so trees will be huge!)
# muons, msTracks, msSegments, muons_baseline: variables related to muons or MS segments
# electrons: info about electrons
# selectedJets, uncalibJets: info about additional jet collections
# jetCleaningFlags: detailed variables used for jet cleaning (written out for uncalibJets collection)
# detailedOR: relevant for detailed studies of overlap removal
algsToRun["calculateRegionVars"].listOfDetailedObjects      = ROOT.std.vector(ROOT.std.string)()
algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["dvTracks", "LLP_recoTracks", "LLP_truth", "uncalibJets", "DVJETS_DRAW_flags","truthJets"] #Default for bulk production
#algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["hadInt"] # activate for MC samples needed for HI template
#algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["prescaled_idTracks"] # activate for Track variables for all tracks
#algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["muons","msTracks","msSegments","muons_baseline","electrons","selectedJets","uncalibJets"] # activate for details on more physics objects
#algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["jetCleaningFlags","detailedOR"] # activate for detailed studies of OR and cleaning


#--------------
#  W R I T E
#--------------
# These correspond to writing out the various trees used in the analysis
#for regionName in ["SRMET","SRLEP"]:
print "Writing out to ntuple: "
for regionName in ["SRDV"]:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple

if options.doSystematics :
	algsToRun = commonOptions.doSystematics(algsToRun,fullChainOnWeightSysts = 0, excludeStrings = ["JET_Rtrk_","TAUS_"])

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options)

#--------------
# Monitoring
#--------------
if options.nevents > 0 :
    logging.info("Running " + str(options.nevents) + " events")
    job.options().setDouble (ROOT.EL.Job.optMaxEvents, float(options.nevents));

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents, extraSubmitFlags = options.extraSubmitFlags)
