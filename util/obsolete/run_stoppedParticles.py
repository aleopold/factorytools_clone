#!/usr/bin/env python

import ROOT, logging, collections, commonOptions
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
#parser.add_option('--verbosity', help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")

(options, args) = parser.parse_args()


job = commonOptions.initializeRunScript(options, args)

logging.info("creating algorithms")

outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);


###########################################################
## Here we add the algorithms we want to run over
algsToRun = collections.OrderedDict()

## BasicEventSelection

algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
setattr(algsToRun["basicEventSelection"], "m_applyPrimaryVertexCut" , False )
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , False )

## Preselection

## Selection

# Main selection and sorting algorithm
algsToRun["selectSP"]        = ROOT.SelectSPEvents()

## Output Variable Calculators

# These are the calculators that calculate various derived quantities
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].calculatorName       = ROOT.CalculateRegionVars.spCalculator

# The ntuple writer will automatically write-out the trig decisions for the triggers listed below
algsToRun["calculateRegionVars"].triggerBranches      = ROOT.std.vector(ROOT.std.string)() #stupid root... can't initialize the vector here...
algsToRun["calculateRegionVars"].triggerBranches      += [] # list triggers here e.g. ["HLT_xe70","HLT_xe100"]


## NTuple Writing

# These correspond to writing out the various trees used in the analysis
for regionName in ["SRSP"]:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple


##
##############################################################


if options.doSystematics :
	algsToRun = commonOptions.doSystematics(algsToRun,fullChainOnWeightSysts = 0, excludeStrings = ["JET_Rtrk_","TAUS_"])

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options.verbosity)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , options.gridUser , options.gridTag, options.nevents)





