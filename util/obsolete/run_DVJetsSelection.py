#!/usr/bin/env python
#=======================================
# Last edited: Feb 5th 2018
# Contributors: rcarney@lbl.gov
# Descr:
#   -  run on standard AOD
#   -  'apply' DRAW filter to select
#       specific events.
#   - save filtered events in tuple
#=======================================

#*************** 
# S E T U P
#****************
import ROOT, logging, collections, commonOptions, os.path
ROOT.PyConfig.StartGuiThread = False # Workaround to fix threadlock issues with GUI

parser = commonOptions.parseCommonOptions()
(options, args) = parser.parse_args()
job = commonOptions.initializeRunScript(options, args)

logging.info("Creating algorithms")
outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#*******************
#   R U N  A L G S
#*******************
algsToRun = collections.OrderedDict()

#--------------------------
# Basic Event Selection
#--------------------------
algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" )
setattr(algsToRun["basicEventSelection"], "m_applyTriggerCut" , False )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
setattr(algsToRun["basicEventSelection"], "m_GRLxml" , "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_DAOD_RPVLL_r8669.xml")
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , True)
#setattr(algsToRun["basicEventSelection"], "m_GRLxml" , "$FactoryToolsWrapper_DIR/data/FactoryTools/GRL/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml")

#--------------------------
# Calibrate SUSYTOOLS
#--------------------------
algsToRun["calibrateST"]               = ROOT.CalibrateST()
algsToRun["calibrateST" ].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_dvjets.conf"
algsToRun["calibrateST" ].doPRW                    = False;
algsToRun["calibrateST" ].PRWConfigFileNames       = "$FactoryToolsWrapper_DIR/data/FactoryTools/DV_JETS/NTUP_PILEUP.SUSY15_MC16_13TeV_MC16d.pool.root.1"
algsToRun["calibrateST" ].PRWLumiCalcFileNames     = "$FactoryToolsWrapper_DIR/data/FactoryTools/DV_JETS/physics_25ns_JetHLT_Normal2017.lumicalc.OflLumi-13TeV-010.root"
algsToRun["calibrateST" ].PRWActualMuFileNames     = "$FactoryToolsWrapper_DIR/data/FactoryTools/DV_JETS/physics_25ns_JetHLT_Normal2017.actualMu.OflLumi-13TeV-010.root"

#--------------------------
# Store DV objects
#--------------------------
algsToRun["storeDVObjects"]               = ROOT.StoreDVObjects()
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_FileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/MaterialMap_v3.2_Inner.root")
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_HistName", "FinalMap_inner")
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_MatrixName", "FoldingInfo")
setattr(algsToRun["storeDVObjects"], "materialMap_Outer_FileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/MaterialMap_v3_Outer.root")
setattr(algsToRun["storeDVObjects"], "materialMap_Outer_HistName", "matmap_outer")
setattr(algsToRun["storeDVObjects"], "m_DVntrkMin", 2)

#--------------------------
# Select DV
#--------------------------
algsToRun["SelectDV"]     = ROOT.SelectDVEvents() 

#--------------------------
# Calculate region var
#--------------------------
#Adds uncalib. and uncalib. trackless jets to storeGate
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].calculatorName       = ROOT.CalculateRegionVars.dvCalculator
algsToRun["calculateRegionVars"].triggerBranches      = ROOT.std.vector(ROOT.std.string)() #necessary, don't delete

#%%%%%%%%%%%%%%%%%%%%%%%
#Get triggers from list
#%%%%%%%%%%%%%%%%%%%%%%%
fname = os.path.expandvars('$FactoryToolsWrapper_DIR/data/FactoryTools/DV/DVPlusJetsFilterTriggerList.conf')
print "Adding triggers to calculateRegionVars.triggerBranches from: ", fname
with open(fname) as f:
    trigList = f.readlines()
trigList = [x.strip('\n') for x in trigList] 
print "!!!  Trig list:\n ", trigList
algsToRun["calculateRegionVars"].triggerBranches      += trigList

#%%%%%%%%%%%%%%%%%%%%%%%%%%
# list of detailed objects
#%%%%%%%%%%%%%%%%%%%%%%%%%%
# The ntuple writer will write out additional information about the following objects
algsToRun["calculateRegionVars"].listOfDetailedObjects      = ROOT.std.vector(ROOT.std.string)() 
algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["uncalibJets","DVJETS_DRAW_flags","dvTracks","baselineDVJETS","LLP_recoTracks","LLP_truth"] 

#--------------
#  W R I T E 
#--------------
print "Writing out to ntuple: "
for regionName in ["SRDV"]:
    print "Writing out region: "+regionName
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options)

#--------------
# Monitoring
#--------------
if options.nevents > 0 :
    logging.info("Running " + str(options.nevents) + " events")
    job.options().setDouble (ROOT.EL.Job.optMaxEvents, float(options.nevents));

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , options.gridUser , options.gridTag, options.nevents)
