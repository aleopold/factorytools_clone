#=======================================
# Last edited: Feb 5th 2018
# Contributors: rcarney@lbl.gov
# Descr:
#   -  run on standard AOD
#   -  'apply' DRAW filter to select
#       specific events.
#   - save filtered events in tuple
#=======================================

#***************
# S E T U P
#****************
import ROOT
ROOT.PyConfig.StartGuiThread = False # Workaround to fix threadlock issues with GUI

import logging
logging.basicConfig(level=logging.INFO)

import commonOptions
parser = commonOptions.parseCommonOptions()
(options, args) = parser.parse_args()

import os.path

ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

#Create a new sample handler to describe the data files we use
logging.info("creating new sample handler")
sh_all = ROOT.SH.SampleHandler()
commonOptions.fillSampleHandler(sh_all, options.inputDS)
sh_all.setMetaString ("nc_tree", "CollectionTree");
sh_all.printContent();

#This is the basic description of our job
logging.info("Creating new job")
job = ROOT.EL.Job()
job.sampleHandler(sh_all)
job.useXAOD()

logging.info("Creating algorithms")
outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#*******************
#   R U N  A L G S
#*******************

import collections
algsToRun = collections.OrderedDict()

algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , False )
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" ) #Regex: .+ matches everything available
setattr(algsToRun["basicEventSelection"], "m_applyTriggerCut" , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
setattr(algsToRun["basicEventSelection"], "m_GRLxml" , "$FactoryToolsWrapper_DIR/data/FactoryTools/GRL/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml")

#For now this just selects a regionName
algsToRun["Select_DRAWFilterEvents"]                  = ROOT.Select_DRAWFilterEvents()

#Adds uncalib. and uncalib. trackless jets to storeGate
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].calculatorName       = ROOT.CalculateRegionVars.DRAWFilterCalculator
algsToRun["calculateRegionVars"].triggerBranches      = ROOT.std.vector(ROOT.std.string)() #necessary, don't delete

#Get triggers from list
fname = os.path.expandvars('$FactoryToolsWrapper_DIR/data/FactoryTools/DV/DVPlusJetsFilterTriggerList.conf')
print "Adding triggers to calculateRegionVars.triggerBranches from: ", fname
with open(fname) as f:
    trigList = f.readlines()
trigList = [x.strip('\n') for x in trigList]

algsToRun["calculateRegionVars"].triggerBranches      += trigList

print "Writing out to ntuple: "

#***************
#  W R I T E
#***************
for regionName in ["earlyRun2"]:
    print "Writing out region"+regionName
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options.verbosity)

if options.nevents > 0 :
    logging.info("Running " + str(options.nevents) + " events")
    job.options().setDouble (ROOT.EL.Job.optMaxEvents, float(options.nevents));

#commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , options.gridUser , options.gridTag, options.nevents)
