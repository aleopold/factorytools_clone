#!/usr/bin/env python

import ROOT, logging, collections, commonOptions
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
#parser.add_option('--verbosity', help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")

(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)


######################################################################
##


logging.info("creating algorithms")

outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#here we add the algorithms we want to run over
import collections
algsToRun = collections.OrderedDict()

algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
# setattr(algsToRun["basicEventSelection"], "m_derivationName"  , "StreamAOD" )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
# setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , "$ROOTCOREBIN/data/FactoryTools/DV/mc15c_signal_r8788.NTUP_PILEUP.merged.root" )
# setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , "$ROOTCOREBIN/data/FactoryTools/DV/PRW.mc15_13TeV.PythiaRhad.r7772_r7676.root")
setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/rpvll_DV.prw.root")
setattr(algsToRun["basicEventSelection"], "m_GRLxml" , "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_DAOD_RPVLL_r8669.xml")
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , False)


algsToRun["calibrateST"]               = ROOT.CalibrateST()
algsToRun["calibrateST" ].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_dvmu.conf"
# algsToRun["calibrateST" ].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/SUSYTools/SUSYTools_Default.conf"
# algsToRun["calibrateST" ].systVar     = 0
algsToRun["calibrateST" ].doPRW                    = algsToRun["basicEventSelection"].m_doPUreweighting;
algsToRun["calibrateST" ].PRWConfigFileNames       = algsToRun["basicEventSelection"].m_PRWFileNames
algsToRun["calibrateST" ].PRWLumiCalcFileNames     = algsToRun["basicEventSelection"].m_lumiCalcFileNames

# algsToRun["storeDVObjects"]               = ROOT.StoreDVObjects()
# setattr(algsToRun["storeDVObjects"], "materialMapFileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/materialMap3D_Run2_v2.root")
# setattr(algsToRun["storeDVObjects"], "materialMapHistName", "map")
# setattr(algsToRun["storeDVObjects"], "m_DVntrkMin", 2)


# Main selection and sorting algorithm
algsToRun["selectBJets"]        = ROOT.SelectBJetEvents()

# These are the calculators that calculate various derived quantities
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].calculatorName       = ROOT.CalculateRegionVars.slbjetsCalculator

# The ntuple writer will automatically write-out the trig decisions for the triggers listed below
algsToRun["calculateRegionVars"].triggerBranches      = ROOT.std.vector(ROOT.std.string)() #stupid root... can't initialize the vector here...
algsToRun["calculateRegionVars"].triggerBranches      += [] # list triggers here e.g. ["HLT_xe70","HLT_xe100"]

# These correspond to writing out the various trees used in the analysis
#for regionName in ["SRMET","SRLEP"]:
for regionName in ["BJets"]:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    # tmpWriteOutputNtuple.systVar            = 0
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple



##
######################################################################




if options.doSystematics :
	algsToRun = commonOptions.doSystematics(algsToRun,fullChainOnWeightSysts = 0, excludeStrings = ["JET_Rtrk_","TAUS_"])

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options.verbosity)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , options.gridUser , options.gridTag, options.nevents)
