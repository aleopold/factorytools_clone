#!/usr/bin/env python

import ROOT, logging, collections, commonOptions
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
#parser.add_option('--verbosity', help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")

(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)


######################################################################
##

logging.info("creating algorithms")

outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#here we add the algorithms we want to run over
import collections
algsToRun = collections.OrderedDict()

algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , True )
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" )
setattr(algsToRun["basicEventSelection"], "m_applyTriggerCut" , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/merged_prw_mc16a.root")
setattr(algsToRun["basicEventSelection"], "m_lumiCalcFileNames", "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/lumicalc2015-2016.root")
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , True )


algsToRun["calibrateST"]               = ROOT.CalibrateST()
algsToRun["calibrateST" ].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_jpsi.conf"
algsToRun["calibrateST" ].doPRW                    = algsToRun["basicEventSelection"].m_doPUreweighting;
algsToRun["calibrateST" ].PRWConfigFileNames       = algsToRun["basicEventSelection"].m_PRWFileNames
algsToRun["calibrateST" ].PRWLumiCalcFileNames     = algsToRun["basicEventSelection"].m_lumiCalcFileNames
setattr(algsToRun["calibrateST"], "runningMediumd0Analy", 1)

# Main selection and sorting algorithm
algsToRun["selectJpsi"]        = ROOT.SelectJpsiEvents()
#setattr(algsToRun["selectJpsi"], "m_PMGToolFiles" , "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/PMGTools")

# These are the calculators that calculate various derived quantities
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].m_calculator         = ROOT.RegionVarCalculator_jpsi()


# These correspond to writing out the various trees used in the analysis
for regionName in ["JPSI"]:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    # tmpWriteOutputNtuple.systVar            = 0
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple



##
######################################################################




if options.doSystematics :
	algsToRun = commonOptions.doSystematics(algsToRun,fullChainOnWeightSysts = 0, excludeStrings = ["JET_Rtrk_","TAUS_"])

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents, excludeSite = options.excludeSite, gridSite = options.gridSite)
