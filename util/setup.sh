# This is the setup script that should be run each time you start a new session.
# It takes care of the various tools that you'll need.

echo ""
echo "####################################################"
echo "##"
echo "## FactoryTools - util/setup.sh"
echo "##"
echo "## ... Going to setup tools and environment for you!"
echo "##"

# IMPORTANT ###############################
# This is read by a git pre-commit hook
# If the release used in the CI config in .gitlab-ci.yml
# differs from this, that file is automatically
# updated to use the version defined here.
#
# It should be enough to change just this variable here!
#

ANALYSIS_BASE_VERSION=21.2.173


function ver { printf "%03d%03d%03d%03d" $(echo "$1" | tr '.' ' '); }

# ATLAS ENVIRONMENT ####################
#

if ! type lsetup &> /dev/null; then
	setupATLAS
fi

# You need rucio for dataset handling ###
#

if ! type rucio &> /dev/null; then
	lsetup rucio
fi

# You need panda for interaction with the grid ###
#

if ! type pbook &> /dev/null; then
	lsetup panda
fi

# You need a relatively recent version of git ###
#

GIT_VERSION=(`git --version`)
GIT_VERSION=${GIT_VERSION[2]}

if [ $(ver ${GIT_VERSION}) -lt $(ver 2.0.0) ]; then
	lsetup git
fi

# Setting up environment ###########
#

UTILDIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

export FT=`dirname ${UTILDIR}`
export BUILDDIR=${FT}/../../build/

# INSTALL.sh helper ###
#

if ! [ -e ${FT}/.git/hooks/pre-commit ] ; then
	echo "## ... Linking pre-commit hook"
	ln -s ${FT}/util/pre-commit ${FT}/.git/hooks/.
fi

# Setting up AnalysisBase Athena! #####
#

if [ $(ver ${AtlasVersion}) -lt $(ver ${ANALYSIS_BASE_VERSION}) ]; then
	asetup ${ANALYSIS_BASE_VERSION},AnalysisBase --testarea=${BUILDDIR}
fi

# Now check if you've already compiled everything, and if so, link everything ###
#

if [ -e ${BUILDDIR}/x*/setup.sh ] ; then
	echo "## ... Found a build/*/setup.sh! -- Sourcing for you!"
	source ${BUILDDIR}/x*/setup.sh
fi

echo "##"
echo "####################################################"
echo ""

# Done!
