#!/usr/bin/env python

import ROOT, logging, collections, commonOptions
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
#parser.add_option('--verbosity', help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")

(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)


######################################################################
##


logging.info("creating algorithms")

outputFilename = "VsiAnaExample"
output = ROOT.EL.OutputStream(outputFilename);

#here we add the algorithms we want to run over
import collections
algsToRun = collections.OrderedDict()

#from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
vsialg       = ROOT.VsiAnaExample()
vsialg_trim  = ROOT.VsiAnaExample()

algsToRun["ntupSvc"] = ROOT.EL.NTupleSvc( outputFilename )

def setupVsiAlg ( vsiana ):
    vsiana.setMsgLevel( 3 )
    vsiana.prop_probeTruth            = "Rhadron"
    vsiana.prop_mode                  = ROOT.VsiAnaExample.r21
    vsiana.prop_chi2Cut               = 9999.
    vsiana.prop_hitPatternReq         = 0
    vsiana.prop_doDropAssociated      = False

for vsiana in [ vsialg, vsialg_trim ]:
    setupVsiAlg( vsiana )

vsialg.prop_outputName            = "svTree_default"

vsialg_trim.prop_outputName       = "svTree_trimmed"
vsialg_trim.prop_fillTracks            = True
vsialg_trim.prop_trackStorePrescale    = 1.e-3
vsialg_trim.prop_hitPatternReq    = 3 # [ 0 = NONE, 1 = LOOSE, 2 = MEDIUM, 3 = TIGHT ]
vsialg_trim.prop_chi2_toSVCut     = 5.0
vsialg_trim.prop_d0_wrtSVCut      = 1.0
vsialg_trim.prop_z0_wrtSVCut      = 2.0
vsialg_trim.prop_d0signif_wrtSVCut = 5.0
vsialg_trim.prop_z0signif_wrtSVCut = 5.0
vsialg_trim.prop_vtx_suffix        = ""

#######################################
#
# Options for event display
#
# if you want to show/dump them
# 
#vsialg_trim.prop_doDisplay = True
#vsialg_trim.prop_displayDoBatch = True # intractive mode if False
#vsialg_trim.prop_displayMaxEvents = 500
#
#######################################

#algsToRun["vsialg"]       = vsialg
algsToRun["vsialg_trim"]  = vsialg_trim

##
######################################################################

job.outputAdd(output)

commonOptions.addAlgsFromDict(job , algsToRun , options.verbosity)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents)

