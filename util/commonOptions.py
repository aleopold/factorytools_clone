import os
import sys
import logging
import shutil
import ROOT
from copy import copy
from copy import deepcopy

import basicEventSelectionConfig

from optparse import OptionParser

from datetime import date

import collections

import subprocess

import glob

from ROOT.asg import ToolStore

def parseCommonOptions() :
    parser = OptionParser()
    parser.add_option("--submitDir",  help   = "dir to store the output", default="submit_dir")
    parser.add_option("--inputDS",    help     = "You can pass either the directory locally, the file containing the list of grid datasets, or directly the name of a grid dataset. ", action='append', default=[])

    parser.add_option("--groupRole",   help    = "if you want to use a group role, set this to the name of the group. e.g. phys-susy"  , default= '')
    parser.add_option("--gridUser",   help    = "gridUser"  , default= '')
    parser.add_option("--gridTag",    help    = "gridTag"   , default= '')
    parser.add_option("--extraSubmitFlags",    help    = "additional submit flags to hand to prun"   , default= '')
    parser.add_option("--driver",     help      = "select where to run", choices=("direct", "prooflite", "lsf", "grid", "condor","lxplus","net3"), default="direct")
    parser.add_option('--doOverwrite',help = "Overwrite submit dir if it already exists", action="store_true", default=False)
    parser.add_option('--nevents',    help     = "Run n events ", default = -1 )
    parser.add_option('--skipevents', help     = "skip n events ", default = 0 )
    parser.add_option('--verbosity',  help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")
    parser.add_option('--doSystematics', help = "Do systematics.  This will take *MUCH* longer!",action="store_true", default=False)
    parser.add_option('--rse',        help = "Name the grid site (RSE) that you'd like to stream the dataset from.", default="")
    parser.add_option('--nRSEFiles',  help = "Number of files you want to stream. Defaults to a 1-file test job.", default=1,type=int)
    parser.add_option('--excludeSite',help = "Exclude an annoying grid site", default= '')
    parser.add_option('--gridSite',help = "Choose which wonderful grid site you'd like to run on", default= '')
    parser.add_option('--keepTags', action="store_true",  help = "Keep the tags of the input DS in the name of the output DS")
    return parser

import atexit
@atexit.register
def quiet_exit():
    ROOT.gSystem.Exit(0)

def setVerbosity ( alg , levelString ) :
    level = None
    if levelString == "info"    : level = ROOT.MSG.INFO
    if levelString == "warning" : level = ROOT.MSG.WARNING
    if levelString == "error"   : level = ROOT.MSG.ERROR
    if levelString == "debug"   : level = ROOT.MSG.DEBUG
    if levelString == "verbose" : level = ROOT.MSG.VERBOSE

    if not level :
        logging.info(">>> You set an illegal verbosity! Exiting.")
        commonOptions.quiet_exit()

    try:
        alg.setMsgLevel(level)
        alg.m_msgLevel = level
        alg.OutputLevel = level
    except:
        pass

    return

def translateLoggingVerbosity(levelString):
    level = None
    if   levelString == "info"    : level=logging.INFO
    elif levelString == "warning" : level=logging.INFO
    elif levelString == "error"   : level=logging.INFO
    elif levelString == "debug"   : level=logging.DEBUG
    elif levelString == "verbose" : level=logging.DEBUG
    return level

def fillSampleHandler ( sh_all, inp, options ) :
    """You can pass either the directory locally, the file containing the list of grid datasets, or directly the name of a grid dataset. """

    if len(inp)>1:
        # If I'm handed a bunch of input strings...
        logging.info(">>> I've been handed multiple strings for input.")
        logging.info(">>> I'll assume this is for multiple .ds lists instead of multiple root files.")
        logging.info(">>> If you want multiple root files, put them in a directory and hand me that directory.")
        for fileName in inp:
            logging.debug(">>> ... Scanning file %s"%fileName)
            with open(fileName) as f:
                for ds in f :
                    #this removes the scope and trailing whitespace
                    cleanedDs = (ds.split(":")[-1]).rstrip()
                    ROOT.SH.addGrid(sh_all, cleanedDs )
    else:
        # This is reverting to single-input behavior
        inp = inp[0]

    if "*" not in inp and os.path.isfile(inp):
        if ".root" in inp[-9:]:
            logging.info(">>> Going to run over single ROOT file %s"%inp)
            tmpFileName = inp.split("/")[-1]
            tmpDirName  = "/".join(inp.split("/")[:-1])
            ROOT.SH.ScanDir().sampleDepth(0).samplePattern(tmpFileName).scan(sh_all,tmpDirName)
        else:
            # This is if you're handing a file full of dataset names
            logging.info(">>> Going to run over the datasets listed in input file. Printing list to debug.")
            with open(inp) as f:
                for ds in f :
                    #this removes the scope and trailing whitespace
                    cleanedDs = (ds.split(":")[-1]).rstrip()
                    logging.debug(">>> ... %s"%cleanedDs)
                    ROOT.SH.addGrid(sh_all, cleanedDs )
    elif "*" in inp:
        # Assuming you want me to glob files with dataset lists
        logging.info(">>> Assuming you want me to glob for files full of dataset names. Printing datasets to debug.")
        for fileName in glob.glob(inp):
            logging.info(">>> ... Scanning file %s"%fileName)
            with open(fileName) as f:
                for ds in f :
                    #this removes the scope and trailing whitespace
                    cleanedDs = (ds.split(":")[-1]).rstrip()
                    logging.debug(">>> ... %s"%cleanedDs)
                    ROOT.SH.addGrid(sh_all, cleanedDs )
    elif os.path.isdir(inp) :
        logging.info(">>> Running over files in directory. Also going to try different depths if I don't find anything.")
        mylist = ROOT.SH.DiskListLocal(inp)
        ROOT.SH.scanDir(sh_all,mylist, "*")
        iSampleDepth = 0
        while sh_all.size()==0 and iSampleDepth<3:
            ROOT.SH.ScanDir().sampleDepth(iSampleDepth).scan(sh_all,inp)
            iSampleDepth += 1
    elif "root://" in inp:
        logging.info(">>> Interpreting as XRD file path.")
        server, path = inp.replace('root://','').split('//')
        sh_list = ROOT.SH.DiskListXRD(server, os.path.join('/', path), True)
        ROOT.SH.ScanDir().scan(sh_all, sh_list)
    elif options.driver!="grid" :
        # if all else fails, see if it's a grid dataset and let's try to stream it!
        # I tried to just import the rucio module but not clear why it wasn't working...
        logging.info(">>> Can't find the dataset locally and you're not running on the grid. Will try to stream dataset for you.")
        if options.rse=="":
            logging.info(">>> No RSE specified for dataset streaming (--rse option). Will list available sites here and try to proceed with the first.")
            logging.info(">>> If this hangs for a while, try another RSE site listed below.")
            consoleOutput = subprocess.check_output(['rucio', 'list-dataset-replicas', inp])
            options.rse=consoleOutput.split("\n")[5].split("|")[1].strip() #should grab the first RSE
            logging.info(consoleOutput)
        consoleOutput = subprocess.check_output(['rucio', 'list-file-replicas', '--protocol', 'root', '--pfns', inp, '--rse', options.rse])
        listOfFiles = [x for x in consoleOutput.split("\n") if x]
        nFiles = 0
        if options.nRSEFiles==1:
            logging.info(">>> I'm only going to stream one file. If you want more add `--nRSEFiles n` or `--nRSEFiles -1` (all)")
        for f in listOfFiles:
            if nFiles>=options.nRSEFiles and options.nRSEFiles>0:
                break
            if f.startswith("."):
                logging.info(">>> ... Skipping hidden file: %s"%f)
                continue
            logging.info(">>> ... Registering file: %s"%f)
            server, path = f.replace('root://','').split('/',1)
            chunks = path.split("/")
            path, filename = "/".join(chunks[:-1]), chunks[-1]
            sh_list = ROOT.SH.DiskListXRD(server, os.path.join('/', path), True)
            ROOT.SH.ScanDir().filePattern(filename).samplePostfix("_gridDataset*").sampleDepth(0).scan(sh_all, sh_list)
            nFiles+=1
    else :
        logging.info(">>> Interpreting as grid dataset for submission to the grid.")
        ROOT.SH.scanRucio(sh_all, inp)
        sh_all.setMetaString("nc_grid_filter", "*");
    if not sh_all.size() :
        functionName = lambda : sys._getframe(1).f_code.co_name
        logging.warning(">>> Failed to find any samples in " + functionName())
    return

def addAlgsFromDict( job,  algsToRun , options ):
    for name,alg in algsToRun.iteritems() :
        setNameFunc = getattr(alg, "SetName", None)
        if setNameFunc != None:
            setVerbosity(alg , options.verbosity)
        logging.info(">>> ... Adding " + name + " to algs" )
        alg.SetName(name)#this is needed to see the alg names with athena messaging
        job.algsAdd(alg)
        if "basicEventSelection" in name:
            grlSanityCheck(options.inputDS,alg)
    return

def grlSanityCheck(inputDS,alg):

    if not hasattr(alg,"m_applyGRLCut"):
        return
    if not alg.m_applyGRLCut:
        return

    GRL = alg.m_GRLxml

    if not isinstance(inputDS, basestring):
        inputDS = " ".join(inputDS)

    chunks = inputDS.lower().split("data")
    for chunk in chunks:
        if chunk[:2].isdigit(): #probably looking at a year.
            year = chunk[:2]
            if not year in GRL:
                logging.warning(">>>")
                logging.warning(">>> " + "!"*100)
                logging.warning(">>> " + "!"*100)
                logging.warning(">>> WARNING: I think your input is from year %s."%(year))
                logging.warning(">>> The GRL you've handed xAH::BES doesn't have that in the name!")
                logging.warning(">>> Make sure you're using a relevant GRL!")
                logging.warning(">>> " + "!"*100)
                logging.warning(">>> " + "!"*100)
                logging.warning(">>>")

    return


def overwriteSubmitDir (submitDir, doOverwrite) :
    if os.path.isdir(submitDir) :
        logging.info(">>> " + submitDir + " already exists.")
        if doOverwrite :
            logging.info( ">>> Overwriting previous submitDir")
            shutil.rmtree(submitDir)
        else :
            logging.info( ">>> Exiting.  If you want to overwrite the previous submitDir, use --doOverwrite")
            quiet_exit()

def submitJob (job , driverName , submitDir, gridUser = "" , gridTag = "", groupRole = "", nEvents=-1, excludeSite='', gridSite='', extraSubmitFlags='', keepTags=False) :
    if (gridUser or gridTag) and driverName != "grid" :
        logging.error(">>> You have specified a gridUser or gridTag but are not using the grid driver.  Exiting without submitting.")
        quiet_exit()

    logging.info(">>> Running " + str(nEvents) + " events")
    job.options().setDouble (ROOT.EL.Job.optMaxEvents, float(nEvents));

    logging.info(">>> Creating driver")
    driver = None

    if (driverName == "direct"):
        logging.info( ">>> Direct driver")
        driver = ROOT.EL.DirectDriver()
        job.options().setDouble (ROOT.EL.Job.optCacheSize, 100*1024*1024);
        job.options().setDouble (ROOT.EL.Job.optCacheLearnEntries, 1000);
        logging.info(">>> Submit job")
        logging.info(">>> ")
        driver.submit(job, submitDir)

    elif driverName == "lsf" :
        driver = ROOT.EL.LSFDriver()
        # ROOT.SH.scanNEvents(sh);
        # sh.setMetaDouble(ROOT.EL.Job.optEventsPerWorker, 50000);
        job.options().setString(ROOT.EL.Job.optSubmitFlags, "-q " + "1nh");
        driver.submit(job, submitDir)

    elif (driverName == "prooflite"):
        logging.info( ">>> prooflite")
        driver = ROOT.EL.ProofDriver()
        logging.info(">>> Submit job")
        logging.info(">>> ")
        driver.submit(job, submitDir)

    elif (driverName == "grid"):
        logging.info( ">>> Grid driver")
        driver = ROOT.EL.PrunDriver()
        if not gridUser : gridUser = os.environ.get("USER")
        if not gridTag  : gridTag  = date.today().strftime("%m%d%y")
        submitFlags = " --addNthFieldOfInDSToLFN=2,6 "
        submitFlags += extraSubmitFlags
        if groupRole:
            dsPrefix = "group."+groupRole
            submitFlags += " --official "
        else:
            dsPrefix = "user."+gridUser
        if keepTags:
            gridOutputDatasetName = "%s.%%in:name[1]%%.%%in:name[2]%%.%%in:name[3]%%.%%in:name[6]%%.%s"%(dsPrefix,gridTag)
        else:
            gridOutputDatasetName = "%s.%%in:name[1]%%.%%in:name[2]%%.%%in:name[3]%%.%s"%(dsPrefix,gridTag)
        driver.options().setString("nc_outputSampleName", gridOutputDatasetName  );
        driver.options().setString("nc_EventLoop_SubmitFlags", submitFlags)
        driver.options().setString(ROOT.EL.Job.optGridNGBPerJob,  "MAX");
        #driver.options().setString(ROOT.EL.Job.optGridNFilesPerJob,  "1");
        driver.options().setDouble(ROOT.EL.Job.optGridMergeOutput, 0); # turning this off until this issue is fixed /CO : https://gitlab.cern.ch/atlas-phys-susy-wg/Common/FactoryTools/-/issues/42
        driver.options().setString(ROOT.EL.Job.optGridExcludedSite, excludeSite);
        driver.options().setString(ROOT.EL.Job.optGridSite, gridSite);

        if nEvents > 0:
            logging.warning( ">>> Grid driver doesn't support ---nevents option. Assuming you want a test job so I'll submit a 1-file job." )
            driver.options().setDouble(ROOT.EL.Job.optGridNFiles, 1);

        logging.info(">>> Submit job")
        logging.info(">>> ")
        driver.submitOnly(job, submitDir)

    elif (driverName == "condor"):
        driver = ROOT.EL.CondorDriver()
        # This is a hack because the CondorDriver is hardcoded to expect WorkDir_DIR
        os.environ["WorkDir_DIR"] = os.environ.get("FactoryToolsWrapper_DIR")
        driver.options().setBool(ROOT.EL.Job.optBatchSharedFileSystem, False)
        driver.options().setString(ROOT.EL.Job.optCondorConf, "stream_output = true")
        driver.submitOnly(job, submitDir)

    elif (driverName == "lxplus"):
        driver = ROOT.EL.CondorDriver()
        # This is a hack because the CondorDriver is hardcoded to expect WorkDir_DIR
        os.environ["WorkDir_DIR"] = os.environ.get("FactoryToolsWrapper_DIR")
        driver.options().setBool(ROOT.EL.Job.optBatchSharedFileSystem, True)
        driver.options().setString(ROOT.EL.Job.optCondorConf, "stream_output = true")
        driver.submitOnly(job, submitDir)

    elif (driverName == "net3"):
        driver = ROOT.EL.GEDriver()
        driver.options().setDouble(ROOT.EL.Job.optFilesPerWorker, 1);
        driver.options().setString(ROOT.EL.Job.optSubmitFlags, "-V -q tier3")
        driver.submitOnly(job, submitDir)

    else :
        logging.info( ">>> You gave an illegal driver name.  Not submitting.")


def configxAODAnaHelperAlg(alg , configDict = basicEventSelectionConfig.basicEventSelectionDict ) :
    for key, value in configDict.iteritems() :
        if not hasattr(alg, key) :
            raise AttributeError(key)
        setattr(alg, key, value )

def getSystList(dataSource = 1) :
    if( dataSource == 0 ) : return []#for data, just run once with no systematics

    susyTools = ROOT.ST.SUSYObjDef_xAOD("getlist")
    susyTools.setDataSource(dataSource)
    susyTools.setProperty("ConfigFile", "SUSYTools/SUSYTools_Default.conf")

    PRWLumiCalcFiles = ROOT.std.vector('string')(0)
    PRWLumiCalcFiles.push_back("$FactoryToolsWrapper_DIR/data/FactoryTools/ilumicalc_histograms_None_276262-304494_OflLumi-13TeV-005.root")

    PRWConfigFiles = ROOT.std.vector('string')(0)
    PRWConfigFiles.push_back("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc15c.root")

    susyTools.setProperty("PRWConfigFiles", PRWConfigFiles )
    susyTools.setProperty("PRWLumiCalcFiles", PRWLumiCalcFiles  )
    logging.info(">>> Initializing SUSYTools")

    susyTools.initialize()

    # registry = ROOT.CP.SystematicRegistry.getInstance()
    # recommendedSystematics = registry.recommendedSystematics()

    recommendedSystematics = susyTools.getSystInfoList()

    logging.debug(">>> Full list of recommended systematics is (%d):"%recommendedSystematics.size())
    for systInfo in recommendedSystematics:
        logging.debug(systInfo.systset.name() )

    return recommendedSystematics

def doSystematics(algsToRun, nonSystAlgs = ["basicEventSelection", "mcEventVeto"], fullChainOnWeightSysts = 0 , excludeStrings = []) :
    '''This function will get the list of systematics from an initialized SUSYTools instance.  For each algorithm in algsToRun, it will add a copy of that algorithm to the algsToRun with an additional string for the systematic.  It will apply the systematic to those algorithms which have systName as a member of the class.  By default, we will skip running systematics on the algorithms : ''' , nonSystAlgs ,  '''.'''
    tmpAlgsToRun = deepcopy(algsToRun)

    for systInfo in getSystList() :
        syst = systInfo.systset
        if syst.name() == "":
            continue
        if len( [substring for substring in excludeStrings if substring in syst.name()] ):
            logging.info(">>> Skipping excluded systematic %s"%syst.name())
            continue

        treatAsWeightSyst = False
        if systInfo.affectsWeights and fullChainOnWeightSysts == 0:
            logging.info(">>> Marking as weight-affecting systematic %s"%syst.name())
            treatAsWeightSyst = True

        logging.info(">>> Adding full chain of algs with systematic %s"%syst.name())
        for algname, alg in algsToRun.iteritems() :
            if algname not in nonSystAlgs :
                newalg = deepcopy(alg)
                if hasattr(newalg,"systVar") :
                    setattr(newalg, "systVar" , syst)
                # if hasattr(newalg,"systVarInfo") :
                #     logging.info("Applying systInfo called %s to %s"%(systInfo.systset.name(), algname  ) )
                #     setattr(newalg, "systVarInfo" , systInfo )
                if treatAsWeightSyst and "alibrate" not in algname:
                    continue
                tmpAlgsToRun[algname + '_' + syst.name() ] = newalg
                if treatAsWeightSyst:
                    tmpAlgsToRun = move_element(tmpAlgsToRun,
                        algname + '_' + syst.name(),
                        algsToRun.keys().index(algname)
                        )

    logging.debug(">>> List of syst algorithms:")
    for tmpalg in tmpAlgsToRun:
        logging.debug(">>> ... \t %s"%tmpalg)

    return tmpAlgsToRun



def move_element(odict, thekey, newpos):
    odict[thekey] = odict.pop(thekey)
    i = 0
    for key, value in odict.items():
        if key != thekey and i >= newpos:
            odict[key] = odict.pop(key)
        i += 1
    return odict


def initializeRunScript(options, args):

    ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

    logging.basicConfig()
    logging.getLogger().setLevel(translateLoggingVerbosity(options.verbosity) )

    logging.info( ">>> \a" )
    logging.info( ">>> You're running FactoryTools!" )
    logging.info( ">>> " )
    logging.info( ">>> Your current settings are:" )

    # Print out the settings
    for setting in dir(options):
        if not "_" in setting[0]:
            if "ensure_value" in setting or "read_" in setting: #cleaning out junk from options
                continue
            logging.info( ">>> ... Setting: {: >20} {: >40}".format(setting, eval("options.%s"%setting) ) )

    logging.info( ">>> " )

    # logging.basicConfig(level=translateLoggingVerbosity(options.verbosity) )

    # create a new sample handler to describe the data files we use
    logging.info(">>> Creating new sample handler")
    sh_all = ROOT.SH.SampleHandler()

    fillSampleHandler(sh_all, options.inputDS, options )

    sh_all.setMetaString ("nc_tree", "CollectionTree");

    # this is the basic description of our job
    logging.info(">>> Creating new job")

    job = ROOT.EL.Job()
    job.sampleHandler(sh_all)
    job.useXAOD()
    job.options().setString( ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_athena );

    return job
