
# These names should be as they are called in your samplelist
outputSampleNames = [
	"data15",
	"data16",
	"data17",
	"data18",
	"signal",
	"qcd",
	"top",
	"wjets",
	"zjets",
	"diboson",
]


def tagConfigFunction(sample):

	sample_name = sample.getMetaString("sample_name")
	dsid = sample.getMetaString( "dsid" )
	short_name = sample.getMetaString( "short_name" )

	## Signals
	if "GG_direct" in short_name:
		sample.addTag("signal")
		sample.addTag("gg_direct")
	if "GG_rpv_ALL" in short_name:
		sample.addTag("signal")
		sample.addTag("gg_rpv_all")
	if "GG_rpv_UDB" in short_name:
		sample.addTag("signal")
		sample.addTag("gg_rpv_udb")
	if "GG_rpv_UDS" in short_name:
		sample.addTag("signal")
		sample.addTag("gg_rpv_uds")
	if "SS_direct" in short_name:
		sample.addTag("signal")
		sample.addTag("ss_direct")
	if "SM_GG_N2" in short_name:
		sample.addTag("signal")
		sample.addTag("gg_n2")
	if "GG_onestepCC" in short_name:
		sample.addTag("signal")
		sample.addTag("gg_onestepcc")
	if "SS_onestepCC" in short_name:
		sample.addTag("signal")
		sample.addTag("ss_onestepcc")
	if "PythiaRhad" in short_name:
		sample.addTag("signal")
		sample.addTag("RHadron")
	if "lamp211_10" in short_name:
		sample.addTag("signal")
		sample.addTag("ctau_10")
	if "lamp211_100" in short_name:
		sample.addTag("signal")
		sample.addTag("ctau_100")
	if "lamp211_1000" in short_name:
		sample.addTag("signal")
		sample.addTag("ctau_1000")

	if "SlepSlep_directLLP" in short_name:
		sample.addTag("signal")

	## Top samples
	if "410000" in sample_name:
		sample.addTag("top")
		sample.addTag("ttbar")
	if int(dsid) in range(410011,410015):
		sample.addTag("top")
		sample.addTag("singletop")
	if int(dsid) in range(410066,410082):
		sample.addTag("top")
		sample.addTag("ttv")

	if "410470" in sample_name:
		sample.addTag("top")
		sample.addTag("ttbar")

	## QCD Samples
	if "jetjet" in short_name:
		sample.addTag("qcd")

	## Photon+Jets Samples
	# if "1Gam" in short_name:
	# 	sample.addTag("gamma")

	## V+Jets Samples
	if "Sherpa_CT10_W" in short_name and "_Pt" in short_name:
		sample.addTag("wjets")
	if "Sherpa_CT10_Z" in short_name and "_Pt" in short_name:
		sample.addTag("zjets")

	## Diboson Samples
	if int(dsid) in range(361063,361088):
		sample.addTag("diboson")
