#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/SelectHighd0Events.h>

#include <FactoryTools/HelperFunctions.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"

#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

// this is needed to distribute the algorithm to the workers
ClassImp(SelectHighd0Events)

typedef FactoryTools::HelperFunctions HF;

SelectHighd0Events :: SelectHighd0Events () {}
EL::StatusCode SelectHighd0Events :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectHighd0Events :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectHighd0Events :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectHighd0Events :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectHighd0Events :: initialize () {return EL::StatusCode::SUCCESS;}


EL::StatusCode SelectHighd0Events :: execute ()
{

  //////////////////////////////////////////////////
  // Setup

  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

  //////////////////////////////////////////////////
  // Some handy typedefs...

  typedef xAOD::IParticleContainer IPC;
  typedef xAOD::ParticleAuxContainer IPC_aux;

  if (HF::checkForSkip(eventInfo)) return EL::StatusCode::SUCCESS;


  //////////////////////////////////////////////////
  // Creating empty containers and setting them up

  auto selectedMuons             = HF::createContainerInTStore<IPC,IPC_aux>("selectedMuons",store);
  auto selectedBaselineMuons     = HF::createContainerInTStore<IPC,IPC_aux>("selectedBaselineMuons",store);
  auto selectedElectrons         = HF::createContainerInTStore<IPC,IPC_aux>("selectedElectrons",store);
  auto selectedBaselineElectrons = HF::createContainerInTStore<IPC,IPC_aux>("selectedBaselineElectrons",store);
  auto selectedPhotons           = HF::createContainerInTStore<IPC,IPC_aux>("selectedPhotons",store);
  auto selectedJets              = HF::createContainerInTStore<IPC,IPC_aux>("selectedJets",store);


  //////////////////////////////////////////////////
  // Grabbing containers from the TStore
  auto jets_nominal      = HF::grabFromStore<xAOD::JetContainer>("STCalibAntiKt4EMTopoJets",store);
  auto muons_nominal     = HF::grabFromStore<xAOD::MuonContainer>("STCalibMuons",store);
  auto electrons_nominal = HF::grabFromStore<xAOD::ElectronContainer>("STCalibElectrons",store);
  auto photons_nominal   = HF::grabFromStore<xAOD::PhotonContainer>("STCalibPhotons",store);


  for (const auto& jet : *jets_nominal) {
    if ((int)jet->auxdata<char>("baseline") == 0) continue;
   // if ((int)jet->auxdata<char>("passOR") != 1) continue;
    if ((int)jet->auxdata<char>("signal") != 1) continue;
    ATH_MSG_VERBOSE( "jet pt : " << jet->pt() );
    selectedJets.first->push_back(jet  );
  }


  for (const auto& mu : *muons_nominal) {
    if ((int)mu->auxdata<char>("baseline") == 0) continue;
    //if ((int)mu->auxdata<char>("passOR") != 1) continue;

    selectedBaselineMuons.first->push_back( mu );

    if ((int)mu->auxdata<char>("signal") != 1) continue;
    ATH_MSG_VERBOSE( "mu pt : " << mu->pt() );

    selectedMuons.first->push_back( mu );
  }

  for (const auto& el : *electrons_nominal) {
    if ((int)el->auxdata<char>("baseline") == 0) continue;
    //if ((int)el->auxdata<char>("passOR") != 1) continue;

    selectedBaselineElectrons.first->push_back( el );

    if ((int)el->auxdata<char>("signal") != 1) continue;
    ATH_MSG_VERBOSE( "el pt : " << el->pt() );

    selectedElectrons.first->push_back( el );
  }


  for (const auto& ph : *photons_nominal) {
    if ((int)ph->auxdata<char>("baseline") == 0) continue;
    //if ((int)ph->auxdata<char>("passOR") != 1) continue;
    if ((int)ph->auxdata<char>("signal") != 1) continue;

    ATH_MSG_VERBOSE( "ph pt : " << ph->pt() );

    selectedPhotons.first->push_back( ph );
  }


  int const nBaselineMuons = selectedBaselineMuons.first->size();
  int const nBaselineElectrons = selectedBaselineElectrons.first->size();
  int const nMuons = selectedMuons.first->size();
  int const nElectrons = selectedElectrons.first->size();
  int const nPhotons = selectedPhotons.first->size();

  ATH_MSG_DEBUG("Number of Selected Baseline Muons: " << nBaselineMuons );
  ATH_MSG_DEBUG("Number of Selected Baseline Electrons: " << nBaselineElectrons );
  ATH_MSG_DEBUG("Number of Selected Muons: " << nMuons  );
  ATH_MSG_DEBUG("Number of Selected Electrons: " << nElectrons  );
  ATH_MSG_DEBUG("Number of Selected Photons: " << nPhotons  );

  // Trigger ///////////////////

  /*
  bool passTM = false;
  for (auto muon: *selectedMuons.first){
    if(muon->auxdecor< int >( "passTM" ) ) passTM = true;
  }
  for (auto electron: *selectedElectrons.first){
    if(electron->auxdecor< int >( "passTM" ) ) passTM = true;
  }
  */
  /* // Commenting out as not used /CO
  auto trigORFromString = [](std::vector< std::string > passTrigs, std::string trigString){
      boost::replace_all(trigString, "_OR_", ":");
      std::vector<std::string> trigVect;
      boost::split(trigVect,trigString,boost::is_any_of(":") );
      bool trigDecision = 0;
      for(auto iTrig : trigVect){
          trigDecision |= std::find(passTrigs.begin(), passTrigs.end(), iTrig ) != passTrigs.end();
      }
      return trigDecision;
  };
  */

  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  eventInfo->auxdecor<bool>("passHLTmsonly") = HF::stringInVector(passTrigs, "HLT_mu60_0eta105_msonly");
  eventInfo->auxdecor<bool>("passHLTmu4msonly_cosmic_L1MU11") = HF::stringInVector(passTrigs, "HLT_mu60_0eta105_msonly");
  eventInfo->auxdecor<bool>("passHLTmu4msonly_cosmic_L1MU4")  = HF::stringInVector(passTrigs, "HLT_mu60_0eta105_msonly");

  // augmenting SUSYTools decision with MET triggers from the DRAW filters
  // See Issue #27

  eventInfo->auxdecor<bool>("isMETTrigPassed") |= HF::stringInVector(passTrigs, "HLT_xe120_pufit_L1XE60");
  eventInfo->auxdecor<bool>("isMETTrigPassed") |= HF::stringInVector(passTrigs, "HLT_xe110_pufit_L1XE60");

  // !!!!!!!!!!!
  // *
  // Apply event selection
  // *
  // if no selection string or MC stream out all events
  // if selection string and data apply selection

  bool isMC = eventInfo->auxdata<float>("isMC");

  std::string regionName = "";
  if( isMC) regionName = "SR_highd0";
  if ( !isMC )
  {
      if ( (nBaselineMuons + nBaselineElectrons + nPhotons )> 1) regionName = "SR_highd0";
  }
  

  eventInfo->auxdecor< std::string >("regionName") = regionName;

  ATH_MSG_DEBUG("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   );

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode SelectHighd0Events :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectHighd0Events :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectHighd0Events :: histFinalize () {return EL::StatusCode::SUCCESS;}
