/*###############################################
 name: DRAW_DVJETS_filters
 type: C++ namespace
 responsible: rcarney@lbl.gov

 description:
    Contains a struct, FilterFlags, used to store
    DRAWFilter decisions. Contains flag-setting
    functions and filter logic functions.

 last updated: Mar. 2018
###############################################*/
#include "FactoryTools/DRAW_DVJETS_filters.h"

//*********************************
// F L A G   C H E C K
//*********************************
//=========================
//Multiplicity flags - early Run2
//=========================
bool DRAW_DVJETS_filters::DV_2JetFilterFlags( float jetPt){

    if( jetPt >= 220. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_3JetFilterFlags( float jetPt){

    if( jetPt >= 120. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_4JetFilterFlags( float jetPt){

    if( jetPt >= 100. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_5JetFilterFlags( float jetPt){

    if( jetPt >= 75. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_6JetFilterFlags( float jetPt){

    if( jetPt >= 50. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_7JetFilterFlags( float jetPt){

    if( jetPt >= 45. ) return true;
    else return false;
}

//=========================
// Trackless jet flags
//=========================
bool DRAW_DVJETS_filters::DV_SingleTracklessJetFlag( float jetPt, float jetEta, float sumTrkPt ){

    if( jetPt >= 70. && fabs(jetEta)<= 2.5 && sumTrkPt <= 5.) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_DoubleTracklessJetsFlag( float jetPt, float jetEta, float sumTrkPt ){

    if( jetPt >= 50. && fabs(jetEta)<= 2.5 && sumTrkPt <=5. ) return true;
    else return false;
}

//=========================
// Multiplicity flags: high pT, late Run2
//=========================
bool DRAW_DVJETS_filters::DV_2JetFilterFlags_HighPtCut( float jetPt){

    if( jetPt >= 500. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_3JetFilterFlags_HighPtCut( float jetPt){

    if( jetPt >= 180. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_4JetFilterFlags_HighPtCut( float jetPt){

    if( jetPt >= 220. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_5JetFilterFlags_HighPtCut( float jetPt){

    if( jetPt >= 170. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_6JetFilterFlags_HighPtCut( float jetPt){

    if( jetPt >= 100. ) return true;
    else return false;
}

bool DRAW_DVJETS_filters::DV_7JetFilterFlags_HighPtCut( float jetPt){

    if( jetPt >= 75. ) return true;
    else return false;
}

//=========================
// This function just checks that all bools in the vector are true
//=========================
bool DRAW_DVJETS_filters::Pass_triggerFlags( std::vector<int>& trigDecisions ){

    if( trigDecisions.size() == 0 ){
        std::cout<<"WARNING: trigDecisions vector is empty! "<<std::endl;
        return false;
    }

    if(  std::any_of(trigDecisions.begin(), trigDecisions.end(), [](int i){return i>0;}) ){
        return true;
    } else {
        return false;
    }
}


//*********************************
// S E T   F I L T E R   F L A G S
//*********************************
void DRAW_DVJETS_filters::setFilterFlags( const xAOD::JetContainer* uncalibJets, std::vector<int>& trigDecisions, FilterFlags& filterFlags ){

    TH1F* h_filters = new TH1F("JetFilters", "JetFilters; JetFilters; Entries;", 100, 0, 100);
    h_filters->SetDirectory(0);

    //Counts how many jets pass filters
    for (const auto& jet : *uncalibJets ){

        //Get jet attributes (convert to GeV where necessary)
        float jetPt = jet->pt()*0.001;
        float jetEta = jet->p4().Eta();
        std::vector<float> sumPtTrkvec;
        jet->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec);
        float sumTrkPt = sumPtTrkvec[0]*0.001;

        if( DRAW_DVJETS_filters::DV_2JetFilterFlags( jetPt ) ) h_filters->AddBinContent(1);
        if( DRAW_DVJETS_filters::DV_3JetFilterFlags( jetPt ) ) h_filters->AddBinContent(2);
        if( DRAW_DVJETS_filters::DV_4JetFilterFlags( jetPt ) ) h_filters->AddBinContent(3);
        if( DRAW_DVJETS_filters::DV_5JetFilterFlags( jetPt ) ) h_filters->AddBinContent(4);
        if( DRAW_DVJETS_filters::DV_6JetFilterFlags( jetPt ) ) h_filters->AddBinContent(5);
        if( DRAW_DVJETS_filters::DV_7JetFilterFlags( jetPt ) ) h_filters->AddBinContent(6);

        if( DRAW_DVJETS_filters::DV_SingleTracklessJetFlag( jetPt, jetEta, sumTrkPt ) )
            h_filters->AddBinContent(7);
        if( DRAW_DVJETS_filters::DV_DoubleTracklessJetsFlag( jetPt, jetEta, sumTrkPt) )
            h_filters->AddBinContent(8);

        //HighPt Multiplicity
        if( DRAW_DVJETS_filters::DV_2JetFilterFlags_HighPtCut( jetPt ) ) h_filters->AddBinContent(9);
        if( DRAW_DVJETS_filters::DV_3JetFilterFlags_HighPtCut( jetPt ) ) h_filters->AddBinContent(10);
        if( DRAW_DVJETS_filters::DV_4JetFilterFlags_HighPtCut( jetPt ) ) h_filters->AddBinContent(11);
        if( DRAW_DVJETS_filters::DV_5JetFilterFlags_HighPtCut( jetPt ) ) h_filters->AddBinContent(12);
        if( DRAW_DVJETS_filters::DV_6JetFilterFlags_HighPtCut( jetPt ) ) h_filters->AddBinContent(13);
        if( DRAW_DVJETS_filters::DV_7JetFilterFlags_HighPtCut( jetPt ) ) h_filters->AddBinContent(14);
    }

    //Set filter flags
    filterFlags.reset();

    if( Pass_triggerFlags( trigDecisions ) ){
        filterFlags.pass_triggerFlags = 1;
    }
    if ( h_filters->GetBinContent(7) >= 1 ){
        filterFlags.pass_singleTracklessFlag = 1;
    }
    if ( h_filters->GetBinContent(8) >=2 ){
        filterFlags.pass_doubleTracklessFlag = 1;
    }
    if( h_filters->GetBinContent(1) >= 2){
        filterFlags.pass_2_jetMultFlag = 1;
    }
     if( h_filters->GetBinContent(2) >= 3 ){
        filterFlags.pass_3_jetMultFlag = 1;
    }
    if( h_filters->GetBinContent(3) >= 4){
        filterFlags.pass_4_jetMultFlag = 1;
    }
    if( h_filters->GetBinContent(4) >= 5){
        filterFlags.pass_5_jetMultFlag = 1;
    }
    if( h_filters->GetBinContent(5) >= 6){
        filterFlags.pass_6_jetMultFlag = 1;
    }
    if( h_filters->GetBinContent(6) >= 7 ){
        filterFlags.pass_7_jetMultFlag = 1;
    }
    if( h_filters->GetBinContent(9)  >= 2 ){
        filterFlags.pass_2_highpT_jetMultFlag = 1;
    }
    if( h_filters->GetBinContent(10) >= 3 ){
        filterFlags.pass_3_highpT_jetMultFlag = 1;
    }
    if(h_filters->GetBinContent(11) >= 4){
        filterFlags.pass_4_highpT_jetMultFlag = 1;
    }
    if(h_filters->GetBinContent(12) >= 5){
        filterFlags.pass_5_highpT_jetMultFlag = 1;
    }
    if(h_filters->GetBinContent(13) >= 6){
        filterFlags.pass_6_highpT_jetMultFlag = 1;
    }
    if(h_filters->GetBinContent(14) >= 7){
        filterFlags.pass_7_highpT_jetMultFlag = 1;
    }

    delete h_filters;
    return;
}

//*********************************
// F I L T E R   E V E N T S
//*********************************
EL::StatusCode DRAW_DVJETS_filters::filterEvents(const xAOD::JetContainer* uncalibJets, std::vector<int>& trigDecisions, FilterFlags& filterFlags){

    if( uncalibJets == NULL ){
        std::cout<<"ERROR! Uncalibrated jets pointer is null. Exiting."<<std::endl;
        return EL::StatusCode::FAILURE;
    }

    setFilterFlags( uncalibJets, trigDecisions, filterFlags );

    //Filter logic
    //Low pT jet mult
    if(     filterFlags.pass_2_jetMultFlag==1 &&
            filterFlags.pass_3_jetMultFlag==1 ){
        filterFlags.pass_2AND3_jetMultFlag = 1;
    }

    if(     filterFlags.pass_2AND3_jetMultFlag==1  ||
            filterFlags.pass_4_jetMultFlag==1      ||
            filterFlags.pass_5_jetMultFlag==1      ||
            filterFlags.pass_6_jetMultFlag==1      ||
            filterFlags.pass_7_jetMultFlag==1 ){
        filterFlags.pass_lowPtJetMult = 1;
    }

    //High pT jet mult
    if(     filterFlags.pass_2_highpT_jetMultFlag==1 &&
            filterFlags.pass_3_highpT_jetMultFlag==1 ){
        filterFlags.pass_2AND3_highpT_jetMultFlag = 1;
    }

    if(     filterFlags.pass_2AND3_highpT_jetMultFlag==1   ||
            filterFlags.pass_4_highpT_jetMultFlag==1       ||
            filterFlags.pass_5_highpT_jetMultFlag==1       ||
            filterFlags.pass_6_highpT_jetMultFlag==1       ||
            filterFlags.pass_7_highpT_jetMultFlag==1 ){
        filterFlags.pass_highPtJetMult = 1;
    }

    //Trackless jets
    if(     filterFlags.pass_singleTracklessFlag==1 ||
            filterFlags.pass_doubleTracklessFlag==1 ){
        filterFlags.pass_TracklessPrescale = 1;
    }

    //DRAW decision
    if(     filterFlags.pass_triggerFlags==1       &&
            filterFlags.pass_TracklessPrescale==1  &&
            filterFlags.pass_lowPtJetMult==1 ){
        filterFlags.pass_earlyRun2 = 1;
        filterFlags.pass_DRAW = 1;
    }

    if( filterFlags.pass_highPtJetMult==1 && filterFlags.pass_triggerFlags==1){
        filterFlags.pass_DRAW = 1;
    }

    return EL::StatusCode::SUCCESS;
}

