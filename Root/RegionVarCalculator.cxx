#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "FactoryTools/RegionVarCalculator.h"
#include "FactoryTools/strongErrorCheck.h"
#include "FactoryTools/TreeManager.h"

#include <xAODAnaHelpers/HelperFunctions.h>

#include <FactoryTools/HelperFunctions.h>

// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator)

typedef FactoryTools::HelperFunctions HF;

std::vector<xAOD::IParticle*> RegionVarCalculator::sortByPt(xAOD::IParticleContainer* inputIParticleContainer){
  // Sort by pt
  std::vector<xAOD::IParticle*> outputVector;
  for( xAOD::IParticle * thisIParticle : *inputIParticleContainer) outputVector.push_back(thisIParticle);
  std::sort(outputVector.begin(), outputVector.end(), PtOrder<xAOD::IParticle>);
  return outputVector;
}

std::vector<xAOD::IParticle*> RegionVarCalculator::sortByPt(std::vector<xAOD::IParticle*>* inputVec){
  // Sort by pt
  std::vector<xAOD::IParticle*> outputVector = std::vector<xAOD::IParticle*>(*inputVec);
  std::sort(outputVector.begin(), outputVector.end(), PtOrder<xAOD::IParticle>);
  return outputVector;
}

std::vector<xAOD::IParticle*> RegionVarCalculator::sortByPt(std::vector<xAOD::IParticle*> inputVec){
  // Sort by pt
  std::vector<xAOD::IParticle*> outputVector = inputVec;
  std::sort(outputVector.begin(), outputVector.end(), PtOrder<xAOD::IParticle>);
  return outputVector;
}

EL::StatusCode RegionVarCalculator::doGeneralCalculations(std::map<std::string, anytype>& vars )
{
  xAOD::TEvent * event = m_worker->xaodEvent();
  // Get relevant info from the EventInfo object

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

  // Include variables for all samples ///////////////////////////////
  //
  vars["runNumber"]   = eventInfo->runNumber();
  vars["lumiBlock"]   = eventInfo->lumiBlock();
  vars["bcid"]        = eventInfo->bcid();
  vars["eventNumber"] = eventInfo->eventNumber();

  //
  /////////////////////////////////////////////////////////////////////

  // For MC Samples only (Not defined for data) ///////////////////////
  //

  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    vars["mcChannelNumber"] = eventInfo->mcChannelNumber();
    vars["mcEventWeight"]   = eventInfo->auxdecor< float >("mcEventWeight");
  }
  //
  /////////////////////////////////////////////////////////////////////

  // Include variables for all samples ///////////////////////////////
  //
  vars["pileupWeight"]                   = eventInfo->auxdecor< float >("PileupWeight");
  vars["pileupReweightHash"]             = static_cast< long long >(eventInfo->auxdecor< ULong64_t >("PRWHash"));
  vars["actualInteractionsPerCrossing"]  = eventInfo->actualInteractionsPerCrossing();
  vars["averageInteractionsPerCrossing"] = eventInfo->averageInteractionsPerCrossing();

  //
  /////////////////////////////////////////////////////////////////////

  // Get relevant info from the vertex container //////////////////////
  //

  auto vertices = HF::grabFromEvent<xAOD::VertexContainer>("PrimaryVertices",event);
  vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);

  //
  /////////////////////////////////////////////////////////////////////


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RegionVarCalculator::storeTriggerDecisions(std::map<std::string, anytype>& vars, const std::vector<std::string>& triggerList ) {

  xAOD::TEvent * event = m_worker->xaodEvent();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  // Clear trigger decisions from last event
  m_event_triggerDecisions.clear();

  if( !triggerList.empty() ){
      for (auto triggerName : triggerList){
          int triggerDecision = std::find(passTrigs.begin(), passTrigs.end(), triggerName) != passTrigs.end();
          vars[Form("pass_%s",triggerName.c_str() )] = triggerDecision;
          addToVectorBranch(vars, "TriggerDecisions", triggerDecision );
          m_event_triggerDecisions.push_back( triggerDecision ); //TODO is this the best way?
      }
  }

  return EL::StatusCode::SUCCESS;
}




bool RegionVarCalculator::inListOfDetailedObjects (std::string object="")
{
  if(object=="") return false;

  xAOD::TEvent * event = m_worker->xaodEvent();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

  return std::find(
    eventInfo->auxdecor< std::vector<std::string> >("listOfDetailedObjects").begin(),
    eventInfo->auxdecor< std::vector<std::string> >("listOfDetailedObjects").end(),
    object)!=eventInfo->auxdecor< std::vector<std::string> >("listOfDetailedObjects").end();
}

const xAOD::TruthParticle* RegionVarCalculator::getGeneratorParent(const xAOD::TruthParticle* p){
    
    if(!p)
        return 0;
    if(p->barcode() < 200000)
        return p;
    else 
        return getGeneratorParent(p->parent(0));
}
