#include <FactoryTools/AnyType.h>
#include <FactoryTools/TreeManager.h>
#include <FactoryTools/CalculateRegionVars.h>
#include <FactoryTools/CalibrateST.h>
#include <FactoryTools/HelperFunctions.h>
#include <FactoryTools/RegionVarCalculator.h>
#include <FactoryTools/RegionVarCalculator_mediumd0.h>
#include <FactoryTools/RegionVarCalculator_jpsi.h>
#include <FactoryTools/RegionVarCalculator_dv.h>
#include <FactoryTools/RegionVarCalculator_rpvmultijet.h>
#include <FactoryTools/RegionVarCalculator_DRAWSelection.h>
#include <FactoryTools/RegionVarCalculator_passthrough.h>
#include <FactoryTools/RegionVarCalculator_highd0.h>
#include <FactoryTools/SelectMediumD0Events.h>
#include <FactoryTools/SelectJpsiEvents.h>
#include <FactoryTools/SelectDVEvents.h>
#include <FactoryTools/Select_DRAWFilterEvents.h>
#include <FactoryTools/SelectHighd0Events.h>
#include <FactoryTools/StoreDVObjects.h>
#include <FactoryTools/StoreMuonDVObjects.h>
#include <FactoryTools/WriteOutputNtuple.h>
#include <FactoryTools/JetMakerTool.h>
#include <FactoryTools/TracksForJets.h>


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class TreeManager+;
#pragma link C++ class CalculateRegionVars+;
#pragma link C++ class CalibrateST+;
#pragma link C++ class HelperFunctions+;
#pragma link C++ class RegionVarCalculator+;
#pragma link C++ class RegionVarCalculator_mediumd0+;
#pragma link C++ class RegionVarCalculator_jpsi+;
#pragma link C++ class RegionVarCalculator_dv+;
#pragma link C++ class RegionVarCalculator_rpvmultijet+;
#pragma link C++ class RegionVarCalculator_DRAWSelection+;
#pragma link C++ class RegionVarCalculator_passthrough+;
#pragma link C++ class RegionVarCalculator_highd0+;
#pragma link C++ class SelectDVEvents+;
#pragma link C++ class SelectRPVMultijetEvents+;
#pragma link C++ class SelectMediumD0Events+;
#pragma link C++ class SelectJpsiEvents+;
#pragma link C++ class Select_DRAWFilterEvents+;
#pragma link C++ class SelectHighd0Events+;
#pragma link C++ class StoreDVObjects+;
#pragma link C++ class StoreMuonDVObjects+;
#pragma link C++ class WriteOutputNtuple+;
#pragma link C++ class JetMakerTool+;
#pragma link C++ class TracksForJets+;

#include <FactoryTools/VsiAnalysis/VsiTruthHelper.h>
#include <FactoryTools/VsiAnalysis/VsiBonsaiTool.h>
#include <FactoryTools/VsiAnalysis/VsiAnaExample.h>
#include <FactoryTools/VsiAnalysis/VsiAnaModular.h>
#include <FactoryTools/ModularAlgorithmBase.h>
#include <FactoryTools/ProcessorBase.h>

#pragma link C++ class VsiTruthHelpder+;
#pragma link C++ class VsiAnaExample+;
#pragma link C++ class VsiAnaModular+;
#pragma link C++ class ModularAlgorithmBase+;
#pragma link C++ class ProcessorBase+;

#endif
