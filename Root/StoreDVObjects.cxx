#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventAuxInfo.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

#include "FactoryTools/StoreDVObjects.h"
#include "FactoryTools/strongErrorCheck.h"
#include "FactoryTools/HelperFunctions.h"

#include "TFile.h"

#include <boost/algorithm/string/replace.hpp>

typedef FactoryTools::HelperFunctions HF;

// this is needed to distribute the algorithm to the workers
ClassImp(StoreDVObjects)

StoreDVObjects :: StoreDVObjects () {}
EL::StatusCode StoreDVObjects :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode StoreDVObjects :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode StoreDVObjects :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode StoreDVObjects :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}

//================================
// T R K   P R O P E R T I E S 
//================================

//**************************
//Calculate PV(x,y,z)-DV(x,y,z)
//**************************
//To get gradient of PV-DV vector.
TVector3 get_PVDV( xAOD::Vertex* pv,xAOD::Vertex* dv ){

	TVector3 PV( pv->x(), pv->y(), pv->z() );
	TVector3 DV( dv->x(), dv->y(), dv->z() );
	TVector3 PV_DV = DV-PV;
	return PV_DV;

}

//*******************************************************
//Calculate the deltaPhi between track and PV-DV vector
//*******************************************************
double get_dPhi( TVector3 PV_DV, double phi_DV ){

	//Calculate PV-DV vector angle wrt x-axis in x-y plane (phi)
	double phi_PVDV = PV_DV.Phi();

	//Calculate deltaPhi 
	double deltaPhi = phi_PVDV - phi_DV;
	
	return deltaPhi;
}

//***********************************
// Hit pattern consistency check
//***********************************
// This is already applied to selected tracks in vertexing step. 
// We are considering applying this to attached tracks as well
// Adapted from:
// https://gitlab.cern.ch/atlas/athena/-/blob/21.0/Reconstruction/VKalVrt/VrtSecInclusive/src/Utilities.cxx#L1396
bool pass_patternCheck( const uint32_t& hitPattern, double rad, double absz) {

   enum vertexArea {
    insideBeamPipe,

    insidePixelBarrel0,
    aroundPixelBarrel0,

    outsidePixelBarrel0_and_insidePixelBarrel1,
    aroundPixelBarrel1,	

    outsidePixelBarrel1_and_insidePixelBarrel2,
    aroundPixelBarrel2,

    outsidePixelBarrel2_and_insidePixelBarrel3,
    aroundPixelBarrel3,

    outsidePixelBarrel3_and_insideSctBarrel0,
    aroundSctBarrel0,
  
    outsideFidVolume
   } vertexPattern;

   enum DetectorType{
    pixelBarrel0,
    pixelBarrel1,
    pixelBarrel2,
    pixelBarrel3,
    sctBarrel0,
   };

   if( rad < 23.50 ) {
      vertexPattern = insideBeamPipe;
   } else if( rad < 31.0 && absz < 331.5 ) {
      vertexPattern = insidePixelBarrel0;
   } else if( rad < 38.4 && absz < 331.5 ) {
      vertexPattern = aroundPixelBarrel0;
   } else if( rad < 47.7 && absz < 400.5 ) {
      vertexPattern = outsidePixelBarrel0_and_insidePixelBarrel1;
   } else if( rad < 54.4 && absz < 400.5 ) {
      vertexPattern = aroundPixelBarrel1;
   } else if( rad < 85.5 && absz < 400.5 ) {
      vertexPattern = outsidePixelBarrel1_and_insidePixelBarrel2;
   } else if( rad < 92.2 && absz < 400.5 ) {
      vertexPattern = aroundPixelBarrel2;
   } else if( rad < 119.3 && absz < 400.5 ) {
      vertexPattern = outsidePixelBarrel2_and_insidePixelBarrel3;
   } else if( rad < 126.1 && absz < 400.5 ) {
      vertexPattern = aroundPixelBarrel3;
   } else if( rad < 290 && absz < 749.0 ) {
      vertexPattern = outsidePixelBarrel3_and_insideSctBarrel0;
   } else if( rad < 315 && absz < 749.0 ) {
      vertexPattern = aroundSctBarrel0;
   } else {
      vertexPattern = outsideFidVolume; //shouldn't happen
   }

   // Check for forbidden hits: create a forbiddenHits pattern using the DetectorType enum
   // and compare for shared hits with track's hit pattern
   uint32_t forbiddenHits = 0;
   switch( vertexPattern ){

     case( insideBeamPipe): break;
     case( insidePixelBarrel0): break;
     case( aroundPixelBarrel0): break; 
     
     case( outsidePixelBarrel0_and_insidePixelBarrel1):
       // forbidden hits: 000000001
       forbiddenHits = std::pow(2.,pixelBarrel1)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;
     case( aroundPixelBarrel1):
       forbiddenHits = std::pow(2.,pixelBarrel1)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;

     case( outsidePixelBarrel1_and_insidePixelBarrel2):
       // forbidden hits: 000000011
       forbiddenHits = std::pow(2.,pixelBarrel2)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;
     case( aroundPixelBarrel2):
       forbiddenHits = std::pow(2.,pixelBarrel2)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;

     case( outsidePixelBarrel2_and_insidePixelBarrel3):
       // forbidden hits: 000000111
       forbiddenHits = std::pow(2.,pixelBarrel3)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;
     case( aroundPixelBarrel3):
       forbiddenHits = std::pow(2.,pixelBarrel3)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;

     case( outsidePixelBarrel3_and_insideSctBarrel0):
       // forbidden hits: 000001111
       forbiddenHits = std::pow(2.,sctBarrel0)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;
     case( aroundSctBarrel0):
       forbiddenHits = std::pow(2.,sctBarrel0)-1;
       if( hitPattern & forbiddenHits ) return false;
       break;

     case( outsideFidVolume): break;
   }

   return true;
}


EL::StatusCode StoreDVObjects :: initialize ()
{
	
    TFile* materialMap_InnerFile = new TFile(materialMap_Inner_FileName.c_str(), "READ");
	if(materialMap_InnerFile->IsOpen()){
	        materialMap_InnerFile->GetObject(materialMap_Inner_HistName.c_str(), m_materialMap_Inner);
		materialMap_InnerFile->GetObject(materialMap_Inner_MatrixName.c_str(), m_materialMap_Matrix);
		if(m_materialMap_Inner) m_materialMap_Inner->SetDirectory(0);
	}
	materialMap_InnerFile->Close();
	delete materialMap_InnerFile;

	TFile* materialMap_OuterFile = new TFile(materialMap_Outer_FileName.c_str(), "READ");
	if(materialMap_OuterFile->IsOpen()){
		materialMap_OuterFile->GetObject(materialMap_Outer_HistName.c_str(), m_materialMap_Outer);
		if(m_materialMap_Outer) m_materialMap_Outer->SetDirectory(0);
	}
	materialMap_OuterFile->Close();
	delete materialMap_OuterFile;


	return EL::StatusCode::SUCCESS;
}


EL::StatusCode StoreDVObjects :: execute ()
{

	xAOD::TStore * store = wk()->xaodStore();
	xAOD::TEvent * event = wk()->xaodEvent();

	// auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	// Store PV //////////////////////////////////////////

	auto selectedPV = HF::createContainerInTStore<xAOD::VertexContainer,xAOD::VertexAuxContainer>("selectedPV",store);
	auto pvc = HF::grabFromEvent<const xAOD::VertexContainer>("PrimaryVertices",event);

	xAOD::Vertex* pv(0);
	int nPV = 0;
	for ( const auto& vx : *pvc )
	{
		nPV++;
		if (vx->vertexType() == xAOD::VxType::PriVtx)
		{
			if(pv == nullptr || (vx->auxdataConst<float>( "sumPt2" ) > pv->auxdataConst<float>( "sumPt2" )))
			{
				pv = vx;
			}
		}
	}
	if (pv!=0) selectedPV.first->push_back( pv );
	std::string dvContainerName = "VrtSecInclusive_SecondaryVertices" + m_dvContainerSuffix;
	auto dvc = HF::grabFromEvent<xAOD::VertexContainer>(dvContainerName, event);

	Int_t nTracks = 0;
	Int_t nTracksSel = 0;
	Int_t nTracksLRT = 0;

	for ( const auto& dv : *dvc )
	{
		nTracks = 0; nTracksLRT = 0; nTracksSel = 0;
		TLorentzVector p; // Correct method using tracks w.r.t DV

                double dv_rxy = hypot(dv->x(), dv->y());

                float maxPt = 0.;
                float sumPt = 0.;

		auto tpLinks = dv->trackParticleLinks();
		for (auto link : tpLinks) {
                   const xAOD::TrackParticle *trk = (*link);
                   trk->auxdecor<bool>("pass_cleaning") = false;

		   //----------------
		   // Track vetos
		   //----------------

                   // NB: Check if this is fixed with vsi in new SUSY15 production 
	           //Reject tracks that fail extrapolation in VrtSecInclusive
                   trk->auxdecor<int>("failedExtrapolation") = 0;
		   if( std::fabs(trk->auxdataConst<float>("pt_wrtSV"+ m_dvContainerSuffix)/1000.) < 1e-12 ){
                      trk->auxdecor<int>("failedExtrapolation") = 1;
		   }

		   //Check if track is backwards-pointing.
		   //Currently, rejects tracks with angles > 3 radians (172 degrees) and d0 < 1 mm
		   double dPhi = get_dPhi( get_PVDV( pv, dv ), trk->auxdataConst<float>("phi_wrtSV"+ m_dvContainerSuffix) );
                   trk->auxdecor<int>("isBackwardsTrack") = 0;
		   if( std::fabs(dPhi) > 3. && std::fabs(trk->d0()) < 1. ){
                      trk->auxdecor<int>("isBackwardsTrack") = 1;
		   }

                   // Check for forbidden hits. Currently not used for selection, but stored in ntuple
                   int patternCheck = pass_patternCheck(trk->auxdata<unsigned int>("hitPattern"), dv_rxy, fabs(dv->z()));
                   trk->auxdecor<int>("pass_patternCheck") = patternCheck;

		   TLorentzVector p4_track;
		   p4_track.SetPtEtaPhiM(	trk->auxdataConst<float>( "pt_wrtSV"+ m_dvContainerSuffix ),
						trk->auxdataConst<float>( "eta_wrtSV"+ m_dvContainerSuffix ),
						trk->auxdataConst<float>( "phi_wrtSV"+ m_dvContainerSuffix ),
						trk->m() );
           
                   // After setting all track flags, apply track selection for DV mass, ntrk calculation

                   // Quality selection
                   if (trk->auxdecor<int>("failedExtrapolation")) continue;

		   //Upstream Hit Veto
		   if (trk->auxdataConst<float>("radiusOfFirstHit") < dv_rxy) continue;
                   
                   // pT selection
                   bool isAssociated = (trk->isAvailable<char>("is_associated"+ m_dvContainerSuffix)) ? trk->auxdataConst<char>("is_associated"+ m_dvContainerSuffix) : false; 
		   
		   if ( p4_track.Pt()/1000. < 2. ) continue; // require pt>2GeV
		   if ( isAssociated ){
		     if ( (dv_rxy > 25) && (p4_track.Pt()/1000. < 3.) ) continue; // require pt>3GeV for attached tracks outside beam pipe
		     if ( (dv_rxy > 145) && (p4_track.Pt()/1000. < 4.) ) continue; // require pt>3GeV for attached tracks outside last pixel
		   }
                   // d0-significance selection
		   if ( (dv_rxy < 25 ) && ( fabs(trk->d0()/TMath::Sqrt(trk->definingParametersCovMatrix()(0, 0))) < 10.) ) 
		        continue; // require d0-significance > 10 for all tracks inside beam pipe 
		   if ( (dv_rxy < 145 ) && ( fabs(trk->d0()/TMath::Sqrt(trk->definingParametersCovMatrix()(0, 0))) < 15.)
			&& isAssociated )  continue; // require d0-significance > 15 for attached tracks inside last pixel 
		   if ( (dv_rxy > 145 ) && ( fabs(trk->d0()/TMath::Sqrt(trk->definingParametersCovMatrix()(0, 0))) < 10.)
			&& !isAssociated ) continue; // require d0-significance > 10 for selected tracks outside last pixel 
		   // angular selection
		   if ( isAssociated && (p4_track.Angle( get_PVDV( pv,dv ) ) ) > TMath::Pi()/2) continue; // require attached tracks to be within 90 degrees of the pvdv-direction
		   // low pt forward
		   if ( (dv_rxy > 25) && ( p4_track.Angle( get_PVDV( pv,dv ) ) <0.2 ) && (p4_track.Pt()/1000. < 4.) ) continue; // remove tracks which continue in the direction of the pvdv within 0.4 radians to have a pt greater than 4

		   p += p4_track;
                   sumPt += p4_track.Pt();
                   if (p4_track.Pt() > maxPt) maxPt = p4_track.Pt();

	           ++nTracks;
                   trk->auxdecor<bool>("pass_cleaning") = true;
                   if(!isAssociated) ++nTracksSel;
		   if(trk->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0)) {
		      ++nTracksLRT;
	           }

		}

		dv->auxdecor<int>("nTracks_cleaned")         = nTracks;
		dv->auxdecor<int>("nTracksSel_cleaned")      = nTracksSel;
		dv->auxdecor<int>("nTracksLRT_cleaned")      = nTracksLRT;
		dv->auxdecor<float>("mass_cleaned")   = p.M();

                float maxPtOverSumPt = 0.;
                if (sumPt>0.) maxPtOverSumPt = maxPt/sumPt;
		dv->auxdecor<float>("maxPtOverSumPt") = maxPtOverSumPt;

		auto dv_pos = dv->position();
		dv->auxdecor<bool>("passFiducialCuts") =  (dv_pos.perp() < m_rDVMax && std::fabs(dv_pos.z()) < m_zDVMax);
		dv->auxdecor<bool>("passChisqCut")     =  (dv->chiSquared() / dv->numberDoF() < m_chisqDof);

		dv->auxdecor<bool>("passDistCut") = true;
		for(const auto & pv: *pvc){
			if( (pv->position() - dv_pos).perp() < m_distMin ) dv->auxdecor<bool>("passDistCut") = false;
		}

		dv->auxdecor<bool>("passMassCut") = (p.M() > m_DVMassMin);
                // In addition to counting overall tracks, 
                // check if DVs inside BP have minimum # of selected tracks
                bool passesNtrkCut = true;
                if (nTracks < m_DVntrkMin) passesNtrkCut = false;
		//Require that 2 selected tracks survive the track cleaning
                if (nTracksSel < 2) passesNtrkCut = false;
		dv->auxdecor<bool>("passNtrkCut") = passesNtrkCut;

		bool passMaterialVeto=0;
                // Add 'stricter' material map veto
                // All bins in +/- Rxy, phi, and z directions are checked for presence of material
                // Granularity of material map was taken from DV+mu internal note (https://cds.cern.ch/record/2633973/files/ATL-COM-PHYS-2018-1138.pdf)
                // Unlike normal material map veto, stricter material map veto assumes DV passes flag (=1), unless material is found in bin
                bool passMaterialVeto_strict=1;

                if (dv_pos.perp() > 150){

		   passMaterialVeto = ( m_materialMap_Outer->GetBinContent(m_materialMap_Outer->FindBin( dv_pos.perp(), dv_pos.phi(), dv_pos.z() ) ) == 0);

                   std::vector<double> dR_values = {-0.8, 0.0, 0.8};
                   std::vector<double> dPhi_values = {-0.014477, 0.0, 0.014477};
                   std::vector<double> dz_values = {-2.0, 0.0, 2.0};

                   for (auto & dR : dR_values){
                     for (auto & dPhi : dPhi_values){
                       for (auto & dz : dz_values){

                          if (m_materialMap_Outer->GetBinContent(m_materialMap_Outer->FindBin( dv_pos.perp()+dR , dv_pos.phi()+dPhi , dv_pos.z()+dz ) ) == 1) passMaterialVeto_strict = 0;
 
                       }
                     }
                   }
		}
		else {
		   for (Int_t i=0;i<5;i++){
                      if (dv_pos.perp() < (*m_materialMap_Matrix)[i][0]){
	                 float test_x = dv_pos.x() + (*m_materialMap_Matrix)[i][1];
		         float test_y = dv_pos.y() + (*m_materialMap_Matrix)[i][2];
                         float test_rxy = sqrt(test_x*test_x + test_y*test_y);
			 double calc_phi = fmod( TMath::ATan2(test_y,test_x),TMath::Pi()/(*m_materialMap_Matrix)[i][3] );
			 if (calc_phi <0) calc_phi = calc_phi + TMath::Pi()/(*m_materialMap_Matrix)[i][3];
					
			 passMaterialVeto = ( m_materialMap_Inner->GetBinContent(m_materialMap_Inner->FindBin(sqrt(test_x*test_x + test_y*test_y), calc_phi, dv_pos.z() ) ) == 0);

                         std::vector<double> dR_values = {-0.2, 0.0, 0.2};
                         std::vector<double> dPhi_values = {-0.007427, 0.0, 0.007427};
                         std::vector<double> dz_values = {-1.0, 0.0, 1.0};

                         for (auto & dR : dR_values){
                            for (auto & dPhi : dPhi_values){
                               for (auto & dz : dz_values){

                                  if (m_materialMap_Inner->GetBinContent(m_materialMap_Inner->FindBin( test_rxy+dR , calc_phi+dPhi , dv_pos.z()+dz ) ) == 1) passMaterialVeto_strict = 0;

                               }
                            }
                         }
			 break;
                      }
		   }
		}
		dv->auxdecor<bool>("passMaterialVeto") = passMaterialVeto;
		dv->auxdecor<bool>("passMaterialVeto_strict") = passMaterialVeto_strict;


	}

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode StoreDVObjects :: postExecute () {return EL::StatusCode::SUCCESS;}

EL::StatusCode StoreDVObjects :: finalize ()
{
    delete m_materialMap_Inner;
    delete m_materialMap_Matrix;
    delete m_materialMap_Outer;
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode StoreDVObjects :: histFinalize () {return EL::StatusCode::SUCCESS;}
