#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventAuxInfo.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODBase/IParticleContainer.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "FactoryTools/StoreMuonDVObjects.h"
#include "FactoryTools/strongErrorCheck.h"
#include "FactoryTools/HelperFunctions.h"
#include "TFile.h"

typedef FactoryTools::HelperFunctions HF;

// this is needed to distribute the algorithm to the workers
ClassImp(StoreMuonDVObjects)

float eta(float theta)
{
	return -log( tan( theta /2.0 )  );
}

StoreMuonDVObjects :: StoreMuonDVObjects () {}
EL::StatusCode StoreMuonDVObjects :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode StoreMuonDVObjects :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode StoreMuonDVObjects :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode StoreMuonDVObjects :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}

EL::StatusCode StoreMuonDVObjects :: initialize ()
{

	auto segmentMapFile = new TFile(segmentMapFileName.c_str(), "READ");
	if(segmentMapFile->IsOpen()){

		segmentMapFile->GetObject(InnerMapHistName     .c_str(), m_InnerMap);
		segmentMapFile->GetObject(MiddleMapHistName    .c_str(), m_MiddleMap);
		segmentMapFile->GetObject(OuterMapHistName     .c_str(), m_OuterMap);

		if(m_InnerMap ) m_InnerMap  ->SetDirectory(0);
		if(m_MiddleMap) m_MiddleMap ->SetDirectory(0);
		if(m_OuterMap ) m_OuterMap  ->SetDirectory(0);
	}
	segmentMapFile->Close();
	delete segmentMapFile;

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode StoreMuonDVObjects :: execute ()
{

	xAOD::TStore * store = wk()->xaodStore();
	xAOD::TEvent * event = wk()->xaodEvent();

	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	// Get baseline muons, and check to see if they pass selection
	auto muons_baseline = HF::grabFromStore<xAOD::IParticleContainer>("selectedBaselineMuons",store);

	// Get muon segments to use for cosmic veto
	auto ms_segments = HF::grabFromEvent<const xAOD::MuonSegmentContainer>("MuonSegments",event);

	// Loop over muons and segments
	//for( const xAOD::Muon * mu : *muons){
	int lead_mu = -99;
	float max_pt = m_minPt;
	for( xAOD::IParticle * muon : *muons_baseline){

		xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

		const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
		const xAOD::TrackParticle* cbtrack = mu->trackParticle( xAOD::Muon::CombinedTrackParticle );

		// Save preselection information
		float d0 = (idtrack) ? idtrack->d0() : -9999.;
		mu->auxdecor<bool>("passPreselection") = (fabs(mu->eta())<m_minEta && mu->pt()/1000.>m_minPt && fabs(d0)>m_minD0 && fabs(d0)<m_maxD0 ) ? true : false;

		// Save fake veto information
		float chi2 = (cbtrack) ? cbtrack->auxdata< float >("chiSquared")/(cbtrack->auxdata< float >("numberDoF")) : 999.;
		float npres = mu->auxdataConst<unsigned char>("numberOfPrecisionLayers");
		mu->auxdecor<bool>("passFakeVeto") = (chi2 < 8 && npres >= 3) ? true : false;

		// Save cosmic veto information
		bool hasMatchedSegment    = isMatchedToSegment(mu,ms_segments);
		bool passCosmicAcceptance = passSegmentAcceptance(mu, eventInfo->runNumber() );

		mu->auxdecor<bool>("passSegmentMatch"    ) = !hasMatchedSegment;
		mu->auxdecor<bool>("passCosmicAcceptance") = passCosmicAcceptance;
		mu->auxdecor<bool>("passCosmicVeto"      ) = (!hasMatchedSegment && passCosmicAcceptance);

		// Save heavy flavor information
		float track_iso = mu->auxdata< float >("ptvarcone30") /mu->pt();
		float calo_iso  = mu->auxdata< float >("topoetcone20")/mu->pt();
		mu->auxdecor<bool>("passIsolation") =  ( track_iso < m_trackIso && calo_iso < m_caloIso) ? true : false ;

		mu->auxdecor<bool>("passFullSelection") = (mu->auxdata<bool>("passPreselection") && mu->auxdata<bool>("passIsolation") && mu->auxdata<bool>("passCosmicVeto") && mu->auxdata<bool>("passFakeVeto") ) ? true : false;

		// find leading muons
		if (mu->auxdata<bool>("passPreselection") && mu->pt()/1000. > max_pt)
		{
			lead_mu = mu->index();
			max_pt = mu->pt()/1000.;
		}
	}

	// second loop to save if leading
	for( xAOD::IParticle * muon : *muons_baseline){
		xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);
		mu->auxdecor<bool>("isLeading") =  ( (int) mu->index() == lead_mu ) ? true : false ;
	}

	// also save if event passes Z selection
	float zmass = zMass(muons_baseline);
	eventInfo->auxdecor<float>( "zMass" ) = zmass;
	eventInfo->auxdecor<bool>( "passZselection" ) = ( zmass > 81. && zmass < 101.);

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode StoreMuonDVObjects :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode StoreMuonDVObjects :: finalize () {
	delete m_InnerMap ;
	delete m_MiddleMap;
	delete m_OuterMap ;
	return EL::StatusCode::SUCCESS;
}
EL::StatusCode StoreMuonDVObjects :: histFinalize () {return EL::StatusCode::SUCCESS;}


TLorentzVector StoreMuonDVObjects::seg_p4(xAOD::MuonSegment *seg)
{
	// takes in a segment and returns 4 vector
	TLorentzVector four_vector;
	four_vector.SetXYZM(seg->x(),seg->y(),seg->z(),m_muon);
	return four_vector;
}
float StoreMuonDVObjects::eta_corr_R(xAOD::Muon *mu, float detector_r)
{
	// convert muon z to detector z, minus sign for back-to-back
	const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
	float z_muon = (idtrack) ? idtrack->z0() : 0;
	float z_detector = -z_muon + detector_r/tan( mu->p4().Theta() );

	// compute muon theta at detector r
	float theta_detector = atan( fabs(detector_r / z_detector) );

	// and convert to muon eta at segment R
	float eta_detector =  eta(theta_detector);

	// account for arctans inability to handle inverses ideally
	if (z_detector < 0) eta_detector = -eta_detector;

	return eta_detector;
}
float StoreMuonDVObjects::eta_corr(xAOD::Muon *mu, xAOD::MuonSegment *seg)
{
	// takes a displaced muon and segment
	// and corrects the muon eta (measured at DOCA to primary vertex)
	// to the segment eta (measured at a particular detector R)
	// should only be applied to barrel muons

	// find detector r
	float detector_r = TMath::Hypot(seg->x(),seg->y());

	// convert muon z to detector z, minus sign for back-to-back
	const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
	float z_muon = (idtrack) ? idtrack->z0() : 0;
	float z_detector = -z_muon + detector_r/tan( mu->p4().Theta() );

	// compute muon theta at detector r
	float theta_detector = atan( fabs(detector_r / z_detector) );

	// and convert to muon eta at segment R
	float eta_detector =  eta(theta_detector);

	// account for arctans inability to handle inverses ideally
	if (z_detector < 0) eta_detector = -eta_detector;

	return eta_detector;
}
bool StoreMuonDVObjects::isMatchedToSegment(xAOD::Muon *mu,const xAOD::MuonSegmentContainer* segments )
{
	// checks if any segment is back to back in eta phi
	bool backToBack=false;

	for (auto *segment : *segments){

		// compute back-to-backness in eta phi
		TLorentzVector segment_p4 = seg_p4(segment);
		float sumEta_corr = fabs( eta_corr(mu,segment) + segment_p4.Eta() );
		float deltaPhi    = fabs(fabs(mu->p4().DeltaPhi(segment_p4)) - TMath::Pi() );

		// save decision
		if (deltaPhi < m_deltaPhi && sumEta_corr < m_sumEta ){
			backToBack=true;
		}
	}

	return backToBack;

}
bool StoreMuonDVObjects::passSegmentAcceptance(xAOD::Muon *mu,int run ){
	// for barrel muons, looks back-to-back in eta-phi
	// to determine if there is detector coverage
	// if there is not sufficient coverage returns false

	// Just to prevent compiler warning
	(void)run;

	// get N possible segments
	int n_poss_seg=0;

	if (fabs(mu->eta()) > 1.05) return true;
	else { // for barrel only

		// compute back-to-back phi
		float phi = TMath::Pi() + mu->phi();
		if (phi >= TMath::Pi() ) phi -= 2*TMath::Pi();
		if (phi < -TMath::Pi() ) phi += 2*TMath::Pi();

		// check each station to see if there is detector coverage
		if (m_InnerMap ->GetBinContent( m_InnerMap ->GetXaxis()->FindBin( -eta_corr_R(mu,4500)),m_InnerMap ->GetYaxis()->FindBin(phi) ) == 1) n_poss_seg+=1 ;
		if (m_MiddleMap->GetBinContent( m_MiddleMap->GetXaxis()->FindBin( -eta_corr_R(mu,7000)),m_MiddleMap->GetYaxis()->FindBin(phi) ) == 1) n_poss_seg+=1 ;
		if (m_OuterMap ->GetBinContent( m_OuterMap ->GetXaxis()->FindBin( -eta_corr_R(mu,9200)),m_OuterMap ->GetYaxis()->FindBin(phi) ) == 1) n_poss_seg+=1 ;
	}

	// if we can form two segments, return true
	if (n_poss_seg>1) return true;
	else return false;
}

float StoreMuonDVObjects::dilepMass( std::vector<xAOD::Muon*> muons )
{
	if ( muons.size() != 2 ) return -99;

	TLorentzVector mu1;
	TLorentzVector mu2;
	mu1.SetPtEtaPhiM(muons.at(0)->pt()/1000., muons.at(0)->eta(), muons.at(0)->phi(), m_muon);
	mu2.SetPtEtaPhiM(muons.at(1)->pt()/1000., muons.at(1)->eta(), muons.at(1)->phi(), m_muon);

	return (mu1+mu2).M() ;

}
float StoreMuonDVObjects::zMass( xAOD::IParticleContainer *muons )
{
	// returns true if event has exactly two muons with pT > 25 GeV and |eta|<2.5
	// with opposite sign charge and mass consistent with a Z boson

	std::vector< xAOD::Muon* > z_muons;
	for( xAOD::IParticle * muon : *muons){

		xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);
		if ( fabs(mu->eta()) < m_minEta && mu->pt()/1000. > m_minPt ) z_muons.push_back(mu);

	}

	if (z_muons.size() != 2) return -99; // exactly two muons

	if ( ( z_muons.at(0)->charge() + z_muons.at(1)->charge() ) != 0 ) return -98; // OS charge

	// now we have exactly two OS muons, return di-muon invariant mass
	return dilepMass(z_muons);

}
