#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <FactoryTools/SelectMediumD0Events.h>
#include <FactoryTools/HelperFunctions.h>
#include <FactoryTools/strongErrorCheck.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

#include <boost/algorithm/string.hpp>
                                      


// this is needed to distribute the algorithm to the workers
ClassImp(SelectMediumD0Events)

typedef FactoryTools::HelperFunctions HF;
typedef xAOD::IParticleContainer IPC;
typedef xAOD::ParticleAuxContainer IPC_aux;

SelectMediumD0Events :: SelectMediumD0Events (){}
EL::StatusCode SelectMediumD0Events :: setupJob (EL::Job& /*job*/){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: histInitialize (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: fileExecute (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: changeInput (bool /*firstFile*/){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: initialize (){return EL::StatusCode::SUCCESS;}


EL::StatusCode SelectMediumD0Events :: execute (){
  bool isJpsiSample = false;

  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();
  
  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);
  
  // If the event didn't pass the preselection alg, don't bother doing anything with it...
  if (HF::checkForSkip(eventInfo)) return EL::StatusCode::SUCCESS;


  //TODO fix this properly so we don't get warnings if container is not found
  const xAOD::VertexContainer *container_jpsi = nullptr;
  if(event->retrieve(container_jpsi, "BPHY5JpsiCandidates")){
    STRONG_CHECK(event->retrieve(container_jpsi, "BPHY5JpsiCandidates"));
    isJpsiSample=true;
  }

  eventInfo->auxdecor<bool>("isJpsiSample")= isJpsiSample; 

  // Creating empty containers and setting them up
  auto selectedMuons         = HF::createContainerInTStore<IPC,IPC_aux>("selectedMuons",store);
  auto selectedBaselineMuons = HF::createContainerInTStore<IPC,IPC_aux>("selectedBaselineMuons",store);

 // Grabbing containers from the TStore
  auto muons_nominal = HF::grabFromStore<xAOD::MuonContainer>("STCalibMuons",store);

    for (const auto& mu : *muons_nominal) {         
      if ((int)mu->auxdata<char>("baseline") == 0) continue; //if pass baseline cuts
      if ((int)mu->auxdata<char>("passOR") != 1) continue; //overlap removal     
      selectedBaselineMuons.first->push_back( mu );
      if ((int)mu->auxdata<char>("signal") == 0) continue;//TODO if want to use this need to fix calST
      selectedMuons.first->push_back( mu );
    }

  int const nBaselineMuons = selectedBaselineMuons.first->size();
  int const nMuons = selectedMuons.first->size();

  ATH_MSG_DEBUG("Number of Selected Baseline Muons: " << nBaselineMuons );
  ATH_MSG_DEBUG("Number of Selected Muons: " << nMuons  );


  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%//
  //%%%%% T R I G G E R S %%%%%//
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%// 
  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  /*
  //print out triggers
  for(long unsigned int i=0; i< passTrigs.size(); i++){
    ATH_MSG_DEBUG("triggers: " << passTrigs.at(i));
  }
  */
  std::map<std::string,int> passedTriggers;
  passedTriggers["HLT_2mu14"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_2mu14") != passTrigs.end();
  
  //TODO we probably don't care about MET trigger but may as well have it in for now
  if(eventInfo->auxdecor<float>("year")==2015){
    passedTriggers["MET"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_xe70") != passTrigs.end();
  } else {
    passedTriggers["MET"] = HF::trigORFromString(passTrigs,"HLT_xe100_mht_L1XE50_OR_HLT_xe110_mht_L1XE50");
  }

  // save to event info
  eventInfo->auxdecor<bool>("passHLT_2mu14") = passedTriggers["HLT_2mu14"];
  eventInfo->auxdecor<bool>("isMETTrigPassed") = passedTriggers["MET"];

  
  
  //TODO can change this if we need other regions later, so that we can write diff trees
  std::string regionName = "";

  if(isJpsiSample){
    //only select events that have 3 muons and a jpsi candidate
    if(nBaselineMuons>2 && !container_jpsi->empty() ){
      regionName = "MEDIUMD0";
    }
  }
    
  if(!isJpsiSample){
    if(nBaselineMuons>1){
      regionName = "MEDIUMD0";
    }
  }
    
  eventInfo->auxdecor< std::string >("regionName") = regionName;

  ATH_MSG_INFO("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   ); 
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SelectMediumD0Events :: postExecute (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: finalize (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: histFinalize (){return EL::StatusCode::SUCCESS;}
