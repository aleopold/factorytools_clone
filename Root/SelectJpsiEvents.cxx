#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <FactoryTools/SelectJpsiEvents.h>
#include <FactoryTools/HelperFunctions.h>
#include <FactoryTools/strongErrorCheck.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"

#include <boost/algorithm/string.hpp>
                                      


// this is needed to distribute the algorithm to the workers
ClassImp(SelectJpsiEvents)

  typedef FactoryTools::HelperFunctions HF;
typedef xAOD::IParticleContainer IPC;
typedef xAOD::ParticleAuxContainer IPC_aux;

SelectJpsiEvents :: SelectJpsiEvents (){}
EL::StatusCode SelectJpsiEvents :: setupJob (EL::Job& /*job*/){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectJpsiEvents :: histInitialize (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectJpsiEvents :: fileExecute (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectJpsiEvents :: changeInput (bool /*firstFile*/){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectJpsiEvents :: initialize (){return EL::StatusCode::SUCCESS;}


EL::StatusCode SelectJpsiEvents :: execute (){
  auto toGeV = [](double a){return a*.001;};

  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();
  
  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);
  
  // If the event didn't pass the preselection alg, don't bother doing anything with it...
  if (HF::checkForSkip(eventInfo)) return EL::StatusCode::SUCCESS;

  const xAOD::VertexContainer *container_jpsi = nullptr;
  STRONG_CHECK(event->retrieve(container_jpsi, "BPHY5JpsiCandidates"));
  
  // Creating empty containers and setting them up
  auto selectedMuons         = HF::createContainerInTStore<IPC,IPC_aux>("selectedMuons",store);
  auto selectedBaselineMuons = HF::createContainerInTStore<IPC,IPC_aux>("selectedBaselineMuons",store);

  // Grabbing containers from the TStore
  auto muons_nominal   = HF::grabFromStore<xAOD::MuonContainer>("STCalibMuons",store);
  
  for (const auto& mu : *muons_nominal) {         
    if ((int)mu->auxdata<char>("baseline") == 0) continue; //if pass baseline cuts
    if ((int)mu->auxdata<char>("passOR") == 0) continue; //overlap removal     
    selectedBaselineMuons.first->push_back( mu );
    if ((int)mu->auxdata<char>("signal") == 0) continue;
    selectedMuons.first->push_back( mu );
  }

  int const nBaselineMuons = selectedBaselineMuons.first->size();
  int const nMuons = selectedMuons.first->size();

  ATH_MSG_DEBUG("Number of Selected Baseline Muons: " << nBaselineMuons );
  ATH_MSG_DEBUG("Number of Selected Muons: " << nMuons  );

  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
  bool JpsiHasOtherMuon=false;
  for (const xAOD::Vertex* jpsi_cand : *container_jpsi){
    xAOD::BPhysHypoHelper jpsi_helper("Jpsi", jpsi_cand);
    
    if(jpsi_helper.nRefTrks()!= 2 || jpsi_helper.nMuons()!= 2){
      ANA_MSG_WARNING("Expected two muon tracks, skip");
      continue;     
    }
    
    size_t firstMuonIndex=-999;
    size_t secondMuonIndex=-999;
    
    firstMuonIndex=jpsi_helper.muon(0)->index();
    secondMuonIndex=jpsi_helper.muon(1)->index();

    bool firstMuonIsInReco=false;
    bool secondMuonIsInReco=false;
    
    //are the first and second muons in our reco muons?
    //perhaps use find..
    for (const auto& mu : *muons_nominal) {
      if((int)mu->auxdata<char>("baseline") == 1){
	if(mu->index()==firstMuonIndex){firstMuonIsInReco=true; }
	if(mu->index()==secondMuonIndex){secondMuonIsInReco=true;}	
      }
    }

    if(firstMuonIsInReco==false || secondMuonIsInReco==false){
      ANA_MSG_WARNING("first and/or second jpsi muons are not in our reco muons, firstMuonIsInReco: " << firstMuonIsInReco << ", secondMuonIsInReco: " << secondMuonIsInReco );

      jpsi_cand->auxdecor< bool >( "JpsiMuonsNotInReco") = false;
      continue;
    }

    jpsi_cand->auxdecor< bool >( "JpsiMuonsNotInReco") = true;

    //TODO can probably combine the loops so don't have to do two loops over the muons
    //loop over reco muons to find "other" muon
    for (const auto& mu : *muons_nominal) {
      if((int)mu->auxdata<char>("baseline") == 1){
	if(mu->index()!=firstMuonIndex && mu->index()!=secondMuonIndex && toGeV(mu->pt()) >= 20){
	  JpsiHasOtherMuon=true;
	  goto nextJpsiCand;
	}
      }
    }
  nextJpsiCand: ;
    jpsi_cand->auxdecor< bool >( "JpsiHasOtherMuon") = JpsiHasOtherMuon;
  }
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
  
  
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%//
  //%%%%% T R I G G E R S %%%%%//
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%// 
  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::map<std::string,int> passedTriggers;
  passedTriggers["HLT_2mu14"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_2mu14") != passTrigs.end();
  
  //TODO we probably don't care about MET trigger but may as well have it in for now
  if(eventInfo->auxdecor<float>("year")==2015){
    passedTriggers["MET"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_xe70") != passTrigs.end();
  } else {
    passedTriggers["MET"] = HF::trigORFromString(passTrigs,"HLT_xe100_mht_L1XE50_OR_HLT_xe110_mht_L1XE50");
  }

  // save to event info
  eventInfo->auxdecor<bool>("passHLT_2mu14") = passedTriggers["HLT_2mu14"];
  eventInfo->auxdecor<bool>("isMETTrigPassed") = passedTriggers["MET"];
  
  std::string regionName = "";

  //only select events that have 3 muons and a jpsi candidate with another muon
  if(nBaselineMuons>2 && JpsiHasOtherMuon ){
    regionName = "JPSI";
  }
  
  eventInfo->auxdecor< std::string >("regionName") = regionName;

  ATH_MSG_INFO("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   ); 
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SelectJpsiEvents :: postExecute (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectJpsiEvents :: finalize (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectJpsiEvents :: histFinalize (){return EL::StatusCode::SUCCESS;}
