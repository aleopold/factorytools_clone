#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>

#include <PATInterfaces/SystematicRegistry.h>

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventAuxInfo.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"

#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"

#include "TrackVertexAssociationTool/LooseTrackVertexAssociationTool.h"
#include "TrackVertexAssociationTool/TightTrackVertexAssociationTool.h"

#include "TauAnalysisTools/TauSmearingTool.h"
#include "TauAnalysisTools/ITauSmearingTool.h"

#include "FactoryTools/CalibrateST.h"
#include "FactoryTools/strongErrorCheck.h"

#include <FactoryTools/HelperFunctions.h>

#include <boost/algorithm/string/replace.hpp>

// this is needed to distribute the algorithm to the workers
ClassImp(CalibrateST)

typedef FactoryTools::HelperFunctions HF;


CalibrateST :: CalibrateST () :
systVar(),
m_objTool("ST::SUSYObjDef_xAOD/SUSYTools") {}

EL::StatusCode CalibrateST :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}


EL::StatusCode CalibrateST :: initialize ()
{
	// Uncomment this if you want the whole job to fail on an unchecked status code
	// StatusCode::enableFailure();

	xAOD::TEvent* event = wk()->xaodEvent();
	//xAOD::TStore* store = wk()->xaodStore();

	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	// check if the event is data or MC
	// (many tools are applied either to data or MC)
	bool const isData = !(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ));
	bool const isAtlfast = false;

	ST::ISUSYObjDef_xAODTool::DataSource datasource = (isData ? ST::ISUSYObjDef_xAODTool::Data : (isAtlfast ? ST::ISUSYObjDef_xAODTool::AtlfastII : ST::ISUSYObjDef_xAODTool::FullSim));

	if( doPRW == true ){
		ATH_MSG_INFO("Do PRW: true. Set to false in run script if not needed");


			if( PRWLumiCalcFileNames == notSetString()) {
				ATH_MSG_ERROR( "you need to set the lumicalc file path in your run script!");
				return EL::StatusCode::FAILURE;
			}

			if( PRWConfigFileNames == notSetString()) {
				ATH_MSG_ERROR( "you need to set the pileup reweighting file path in your run script!");
				return EL::StatusCode::FAILURE;
			}
	} else {
		ATH_MSG_INFO("Do PRW: FALSE. Set to TRUE in run script if needed.");
	}



	m_objTool.setTypeAndName("ST::SUSYObjDef_xAOD/SUSYObjDef_xAOD" + systVar.name() );
	//m_objTool.isUserConfigured(); // this doesn't do anything (marked const), do we want to assert a status? /CO
	STRONG_CHECK( m_objTool.setProperty("DataSource", datasource) );
	STRONG_CHECK( m_objTool.setProperty("OutputLevel", this->msg().level()) );


	if( SUSYToolsConfigFileName.empty() ) {
		ATH_MSG_DEBUG("No config file set. Letting SUSYTools use default config file.");
		//TODO: why is this code below done? SUSYTool has a default config value
		//		STRONG_CHECK( m_objTool.setProperty("ConfigFile", "SUSYTools/SUSYTools_Default.conf") );
	} else {
		ATH_MSG_DEBUG( "analysisName set. Trying to open config file "<< SUSYToolsConfigFileName );
		STRONG_CHECK( m_objTool.setProperty("ConfigFile", SUSYToolsConfigFileName ) );
	}



	if( doPRW && xAOD::EventInfo::IS_SIMULATION ){
        
        std::vector<std::string> PRWFiles;
        PRWFiles = HF::parseCSV(PRWConfigFileNames);
        std::vector<std::string> lumiCalcFiles = HF::parseCSV(PRWLumiCalcFileNames);
        

        STRONG_CHECK( m_objTool.setProperty( "PRWConfigFiles",   PRWFiles     ) );
        STRONG_CHECK( m_objTool.setProperty("PRWLumiCalcFiles", lumiCalcFiles   ) );


	}

	asg::AnaToolHandle<CP::MuonSelectionTool> m_muonSelectionTool;
	m_muonSelectionTool.setTypeAndName("CP::MuonSelectionTool/MuonSelectionTool_Medium");
	asg::AnaToolHandle<CP::MuonSelectionTool> m_muonSelectionToolBaseline;
	m_muonSelectionToolBaseline.setTypeAndName("CP::MuonSelectionTool/MuonSelectionTool_Baseline_Medium");

	if( useLLPMuons ){

		// creating own  muon selection tool for ST to pick up
		m_muonSelectionTool.isUserConfigured(); //need this to prevent ST from making its own
		STRONG_CHECK( m_muonSelectionTool.setProperty("MaxEta",2.7) );
		STRONG_CHECK( m_muonSelectionTool.setProperty("MuQuality",1) );
		STRONG_CHECK( m_muonSelectionTool.setProperty("PixCutOff",true) );
		STRONG_CHECK( m_muonSelectionTool.setProperty("TrtCutOff",true) );
		STRONG_CHECK( m_muonSelectionTool.setProperty("OutputLevel", this->msg().level()) );
		STRONG_CHECK( m_muonSelectionTool.retrieve() );

		m_muonSelectionToolBaseline.isUserConfigured();
		STRONG_CHECK( m_muonSelectionToolBaseline.setProperty("MaxEta",400) );
		STRONG_CHECK( m_muonSelectionToolBaseline.setProperty("MuQuality",1) );
		STRONG_CHECK( m_muonSelectionToolBaseline.setProperty("PixCutOff",true) );
		STRONG_CHECK( m_muonSelectionToolBaseline.setProperty("TrtCutOff",true) );
		STRONG_CHECK( m_muonSelectionToolBaseline.setProperty("OutputLevel", this->msg().level()) );
		STRONG_CHECK( m_muonSelectionToolBaseline.retrieve() );

		// const asg::AsgTool* toolPtr = dynamic_cast< const asg::AsgTool* >( m_muonSelectionToolBaseline.get() );
		// std::cout << "pix off? " << *toolPtr->getProperty< bool >("PixCutOff") << std::endl;

	}


	STRONG_CHECK( m_objTool.retrieve() );

	ATH_MSG_DEBUG( "Applying systematic variation:  "<< systVar.name() );
	STRONG_CHECK( m_objTool->applySystematicVariation(systVar) );//apply the systematic variation

	// Algorithm is handed a systname as a string
	// Need to find the corresponding SystInfo object for later "affects" functionality
	// ST::SystInfo m_systVarInfo;
	for (const auto& tmpSysInfo : m_objTool->getSystInfoList()) {
		if(tmpSysInfo.systset.name() == systVar.name() ){
			m_systVarInfo = tmpSysInfo;
			break;
		}
	}


	return EL::StatusCode::SUCCESS;
}

EL::StatusCode CalibrateST :: addToSystList(std::string listName, std::string systName){

	xAOD::TEvent * event = wk()->xaodEvent();
	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	std::vector<std::string> myvector;
	myvector = eventInfo->auxdecor< std::vector<std::string> >(listName);
	myvector.push_back(systName);
	eventInfo->auxdecor< std::vector<std::string> >(listName) = myvector;

	return EL::StatusCode::SUCCESS;

}


EL::StatusCode CalibrateST :: execute ()
{

	xAOD::TStore * store = wk()->xaodStore();
	xAOD::TEvent * event = wk()->xaodEvent();

	// //  store->print();
	store->clear();//We must clear the store when doing systematics so that we have a brand new setup.

	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	// Set Random Run number if you aren't applying pileup reweighting - needed for calibrations
	if (!doPRW) eventInfo->auxdecor<unsigned int>( "RandomRunNumber" ) = 300000;

	if(m_systVarInfo.affectsKinematics==true) {
		std::vector<std::string> emptyVec;
		eventInfo->auxdecor< std::vector<std::string> >("muSF_systs") = emptyVec;
		eventInfo->auxdecor< std::vector<std::string> >("elSF_systs") = emptyVec;
		eventInfo->auxdecor< std::vector<std::string> >("btagSF_systs") = emptyVec;
	}


	if( doPRW && eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) m_objTool->ApplyPRWTool();

	bool hasElectrons = event->contains<xAOD::ElectronContainer>( "Electrons" );
	bool hasPhotons = event->contains<xAOD::PhotonContainer>( "Photons" );
	bool hasMuons = event->contains<xAOD::MuonContainer>( "Muons" );
	//bool hasTaus = event->contains<xAOD::TauJetContainer>( "TauJets" );

	// Get the nominal object containers from the event
	// Electrons
	xAOD::ElectronContainer* electrons_nominal(nullptr);
	xAOD::ShallowAuxContainer* electrons_nominal_aux(nullptr);
	if(hasElectrons) {
		STRONG_CHECK( m_objTool->GetElectrons(electrons_nominal, electrons_nominal_aux, true) );
	}

	// Photons
	xAOD::PhotonContainer* photons_nominal(nullptr);
	xAOD::ShallowAuxContainer* photons_nominal_aux(nullptr);
	if(hasPhotons) {
		STRONG_CHECK( m_objTool->GetPhotons(photons_nominal,photons_nominal_aux, true) );

		// keep leading photon only
		int nBasePhotons = 0;
		for (auto ph: *photons_nominal){
			if ((int)ph->auxdata<char>("baseline") == 1) {
				if (nBasePhotons > 0) {
					ph->auxdata<char>("baseline") = 0;
					ph->auxdata<char>("signal") = 0;
					ph->auxdata<char>("isol") = 0;
				}else{
					nBasePhotons++;
				}
			}
		}
	}

	// Muons
	xAOD::MuonContainer* muons_nominal(nullptr);
	xAOD::ShallowAuxContainer* muons_nominal_aux(nullptr);
	if(hasMuons) {
		STRONG_CHECK( m_objTool->GetMuons(muons_nominal, muons_nominal_aux, true) );
	}

	// Jets
	xAOD::JetContainer* jets_nominal(nullptr);
	xAOD::ShallowAuxContainer* jets_nominal_aux(nullptr);
	STRONG_CHECK( m_objTool->GetJets(jets_nominal, jets_nominal_aux, true) );

	// Fat Jets
	if (event->contains<xAOD::JetContainer>( fatJetContainerName) == 0) ATH_MSG_DEBUG( "TEvent doesn't seem to contain fat jets:  "<< fatJetContainerName );

	if(fatJetContainerName != notSetString() && event->contains<xAOD::JetContainer>( fatJetContainerName ) ){
		xAOD::JetContainer* fatjets_nominal(nullptr);
		xAOD::ShallowAuxContainer* fatjets_nominal_aux(nullptr);
		STRONG_CHECK( m_objTool->GetFatJets(fatjets_nominal, fatjets_nominal_aux, true, fatJetContainerName ) );
	}

	// Taus
	//xAOD::TauJetContainer* taus_nominal(nullptr);
	//xAOD::ShallowAuxContainer* taus_nominal_aux(nullptr);
	//if(hasTaus) {
	//  STRONG_CHECK( m_objTool->GetTaus(taus_nominal,taus_nominal_aux, true) );
	//}

	auto newMetContainer    = new xAOD::MissingETContainer();
	auto newMetAuxContainer = new xAOD::MissingETAuxContainer();
	newMetContainer->setStore(newMetAuxContainer);

	//todo this needs to be moved and calculated after selections
	if (hasElectrons && hasMuons && hasPhotons) STRONG_CHECK( m_objTool->GetMET(*newMetContainer,
						jets_nominal,
						electrons_nominal,
						muons_nominal,
						photons_nominal,
						nullptr, //taus_nominal,
						true,    //tst
						true,    //dojvt  cut
						nullptr  //no invisible particles in met
						));


	STRONG_CHECK( store->record( newMetContainer    , "STCalibMET"    ) );//todo configurable if needed
	STRONG_CHECK( store->record( newMetAuxContainer , "STCalibMETAux.") );//todo configurable if needed
	//STRONG_CHECK( store->record( newMetMap , "STCalibMETMap") );//todo configurable if needed


	if (hasElectrons && hasMuons) STRONG_CHECK(  m_objTool->OverlapRemoval(electrons_nominal, muons_nominal, jets_nominal, photons_nominal) );


	/////////////////////////////////////////////////////////////////
	// Muons! - Figuring out year, getting SF
	//

	float muSF = 1.0;
	bool passTM = false;
	
	//TODO this can be done in a cleaner way, by passing triggers in running script
	//if running mediumd0 analysis use their triggers
	if(runningMediumd0Analy){
	  if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) && hasMuons){
	    muSF = (float) m_objTool->GetTotalMuonSF(*muons_nominal,true,true, "HLT_2mu14");
	  }

	  for (auto mu: *muons_nominal){
	    passTM=false;
	    passTM |= m_objTool->IsTrigMatched(mu, "HLT_2mu14");
	    (mu)->auxdecor< int >( "PassTM" ) = passTM;
	  }	  
	}

	//otherwise use dv triggers
	//%%%%%%%%%%%%%%%%%%%%
	else{
	  if(muTrig2015=="" ) muTrig2015 = "HLT_mu20_iloose_L1MU15_OR_HLT_mu50";
	  if(muTrig2016=="" ) muTrig2016 = "HLT_mu26_ivarmedium";
	  
	  if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) && hasMuons){
	    
	    muSF = (float) m_objTool->GetTotalMuonSF(*muons_nominal,true,true, m_objTool->treatAsYear()==2015 ? muTrig2015: muTrig2016);
	    //loop over muons and attach trigger matching
	    
	  }

	  bool passTMmsonly = false;
	  for (auto mu: *muons_nominal){
	    passTM=false;
	    passTMmsonly = false;
	    passTM |= m_objTool->IsTrigMatched(mu, m_objTool->treatAsYear()==2015 ?
					       boost::replace_all_copy(muTrig2015, "_OR_", "") :
					       boost::replace_all_copy(muTrig2016, "_OR_", "") );
	    passTMmsonly |= m_objTool->IsTrigMatched(mu, "HLT_mu60_0eta105_msonly" );
	    (mu)->auxdecor< int >( "passTM" ) = passTM;
	    (mu)->auxdecor< int >( "passTMmsonly" ) = passTMmsonly;
	  }
	}
	//%%%%%%%%%%%%%%%%%%%%

	
	if(systVar.name()=="")	eventInfo->auxdecor<float>("muSF") = muSF ;
	else if(ST::testAffectsObject(xAOD::Type::Muon, m_systVarInfo.affectsType) ){
		eventInfo->auxdecor<float>("muSF_"+systVar.name()) = muSF ;
		addToSystList("muSF_systs",systVar.name());
	}



	//
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
	// Electrons! - Figuring out year, getting SF
	//

	if(elTrig2015=="") elTrig2015 = "HLT_e24_lhmedium_L1EM18VH";
	if(elTrig2016=="") elTrig2016 = "HLT_e26_lhtight_nod0_ivarloose";

	float elSF = 1.0;
	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) && hasElectrons){
		elSF = (float) m_objTool->GetTotalElectronSF(*electrons_nominal);

	}

	passTM = false;
	for (auto el: *electrons_nominal){
		passTM=false;
		passTM |= m_objTool->IsTrigMatched(el, m_objTool->treatAsYear()==2015 ?
			boost::replace_all_copy(elTrig2015, "_OR_", "") :
			boost::replace_all_copy(elTrig2016, "_OR_", "") );
		(el)->auxdecor< int >( "passTM" ) = passTM;
	}

	if(systVar.name()=="")	eventInfo->auxdecor<float>("elSF") = elSF ;
	else if(ST::testAffectsObject(xAOD::Type::Electron, m_systVarInfo.affectsType) ){
		eventInfo->auxdecor<float>("elSF_"+systVar.name()) = elSF ;
		addToSystList("elSF_systs",systVar.name());
	}

	//
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
	// B-Tagging! - Getting SF
	//

	float btagSF = 1.0;
	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
		btagSF = (float) m_objTool->BtagSF(jets_nominal);
	}
	if(systVar.name()=="")	eventInfo->auxdecor<float>("btagSF") = btagSF ;
	else if(ST::testAffectsObject(xAOD::Type::BTag, m_systVarInfo.affectsType) ){
		eventInfo->auxdecor<float>("btagSF_"+systVar.name()) = btagSF ;
		addToSystList("btagSF_systs",systVar.name());
	}

	//
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
	// Photons! -
	//

	if(phTrig=="") phTrig = "HLT_g140_loose";

	float phSF = 1.0;
	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) && hasPhotons){
		phSF = (float) m_objTool->GetTotalPhotonSF(*photons_nominal);

	}
	passTM = false;
	for (auto ph: *photons_nominal){
		passTM=false;
		passTM |= m_objTool->IsTrigMatched(ph, boost::replace_all_copy(phTrig, "_OR_", ""));
		(ph)->auxdecor< int >( "passTM" ) = passTM;
	}
	if(systVar.name()=="")	eventInfo->auxdecor<float>("phSF") = phSF ;
	else if(ST::testAffectsObject(xAOD::Type::Photon, m_systVarInfo.affectsType) ){
		eventInfo->auxdecor<float>("phSF_"+systVar.name()) = phSF ;
		addToSystList("phSF_systs",systVar.name());
	}

	//
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
	// Storing triggers and year to eventInfo
	//

	if(doPRW){
		eventInfo->auxdecor<float>("year") =  m_objTool->treatAsYear();
		eventInfo->auxdecor<bool>("isMETTrigPassed") = m_objTool->IsMETTrigPassed();
	} else {
		// manually handing a run number so the random run number function is not called (requires the PRW tool)
		eventInfo->auxdecor<float>("year") =  m_objTool->treatAsYear(1e6);
		eventInfo->auxdecor<bool>("isMETTrigPassed") = m_objTool->IsMETTrigPassed(1e6);
	}

	eventInfo->auxdecor<std::string>("muTrig2015") =  muTrig2015;
	eventInfo->auxdecor<std::string>("muTrig2016") =  muTrig2016;
	eventInfo->auxdecor<std::string>("elTrig2015") =  elTrig2015;
	eventInfo->auxdecor<std::string>("elTrig2016") =  elTrig2016;
	eventInfo->auxdecor<std::string>("phTrig") = phTrig;

	//
	/////////////////////////////////////////////////////////////////


	/////////////////////////////////////////////////////////////////
	// Calculate Sherpa v2.2 Njet reweight
	//

	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION)  ){
		int mcChannelNumber = eventInfo->mcChannelNumber();
		float WZweight = 1.;

		if ( (mcChannelNumber>=363102 && mcChannelNumber<=363122) ||
			 (mcChannelNumber>=363311 && mcChannelNumber<=363354) ||
			 (mcChannelNumber>=363361 && mcChannelNumber<=363483)
			 ){
			WZweight = m_objTool->getSherpaVjetsNjetsWeight("AntiKt4TruthJets");
		}

		eventInfo->auxdecor<float>("WZweight") = WZweight;
	}

	//
	/////////////////////////////////////////////////////////////////
	// Saving if is MC
	eventInfo->auxdecor<float>("isMC") = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION) ) ? 1 : 0 ;

	return EL::StatusCode::SUCCESS;
}



EL::StatusCode CalibrateST :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: histFinalize () {return EL::StatusCode::SUCCESS;}
