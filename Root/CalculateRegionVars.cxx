// Infrastructure include(s):
//
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/RegionVarCalculator.h>
#include <FactoryTools/CalculateRegionVars.h>
#include <FactoryTools/TreeManager.h>

#include <FactoryTools/printDebug.h>
#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"

#include <FactoryTools/HelperFunctions.h>

// this is needed to distribute the algorithm to the workers
ClassImp(CalculateRegionVars)

typedef FactoryTools::HelperFunctions HF;

CalculateRegionVars :: CalculateRegionVars(){}

EL::StatusCode CalculateRegionVars :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalculateRegionVars :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalculateRegionVars :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalculateRegionVars :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}


EL::StatusCode CalculateRegionVars :: initialize ()
{
  if (!m_calculator) ATH_MSG_ERROR( "No calculator is defined. Please hand me a RegionVarCalculator object!");

  STRONG_CHECK_SC( m_calculator->initialize(wk()));

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode CalculateRegionVars :: execute ()
{

  printDebug();

  xAOD::TEvent* event = wk()->xaodEvent();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

  eventInfo->auxdecor< bool >("writeDebugVars") = writeDebugVars;
  eventInfo->auxdecor< std::vector<std::string> >("listOfDetailedObjects") = listOfDetailedObjects;
  eventInfo->auxdecor< std::vector<std::string> >("listOfTriggers") = triggerBranches;

  auto& vars = TreeManager::collection( "RegionVars" );

  printDebug();

  STRONG_CHECK_SC( m_calculator->doGeneralCalculations( vars ) );
  STRONG_CHECK_SC( m_calculator->storeTriggerDecisions( vars, triggerBranches ) );
  STRONG_CHECK_SC( m_calculator->calculate( vars ) );

  printDebug();
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode CalculateRegionVars :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalculateRegionVars :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalculateRegionVars :: histFinalize () {return EL::StatusCode::SUCCESS;}

