#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/SelectRPVMultijetEvents.h>

#include <FactoryTools/HelperFunctions.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"

#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

// This is needed to distribute the algorithm to the workers
ClassImp(SelectRPVMultijetEvents)

typedef FactoryTools::HelperFunctions HF;

// Initialize the class template from the header file
SelectRPVMultijetEvents :: SelectRPVMultijetEvents () :
  m_event(nullptr),
  m_store(nullptr),
  m_baselineSelection(0)
{}

// Setup the public methods that the class template inhereted from the EventLoop Algorithm class
// Return a status code to let the user know if the job succeeded
EL::StatusCode SelectRPVMultijetEvents :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectRPVMultijetEvents :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectRPVMultijetEvents :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectRPVMultijetEvents :: initialize () {return EL::StatusCode::SUCCESS;}

EL::StatusCode SelectRPVMultijetEvents :: changeInput (bool /*firstFile*/) {
  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore(); 
  return EL::StatusCode::SUCCESS;
}

bool SelectRPVMultijetEvents :: keepEvent () {
  
  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",m_event);

  xAOD::IParticleContainer* jets(nullptr);
  STRONG_CHECK(m_store->retrieve(jets, "selectedJets"));

  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::map<std::string,int> passedTriggers;
  passedTriggers["HLT_ht1000_L1J100"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_ht1000_L1J100") != passTrigs.end();

  int nj50 = 0;
  for( const auto& jet : *jets){
    if(jet->pt() > 50000) { nj50++;  } 
  } 

  bool pass(false);
  switch(m_baselineSelection){
  case selection::none:
    pass = true;
    break;
  case selection::trig:
    if (passedTriggers["HLT_ht1000_L1J100"]>0){
      pass = true;
    }
    break;
  case selection::trig_pt5:
    if (passedTriggers["HLT_ht1000_L1J100"]>0 && nj50>=6){
      pass = true;
    }
    break;
  default:
    ATH_MSG_ERROR("You should not be here, check the value of m_baselineSelection");
    break;
  }
  
  return pass;
}


// Primary method to execute the event selection
EL::StatusCode SelectRPVMultijetEvents :: execute ()
{

  //////////////////////////////////////////////////

  // pick up the event info 
  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",m_event);

  // handy typedefs 
  typedef xAOD::IParticleContainer IPC;
  typedef xAOD::ParticleAuxContainer IPC_aux;

  // check if the event tells us to skip the event
  if (HF::checkForSkip(eventInfo)) return EL::StatusCode::SUCCESS;


  //////////////////////////////////////////////////
  // Creating empty containers and setting them up
  auto selectedJets              = HF::createContainerInTStore<IPC,IPC_aux>("selectedJets",m_store);

  //////////////////////////////////////////////////
  // Grabbing containers from the TStore
  auto jets_nominal      = HF::grabFromStore<xAOD::JetContainer>("STCalibAntiKt4EMPFlowJets",m_store); //EMTopoJets",store);

  // Loop over the jets inside of the current event and only keep baseline jets
  for (const auto& jet : *jets_nominal) {
    if ((int)jet->auxdata<char>("baseline") == 0) continue;
    ATH_MSG_VERBOSE( "jet pt : " << jet->pt() );
    selectedJets.first->push_back(jet  );
  }
  
  int const nBaselineJets = selectedJets.first->size();

  ATH_MSG_DEBUG("Number of Selected Baseline Jets: " << nBaselineJets );
  
  //////////////////////////////////////////////////
  // Apply event selection
  // only take events with greater than 6 jets
  // The event selection is applied by setting regionName to different strings
  // Downstream algorithm picks up the auxdecor regionName and decides what to do with the event

  std::string regionName = "";  
  if( keepEvent() ){
    ATH_MSG_DEBUG("PASSED EVENT SELECTION");
    regionName = "SRRPV";      
  }
  else { ATH_MSG_DEBUG("DID NOT PASS EVENT SELECTION"); }

  eventInfo->auxdecor< std::string >("regionName") = regionName;
  ATH_MSG_DEBUG("Writing to eventInfo decoration: " << eventInfo->auxdecor< std::string >("regionName") );

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode SelectRPVMultijetEvents :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectRPVMultijetEvents :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectRPVMultijetEvents :: histFinalize () {return EL::StatusCode::SUCCESS;}
