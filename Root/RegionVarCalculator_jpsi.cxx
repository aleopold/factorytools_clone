#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "AsgTools/MessageCheck.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"

//%%%%%%%%
//includes needed for isolation 
#include "xAODPrimitives/IsolationType.h"
#include "IsolationSelection/IsolationWP.h"
#include "IsolationSelection/IsolationSelectionTool.h"
//%%%%%%%%

#include <FactoryTools/HelperFunctions.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include "FactoryTools/RegionVarCalculator_jpsi.h"
#include "FactoryTools/strongErrorCheck.h"

#include <fstream>

// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_jpsi)

using namespace asg::msgUserCode;

typedef FactoryTools::HelperFunctions HF;
typedef ElementLink<xAOD::VertexContainer> VertexLink;
typedef std::vector<VertexLink> VertexLinkVector;

RegionVarCalculator_jpsi::RegionVarCalculator_jpsi()
  : m_iso("CP::IsolationSelectionTool")
{  
}

//*************************
// D O  I N I T I A L I Z E
//*************************
EL::StatusCode RegionVarCalculator_jpsi::doInitialize(EL::IWorker * worker) {
  
  if(m_worker != nullptr){
    std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
    return EL::StatusCode::FAILURE;
  }
  m_worker = worker;

  //Instantiate class to resolve OR link
  m_overlapLinkHelper = std::make_unique<ORUtils::OverlapLinkHelper>("overlapLinkHelper");

  //Need isolation tool so we can set bool in ntuple to see if muons passed isolation WPs
  ANA_CHECK( m_iso.setProperty("MuonWP","FCLoose") );
  ANA_CHECK( m_iso.initialize() );
    
  return EL::StatusCode::SUCCESS;
}


//**************************
// D O   C A L C U L A T E
//**************************
EL::StatusCode RegionVarCalculator_jpsi::doCalculate(std::map<std::string, anytype>& vars) {

  //Setup
  xAOD::TEvent* event = m_worker->xaodEvent();
  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

  if(regionName.empty()){
    // If it hasn't been selected in any of the regions from any of the select algs,
    //don't bother calculating anything...
    ANA_MSG_DEBUG("No region name set, no calculations performed.");
    return EL::StatusCode::SUCCESS;
  }
  else if(regionName == "JPSI"){
    //other SR definitions can have other trees written out with other functions here
    ANA_MSG_DEBUG("Selection region set: JPSI");
    return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); 
  }
  return EL::StatusCode::SUCCESS;
}


//**************************
// D O   A L L   C A L C  
//**************************
EL::StatusCode RegionVarCalculator_jpsi::doAllCalculations(std::map<std::string, anytype>& vars ) {
  xAOD::TStore * store = m_worker->xaodStore();
  xAOD::TEvent * event = m_worker->xaodEvent();

  auto toGeV = [](double a){return a*.001;};
    
  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  doGeneralCalculations(vars);
  
  xAOD::IParticleContainer* muons_baseline(nullptr);
  STRONG_CHECK(store->retrieve(muons_baseline, "selectedBaselineMuons"));

  std::vector<xAOD::IParticle*> muons_baseline_sorted = sortByPt(muons_baseline);
  
  //%%%%%%%%%%%%%%%%%%%%
  //  J P S I
  //%%%%%%%%%%%%%%%%%%%%

  const xAOD::VertexContainer *container_jpsi = nullptr;
  STRONG_CHECK(event->retrieve(container_jpsi, "BPHY5JpsiCandidates"));

  const xAOD::VertexContainer *container_bplus = nullptr;
  STRONG_CHECK(event->retrieve(container_bplus, "BPHY5BpmJpsiKpmCandidates"));
   
  std::vector<std::pair<unsigned int, const xAOD::Muon*> > jpsiIndexOtherMuon;
  std::vector<const xAOD::Vertex *> good_jpsis;

  
  // %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%%
  //look at the jpsi candidates in the event and get the muons from their decay
  //then see if there is another muon on the "other side"

  for (const xAOD::Vertex* jpsi_cand : *container_jpsi){
    xAOD::BPhysHypoHelper jpsi_helper("Jpsi", jpsi_cand);

    //bool firstMuonIsInReco=false;
    //bool secondMuonIsInReco=false;   
    int NumOtherMuon=0;
    const xAOD::Muon* firstMuon;
    const xAOD::Muon* secondMuon;
    unsigned int firstMuonIndex=999;
    unsigned int secondMuonIndex=999;
      
    if(jpsi_helper.nRefTrks()!= 2 || jpsi_helper.nMuons()!= 2){
      ANA_MSG_WARNING("Expected two muon tracks, skip");
      continue;     
    }

    TLorentzVector muon0_ref_track = jpsi_helper.refTrk(0, 105.65837);
    TLorentzVector muon1_ref_track = jpsi_helper.refTrk(1, 105.65837);
    TLorentzVector jpsi_ref_track = muon0_ref_track + muon1_ref_track;
    
    firstMuonIndex=jpsi_helper.muon(0)->index();
    secondMuonIndex=jpsi_helper.muon(1)->index();

    firstMuon=jpsi_helper.muon(0);
    secondMuon=jpsi_helper.muon(1);                 

    if(jpsi_cand->auxdata<bool>("JpsiMuonsNotInReco")==false || jpsi_cand->auxdata<bool>("JpsiHasOtherMuon")==false)continue;
         
    //loop over reco muons to find the "other" muon
    for(xAOD::IParticle * mu : muons_baseline_sorted){
      xAOD::Muon* muon = dynamic_cast<xAOD::Muon*>(mu);

      //link the ID track to the muon
      const xAOD::TrackParticle* muIdTrack = muon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
      int passFCLooseIsolation = m_iso->accept(*muon);
      float d0err = -999;
      float z0err = -999;
      
      if(muIdTrack){	
	//get the d0 and z0 errors
	auto &CovariantMatrix = muIdTrack->definingParametersCovMatrix();
	d0err=std::sqrt(CovariantMatrix(0,0));
	z0err=std::sqrt(CovariantMatrix(1,1));
      }

      if(muon->index()!=firstMuonIndex && muon->index()!=secondMuonIndex && toGeV(muon->pt()) >= 20){
	NumOtherMuon++;
	jpsiIndexOtherMuon.push_back(std::make_pair(jpsi_cand->index(), muon ) ) ;

	addToVectorBranch( vars, "otherMuonPt",      toGeV(muon->pt()));
	addToVectorBranch( vars, "otherMuonE",       toGeV(muon->e()));
	addToVectorBranch( vars, "otherMuonEta",     muon->eta());
	addToVectorBranch( vars, "otherMuonPhi",     muon->phi());
	addToVectorBranch( vars, "otherMuonCharge",  muon->charge());     
	addToVectorBranch( vars, "otherMuonIndex",   muon->index());
	addToVectorBranch( vars, "otherMuonD0sig",   (float)muon->auxdata<float>("d0sig"));
	addToVectorBranch( vars, "otherMuonPassOR" , muon->auxdata<char>("passOR") ? 1 : 0); //TODO selected muons should pass or anyway
	addToVectorBranch( vars, "otherMuonPassFCLooseIso", passFCLooseIsolation);
	addToVectorBranch( vars, "otherMuonTrigMatched",    (muon)->auxdecor< int >( "PassTM"));
	addToVectorBranch( vars, "otherMuonPtvarcone30_TightTTVA_pt1000", toGeV( muon->isolation(xAOD::Iso::ptvarcone30_TightTTVA_pt1000) ));
	addToVectorBranch( vars, "otherMuonTopoetcone20", toGeV( muon->isolation(xAOD::Iso::topoetcone20) ));    
	addToVectorBranch( vars, "otherMuonIdTrackPt",    (muIdTrack) ?  toGeV(muIdTrack->pt())  : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackEta",   (muIdTrack) ?  muIdTrack->eta()  : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackPhi",   (muIdTrack) ?  muIdTrack->phi()  : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackD0",    (muIdTrack) ?  muIdTrack->d0()  : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackZ0",    (muIdTrack) ?  muIdTrack->z0()  : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackD0err", (muIdTrack) ? d0err : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackZ0err", (muIdTrack) ? z0err : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackChi2",  (muIdTrack && muIdTrack->isAvailable<float>("chiSquared") ) ?  muIdTrack->auxdata< float >("chiSquared")  : -999 );
	addToVectorBranch( vars, "otherMuonIdTrackDOF",   (muIdTrack && muIdTrack->isAvailable<float>("numberDoF") ) ? muIdTrack->auxdata< float >("numberDoF")  : -999 );
	  
	TLorentzVector muonTL;
	muonTL.SetPtEtaPhiE(toGeV(muon->pt()), muon->p4().Eta(), muon->p4().Phi(), toGeV(muon->e()));
	
	float deltaR=muonTL.DeltaR(jpsi_ref_track);
	float deltaPhi=muonTL.DeltaPhi(jpsi_ref_track);
	
	addToVectorBranch(vars,"otherMuonJpsiDeltaR",   deltaR);	
	addToVectorBranch(vars,"otherMuonJpsiDeltaPhi", deltaPhi);
      }	
    }//end loop over reco muons

    if(NumOtherMuon==0){continue;}
    
    //link the ID track to the first muon
    const xAOD::TrackParticle* mu1IdTrack = firstMuon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
    int passFCLooseIsolation_mu1 = m_iso->accept(*firstMuon);
    float d0err_mu1 = -999;
    float z0err_mu1 = -999;

    const xAOD::TrackParticle* mu2IdTrack = secondMuon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
    int passFCLooseIsolation_mu2 = m_iso->accept(*secondMuon);
    float d0err_mu2 = -999;
    float z0err_mu2 = -999;
      
    if(mu1IdTrack){	
      //get the d0 and z0 errors
      auto &CovariantMatrix_mu1 =  mu1IdTrack->definingParametersCovMatrix();
      d0err_mu1=std::sqrt(CovariantMatrix_mu1(0,0));
      z0err_mu1=std::sqrt(CovariantMatrix_mu1(1,1));
    }
    if(mu2IdTrack){	
      auto &CovariantMatrix_mu2 =  mu2IdTrack->definingParametersCovMatrix();
      d0err_mu2=std::sqrt(CovariantMatrix_mu2(0,0));
      z0err_mu2=std::sqrt(CovariantMatrix_mu2(1,1));
    }

    addToVectorBranch( vars, "jpsiFirstMuonPt",      toGeV(firstMuon->pt()));
    addToVectorBranch( vars, "jpsiFirstMuonE",       toGeV(firstMuon->e()));
    addToVectorBranch( vars, "jpsiFirstMuonEta",     firstMuon->eta());
    addToVectorBranch( vars, "jpsiFirstMuonPhi",     firstMuon->phi());
    addToVectorBranch( vars, "jpsiFirstMuonCharge",  firstMuon->charge());     
    addToVectorBranch( vars, "jpsiFirstMuonIndex",   firstMuon->index());
    addToVectorBranch( vars, "jpsiFirstMuonPassFCLooseIso", passFCLooseIsolation_mu1);
    addToVectorBranch( vars, "jpsiFirstMuonPtvarcone30_TightTTVA_pt1000", toGeV( firstMuon->isolation(xAOD::Iso::ptvarcone30_TightTTVA_pt1000) ));
    addToVectorBranch( vars, "jpsiFirstMuonTopoetcone20", toGeV( firstMuon->isolation(xAOD::Iso::topoetcone20) ));    
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackPt",    (mu1IdTrack) ?  toGeV(mu1IdTrack->pt())  : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackEta",   (mu1IdTrack) ?  mu1IdTrack->eta()  : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackPhi",   (mu1IdTrack) ?  mu1IdTrack->phi()  : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackD0",    (mu1IdTrack) ?  mu1IdTrack->d0()  : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackZ0",    (mu1IdTrack) ?  mu1IdTrack->z0()  : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackD0err", (mu1IdTrack) ? d0err_mu1 : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackZ0err", (mu1IdTrack) ? z0err_mu1 : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackChi2",  (mu1IdTrack && mu1IdTrack->isAvailable<float>("chiSquared") ) ?  mu1IdTrack->auxdata< float >("chiSquared")  : -999 );
    addToVectorBranch( vars, "jpsiFirstMuonIdTrackDOF",   (mu1IdTrack && mu1IdTrack->isAvailable<float>("numberDoF") ) ? mu1IdTrack->auxdata< float >("numberDoF")  : -999 );
	  
    addToVectorBranch( vars, "jpsiSecondMuonPt",      toGeV(secondMuon->pt()));
    addToVectorBranch( vars, "jpsiSecondMuonE",       toGeV(secondMuon->e()));
    addToVectorBranch( vars, "jpsiSecondMuonEta",     secondMuon->eta());
    addToVectorBranch( vars, "jpsiSecondMuonPhi",     secondMuon->phi());
    addToVectorBranch( vars, "jpsiSecondMuonCharge",  secondMuon->charge());     
    addToVectorBranch( vars, "jpsiSecondMuonIndex",   secondMuon->index());
    addToVectorBranch( vars, "jpsiSecondMuonPassFCLooseIso", passFCLooseIsolation_mu2);
    addToVectorBranch( vars, "jpsiSecondMuonPtvarcone30_TightTTVA_pt1000", toGeV( secondMuon->isolation(xAOD::Iso::ptvarcone30_TightTTVA_pt1000) ));
    addToVectorBranch( vars, "jpsiSecondMuonTopoetcone20", toGeV( secondMuon->isolation(xAOD::Iso::topoetcone20) ));    
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackPt",    (mu2IdTrack) ?  toGeV(mu2IdTrack->pt())  : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackEta",   (mu2IdTrack) ?  mu2IdTrack->eta()  : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackPhi",   (mu2IdTrack) ?  mu2IdTrack->phi()  : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackD0",    (mu2IdTrack) ?  mu2IdTrack->d0()  : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackZ0",    (mu2IdTrack) ?  mu2IdTrack->z0()  : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackD0err", (mu2IdTrack) ? d0err_mu2 : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackZ0err", (mu2IdTrack) ? z0err_mu2 : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackChi2",  (mu2IdTrack && mu2IdTrack->isAvailable<float>("chiSquared") ) ?  mu2IdTrack->auxdata< float >("chiSquared")  : -999 );
    addToVectorBranch( vars, "jpsiSecondMuonIdTrackDOF",   (mu2IdTrack && mu2IdTrack->isAvailable<float>("numberDoF") ) ? mu2IdTrack->auxdata< float >("numberDoF")  : -999 );
	          
    //only keep jpsi candidates that have a muon on the otherside
    good_jpsis.push_back(jpsi_cand);
      
    float jpsi_mass = jpsi_cand->auxdata<float>("Jpsi_mass");
	
    addToVectorBranch(vars,"jpsiMass",   jpsi_mass);     
    addToVectorBranch(vars,"jpsiPt",     jpsi_ref_track.Pt() );
    addToVectorBranch(vars,"jpsiEta",    jpsi_ref_track.Eta() );
    addToVectorBranch(vars,"jpsiPhi",    jpsi_ref_track.Phi() );
    addToVectorBranch(vars,"jpsiChi2",   jpsi_helper.vtx()->chiSquared());
    addToVectorBranch(vars,"jpsiIndex",  jpsi_cand->index());
    addToVectorBranch(vars,"jpsiLxy",    jpsi_helper.lxy     (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    addToVectorBranch(vars,"jpsiLxyErr", jpsi_helper.lxyErr  (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
    addToVectorBranch(vars,"jpsiA0",     jpsi_helper.a0      (xAOD::BPhysHelper::PV_MAX_SUM_PT2));      
    addToVectorBranch(vars,"jpsiA0Err",  jpsi_helper.a0Err   (xAOD::BPhysHelper::PV_MAX_SUM_PT2));    
    addToVectorBranch(vars,"jpsiA0xy",   jpsi_helper.a0xy    (xAOD::BPhysHelper::PV_MAX_SUM_PT2));    
    addToVectorBranch(vars,"jpsiA0xyErr",jpsi_helper.a0xyErr (xAOD::BPhysHelper::PV_MAX_SUM_PT2));        
    addToVectorBranch(vars,"jpsiZ0",     jpsi_helper.z0      (xAOD::BPhysHelper::PV_MAX_SUM_PT2));   
    addToVectorBranch(vars,"jpsiZ0Err",  jpsi_helper.z0Err   (xAOD::BPhysHelper::PV_MAX_SUM_PT2));   
    addToVectorBranch(vars,"jpsiConstM_tau",   jpsi_helper.tau(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    addToVectorBranch(vars,"jpsiConstM_tauErr",jpsi_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
    addToVectorBranch(vars,"jpsiInvM_tau",     jpsi_helper.tau(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));
    addToVectorBranch(vars,"jpsiInvM_tauErr",  jpsi_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));    
      
    addToVectorBranch(vars,"numOtherMuonPerJpsi",     NumOtherMuon);

  }//end of loop of jpsi candidates
    
  // %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%%
  //now look to see if we have any nice bplus cadidates 
  int numBplus=0;
  for (const xAOD::Vertex* bplus_cand : (*container_bplus)){
    xAOD::BPhysHypoHelper bplus_helper("Bplus", bplus_cand);
    if (bplus_helper.nRefTrks() != 3){
      ANA_MSG_WARNING("Expected three (not %d) tracks for B candidate, skip" << bplus_helper.nRefTrks());
      continue;
    }
    VertexLinkVector prec_vtx = bplus_cand->auxdata<VertexLinkVector>("PrecedingVertexLinks");

    if (prec_vtx.size() != 1){
      ANA_MSG_WARNING("Expected one (not %d) preceding vertices for B candidate, skip" << (int)prec_vtx.size());
      continue;
    }

    //get the jpsi linked to the bplus
    auto jpsi_it = std::find(good_jpsis.begin(), good_jpsis.end(), *(prec_vtx[0]));

    if (jpsi_it == good_jpsis.end()){
      ANA_MSG_WARNING("Unknown preceding vertex for B candidate, skip");
      continue;
    }
     
    TLorentzVector muon0_ref_track = bplus_helper.refTrk(0, 105.65837);
    TLorentzVector muon1_ref_track = bplus_helper.refTrk(1, 105.65837);
    TLorentzVector kaon_ref_track  = bplus_helper.refTrk(2, 493.677);
    TLorentzVector bplus_ref_track = muon0_ref_track + muon1_ref_track + kaon_ref_track;

    float bplus_mass = bplus_helper.mass();
    float kaon_pt = toGeV(kaon_ref_track.Pt());

    if(kaon_pt > 3 && bplus_mass < 5500 && bplus_mass > 5100 && bplus_helper.tau() >0.200){
      numBplus++;
      addToVectorBranch(vars,"bplusMass",       bplus_mass);
      addToVectorBranch(vars,"bplusPt",         bplus_ref_track.Pt());
      addToVectorBranch(vars,"bplusEta",        bplus_ref_track.Eta());
      addToVectorBranch(vars,"bplusPhi",        bplus_ref_track.Phi());
      addToVectorBranch(vars,"bplusJpsiIndex",  (*jpsi_it)->index());
      addToVectorBranch(vars,"bplusChi2",       bplus_helper.vtx()->chiSquared());
      addToVectorBranch(vars,"bplusLxy",    bplus_helper.lxy     (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
      addToVectorBranch(vars,"bplusLxyErr", bplus_helper.lxyErr  (xAOD::BPhysHelper::PV_MAX_SUM_PT2));
      addToVectorBranch(vars,"bplusA0",     bplus_helper.a0      (xAOD::BPhysHelper::PV_MAX_SUM_PT2));      
      addToVectorBranch(vars,"bplusA0Err",  bplus_helper.a0Err   (xAOD::BPhysHelper::PV_MAX_SUM_PT2));    
      addToVectorBranch(vars,"bplusA0xy",   bplus_helper.a0xy    (xAOD::BPhysHelper::PV_MAX_SUM_PT2));    
      addToVectorBranch(vars,"bplusA0xyErr",bplus_helper.a0xyErr (xAOD::BPhysHelper::PV_MAX_SUM_PT2));        
      addToVectorBranch(vars,"bplusZ0",     bplus_helper.z0      (xAOD::BPhysHelper::PV_MAX_SUM_PT2));   
      addToVectorBranch(vars,"bplusZ0Err",  bplus_helper.z0Err   (xAOD::BPhysHelper::PV_MAX_SUM_PT2));   
      addToVectorBranch(vars,"bplusConstM_tau",bplus_helper.tau  (xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
      addToVectorBranch(vars,"bplusConstM_tauErr",bplus_helper.tauErr(xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_CONST_MASS));
      addToVectorBranch(vars,"bplusInvM_tau",bplus_helper.tau        (xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));
      addToVectorBranch(vars,"bplusInvM_tauErr",bplus_helper.tauErr  (xAOD::BPhysHelper::PV_MAX_SUM_PT2, xAOD::BPhysHypoHelper::TAU_INV_MASS));
 
      //loop over other muons, get correct jpsi index
      for(unsigned int i=0; i< jpsiIndexOtherMuon.size(); i++){

	//get the correct jpsi and it's assosicated other muon(s)
	if( jpsiIndexOtherMuon[i].first ==(*jpsi_it)->index()){
	  const xAOD::Muon* muon= jpsiIndexOtherMuon[i].second;

	  TLorentzVector muonTL;
	  muonTL.SetPtEtaPhiE(toGeV(muon->pt()), muon->p4().Eta(), muon->p4().Phi(), toGeV(muon->e()));
	  float deltaR=muonTL.DeltaR(bplus_ref_track);
	  float deltaPhi=muonTL.DeltaPhi(bplus_ref_track);
	  addToVectorBranch(vars,"otherMuonBplusDeltaPhi", deltaPhi );
	  addToVectorBranch(vars,"otherMuonBplusDeltaR",   deltaR );
	  addToVectorBranch(vars,"bplusOtherMuonIndex",    muon->index()  );	    	    
	}
      }
    }     
  }
  if(numBplus==0){
    addToVectorBranch(vars,"bplusMass",       dummy_f);
    addToVectorBranch(vars,"bplusPt",         dummy_d);
    addToVectorBranch(vars,"bplusEta",        dummy_d);
    addToVectorBranch(vars,"bplusPhi",        dummy_d);
    addToVectorBranch(vars,"bplusJpsiIndex",  dummy_ul);
    addToVectorBranch(vars,"bplusChi2",       dummy_f);
    addToVectorBranch(vars,"bplusLxy",        dummy_f);
    addToVectorBranch(vars,"bplusLxyErr",     dummy_f);
    addToVectorBranch(vars,"bplusA0",         dummy_f);      
    addToVectorBranch(vars,"bplusA0Err",      dummy_f);    
    addToVectorBranch(vars,"bplusA0xy",       dummy_f);    
    addToVectorBranch(vars,"bplusA0xyErr",    dummy_f);        
    addToVectorBranch(vars,"bplusZ0",         dummy_f);   
    addToVectorBranch(vars,"bplusZ0Err",      dummy_f);   
    addToVectorBranch(vars,"bplusConstM_tau", dummy_f);
    addToVectorBranch(vars,"bplusConstM_tauErr",    dummy_f);
    addToVectorBranch(vars,"bplusInvM_tau",         dummy_f);
    addToVectorBranch(vars,"bplusInvM_tauErr",      dummy_f);
    addToVectorBranch(vars,"otherMuonBplusDeltaPhi",dummy_f);
    addToVectorBranch(vars,"otherMuonBplusDeltaR",  dummy_f);
    addToVectorBranch(vars,"bplusOtherMuonIndex",   dummy_ul);

  }
  vars["numBplusPerEvent"]= numBplus;
  
  //%%%%%%%%%%%%%%%%%%%%
  // P V 
  //%%%%%%%%%%%%%%%%%%%%

  const xAOD::VertexContainer* vertices = nullptr;
  STRONG_CHECK(event->retrieve( vertices, "PrimaryVertices"));
  vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);

  for ( const auto& vx : *vertices ) {
    if (vx->vertexType() == xAOD::VxType::PriVtx) {
      addToVectorBranch(vars,"PVx",       vx->x() );
      addToVectorBranch(vars,"PVy",       vx->y() );
      addToVectorBranch(vars,"PVz",       vx->z() );
      addToVectorBranch(vars,"PVrxy",     TMath::Hypot(vx->x(),vx->y() ));	
      addToVectorBranch(vars,"PVnTracks", vx->nTrackParticles() );

      int VertexType = vx->vertexType(); //addToVectorBranch won't save type xAOD vertex
      addToVectorBranch(vars,"PVvertexType", VertexType);      
    }
  }

    
  //%%%%%%%%%%%%%%%%%%%%
  // T R I G G E R
  //%%%%%%%%%%%%%%%%%%%%

  //TODO can put this in region options in select alg if we need to reduce size of ntuple
  vars["passHLT_2mu14"]= eventInfo->auxdecor<bool>( "passHLT_2mu14" );


  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::string ListOfBphysTrigs[] ={
    //Level 1 mu triggers
    "L1_MU4",
    "L1_MU6",
    "L1_MU10",
    "L1_MU15",
    "L1_MU20",
    "L1_M11", // single-muon trigger with a threshold of 10 GeV/c with additional tight requirement (three-station coincidence) in the barrel region (|eta_mu|<1.05) with respect to the single-muon trigger with a threshold of 10 GeV/c with only two-station coincidence in the barrel region, MU10.
    "L1_2MU4",
    "L1_2MU6",
    "L1_2MU10",
    "L1_2MU15",
    "L1_2MU20",
    "L1_3MU6"
    "L1_MU10_2MU6",
    "L1_MU11_2MU6",
    "L1_MU6_2MU4",   
    "L1_MU6_3MU4",
    "L1_2MU6_3MU4",

    "L1_BPH_2M9_2MU6_BPH_2DR15_2MU6",
    "L1BPH_2M8_2MU4",
    "L1BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4",
    "L1MU6MU4_BO",
    "L12MU4_B",
    "L1MU4_UNPAIRED_ISO",
    "L1MU6_EMPTY",
      
    //Jpsi triggers
    "HLT_2mu10_bJpsimumu",
    "HLT_2mu10_bJpsimumu_delayed",
    "HLT_2mu10_bJpsimumu_noL2",
    "HLT_2mu4_bJpsimumu_delayed_L1BPH_2M8_2MU4",
    "HLT_2mu4_bJpsimumu_L1BPH_2M8_2MU4",
    "HLT_2mu4_bJpsimumu_Lxy0_delayed_L1BPH_2M8_2MU4",
    "HLT_2mu6_bJpsimumu",
    "HLT_2mu6_bJpsimumu_delayed",
    "HLT_2mu6_bJpsimumu_delayed_L1BPH_2M9_2MU6_BPH_2DR15_2MU6",
    "HLT_2mu6_bJpsimumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6",
    "HLT_2mu6_bJpsimumu_Lxy0",
    "HLT_2mu6_bJpsimumu_Lxy0_delayed",
    "HLT_2mu6_bJpsimumu_Lxy0_delayed_L1BPH_2M9_2MU6_BPH_2DR15_2MU6",
    "HLT_2mu6_bJpsimumu_Lxy0_L1BPH_2M9_2MU6_BPH_2DR15_2MU6",
    "HLT_3mu4_bJpsi",
    "HLT_3mu4_bJpsi_delayed",
    "HLT_3mu6_bJpsi",
    "HLT_mu10_mu6_bJpsimumu",
    "HLT_mu10_mu6_bJpsimumu_delayed",
    "HLT_mu10_mu6_bJpsimumu_Lxy0",
    "HLT_mu10_mu6_bJpsimumu_Lxy0_delayed",
    "HLT_mu18_bJpsi_Trkloose",
    "HLT_mu20_2mu0noL1_JpsimumuFS",
    "HLT_mu20_2mu4_JpsimumuL2",
    "HLT_mu6_2mu4_bJpsi",
    "HLT_mu6_2mu4_bJpsi_delayed",
    "HLT_mu6_mu4_bJpsimumu",
    "HLT_mu6_mu4_bJpsimumu_delayed",
    "HLT_mu6_mu4_bJpsimumu_delayed_L1BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4",
    "HLT_mu6_mu4_bJpsimumu_delayed_L1MU6MU4_BO",
    "HLT_mu6_mu4_bJpsimumu_L12MU4_B",
    "HLT_mu6_mu4_bJpsimumu_L1BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4",
    "HLT_mu6_mu4_bJpsimumu_L1MU6MU4_BO",
    "HLT_mu6_mu4_bJpsimumu_Lxy0",
    "HLT_mu6_mu4_bJpsimumu_Lxy0_delayed",
    "HLT_mu6_mu4_bJpsimumu_Lxy0_delayed_L1BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4",
    "HLT_mu6_mu4_bJpsimumu_Lxy0_L1BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4",

    //Bmumu triggers
    "HLT_2mu4_bBmumu_L1BPH_2M8_2MU4",
    "HLT_2mu4_bBmumu_Lxy0_L1BPH_2M8_2MU4",
    "HLT_2mu6_bBmumu",
    "HLT_2mu6_bBmumu_L1BPH_2M9_2MU6_BPH_2DR15_2MU6",
    "HLT_2mu6_bBmumu_Lxy0",
    "HLT_2mu6_bBmumu_Lxy0_L1BPH_2M9_2MU6_BPH_2DR15_2MU6",
    "HLT_2mu6_bBmumux_BpmumuKp",
    "HLT_mu10_mu6_bBmumux_BpmumuKp",
    "HLT_mu6_mu4_bBmumu",
    "HLT_mu6_mu4_bBmumu_L12MU4_B",
    "HLT_mu6_mu4_bBmumu_L1BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4",
    "HLT_mu6_mu4_bBmumu_L1MU6MU4_BO",
    "HLT_mu6_mu4_bBmumu_Lxy0",
    "HLT_mu6_mu4_bBmumu_Lxy0_L1BPH_2M8_MU6MU4_BPH_0DR15_MU6MU4",
    "HLT_mu6_mu4_bBmumux_BpmumuKp",
      
    //3mu
    "HLT_3mu4",
    "HLT_3mu4_bDimu",
    "HLT_3mu4_nomucomb_delayed",
    "HLT_3mu6",
    "HLT_3mu6_bDimu",
    "HLT_3mu6_msonly",
    "HLT_3mu6_msonly_L1MU4_UNPAIRED_ISO",
    "HLT_3mu6_msonly_L1MU6_EMPTY",
    "HLT_mu20_2mu4noL1",
    "HLT_mu22_2mu4noL1",
    "HLT_mu24_2mu4noL1",
    "HLT_mu6_2mu4",
    "HLT_mu6_nomucomb_2mu4_nomucomb_delayed_L1MU6_3MU4"
    
  };
  
  for(auto ListOfBphysTrigs : ListOfBphysTrigs){     
    vars[ ListOfBphysTrigs ]=std::find(passTrigs.begin(), passTrigs.end(), ListOfBphysTrigs) != passTrigs.end();
  }
  
          
  //%%%%%%%%%%%%%%%%%%%%
  // M E T 
  //%%%%%%%%%%%%%%%%%%%%
    
  xAOD::MissingETContainer * metcont = nullptr;
  STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));
    
  vars["passMETtrigger"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
  vars["MET"]            = toGeV((*metcont)["Final"]->met());
  vars["MET_px"]         = toGeV((*metcont)["Final"]->mpx());
  vars["MET_py"]         = toGeV((*metcont)["Final"]->mpy());
  vars["MET_phi"]        = ((*metcont)["Final"]->phi());
      
  return EL::StatusCode::SUCCESS;
}






