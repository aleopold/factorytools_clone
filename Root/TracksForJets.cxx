#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/TracksForJets.h>

#include <FactoryTools/HelperFunctions.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"

#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

// this is needed to distribute the algorithm to the workers
ClassImp(TracksForJets)

typedef FactoryTools::HelperFunctions HF;

TracksForJets :: TracksForJets () {
  onlyPV = true;
  track_d0Sel = 2.0;
  do_z0sigCut = false;
  value_z0sigCut = 1.0;
  do_z0sinThetaCut = false;
  value_z0sinThetaCut = 3.0;
  do_secondPass = false;
  value_secondPassCut = 1.0; 

}

EL::StatusCode TracksForJets :: initialize () {
   
   return EL::StatusCode::SUCCESS;
}

EL::StatusCode TracksForJets :: execute ()
{

  ///////////
  // Setup
  ///////////
 
  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();
  typedef ConstDataVector<xAOD::TrackParticleContainer> IPC;
  typedef xAOD::ParticleAuxContainer IPC_aux;

  auto vertices = HF::grabFromEvent<xAOD::VertexContainer>("PrimaryVertices",event);

  //////////////////////////////////
  // Create new track containers
  //////////////////////////////////

  auto track_nominal = HF::grabFromEvent<xAOD::TrackParticleContainer>("InDetTrackParticles",event);
  std::vector<long int> used_tracks = {};
  long int trackjet_index = 0;
  for (size_t i=0; i<vertices->size(); i++){

    // Limit only to PV
    if (onlyPV && i!=0) continue;

    auto selectedTracks = HF::createContainerInTStore<IPC,IPC_aux>(NewContainerName+"_"+std::to_string(i),store);

    for (auto *trk: *track_nominal) {
    
      // Set track indices and record tracks passing selection if looking at first PV 
      if (i==0){
        trk->auxdecor<long int>("trackjet_index") = trackjet_index;
        trackjet_index += 1;

        // Select only standard tracks
        if (static_cast<int>(trk->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0))==1) continue;

        // Apply loose selection
        if (fabs(trk->eta())>2.5) continue;

        int num_pix_hits = (int)trk->auxdata<unsigned char>("numberOfPixelHits")+(int)trk->auxdata<unsigned char>("numberOfPixelDeadSensors");
        int num_sct_hits = (int)trk->auxdata<unsigned char>("numberOfSCTHits")+(int)trk->auxdata<unsigned char>("numberOfSCTDeadSensors");
        if ((num_pix_hits+num_sct_hits)<7) continue;

        int num_sh_pix = (int)trk->auxdata<unsigned char>("numberOfPixelSharedHits");    
        int num_sh_sct = (int)trk->auxdata<unsigned char>("numberOfSCTSharedHits"); 
        if (num_sh_pix+((num_sh_sct*1.0)/2.)>1) continue;

        int num_pix_holes = (int)trk->auxdata<unsigned char>("numberOfPixelHoles");   
        int num_sct_holes = (int)trk->auxdata<unsigned char>("numberOfSCTHoles");   
        if (num_pix_holes>1) continue;
        if (num_sct_holes>2) continue;

        if (fabs(trk->d0())>track_d0Sel) continue;

        // Record if tracks pass all above selection
        trk->auxdecor<int>("passes_jet_selection") = 1;
      }

      bool passes_selection = trk->isAvailable<int>("passes_jet_selection") ? trk->auxdataConst<int>("passes_jet_selection") : 0;
      if (!passes_selection) continue;

      if (do_z0sinThetaCut && fabs((trk->z0()+trk->vz()-vertices->at(i)->z())*sin(trk->theta())) > value_z0sinThetaCut) continue;
      if (do_z0sigCut && fabs((trk->z0()+trk->vz()-vertices->at(i)->z())/TMath::Sqrt(trk->definingParametersCovMatrix()(1, 1))) > value_z0sigCut) continue;

      // Don't re-use tracks
      if (std::find(used_tracks.begin(), used_tracks.end(), trk->auxdataConst<long int>("trackjet_index")) != used_tracks.end()){
        trk->auxdecor<int>("multiple_pvs") = 1;
        continue;
      }

      trk->auxdecor<int>("included_with_pv") = 1;
      selectedTracks.first->push_back(trk  );
      used_tracks.push_back(trk->auxdataConst<long int>("trackjet_index"));
    }
  }

  // Second pass
  if (!onlyPV && do_secondPass){
   for (auto *trk: *track_nominal) {
    bool passes_jet_selection = trk->isAvailable<int>("passes_jet_selection") ? trk->auxdataConst<int>("passes_jet_selection") : 0;
    bool included_with_pv = trk->isAvailable<int>("included_with_pv") ? trk->auxdataConst<int>("included_with_pv") : 0;
    if (passes_jet_selection && !included_with_pv){
      // Add to "closest" PV. And keep track of how close that is.
      int closestPV = -1;
      float closestz0 = 100000.; 
      for (size_t i=0; i<vertices->size(); i++){
        float dist = fabs(trk->z0()+trk->vz()-vertices->at(i)->z());
        if (dist<closestz0){
          closestPV = i;
          closestz0 = dist;
        }
      }
      // Don't add track if its > X mm away
      if (fabs(trk->z0()+trk->vz()-vertices->at(closestPV)->z())>value_secondPassCut) continue;
      trk->auxdecor<int>("included_with_pv_secondpass") = 1;
      auto selectedTracks_secondPass = HF::grabFromStore<IPC>(NewContainerName+"_"+std::to_string(closestPV),store);
      selectedTracks_secondPass->push_back(trk);
    }
   }
  }

  // Record info on each track collection, to be used later in JetMakerTool.cxx
  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);
  for (size_t i=0; i<vertices->size(); i++){
    auto selectedTracks_thirdPass = HF::grabFromStore<IPC>(NewContainerName+"_"+std::to_string(i),store);
    eventInfo->auxdecor<int>(NewContainerName+"_"+std::to_string(i)+"_numTracks") = selectedTracks_thirdPass->size();
    float scalar_sum_pt = 0.0;
    for (const auto& trk: *selectedTracks_thirdPass){
      scalar_sum_pt += trk->pt()/1000.;
    }
    eventInfo->auxdecor<float>(NewContainerName+"_"+std::to_string(i)+"_scalarSumPt") = scalar_sum_pt;
  }
    

  return EL::StatusCode::SUCCESS;
}


