#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"
//#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"

#include "FactoryTools/RegionVarCalculator_b4j.h"
#include "FactoryTools/strongErrorCheck.h"

#include <xAODAnaHelpers/HelperFunctions.h>


// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_b4j)

EL::StatusCode RegionVarCalculator_b4j::doInitialize(EL::Worker * worker) {
  if(m_worker != nullptr){
    std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
    return EL::StatusCode::FAILURE;
  }
  m_worker = worker;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode RegionVarCalculator_b4j::doCalculate(std::map<std::string, anytype>& vars ) {
  xAOD::TEvent* event = m_worker->xaodEvent();

  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

  if      ( regionName.empty() ) {return EL::StatusCode::SUCCESS;}
  // If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
  else if ( regionName == "SR" )  {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS &&
							 doSRCalculations  (vars) == EL::StatusCode::SUCCESS);}


  return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_b4j::doAllCalculations(std::map<std::string, anytype>& vars )
{
  xAOD::TStore * store = m_worker->xaodStore();
  xAOD::TEvent * event = m_worker->xaodEvent();

  // Get relevant info from the EventInfo object /////////////////////
  //

  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  doGeneralCalculations(vars);

  //
  /////////////////////////////////////////////////////////////////////

  // Get relevant info from the vertex container //////////////////////
  //
  auto toGeV = [](float a){return a*.001;};


  const xAOD::VertexContainer* vertices = nullptr;
  STRONG_CHECK(event->retrieve( vertices, "PrimaryVertices"));
  vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);

  //
  /////////////////////////////////////////////////////////////////////

  xAOD::MissingETContainer * metcont = nullptr;
  STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

  //  std::cout << "MET : " << (*metcont)["Final"]->met() << std::endl;
  vars     ["MET"]   = toGeV((*metcont)["Final"]->met());

  // xAOD::JetContainer* jets_nominal(nullptr);
  // STRONG_CHECK(store->retrieve(jets_nominal, "STCalibAntiKt4EMTopo"));

  // xAOD::JetContainer* jets_nominal(nullptr);
  // STRONG_CHECK(store->retrieve(jets_nominal,  "STCalibCamKt12LCTopoJets"));

  xAOD::JetContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal,  "STCalibAntiKt10LCTopoTrimmedPtFrac5SmallR20"));


  std::vector<TLorentzVector> jet4MomVec;
  for( const auto& jet : *jets_nominal) {
    jet4MomVec.push_back( TLorentzVector(jet->p4()) );
  }
  auto ptSort = [](TLorentzVector const & a , TLorentzVector const & b){return a.Pt() > b.Pt();};
  std::sort(jet4MomVec.begin(),jet4MomVec.end(), ptSort);

  double mJJ = -1;
  if(jet4MomVec.size()>1){
    mJJ = (jet4MomVec.at(0)+jet4MomVec.at(1)).M();
  }

  vars["mJJ"] = toGeV(mJJ);



  //  const std::vector<xAOD::IParticle*> & jetStdVec = jetcont->stdcont();
  std::vector<float> jetPtVec;
  std::vector<float> jetEtaVec;
  std::vector<float> jetPhiVec;
  std::vector<float> jetEVec;

  std::vector<float> jetTau1Vec;
  std::vector<float> jetTau2Vec;
  std::vector<float> jetTau3Vec;
  std::vector<float> jetTau1wtaVec;
  std::vector<float> jetTau2wtaVec;
  std::vector<float> jetTau3wtaVec;
  std::vector<float> jetDip12Vec;


  std::vector<float> jetECF1Vec;
  std::vector<float> jetECF2Vec;
  std::vector<float> jetECF3Vec;
  // std::vector<float> jetECF1Beta2Vec;
  // std::vector<float> jetECF2Beta2Vec;
  // std::vector<float> jetECF3Beta2Vec;

  for( const auto& jet : *jets_nominal) {
    jetPtVec.push_back( toGeV(jet->pt()));
    jetEtaVec.push_back( jet->p4().Eta() );
    jetPhiVec.push_back( jet->p4().Phi() );
    jetEVec.push_back( toGeV(jet->p4().E()) );

    jetTau1Vec.push_back(  jet->getAttribute<double>("Tau1")    );
    jetTau2Vec.push_back(  jet->getAttribute<double>("Tau2")    );
    jetTau3Vec.push_back(  jet->getAttribute<double>("Tau3")    );
    jetTau1wtaVec.push_back(  jet->getAttribute<double>("Tau1_wta")    );
    jetTau2wtaVec.push_back(  jet->getAttribute<double>("Tau2_wta")    );
    jetTau3wtaVec.push_back(  jet->getAttribute<double>("Tau3_wta")    );
    jetDip12Vec.push_back( jet->getAttribute<double>("Dip12")     );

    jetECF1Vec.push_back(  jet->getAttribute<double>("ECF1")    );
    jetECF2Vec.push_back(  jet->getAttribute<double>("ECF2")    );
    jetECF3Vec.push_back(  jet->getAttribute<double>("ECF3")    );
    // jetECF1Beta2Vec.push_back(  jet->getAttribute<double>("ECF1_Beta2")    );
    // jetECF2Beta2Vec.push_back(  jet->getAttribute<double>("ECF2_Beta2")    );
    // jetECF3Beta2Vec.push_back(  jet->getAttribute<double>("ECF3_Beta2")    );
  }

  vars[ "jetPt" ]  = jetPtVec;
  vars[ "jetEta" ] = jetEtaVec;
  vars[ "jetPhi" ] = jetPhiVec;
  vars[ "jetE" ]   = jetEVec;

  vars[ "jetTau1" ] = jetTau1Vec;
  vars[ "jetTau2" ] = jetTau2Vec;
  vars[ "jetTau3" ] = jetTau3Vec;
  vars[ "jetTau1wta" ] = jetTau1wtaVec;
  vars[ "jetTau2wta" ] = jetTau2wtaVec;
  vars[ "jetTau3wta" ] = jetTau3wtaVec;
  vars[ "jetDip12" ] = jetDip12Vec;


  vars[ "jetECF1" ] = jetECF1Vec;
  vars[ "jetECF2" ] = jetECF2Vec;
  vars[ "jetECF3" ] = jetECF3Vec;
  // vars[ "jetECF1Beta2" ] = jetECF1Beta2Vec;
  // vars[ "jetECF2Beta2" ] = jetECF2Beta2Vec;
  // vars[ "jetECF3Beta2" ] = jetECF3Beta2Vec;

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_b4j::doSRCalculations(std::map<std::string, anytype>& /* vars */ )
{
  return EL::StatusCode::SUCCESS;
}

