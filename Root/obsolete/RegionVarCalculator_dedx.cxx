#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "AsgTools/MessageCheck.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackingPrimitives.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETComposition.h"

#include "FactoryTools/Baseline_DVJETS_Selection.h"
#include "FactoryTools/DRAW_DVJETS_filters.h"
#include "FactoryTools/JetCleaning_DVJETS_custom.h"
#include "FactoryTools/RegionVarCalculator_dedx.h"
#include "FactoryTools/strongErrorCheck.h"

#include <FactoryTools/HelperFunctions.h>

#include <fstream>
// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_dedx)
using namespace asg::msgUserCode;

typedef FactoryTools::HelperFunctions HF;

const xAOD::TruthParticle* getTruthParticle       ( const xAOD::TrackParticle* trk ) {
    
  typedef ElementLink<xAOD::TruthParticleContainer> truthLink;
  const xAOD::TruthParticle *truth { nullptr };
      
  if( trk->isAvailable< truthLink >("truthParticleLink") ) {
    try {
      const truthLink& link = trk->auxdataConst< truthLink >("truthParticleLink");
      truth = *link;
    } catch(...) {}
  }
  return truth;
    
}

//*************************
// D O I N I T I A L I Z E
//*************************
EL::StatusCode RegionVarCalculator_dedx::doInitialize(EL::IWorker * worker) {
    if(m_worker != nullptr){
        std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
        return EL::StatusCode::FAILURE;
    }
    m_worker = worker;

    //Instantiate class to resolve OR link
    m_overlapLinkHelper = std::make_unique<ORUtils::OverlapLinkHelper>("overlapLinkHelper");
 
    return EL::StatusCode::SUCCESS;
}

//**************************
// D O   C A L C U L A T E
//**************************
EL::StatusCode RegionVarCalculator_dedx::doCalculate(std::map<std::string, anytype>& vars) {

    //Setup
    xAOD::TEvent* event = m_worker->xaodEvent();

    auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

    std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

    if      ( regionName.empty() ) {
        // If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
        ANA_MSG_DEBUG("No region name set, no calculations performed.");
        return EL::StatusCode::SUCCESS;
    }
    else if ( regionName == "SRDV" ) {
        // other SR definitions can have other trees written out with other functions here
        ANA_MSG_DEBUG("Selection region set: SRDV");
        return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); 
    }

    return EL::StatusCode::SUCCESS;
}


//**************************
// D O   A L L   C A L C  
//**************************
EL::StatusCode RegionVarCalculator_dedx::doAllCalculations(std::map<std::string, anytype>& vars ) {
    xAOD::TStore * store = m_worker->xaodStore();
    xAOD::TEvent * event = m_worker->xaodEvent();

    auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

    doGeneralCalculations(vars);

    auto toGeV = [](double a){return a*.001;};

    //&&&&&&&&&&&&&&&&&&&&&
    // P V 
    //&&&&&&&&&&&&&&&&&&&&&
    //////////////////////////////////////////////////
    // get the highest-sumpT primary vertex (one is needed for recalculating MET)
    auto pvc = HF::grabFromStore<xAOD::VertexContainer>("selectedPV",store);

    auto pv = pvc->at(0);
    vars["PV_x"] = pv->x();                                                                        
    vars["PV_y"] = pv->y();                                                                        
    vars["PV_z"] =  pv->z();                                                                       
    vars["PV_rxy"] =  TMath::Hypot(pv->x(),pv->y());                                               
    vars["PV_nTracks"] = pv->nTrackParticles();                                                    
    vars["PV_sumpT2"] = pv->auxdataConst<float>( "sumPt2" );                                       

    //&&&&&&&&&&&&&
    // M E T 
    //&&&&&&&&&&&&&
    auto metcont = HF::grabFromStore<xAOD::MissingETContainer>("STCalibMET",store);
    //const xAOD::MissingETAssociationMap *metMap = nullptr;
    //STRONG_CHECK(store->retrieve(metMap, "crap"));
    //STRONG_CHECK(store->retrieve(metMap, "STCalibMETMap"));

    //  std::cout << "MET : " << (*metcont)["Final"]->met() << std::endl;
    vars["passMETtrigger"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
    vars["MET"] = toGeV((*metcont)["Final"]->met());
    vars["MET_phi"] = ((*metcont)["Final"]->phi());

    if ((*metcont)["Muons"]){
      vars["MET_muon"]   = toGeV((*metcont)["Muons"]->met());
      vars["MET_muon_phi"]   = ((*metcont)["Muons"]->phi());
    }
    if ((*metcont)["RefEle"]){
      vars["MET_ele"]    = toGeV((*metcont)["RefEle"]->met());
      vars["MET_ele_phi"]    = ((*metcont)["RefEle"]->phi());
    }
    if ((*metcont)["RefTau"]){
      vars["MET_tau"]    = toGeV((*metcont)["RefTau"]->met());
      vars["MET_tau_phi"]    = ((*metcont)["RefTau"]->phi());
    }
    if ((*metcont)["RefGamma"]){
      vars["MET_gamma"]  = toGeV((*metcont)["RefGamma"]->met());
      vars["MET_gamma_phi"]  = ((*metcont)["RefGamma"]->phi());
    }
    if ((*metcont)["RefJet"]){
      vars["MET_jet"]    = toGeV((*metcont)["RefJet"]->met());
      vars["MET_jet_phi"]    = ((*metcont)["RefJet"]->phi());
    }

    if ((*metcont)["PVSoftTrk"]){
      vars["MET_pvsofttrk"]    = toGeV((*metcont)["PVSoftTrk"]->met());
      vars["MET_pvsofttrk_phi"]    = ((*metcont)["PVSoftTrk"]->phi());
    }

    auto met_lht_container = HF::grabFromEvent<xAOD::MissingETContainer>("MET_LocHadTopo",event);
    bool hasMET = met_lht_container->size() > 0;
    vars["MET_LHT"] = hasMET ? met_lht_container->at(0)->met() * 0.001 : -999.;
    vars["MET_LHT_phi"] = hasMET ? met_lht_container->at(0)->phi() : -999.;

    auto met_hlt_container = HF::grabFromEvent<xAOD::TrigMissingETContainer>("HLT_xAOD__TrigMissingETContainer_TrigEFMissingET",event);
    TVector2 hltMET;
    hasMET = met_hlt_container->size() > 0;
    if (hasMET) hltMET = TVector2(met_hlt_container->at(0)->ex(), met_hlt_container->at(0)->ey());
    vars["MET_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
    vars["MET_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

    auto mht_hlt_container = HF::grabFromEvent<xAOD::TrigMissingETContainer>("HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht",event);
    hasMET = mht_hlt_container->size() > 0;
    if (hasMET) hltMET = TVector2(mht_hlt_container->at(0)->ex(), mht_hlt_container->at(0)->ey());
    vars["MHT_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
    vars["MHT_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

    //&&&&&&&&&&&&&&&&&&&&&&&&&&&
    // S E L E C T E D   J E T S 
    //&&&&&&&&&&&&&&&&&&&&&&&&&&&
    auto selectedJets = HF::grabFromStore<xAOD::IParticleContainer>("selectedJets",store);

    for( const auto& jet : *selectedJets) {
        addToVectorBranch(vars,"jet_Pt", toGeV(jet->pt() ) );
        addToVectorBranch(vars,"jet_Eta", jet->p4().Eta()  );
        addToVectorBranch(vars,"jet_Phi", jet->p4().Phi()  );
        addToVectorBranch(vars,"jet_M", jet->p4().M()  );
        addToVectorBranch(vars,"jet_BTag", jet->auxdata<char>("bjet") );
        addToVectorBranch(vars,"jet_PassOR", (int)jet->auxdata<char>("passOR") ); //TODO isn't this redundant since sleected jets are required to have passOR == 1?

    }

    //&&&&&&&&&&&&&&&&&&&
    //Uncalibrated jets
    //&&&&&&&&&&&&&&&&&&&
    if(inListOfDetailedObjects("uncalibJets")){

      auto uncalibjets_nominal = HF::grabFromEvent<xAOD::JetContainer>("AntiKt4EMTopoJets",event);

        //********************************
        // DRAW DV+JETS filter flags
        //********************************
        //Retrieve triggerDecision from vecvars
        if( inListOfDetailedObjects("DVJETS_DRAW_flags") ){

            DRAW_DVJETS_filters::FilterFlags filterFlags; //Struct to store filter decisions 
            if( DRAW_DVJETS_filters::filterEvents( uncalibjets_nominal, m_event_triggerDecisions, filterFlags ) == EL::StatusCode::FAILURE ){
            
                std::cout<<"DRAW_DV_JETS_filters::filterEvents returned a failure code. Exiting."<<std::endl;
                return EL::StatusCode::FAILURE;

            } else {

                //TODO store other filter decisions, like multiplicity flag
                vars["DRAW_pass_DVJETS"]                =  filterFlags.pass_DRAW;  
                vars["DRAW_pass_earlyRun2Flag"]         =  filterFlags.pass_earlyRun2;
                vars["DRAW_pass_highPtJetFlag"]         =  filterFlags.pass_highPtJetMult;
                vars["DRAW_pass_triggerFlags"]          =  filterFlags.pass_triggerFlags;
                vars["DRAW_pass_singleTracklessJetFlag"]=  filterFlags.pass_singleTracklessFlag;
                vars["DRAW_pass_doubleTracklessJetFlag"]=  filterFlags.pass_doubleTracklessFlag;
            } 
        }

        for (const auto& jet : *uncalibjets_nominal) {

            addToVectorBranch(vars,"uncalib_JetPt", toGeV(jet->pt() ) );
            addToVectorBranch(vars,"uncalib_JetEta", jet->p4().Eta()  );
            addToVectorBranch(vars,"uncalib_JetPhi", jet->p4().Phi()  );
            addToVectorBranch(vars,"uncalib_JetM", jet->p4().M()  );

            //Store relevant info for trackless jets
            std::vector<float> sumPtTrkvec;
            jet->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec);

            //Keep track of if this branch exists to make sure not mixing up entries in flattened branches.
            if( sumPtTrkvec.size() > 0){
                addToVectorBranch(vars,"SumPtTrkPt500", toGeV(sumPtTrkvec[0]) );
            } else {
                addToVectorBranch(vars,"SumPtTrkPt500", -1. );
            }

            //Jet cleaning variables
            float EMFrac;
            jet->getAttribute(xAOD::JetAttribute::EMFrac,EMFrac);
            addToVectorBranch(vars,"uncalibJet_EMFrac", EMFrac );

            float HECFrac = 0;
            jet->getAttribute(xAOD::JetAttribute::HECFrac,HECFrac);
            addToVectorBranch(vars,"uncalibJet_HECFrac", HECFrac );

            float LArQuality = 0;
            jet->getAttribute(xAOD::JetAttribute::LArQuality,LArQuality);
            addToVectorBranch(vars,"uncalibJet_LArQuality", LArQuality );

            float HECQuality = 0;
            jet->getAttribute(xAOD::JetAttribute::HECQuality,HECQuality);
            addToVectorBranch(vars,"uncalibJet_HECQuality", HECQuality );

            float FracSamplingMax = 0;
            jet->getAttribute(xAOD::JetAttribute::FracSamplingMax,FracSamplingMax);
            addToVectorBranch(vars,"uncalibJet_FracSamplingMax", FracSamplingMax );

            float NegativeE = 0;
            jet->getAttribute(xAOD::JetAttribute::NegativeE,NegativeE);
            addToVectorBranch(vars,"uncalibJet_NegativeE", NegativeE );

            float AverageLArQF = 0;
            jet->getAttribute(xAOD::JetAttribute::AverageLArQF,AverageLArQF);
            addToVectorBranch(vars,"uncalibJet_AverageLArQF", AverageLArQF );

            int FracSamplingMaxIndex = -1;
            jet->getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex,FracSamplingMaxIndex);
            addToVectorBranch(vars,"uncalibJet_FracSamplingMaxIndex", FracSamplingMaxIndex );
        }
    }


    //&&&&&&&&&&&&&&&&&
    //Calibrated jets
    //&&&&&&&&&&&&&&&&&
    if( inListOfDetailedObjects("customJetCleaning") || inListOfDetailedObjects("detailedOR") || inListOfDetailedObjects("baselineDVJETS") ){

      auto jets_nominal = HF::grabFromStore<const xAOD::JetContainer>("STCalibAntiKt4EMTopoJets",store);

        //&&&&&&&&&&&&&&&&&&&&&
        // Baseline selections
        //&&&&&&&&&&&&&&&&&&&&&
        if( inListOfDetailedObjects("baselineDVJETS") ){

            Baseline_DVJETS_Selection::Flags baselineFlags; //Struct to store decision
            if( Baseline_DVJETS_Selection::filterEvents( jets_nominal, baselineFlags ) == EL::StatusCode::FAILURE ){

                std::cout<<"Baseline_DVJETS_Selection::filterEvents returned a failure code. Exiting."<<std::endl;
                return EL::StatusCode::FAILURE;

            } else {

                vars["BaselineSel_pass"]                =  baselineFlags.pass_jetSelection;  
                vars["BaselineSel_pass_2_jetMult"]      =  baselineFlags.pass_2_jetMultFlag;
                vars["BaselineSel_pass_3_jetMult"]      =  baselineFlags.pass_3_jetMultFlag;
                vars["BaselineSel_pass_2AND3_jetMult"]  =  baselineFlags.pass_2AND3_jetMultFlag;
                vars["BaselineSel_pass_4_jetMult"]      =  baselineFlags.pass_4_jetMultFlag;
                vars["BaselineSel_pass_5_jetMult"]      =  baselineFlags.pass_5_jetMultFlag;
                vars["BaselineSel_pass_6_jetMult"]      =  baselineFlags.pass_6_jetMultFlag;
                vars["BaselineSel_pass_7_jetMult"]      =  baselineFlags.pass_7_jetMultFlag;
            } 
        }//END baseline cuts
    
        for( const auto& jet : *jets_nominal) {
            addToVectorBranch(vars,"calibJet_Pt", toGeV(jet->pt() ) );
            addToVectorBranch(vars,"calibJet_Eta", jet->p4().Eta()  );
            addToVectorBranch(vars,"calibJet_Phi", jet->p4().Phi()  );
            addToVectorBranch(vars,"calibJet_M", jet->p4().M()  );
            addToVectorBranch(vars,"calibJet_BTag", jet->auxdata<char>("bjet") );
            addToVectorBranch(vars,"calibJet_sPassOR", (int)jet->auxdata<char>("passOR") );

            //*******************************
            // JET CLEANING FLAGS
            //*******************************
            //Jet cleaning variables
            float EMFrac;
            jet->getAttribute(xAOD::JetAttribute::EMFrac,EMFrac);
            addToVectorBranch(vars,"calibJet_EMFrac", EMFrac );

            float HECFrac = 0;
            jet->getAttribute(xAOD::JetAttribute::HECFrac,HECFrac);
            addToVectorBranch(vars,"calibJet_HECFrac", HECFrac );

            float LArQuality = 0;
            jet->getAttribute(xAOD::JetAttribute::LArQuality,LArQuality);
            addToVectorBranch(vars,"calibJet_LArQuality", LArQuality );

            float HECQuality = 0;
            jet->getAttribute(xAOD::JetAttribute::HECQuality,HECQuality);
            addToVectorBranch(vars,"calibJet_HECQuality", HECQuality );

            float FracSamplingMax = 0;
            jet->getAttribute(xAOD::JetAttribute::FracSamplingMax,FracSamplingMax);
            addToVectorBranch(vars,"calibJet_FracSamplingMax", FracSamplingMax );

            float NegativeE = 0;
            jet->getAttribute(xAOD::JetAttribute::NegativeE,NegativeE);
            addToVectorBranch(vars,"calibJet_NegativeE", NegativeE );

            float AverageLArQF = 0;
            jet->getAttribute(xAOD::JetAttribute::AverageLArQF,AverageLArQF);
            addToVectorBranch(vars,"calibJet_AverageLArQF", AverageLArQF );

            int FracSamplingMaxIndex = -1;
            jet->getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex,FracSamplingMaxIndex);
            addToVectorBranch(vars,"calibJet_FracSamplingMaxIndex", FracSamplingMaxIndex );


            //&&&&&&&&&&&&&&&&&&&&&
            // Custom jet cleaning
            //&&&&&&&&&&&&&&&&&&&&&
            //The reason this can't be run over the selectedJets collection is because the definition of signal includes a not-Bad jet, which is defined in the standard jetCleaning.
            if(inListOfDetailedObjects("customJetCleaning")){
                //Run custom jet cleaning to get flags
                JetCleaning_DVJETS::JetCleaningFlags jetCleaningFlags; //Struct to store jet cleaning decisions

                bool doUgly = false;
                bool doLLP = false;

                /*This is the cut applied in ST before JetCleaning
                 *
                 *          if( (int)jet->auxdata<char>("passOR") == 1 && jet->pt()>20e3 && (int)jet->auxdata<char>("passJvt") == 1 ){
                 *
                 * If it doesn't pass OR, don't run jet cleaning. But perhaps we don't agree with all the OR cuts? 
                 * The pT cut is fine.
                 * The JVT cut is meant to cut out the PU jets but as seen in the discussion here: 
                 * https://indico.cern.ch/event/709789/contributions/2927544/attachments/1652191/2644694/rcarney_JVT_SUSYWorkshop_May_2018.pdf 
                 * this could also exclude displaced jets. So best NOT to apply this cut. 
                 */
                if( jet->pt()>20e3 ){
                    if( JetCleaning_DVJETS::checkCleanJets( *jet, doUgly, doLLP, jetCleaningFlags ) == EL::StatusCode::FAILURE ){

                        std::cout<<"JetCleaning_DVJETS::checkCleanJets returned a failed StatusCode. Exiting."<<std::endl;
                        return EL::StatusCode::FAILURE;
                    } else {

                        vars["JetCleanCalib_fail"]             = jetCleaningFlags.fail;
                        vars["JetCleanCalib_fail_jetpT"]       = jetCleaningFlags.fail_jetpT; 
                        vars["JetCleanCalib_fail_ugly"]        = jetCleaningFlags.fail_ugly;       
                        vars["JetCleanCalib_fail_NCB_lowEta"]  = jetCleaningFlags.fail_NCB_lowEta; 
                        vars["JetCleanCalib_fail_NCB_highEta"] = jetCleaningFlags.fail_NCB_highEta;
                        vars["JetCleanCalib_fail_fmaxCut"]     = jetCleaningFlags.fail_fmaxCut;    
                        vars["JetCleanCalib_fail_QF"]          = jetCleaningFlags.fail_QF;         
                        vars["JetCleanCalib_fail_EMCaloNoise"] = jetCleaningFlags.fail_EMCaloNoise;
                        vars["JetCleanCalib_fail_LLPNegE"]     = jetCleaningFlags.fail_LLPNegE;    
                        vars["JetCleanCalib_fail_fmaxMin"]     = jetCleaningFlags.fail_fmaxMin;    
                        vars["JetCleanCalib_fail_NCB_monoJet"] = jetCleaningFlags.fail_NCB_monoJet;
                    }
                } else {

                        //vars["JetCleanCalib_fail"] =  -1; //Keep track of jets that did not make the cut
                }
            }

            //&&&&&&&&&&&&&&&&&&&&&&
            // setup OR link helper
            //&&&&&&&&&&&&&&&&&&&&&&
            //If the jet failed OR, a decoration is set describing what container didn't pass.
            //However, so far the overlapLinkHelper->getObjectLink always returns a null pointer. 
            //TODO: why does this not work?
            if( inListOfDetailedObjects("detailedOR") ){
            
                const xAOD::IParticle* ipart = static_cast<const xAOD::IParticle*>( jet );
                if( (int)jet->auxdata<char>("passOR") != 1 ){
                    if( ipart ){
                        const xAOD::IParticle* linkParticle = m_overlapLinkHelper->getObjectLink( *ipart );
                        if( linkParticle ){

                            std::cout<<"## RVC_dv ##:linkParticle->type: "<<linkParticle->type()<<std::endl;
                            //TODO: if this particle link ever resolves, this may be a way to do it.
                            //Possibly the type enum defined like these: https://gitlab.cern.ch/atlas/athena/blob/21.2/Event/xAOD/xAODJet/Root/Jet_v1.cxx#L98

                        } else {
                            ////std::cout<<"Particle link is NULL."<<std::endl;
                        }
                    } else {
                        //std::cout<<"ipart pointer is NULL"<<std::endl;
                    }
                }
            }
            }//END loop calibJets
        }//END if customJetCleaning

        //&&&&&&&&&&&&&&&&&&&&&
        // L E P T O N  S F
        //&&&&&&&&&&&&&&&&&&&&&
        vars["muSF"] = eventInfo->auxdecor<float>("muSF");

        for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("muSF_systs")) {
            vars["muSF_"+systName+""] = eventInfo->auxdecor<float>("muSF_"+systName);
        }

        vars["elSF"] = eventInfo->auxdecor<float>("elSF");

        for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("elSF_systs")){
            vars["elSF_"+systName+""] = eventInfo->auxdecor<float>("elSF_"+systName);
        }

        double MEff = 0;
        double HT = 0;

        for( const auto& jet : *selectedJets) {
            HT += toGeV(jet->pt());
        }

        MEff = HT + toGeV((*metcont)["Final"]->met());

        vars["MEff"] = MEff;
        vars["HT"] = HT;

    vars["WZweight"] = eventInfo->auxdecor<float>("WZweight");


    //&&&&&&&&&&&&&&&&&&&&&
    // P V   &   D V 
    //&&&&&&&&&&&&&&&&&&&&&
    //////////////////////////////////////////////////
    // get the highest-sumpT primary vertex (one is needed for recalculating MET)
    // const xAOD::VertexContainer* pvc = nullptr;
    // STRONG_CHECK( store->retrieve(pvc, "selectedPV") );

    // auto pv = pvc->at(0);
    // vars["PV_x"] = pv->x();
    // vars["PV_y"] = pv->y();
    // vars["PV_z"] =  pv->z();
    // vars["PV_rxy"] =  TMath::Hypot(pv->x(),pv->y());
    // vars["PV_nTracks"] = pv->nTrackParticles();
    // vars["PV_sumpT2"] = pv->auxdataConst<float>( "sumPt2" );
    // vars["PV_n"] = nPV;

    // ///////////////////////////////////////////////////////////////

    // const xAOD::VertexContainer* dvc = nullptr;
    // STRONG_CHECK( event->retrieve(dvc, "VrtSecInclusive_SecondaryVertices") );

    // std::map<int,int> TrkToVtxMap;

    // int nDV = 0;
    // for (const auto& dv: *dvc){

    //     addToVectorBranch( vars, "DV_index"      , dv->index()) ;
    //     addToVectorBranch( vars, "DV_x"          , dv->x()) ;
    //     addToVectorBranch( vars, "DV_y"          , dv->y()) ;
    //     addToVectorBranch( vars, "DV_z"          , dv->z()) ;
    //     addToVectorBranch( vars, "DV_r"          , dv->position().mag()) ;
    //     addToVectorBranch( vars, "DV_rxy"        , dv->position().perp()) ;
    //     addToVectorBranch( vars, "DV_m"          , toGeV( dv->auxdataConst<float>("visibleMass") ) ) ;
    //     addToVectorBranch( vars, "DV_mass"          , toGeV(dv->auxdataConst<float>("mass") ) ) ;
    //     addToVectorBranch( vars, "DV_mass_selected" , toGeV(dv->auxdataConst<float>("mass_selectedTracks") ) ) ;
    //     addToVectorBranch( vars, "DV_nTracks"    , dv->auxdataConst<int>("nTracks") ) ;
    //     addToVectorBranch( vars, "DV_nLRT"       , dv->auxdataConst<int>("nTracksLRT") ) ;
    //     addToVectorBranch( vars, "DV_chisqPerDoF", dv->chiSquared() / dv->numberDoF()  ) ;

    //     //&&&&&&&&&&&&&&&&&&&&&
    //     // D V   T R A C K S 
    //     //&&&&&&&&&&&&&&&&&&&&&
    //     if(inListOfDetailedObjects("dvTracks")){
    //         auto tpLinks = dv->trackParticleLinks();
    //         for (auto link : tpLinks) {
    //             const xAOD::TrackParticle *trk = (*link);

    //             // Retrieve DV Mapping info
    //             addToVectorBranch( vars, "track_isLRT"   , static_cast<int>(trk->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) );
    //             addToVectorBranch( vars, "track_index"   , trk->index() );
    //             addToVectorBranch( vars, "track_DVIndex" , dv->index()  );
    //             addToVectorBranch( vars, "track_DVIndexOld" ,  ( TrkToVtxMap.find(trk->index()) == TrkToVtxMap.end() ) ? -99 : TrkToVtxMap[trk->index()] );
    //             addToVectorBranch( vars, "track_eta"     , trk->eta() );
    //             addToVectorBranch( vars, "track_phi"     , trk->phi() );
    //             addToVectorBranch( vars, "track_pt"      , toGeV( trk->pt() ) );
    //             addToVectorBranch( vars, "track_d0"      , trk->d0() );
    //             addToVectorBranch( vars, "track_z0"      , trk->z0() );
    //             addToVectorBranch( vars, "track_charge"      , trk->charge() );

    //             // From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
    //             double pt = trk->pt();
    //             double diff_qp = -pt / std::fabs(trk->qOverP());
    //             double diff_theta = pt / tan(trk->theta());
    //             const std::vector<float> &cov = trk->definingParametersCovMatrixVec();
    //             double pt_err2 = diff_qp * (diff_qp * cov[14] + diff_theta * cov[13]) + diff_theta * diff_theta * cov[9];
    //             double errpT = toGeV( TMath::Sqrt(pt_err2) );

    //             addToVectorBranch( vars, "track_errPt"   , errpT );
    //             addToVectorBranch( vars, "track_errd0"   , TMath::Sqrt(trk->definingParametersCovMatrix()(0, 0)) );
    //             addToVectorBranch( vars, "track_errz0"   , TMath::Sqrt(trk->definingParametersCovMatrix()(1, 1)) );

    //             addToVectorBranch( vars, "track_etaWrtDV"   , trk->auxdataConst<float>("eta_wrtSV") );
    //             addToVectorBranch( vars, "track_phiWrtDV"   , trk->auxdataConst<float>("phi_wrtSV") );
    //             addToVectorBranch( vars, "track_ptWrtDV"    , toGeV( trk->auxdataConst<float>("pt_wrtSV") ) );
    //             addToVectorBranch( vars, "track_d0WrtDV"    , trk->auxdataConst<float>("d0_wrtSV")  );
    //             addToVectorBranch( vars, "track_z0WrtDV"    , trk->auxdataConst<float>("z0_wrtSV")  );
    //             addToVectorBranch( vars, "track_errd0WrtDV" , trk->auxdataConst<float>("errd0_wrtSV") );
    //             addToVectorBranch( vars, "track_errz0WrtDV" , trk->auxdataConst<float>("errz0_wrtSV") );
    //             addToVectorBranch( vars, "track_RadFirstHit",   trk->auxdataConst<float>("radiusOfFirstHit") );
    //             addToVectorBranch( vars, "track_isAssociated", (trk->isAvailable<char>("is_associated")) ? trk->auxdataConst<char>("is_associated") : false  );
    //             addToVectorBranch( vars, "track_NPixHits"       , trk->auxdata< unsigned char >("numberOfPixelHits"       ) );
    //             addToVectorBranch( vars, "track_NSctHits"       , trk->auxdata< unsigned char >("numberOfSCTHits"         ) );
    //             addToVectorBranch( vars, "track_NTrtHits"       , trk->auxdata< unsigned char >("numberOfTRTHits"         ) );
    //             addToVectorBranch( vars, "track_NPixHoles"      , trk->auxdata< unsigned char >("numberOfPixelHoles"      ) );
    //             addToVectorBranch( vars, "track_NSctHoles"      , trk->auxdata< unsigned char >("numberOfSCTHoles"        ) );
    //             addToVectorBranch( vars, "track_NTrtOutliers"   , trk->auxdata< unsigned char >("numberOfTRTOutliers"     ) );
    //             addToVectorBranch( vars, "track_NPixDeadSens"   , trk->auxdata< unsigned char >("numberOfPixelDeadSensors") );
    //             addToVectorBranch( vars, "track_NPixSharedHits" , trk->auxdata< unsigned char >("numberOfPixelSharedHits" ) );
    //             addToVectorBranch( vars, "track_NSctDeadSens"   , trk->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) );
    //             addToVectorBranch( vars, "track_NSctSharedHits" , trk->auxdata< unsigned char >("numberOfSCTSharedHits"   ) );
    //             addToVectorBranch( vars, "track_hitPattern" , trk->auxdata< unsigned int >("hitPattern"   ) );

    //         }
    //     }

    //     nDV++;

    //     if (!dv->auxdataConst<bool>("passFiducialCuts") ){ addToVectorBranch(vars,"DV_lastCut",0); continue;}
    //     if (!dv->auxdataConst<bool>("passChisqCut")     ){ addToVectorBranch(vars,"DV_lastCut",1); continue;}
    //     if (!dv->auxdataConst<bool>("passDistCut")      ){ addToVectorBranch(vars,"DV_lastCut",2); continue;}
    //     if (!dv->auxdataConst<bool>("passMaterialVeto") ){ addToVectorBranch(vars,"DV_lastCut",3); continue;}
    //     if (!dv->auxdataConst<bool>("passMassCut")      ){ addToVectorBranch(vars,"DV_lastCut",4); continue;}
    //     if (!dv->auxdataConst<bool>("passNtrkCut")      ){ addToVectorBranch(vars,"DV_lastCut",5); continue;}
    //     addToVectorBranch(vars,"DV_lastCut",-1);
    // }

    // vars["DV_n"] = nDV;

    //&&&&&&&&&&&&&&&
    // L E P T O N S
    //&&&&&&&&&&&&&&&
    ///////////////////////////////////////////////////////////////
    //to do, write out nominal leptons for SRMET & SR2L
    //xAOD::IParticleContainer* muons_nominal(nullptr);
    //STRONG_CHECK(store->retrieve(muons_nominal, "selectedElectrons"));
    //xAOD::IParticleContainer* electrons_nominal(nullptr);
    //STRONG_CHECK(store->retrieve(electrons_nominal, "selectedMuons"));

    vars["passHLTmsonly"] = eventInfo->auxdecor<bool>("passHLTmsonly");
    vars["passMuonTrigger"] = eventInfo->auxdecor<bool>("isMuonTrigPassed");
    //maps to IS & MS tracks
    std::map<int,int> IDtrkToMuonMap;
    std::map<int,int> MStrkToMuonMap;



    auto electrons_nominal = HF::grabFromStore<xAOD::ElectronContainer>("STCalibElectrons",store);

    for( const auto& lep : *electrons_nominal) {
        addToVectorBranch( vars, "el_Pt"        ,  toGeV(lep->pt()));
        addToVectorBranch( vars, "el_Eta"       ,  lep->p4().Eta() );
        addToVectorBranch( vars, "el_Phi"       ,  lep->p4().Phi() );
        // addToVectorBranch( vars, "el_Sign"      ,  lep->charge() * 11. );
        addToVectorBranch( vars, "el_d0"        ,  lep->trackParticle(0)->d0() );
        addToVectorBranch( vars, "el_Baseline"  ,  (int)lep->auxdata<char>("baseline") );
        addToVectorBranch( vars, "el_Signal"    ,  (int)lep->auxdata<char>("signal") );
        // addToVectorBranch( vars, "elPassOR"    ,  (int)lep->auxdata<char>("passOR") );
    }


    // ID DV mapping :/

    auto muons_baseline = HF::grabFromStore<xAOD::IParticleContainer>("selectedBaselineMuons",store);

    //std::cout << "Looping through selectedBaselineMuons " 	<< std::endl;
    if( inListOfDetailedObjects("muons_baseline") ){
    for( xAOD::IParticle * muon : *muons_baseline){

        xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

        addToVectorBranch( vars, "muon_index"  , mu->index());
        addToVectorBranch( vars, "muon_pt"     , toGeV(mu->pt()));
        addToVectorBranch( vars, "muon_eta"    , mu->p4().Eta());
        addToVectorBranch( vars, "muon_phi"    , mu->p4().Phi());
        addToVectorBranch( vars, "muon_charge" , mu->charge() );

        if(inListOfDetailedObjects("muons")){

            const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
            const xAOD::TrackParticle* metrack = mu->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
            const xAOD::TrackParticle* mstrack = mu->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
            const xAOD::TrackParticle* cbtrack = mu->trackParticle( xAOD::Muon::CombinedTrackParticle );

            if (idtrack) IDtrkToMuonMap[ (idtrack)->index() ] = mu->index();
            if (mstrack) MStrkToMuonMap[ (mstrack)->index() ] = mu->index();

            addToVectorBranch( vars, "muon_hasCBtrack" , (cbtrack) ? 1 : 0 );
            addToVectorBranch( vars, "muon_hasIDtrack" , (idtrack) ? 1 : 0 );
            addToVectorBranch( vars, "muon_hasMEtrack" , (metrack) ? 1 : 0 );
            addToVectorBranch( vars, "muon_hasMStrack" , (mstrack) ? 1 : 0 );

            addToVectorBranch( vars, "muon_d0"      , (idtrack) ? idtrack->d0() : -9999.);
            addToVectorBranch( vars, "muon_z0"      , (idtrack) ? idtrack->z0() : -9999.);
            addToVectorBranch( vars, "muon_isLRT"   , (idtrack) ? static_cast<int>(idtrack->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0)) : 0 );
            addToVectorBranch( vars, "muon_RadFirstHit" , (idtrack) ? idtrack->auxdata< float >("radiusOfFirstHit" ) : -999. );

            addToVectorBranch( vars, "muon_ptcone20"    , toGeV( mu->auxdata< float >("ptcone20")     ) );
            addToVectorBranch( vars, "muon_ptcone30"    , toGeV( mu->auxdata< float >("ptcone30")     ) );
            addToVectorBranch( vars, "muon_ptcone40"    , toGeV( mu->auxdata< float >("ptcone40")     ) );
            addToVectorBranch( vars, "muon_ptvarcone20" , toGeV( mu->auxdata< float >("ptvarcone20")  ) );
            addToVectorBranch( vars, "muon_ptvarcone30" , toGeV( mu->auxdata< float >("ptvarcone30")  ) );
            addToVectorBranch( vars, "muon_ptvarcone40" , toGeV( mu->auxdata< float >("ptvarcone40")  ) );
            addToVectorBranch( vars, "muon_topoetcone20", toGeV( mu->auxdata< float >("topoetcone20") ) );
            addToVectorBranch( vars, "muon_topoetcone30", toGeV( mu->auxdata< float >("topoetcone30") ) );
            addToVectorBranch( vars, "muon_topoetcone40", toGeV( mu->auxdata< float >("topoetcone40") ) );

            addToVectorBranch( vars, "muon_nPIX"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfPixelHits") : -999);
            addToVectorBranch( vars, "muon_nSCT"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfSCTHits")   : -999);
            addToVectorBranch( vars, "muon_nTRT"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfTRTHits")   : -999);

            addToVectorBranch( vars, "muon_nPres"      , mu->auxdata< unsigned char >("numberOfPrecisionLayers") );
            addToVectorBranch( vars, "muon_nPresGood"  , mu->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
            addToVectorBranch( vars, "muon_nPresHole"  , mu->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );

            addToVectorBranch( vars, "muon_CBchi2" , (cbtrack) ? cbtrack->auxdata< float >("chiSquared")/(cbtrack->auxdata< float >("numberDoF")) : -999. );

            float qOverPsigma  = -999.;
            float qOverPsignif = -999.;
            float rho = -999.;
            if ( (idtrack) && (metrack) && (cbtrack) ){
                float cbPt = cbtrack->pt();
                float idPt = idtrack->pt();
                float mePt = metrack->pt();
                float meP  = 1.0 / ( sin(metrack->theta()) / mePt);
                float idP  = 1.0 / ( sin(idtrack->theta()) / idPt);

                rho = fabs( idPt - mePt ) / cbPt;
                qOverPsigma  = sqrt( idtrack->definingParametersCovMatrix()(4,4) + metrack->definingParametersCovMatrix()(4,4) );
                qOverPsignif  = fabs( (metrack->charge() / meP) - (idtrack->charge() / idP) ) / qOverPsigma;
            }
            addToVectorBranch( vars, "muon_rho"   , rho );
            addToVectorBranch( vars, "muon_QoverPsignif", qOverPsignif );

            addToVectorBranch( vars, "muon_author" ,     mu->auxdata< unsigned short >("author") );
            addToVectorBranch( vars, "muon_isCommonGood",mu->auxdata< char >("DFCommonGoodMuon") );
            addToVectorBranch( vars, "muon_isSignal",    mu->auxdata< char >("signal") );
            addToVectorBranch( vars, "muon_truthType",   mu->auxdata< int >("truthType") );
            addToVectorBranch( vars, "muon_truthOrigin", mu->auxdata< int >("truthOrigin") );

            // // Retrieve DV Mapping info, 3 attempts
            // float dv_index = -99;
            // if ( idtrack && dvc->size() > 0) {
            //     if ( TrkToVtxMap.find(idtrack->index()) != TrkToVtxMap.end() ) dv_index = TrkToVtxMap[idtrack->index()];
            // }

            // addToVectorBranch( vars, "muon_DVindex"  ,  dv_index );
            addToVectorBranch( vars, "muon_TrigMatch"      ,      mu->auxdata< int >( "passTM" ) );
            addToVectorBranch( vars, "muon_TrigMatchMSOnly",            mu->auxdata< int >( "passTMmsonly" ) );
        }
    }
    }

    if(inListOfDetailedObjects("msTracks")){

        auto muons = HF::grabFromEvent<xAOD::MuonContainer>("Muons",event);

        for ( auto muon : *muons ){

            const xAOD::TrackParticle* mst = muon->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
            if (mst) {

                //std::cout << "MS Track " << mst->index() << " : eta " << mst->eta() << " : phi " << mst->phi() << " : pt " << mst->pt() << std::endl;
                //if (MStrkToMuonMap.find(mst->index()) == MStrkToMuonMap.end()) std::cout << "found standalone" << std::endl;
                //else std::cout << "match cb muons" << std::endl;

                addToVectorBranch( vars, "mstrack_MuonIndex" ,  ( MStrkToMuonMap.find(mst->index()) == MStrkToMuonMap.end() ) ? -99 : MStrkToMuonMap[mst->index()] );
                addToVectorBranch( vars, "mstrack_eta" ,    mst->eta() );
                addToVectorBranch( vars, "mstrack_phi" ,    mst->phi() );
                addToVectorBranch( vars, "mstrack_pt"  ,    toGeV( mst->pt() )  );
                addToVectorBranch( vars, "mstrack_D0"  ,    mst->d0()  );
                addToVectorBranch( vars, "mstrack_Z0"  ,    mst->z0()  );
                addToVectorBranch( vars, "mstrack_ELoss" ,            toGeV(  muon->auxdata<float>("EnergyLoss")   	     ) );
                addToVectorBranch( vars, "mstrack_ELossSigma" ,       toGeV(  muon->auxdata<float>("EnergyLossSigma") 	     ) );
                addToVectorBranch( vars, "mstrack_MeasELoss" ,        toGeV(  muon->auxdata<float>("MeasEnergyLoss") 	     ) );
                addToVectorBranch( vars, "mstrack_MeasELossSigma" ,   toGeV(  muon->auxdata<float>("MeasEnergyLossSigma")      ) );
                addToVectorBranch( vars, "mstrack_ParamELoss" ,       toGeV(  muon->auxdata<float>("ParamEnergyLoss")          ) );
                addToVectorBranch( vars, "mstrack_ParamELossSigmaM" , toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaMinus")) );
                addToVectorBranch( vars, "mstrack_ParamELossSigmaP" , toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaPlus") ) );
                addToVectorBranch( vars, "mstrack_nPres" ,      muon->auxdata< unsigned char >("numberOfPrecisionLayers") );
                addToVectorBranch( vars, "mstrack_nPresGood" ,  muon->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
                addToVectorBranch( vars, "mstrack_nPresHole" ,  muon->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );
            }
        }

    }

    //&&&&&&&&&&&&&&&&&&
    // I D  T R A C K S
    //&&&&&&&&&&&&&&&&&&
    ///////////////////////////////////////////////////////////////
    //write out inner detector tracks
    //to do: only do this if they're opposite a CB muon?
    //or a right angle away? for bkg estimation

    if(inListOfDetailedObjects("idTracks")){

        auto id_tracks = HF::grabFromEvent<xAOD::TrackParticleContainer>("InDetTrackParticles",event);

        addToVectorBranch( vars, "n_idTracks" , id_tracks->size() );

	for (auto *id_track : *id_tracks){

	  const auto* truth = getTruthParticle( id_track );
	  if( !truth ) continue;
  
	  if( (std::abs( truth->pdgId() ) <= 1000000) || (std::abs( truth->pdgId() ) > 3000000) ) continue;

	  //const xAOD::IParticle* ipart = static_cast<const xAOD::IParticle*>( id_track );
	  //std::vector<const xAOD::MissingETAssociation*> assoc = xAOD::MissingETComposition::getAssociations(metMap,ipart);
	  //if (assoc.size() < 1) continue;

            addToVectorBranch( vars, "idTrack_index" , id_track->index() );
            addToVectorBranch( vars, "idTrack_isLRT" , static_cast<int>( id_track->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) );


            // From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
            double id_pt = id_track->pt();
            double id_diff_qp = -id_pt / std::fabs(id_track->qOverP());
            double id_diff_theta = id_pt / tan(id_track->theta());
            const std::vector<float> &id_cov = id_track->definingParametersCovMatrixVec();
            double id_pt_err2 = id_diff_qp * (id_diff_qp * id_cov[14] + id_diff_theta * id_cov[13]) + id_diff_theta * id_diff_theta * id_cov[9];
            double id_errpT = toGeV( TMath::Sqrt(id_pt_err2) );

            addToVectorBranch( vars, "idTrack_errPt"   , id_errpT );
            addToVectorBranch( vars, "idTrack_errd0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(0, 0)) );
            addToVectorBranch( vars, "idTrack_errz0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(1, 1)) );

            addToVectorBranch( vars, "idTrack_theta"   ,id_track->theta() );
            addToVectorBranch( vars, "idTrack_eta"     ,id_track->eta() );
            addToVectorBranch( vars, "idTrack_phi"     ,id_track->phi() );
            addToVectorBranch( vars, "idTrack_pt"      ,toGeV( id_track->pt() ) );
            addToVectorBranch( vars, "idTrack_d0"      ,id_track->d0()  );
            addToVectorBranch( vars, "idTrack_z0"      ,id_track->z0()  );
            //addToVectorBranch( vars, "idTrack_z0WrtPV" ,id_track->z0() + id_track->vz() - pv->z() );
            addToVectorBranch( vars, "idTrack_charge"  ,id_track->charge() );
            addToVectorBranch( vars, "idTrack_chi2"    ,id_track->auxdata< float >("chiSquared")/(id_track->auxdata< float >("numberDoF")) );

            addToVectorBranch( vars, "idTrack_RadFirstHit"     ,id_track->auxdata< float >("radiusOfFirstHit"    ) );

            addToVectorBranch( vars, "idTrack_NPix_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfPixelHits"       ) );
            addToVectorBranch( vars, "idTrack_NSct_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfSCTHits"         ) );
            addToVectorBranch( vars, "idTrack_NTrt_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfTRTHits"         ) );
            addToVectorBranch( vars, "idTrack_NPix_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfPixelHoles"      ) );
            addToVectorBranch( vars, "idTrack_NSct_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfSCTHoles"        ) );
            addToVectorBranch( vars, "idTrack_NTrt_Outliers"   ,(int) id_track->auxdata< unsigned char >("numberOfTRTOutliers"     ) );
            addToVectorBranch( vars, "idTrack_NPix_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfPixelDeadSensors") );
            addToVectorBranch( vars, "idTrack_NPix_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfPixelSharedHits" ) );
            addToVectorBranch( vars, "idTrack_NSct_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) );
            addToVectorBranch( vars, "idTrack_NSct_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfSCTSharedHits"   ) );
	    // ann added
            addToVectorBranch( vars, "idTrack_NIbl_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfInnermostPixelLayerHits"   ) );
	    addToVectorBranch( vars, "idTrack_pixeldEdx"            , id_track->auxdata< float >("pixeldEdx" ) );
	    addToVectorBranch( vars, "idTrack_NIbl_OverflowdEdx"       ,(int) id_track->auxdata< unsigned char >("numberOfIBLOverflowsdEdx" ) );
	    addToVectorBranch( vars, "idTrack_NUsedHitsdEdx"       ,(int) id_track->auxdata< unsigned char >("numberOfUsedHitsdEdx" ) );
        }

    }// end if save id tracks

    //&&&&&&&&&&&&&&&&&&&&&&
    // M S  S E G M E N T S
    //&&&&&&&&&&&&&&&&&&&&&&
    if(inListOfDetailedObjects("msSegments")){

	auto ms_segments = HF::grabFromEvent<xAOD::MuonSegmentContainer>("MuonSegments",event);

        for (auto *ms_segment : *ms_segments){

            addToVectorBranch( vars, "msSegment_x"            , ms_segment->x() );
            addToVectorBranch( vars, "msSegment_y"            , ms_segment->y() );
            addToVectorBranch( vars, "msSegment_z"            , ms_segment->z() );
            addToVectorBranch( vars, "msSegment_px"           , ms_segment->px() );
            addToVectorBranch( vars, "msSegment_py"           , ms_segment->py() );
            addToVectorBranch( vars, "msSegment_pz"           , ms_segment->pz() );
            addToVectorBranch( vars, "msSegment_t0"           , ms_segment->auxdataConst< float >( "t0" ) );
            addToVectorBranch( vars, "msSegment_t0Err"        , ms_segment->auxdataConst< float >( "t0error"      ) );
            addToVectorBranch( vars, "msSegment_clusTimeErr"  , ms_segment->auxdataConst< float >( "clusterTimeError" ) );
            addToVectorBranch( vars, "msSegment_clusTime"     , ms_segment->auxdataConst< float >( "clusterTime"      ) );
            addToVectorBranch( vars, "msSegment_chmbIndex"    , ms_segment->auxdataConst< int >( "chamberIndex" ) );
            addToVectorBranch( vars, "msSegment_tech"         , ms_segment->auxdataConst< int >( "technology"   ) );
            addToVectorBranch( vars, "msSegment_sector"       , ms_segment->auxdataConst< int >( "sector"       ) );
            addToVectorBranch( vars, "msSegment_etaIndex"     , ms_segment->auxdataConst< int >( "etaIndex"     ) );
            addToVectorBranch( vars, "msSegment_nTrigEtaLays" , ms_segment->auxdataConst< int >( "nTrigEtaLayers" ) );
            addToVectorBranch( vars, "msSegment_nPhiLays"     , ms_segment->auxdataConst< int >( "nPhiLayers"     ) );
            addToVectorBranch( vars, "msSegment_nPresHits"    , ms_segment->auxdataConst< int >( "nPrecisionHits" ) );

        }

    }//end doCosmic

    //&&&&&&&&&&&
    // T R U T H 
    //&&&&&&&&&&&
    //TODO: there is no check here to see if the particles looped over are 'final state' or not. Is this inherent at AOD level? 
    ///////////////////////////////////////////////////////////////
    if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

        auto truthEvents = HF::grabFromEvent<xAOD::TruthEventContainer>("TruthEvents",event);
        auto trueSignalVertex = *((*truthEvents->begin())->truthVertexLink(0));

	auto truthParticles = HF::grabFromEvent<xAOD::TruthParticleContainer>("TruthParticles",event);
        for (const auto& p : *truthParticles){
	  continue;
            if(p->pdgId()>1e6 && p->hasDecayVtx()) {

                Int_t nCharged = 0;
                Int_t nCharged1GeV = 0;
                TLorentzVector chargedParticleFourVector;
                for(size_t ii = 0; ii < p->decayVtx()->nOutgoingParticles(); ++ii){
                    if ( p->decayVtx()->outgoingParticle(ii)->charge() ){
                        nCharged++;
                        if( toGeV( p->decayVtx()->outgoingParticle(ii)->pt() ) > 1.  ) {
                            nCharged1GeV++;
                        }
                        chargedParticleFourVector +=  p->decayVtx()->outgoingParticle(ii)->p4();
                    }
                }
                float lifetimeLab = (p->decayVtx()->v4().Vect()-trueSignalVertex->v4().Vect()).Mag()/(p->p4().Vect().Mag()/(p->p4().Gamma()*p->p4().M())*TMath::C());

                addToVectorBranch( vars, "truthSparticle_PdgId"               , (int) p->pdgId() );
                addToVectorBranch( vars, "truthSparticle_Pt"                  , toGeV(p->pt() )  );
                addToVectorBranch( vars, "truthSparticle_Eta"                 , p->eta()         );
                addToVectorBranch( vars, "truthSparticle_Phi"                 , p->phi()         );
                addToVectorBranch( vars, "truthSparticle_M"                   , toGeV(p->m())    );
                addToVectorBranch( vars, "truthSparticle_BetaGamma"           , p->p4().Beta()*p->p4().Gamma()  );
                addToVectorBranch( vars, "truthSparticle_ProperDecayTime"     , lifetimeLab*p->p4().Gamma()  );
                addToVectorBranch( vars, "truthSparticle_VtxX"                , p->decayVtx()->x()  );
                addToVectorBranch( vars, "truthSparticle_VtxY"                , p->decayVtx()->y()  );
                addToVectorBranch( vars, "truthSparticle_VtxZ"                , p->decayVtx()->z()  );
                addToVectorBranch( vars, "truthSparticle_VtxNParticles"       , p->decayVtx()->nOutgoingParticles()  );
                addToVectorBranch( vars, "truthSparticle_VtxNChParticles"     , nCharged  );
                addToVectorBranch( vars, "truthSparticle_VtxNChParticles1GeV" , nCharged1GeV  );
                addToVectorBranch( vars, "truthSparticle_VtxMChParticles"     , toGeV(chargedParticleFourVector.M() )  );
            }
        }
    }	

    return EL::StatusCode::SUCCESS;
}

// EL::StatusCode RegionVarCalculator_dedx::doOtherSRCalculations(std::map<std::string, anytype>& /*vars*/ ) {
// 	return EL::StatusCode::SUCCESS;
// }

//  LocalWords:  vertices
