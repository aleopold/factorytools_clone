#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/SelectDEDxEvents.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"


#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"


#include <boost/algorithm/string.hpp>


// this is needed to distribute the algorithm to the workers
ClassImp(SelectDEDxEvents)



SelectDEDxEvents :: SelectDEDxEvents ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode SelectDEDxEvents :: setupJob (EL::Job& /*job*/)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: execute ()
{

  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  // If the event didn't pass the preselection alg, don't bother doing anything with it...
  std::string preselectedRegionName =  eventInfo->auxdecor< std::string >("regionName");
  ATH_MSG_DEBUG("EventNumber: " << eventInfo->eventNumber()  );
  ATH_MSG_DEBUG("Preselected?: " << preselectedRegionName  );

  if( preselectedRegionName == "" ) return EL::StatusCode::SUCCESS;

  std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> selectedMuons( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
  selectedMuons.first->setStore(selectedMuons.second);

  std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> selectedBaselineMuons( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
  selectedBaselineMuons.first->setStore(selectedBaselineMuons.second);

  std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> selectedElectrons( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
  selectedElectrons.first->setStore(selectedElectrons.second);

  std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> selectedBaselineElectrons( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
  selectedBaselineElectrons.first->setStore(selectedBaselineElectrons.second);

  std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> selectedPhotons( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
  selectedPhotons.first->setStore(selectedPhotons.second);

  std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> selectedJets   ( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
  selectedJets.first->setStore(selectedJets.second);


  STRONG_CHECK( store->record( selectedMuons.first  , "selectedMuons"    ) );//todo configurable if needed
  STRONG_CHECK( store->record( selectedMuons.second , "selectedMuonsAux."    ) );//todo configurable if needed

  STRONG_CHECK( store->record( selectedBaselineMuons.first  , "selectedBaselineMuons"    ) );//todo configurable if needed
  STRONG_CHECK( store->record( selectedBaselineMuons.second , "selectedBaselineMuonsAux."    ) );//todo configurable if needed

  STRONG_CHECK( store->record( selectedElectrons.first  , "selectedElectrons"    ) );//todo configurable if needed
  STRONG_CHECK( store->record( selectedElectrons.second , "selectedElectronsAux."    ) );//todo configurable if needed

  STRONG_CHECK( store->record( selectedBaselineElectrons.first  , "selectedBaselineElectrons"    ) );//todo configurable if needed
  STRONG_CHECK( store->record( selectedBaselineElectrons.second , "selectedBaselineElectronsAux."    ) );//todo configurable if needed

  STRONG_CHECK( store->record( selectedPhotons.first  , "selectedPhotons"    ) );//todo configurable if needed
  STRONG_CHECK( store->record( selectedPhotons.second , "selectedPhotonsAux."    ) );//todo configurable if needed

  STRONG_CHECK( store->record( selectedJets.first     , "selectedJets"    ) );//todo configurable if needed
  STRONG_CHECK( store->record( selectedJets.second    , "selectedJetsAux."    ) );//todo configurable if needed


  xAOD::JetContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal, "STCalibAntiKt4EMTopoJets"));

  xAOD::MuonContainer* muons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(muons_nominal, "STCalibMuons"));

  xAOD::ElectronContainer* electrons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(electrons_nominal, "STCalibElectrons"));

  xAOD::PhotonContainer* photons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(photons_nominal, "STCalibPhotons"));



  for (const auto& jet : *jets_nominal) {
    if ((int)jet->auxdata<char>("baseline") == 0) continue;
    if ((int)jet->auxdata<char>("passOR") != 1) continue;
    if ((int)jet->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful jet
    ATH_MSG_VERBOSE( "jet pt : " << jet->pt() );

    selectedJets.first->push_back(jet  );
  }

  for (const auto& mu : *muons_nominal) {
    if ((int)mu->auxdata<char>("baseline") == 0) continue;
    if ((int)mu->auxdata<char>("passOR") != 1) continue;

    selectedBaselineMuons.first->push_back( mu );

    if ((int)mu->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful muon
    ATH_MSG_VERBOSE( "mu pt : " << mu->pt() );

    selectedMuons.first->push_back( mu );
  }

  for (const auto& el : *electrons_nominal) {
    if ((int)el->auxdata<char>("baseline") == 0) continue;
    if ((int)el->auxdata<char>("passOR") != 1) continue;

    selectedBaselineElectrons.first->push_back( el );

    if ((int)el->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful el
    ATH_MSG_VERBOSE( "el pt : " << el->pt() );

    selectedElectrons.first->push_back( el );
  }


  for (const auto& ph : *photons_nominal) {
    if ((int)ph->auxdata<char>("baseline") == 0) continue;
    if ((int)ph->auxdata<char>("passOR") != 1) continue;
    if ((int)ph->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful el
    ATH_MSG_VERBOSE( "ph pt : " << ph->pt() );

    selectedPhotons.first->push_back( ph );
  }

  int const nBaselineMuons = selectedBaselineMuons.first->size();
  int const nBaselineElectrons = selectedBaselineElectrons.first->size();
  int const nMuons = selectedMuons.first->size();
  int const nElectrons = selectedElectrons.first->size();
  //int const nLeptons = nMuons + nElectrons;
  //int const nBaselineLeptons = nBaselineMuons + nBaselineElectrons;
  int const nPhotons = selectedPhotons.first->size();

  ATH_MSG_DEBUG("Number of Selected Baseline Muons: " << nBaselineMuons );
  ATH_MSG_DEBUG("Number of Selected Baseline Electrons: " << nBaselineElectrons );
  ATH_MSG_DEBUG("Number of Selected Muons: " << nMuons  );
  ATH_MSG_DEBUG("Number of Selected Electrons: " << nElectrons  );
  ATH_MSG_DEBUG("Number of Selected Photons: " << nPhotons  );

  // Trigger ///////////////////

  /*
  bool passTM = false;
  for (auto muon: *selectedMuons.first){
    if(muon->auxdecor< int >( "passTM" ) ) passTM = true;
  }
  for (auto electron: *selectedElectrons.first){
    if(electron->auxdecor< int >( "passTM" ) ) passTM = true;
  }
  */

  auto trigORFromString = [](std::vector< std::string > passTrigs, std::string trigString){
    boost::replace_all(trigString, "_OR_", ":");
    std::vector<std::string> trigVect;
    boost::split(trigVect,trigString,boost::is_any_of(":") );
    bool trigDecision = 0;
    for(auto iTrig : trigVect){
      trigDecision |= std::find(passTrigs.begin(), passTrigs.end(), iTrig ) != passTrigs.end();
    }
    return trigDecision;
  };



  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::map<std::string,int> passedTriggers;
  passedTriggers["HLT_g140_loose"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_g140_loose") != passTrigs.end();
  passedTriggers["HLT_mu60_0eta105_msonly"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu60_0eta105_msonly") != passTrigs.end();
  // for cosmic run
  passedTriggers["HLT_mu4_msonly_cosmic_L1MU11_EMPTY"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_msonly_cosmic_L1MU11_EMPTY") != passTrigs.end();
  passedTriggers["HLT_mu4_msonly_cosmic_L1MU4_EMPTY"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_msonly_cosmic_L1MU4_EMPTY") != passTrigs.end();

  if(eventInfo->auxdecor<float>("year")==2015){
    passedTriggers["MET"] = passedTriggers["HLT_xe70"];
    passedTriggers["Electron"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("elTrig2015") );
    passedTriggers["Muon"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("muTrig2015") );
    passedTriggers["MSOnly"] = passedTriggers["HLT_mu60_0eta105_msonly"];
    passedTriggers["Photon"] = passedTriggers["HLT_g140_loose"];
  } else {
    passedTriggers["MET"] = trigORFromString(passTrigs,"HLT_xe100_mht_L1XE50_OR_HLT_xe110_mht_L1XE50");
    passedTriggers["Electron"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("elTrig2016") );
    passedTriggers["Muon"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("muTrig2016") );
    passedTriggers["MSOnly"] = passedTriggers["HLT_mu60_0eta105_msonly"];
    passedTriggers["Photon"] = passedTriggers["HLT_g140_loose"];
  }

  eventInfo->auxdecor<bool>("isMuonTrigPassed") = passedTriggers["Muon"]; 
  //eventInfo->auxdecor<bool>("isMETTrigPassed") = passedTriggers["MET"]; 
  // what is going on here
  passedTriggers["MET"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
  eventInfo->auxdecor<bool>("passHLTmsonly") = passedTriggers["MSOnly"];
  eventInfo->auxdecor<bool>("passHLTmu4msonly_cosmic_L1MU11") = passedTriggers["MSOnly"];
  eventInfo->auxdecor<bool>("passHLTmu4msonly_cosmic_L1MU4")  = passedTriggers["MSOnly"];

  // // DV ////////////////////////

  // std::pair<xAOD::VertexContainer* , xAOD::VertexAuxContainer*> selectedDVs( new xAOD::VertexContainer(SG::VIEW_ELEMENTS) , nullptr);
  // selectedDVs.first->setStore(selectedDVs.second);

  // STRONG_CHECK( store->record( selectedDVs.first  , "selectedDVs"    ) );//todo configurable if needed
  // STRONG_CHECK( store->record( selectedDVs.second , "selectedDVsAux."    ) );//todo configurable if needed
  
  // const xAOD::VertexContainer* dvc = nullptr;
  // STRONG_CHECK( event->retrieve(dvc, "VrtSecInclusive_SecondaryVertices") );

  // bool passesDVRequirements = false;
  // for (const auto& dv: *dvc){
  //   ATH_MSG_DEBUG("This vertex ###################################" );
  //   ATH_MSG_DEBUG("... passFiducialCuts: " << dv->auxdataConst<bool>("passFiducialCuts")   );
  //   ATH_MSG_DEBUG("... passChisqCut: " << dv->auxdataConst<bool>("passChisqCut")   );
  //   ATH_MSG_DEBUG("... passDistCut: " << dv->auxdataConst<bool>("passDistCut")   );
  //   ATH_MSG_DEBUG("... passMaterialVeto: " << dv->auxdataConst<bool>("passMaterialVeto")   );
  //   ATH_MSG_DEBUG("... passMassCut: " << dv->auxdataConst<bool>("passMassCut")   );
  //   ATH_MSG_DEBUG("... passNtrkCut: " << dv->auxdataConst<bool>("passNtrkCut")   );
  //   ATH_MSG_DEBUG("###################################");

  //   if(!dv->auxdataConst<bool>("passFiducialCuts") ||
  //      !dv->auxdataConst<bool>("passChisqCut")     ||
  //      !dv->auxdataConst<bool>("passDistCut")      ||
  //      !dv->auxdataConst<bool>("passMaterialVeto") ||
  //      !dv->auxdataConst<bool>("passMassCut")      ||
  //      !dv->auxdataConst<bool>("passNtrkCut") ){
  //       passesDVRequirements = true;
  //       selectedDVs.first->push_back( dv ); 
  //   }
  // }

  bool isMC = eventInfo->auxdata<float>("isMC");

  auto  getRegionName = [](std::map<std::string,int>  /*passedTriggers*/, bool /*isMC*/)
  {
  	std::string regionName = "SRDV";
  	return regionName;
  };

  eventInfo->auxdecor< std::string >("regionName") = getRegionName( passedTriggers, isMC ) ;

  ATH_MSG_DEBUG("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   );

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectDEDxEvents :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
