#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/SelectCosmicEvents.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODMuon/MuonContainer.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"

#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"

#include <boost/algorithm/string.hpp>

#define SET_DUAL_TOOL( TOOLHANDLE, TOOLTYPE, TOOLNAME )                \
  ASG_SET_ANA_TOOL_TYPE(TOOLHANDLE, TOOLTYPE);                        \
  TOOLHANDLE.setName(TOOLNAME);


// this is needed to distribute the algorithm to the workers
ClassImp(SelectCosmicEvents)



SelectCosmicEvents :: SelectCosmicEvents () {}
EL::StatusCode SelectCosmicEvents :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectCosmicEvents :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectCosmicEvents :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectCosmicEvents :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}


EL::StatusCode SelectCosmicEvents :: initialize ()
{

  std::string toolName; // to be used for tool init below, keeping explicit string constants a minimum

  toolName = "xAODConfigTool";
  SET_DUAL_TOOL(m_trigConfTool, TrigConf::xAODConfigTool, toolName);
  STRONG_CHECK( m_trigConfTool.retrieve() );

  toolName = "TrigDecisionTool";
  SET_DUAL_TOOL(m_trigDecTool, Trig::TrigDecisionTool, toolName);
  STRONG_CHECK( m_trigDecTool.setProperty("ConfigTool", m_trigConfTool.getHandle()) );
  STRONG_CHECK( m_trigDecTool.setProperty("TrigDecisionKey", "xTrigDecision") );
  STRONG_CHECK( m_trigDecTool.retrieve() );


  toolName = "TrigMatchTool";
  SET_DUAL_TOOL(m_trigMatchingTool, Trig::MatchingTool, toolName);
  STRONG_CHECK( m_trigMatchingTool.setProperty("TrigDecisionTool", m_trigDecTool.getHandle()));
  STRONG_CHECK( m_trigMatchingTool.setProperty("OutputLevel", MSG::WARNING));
  STRONG_CHECK( m_trigMatchingTool.retrieve() );

  toolName = "MuonSelectionTool";
  SET_DUAL_TOOL(m_muonSelectionTool, CP::MuonSelectionTool, toolName);
  STRONG_CHECK( m_muonSelectionTool.setProperty( "MaxEta", 2.5 ));//only CB
  STRONG_CHECK( m_muonSelectionTool.setProperty( "MuQuality", 1 ));//medium
  STRONG_CHECK( m_muonSelectionTool.setProperty( "TrtCutOff", true ));
  STRONG_CHECK( m_muonSelectionTool.setProperty( "PixCutOff", true ));
  STRONG_CHECK( m_muonSelectionTool.setProperty("OutputLevel", MSG::ERROR));
  STRONG_CHECK( m_muonSelectionTool.retrieve() );




  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectCosmicEvents :: execute ()
{
  //xAOD::TStore * store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  // If the event didn't pass the preselection alg, don't bother doing anything with it...
  if(eventInfo->isAvailable<std::string>("regionName")){
    std::string preselectedRegionName =  eventInfo->auxdecor< std::string >("regionName");
    ATH_MSG_DEBUG("EventNumber: " << eventInfo->eventNumber()  );
    ATH_MSG_DEBUG("Preselected?: " << preselectedRegionName  );
    if( preselectedRegionName == "" ) return EL::StatusCode::SUCCESS;
  } else { // but maybe there was no preselection alg run, so ignore this check.
    eventInfo->auxdecor< std::string >("regionName") = "";
  }

  // ****
  // Save Trigger Info
  /*auto trigORFromString =*/ [](std::vector< std::string > passTrigs, std::string trigString){
    boost::replace_all(trigString, "_OR_", ":");
    std::vector<std::string> trigVect;
    boost::split(trigVect,trigString,boost::is_any_of(":") );
    bool trigDecision = 0;
    for(auto iTrig : trigVect){
      trigDecision |= std::find(passTrigs.begin(), passTrigs.end(), iTrig ) != passTrigs.end();
    }
    return trigDecision;
  };



  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::map<std::string,int> passedTriggers;
  // muon triggers
  passedTriggers["HLT_mu60_0eta105_msonly"]            = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu60_0eta105_msonly") != passTrigs.end();
  passedTriggers["HLT_mu0_muoncalib_L1MU4_EMPTY"]      = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu0_muoncalib_L1MU4_EMPTY") != passTrigs.end();
  passedTriggers["HLT_mu4_cosmic_L1MU11_EMPTY"]        = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_cosmic_L1MU11_EMPTY") != passTrigs.end();
  passedTriggers["HLT_mu4_cosmic_L1MU4_EMPTY"]         = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_cosmic_L1MU4_EMPTY") != passTrigs.end();
  passedTriggers["HLT_mu4_msonly_cosmic_L1MU11_EMPTY"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_msonly_cosmic_L1MU11_EMPTY") != passTrigs.end();
  passedTriggers["HLT_mu4_msonly_cosmic_L1MU4_EMPTY"]  = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_msonly_cosmic_L1MU4_EMPTY") != passTrigs.end();
  passedTriggers["HLT_noalg_cosmicmuons_L1MU4_EMPTY"]  = std::find(passTrigs.begin(), passTrigs.end(), "HLT_noalg_cosmicmuons_L1MU4_EMPTY") != passTrigs.end();
  passedTriggers["HLT_noalg_cosmicmuons_L1MU11_EMPTY"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_noalg_cosmicmuons_L1MU11_EMPTY") != passTrigs.end();

  // stopped particle triggers
  passedTriggers["HLT_noalg_L1J12"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_noalg_L1J12") != passTrigs.end();

  // save to event info
  eventInfo->auxdecor<bool>("passHLT_mu60_0eta105_msonly"            ) = passedTriggers["HLT_mu60_0eta105_msonly"]           ;
  eventInfo->auxdecor<bool>("passHLT_mu0_muoncalib_L1MU4_EMPTY"      ) = passedTriggers["HLT_mu0_muoncalib_L1MU4_EMPTY"]     ;
  eventInfo->auxdecor<bool>("passHLT_mu4_cosmic_L1MU11_EMPTY"        ) = passedTriggers["HLT_mu4_cosmic_L1MU11_EMPTY"]       ;
  eventInfo->auxdecor<bool>("passHLT_mu4_cosmic_L1MU4_EMPTY"         ) = passedTriggers["HLT_mu4_cosmic_L1MU4_EMPTY"]        ;
  eventInfo->auxdecor<bool>("passHLT_mu4_msonly_cosmic_L1MU11_EMPTY" ) = passedTriggers["HLT_mu4_msonly_cosmic_L1MU11_EMPTY"];
  eventInfo->auxdecor<bool>("passHLT_mu4_msonly_cosmic_L1MU4_EMPTY"  ) = passedTriggers["HLT_mu4_msonly_cosmic_L1MU4_EMPTY"] ;
  eventInfo->auxdecor<bool>("passHLT_noalg_cosmicmuons_L1MU4_EMPTY"  ) = passedTriggers["HLT_noalg_cosmicmuons_L1MU4_EMPTY" ];
  eventInfo->auxdecor<bool>("passHLT_noalg_cosmicmuons_L1MU11_EMPTY" ) = passedTriggers["HLT_noalg_cosmicmuons_L1MU11_EMPTY"];

  eventInfo->auxdecor<bool>("passHLT_noalg_L1J12") = passedTriggers["HLT_noalg_L1J12"];

  // ****
  // Get muon collection, save trigger matching and quality information
  const xAOD::IParticleContainer* uncalibmuons_nominal(nullptr);
  STRONG_CHECK(event->retrieve(uncalibmuons_nominal, "Muons"));

  bool medium = false;
  bool passTM_L1MU4  = false;
  bool passTM_L1MU11 = false;
  bool passTM_mu4_L1MU4  = false;
  bool passTM_mu4_L1MU11 = false;
  bool passTM_mu60 = false;
  for( xAOD::IParticle *p : *uncalibmuons_nominal){

      // quality
      const xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(p);
      medium = m_muonSelectionTool->accept( *mu );

      mu->auxdecor< int >( "medium" ) = medium;

      // do trigger matching
      //const xAOD::IParticle *p = const_cast<const xAOD::IParticle *>(mu) ;
      //xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

      passTM_L1MU4        = m_trigMatchingTool->match( *p , "L1_MU4_EMPTY"  ); //hrm doesn't work
      passTM_L1MU11       = m_trigMatchingTool->match( *p , "L1_MU11_EMPTY" ); //for L1 triggers??
      passTM_mu4_L1MU4    = m_trigMatchingTool->match( *p , "HLT_mu4_msonly_cosmic_L1MU4_EMPTY"  );
      passTM_mu4_L1MU11   = m_trigMatchingTool->match( *p , "HLT_mu4_msonly_cosmic_L1MU11_EMPTY" );
      passTM_mu60   	  = m_trigMatchingTool->match( *p , "HLT_mu60_0eta105_msonly" );

      mu->auxdecor< int >( "match_L1MU4_EMPTY"         ) = passTM_L1MU4  ;
      mu->auxdecor< int >( "match_L1MU11_EMPTY"        ) = passTM_L1MU11 ;
      mu->auxdecor< int >( "match_HLT_mu4_msonly_cosmic_L1MU4_EMPTY"  ) = passTM_mu4_L1MU4  ;
      mu->auxdecor< int >( "match_HLT_mu4_msonly_cosmic_L1MU11_EMPTY" ) = passTM_mu4_L1MU11 ;
      mu->auxdecor< int >( "match_HLT_mu60_0eta105_msonly"            ) = passTM_mu60 ;

  }

  //bool isMC = eventInfo->auxdata<float>("isMC");

  auto  getRegionName = []()
  {
	// for now, letting all events pass into n-tuples
	// to do: better define signal regions

	std::string regionName = "SRCOS";

	return regionName;
  };

  eventInfo->auxdecor< std::string >("regionName") = getRegionName() ;

  ATH_MSG_DEBUG("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   );

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectCosmicEvents :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectCosmicEvents :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectCosmicEvents :: histFinalize () {return EL::StatusCode::SUCCESS;}
