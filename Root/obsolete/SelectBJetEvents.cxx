#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/SelectBJetEvents.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"


#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"

#include <boost/algorithm/string.hpp>


// this is needed to distribute the algorithm to the workers
ClassImp(SelectBJetEvents)



SelectBJetEvents :: SelectBJetEvents () {}
EL::StatusCode SelectBJetEvents :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectBJetEvents :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectBJetEvents :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectBJetEvents :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectBJetEvents :: initialize () {return EL::StatusCode::SUCCESS;}

EL::StatusCode SelectBJetEvents :: execute ()
{

  xAOD::TStore * store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  // If the event didn't pass the preselection alg, don't bother doing anything with it...
  if(eventInfo->isAvailable<std::string>("regionName")){
    std::string preselectedRegionName =  eventInfo->auxdecor< std::string >("regionName");
    ATH_MSG_DEBUG("EventNumber: " << eventInfo->eventNumber()  );
    ATH_MSG_DEBUG("Preselected?: " << preselectedRegionName  );
    if( preselectedRegionName == "" ) return EL::StatusCode::SUCCESS;
  } else { // but maybe there was no preselection alg run, so ignore this check.
    eventInfo->auxdecor< std::string >("regionName") = "";
  }


  typedef std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> containerPair;

  auto createIParticleContainerInTStore = [](std::string name, xAOD::TStore* store)
  {
    containerPair outputContainers( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
    outputContainers.first->setStore(outputContainers.second);
    store->record( outputContainers.first  , name );
    store->record( outputContainers.second , name+"Aux."  );//todo configurable if needed
    return outputContainers;
  };

  auto selectedMuons             = createIParticleContainerInTStore("selectedMuons",store);
  auto selectedBaselineMuons     = createIParticleContainerInTStore("selectedBaselineMuons",store);
  auto selectedElectrons         = createIParticleContainerInTStore("selectedElectrons",store);
  auto selectedBaselineElectrons = createIParticleContainerInTStore("selectedBaselineElectrons",store);
  auto selectedPhotons           = createIParticleContainerInTStore("selectedPhotons",store);
  auto selectedJets              = createIParticleContainerInTStore("selectedJets",store);


  xAOD::JetContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal, "STCalibAntiKt4EMTopoJets"));

  xAOD::MuonContainer* muons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(muons_nominal, "STCalibMuons"));

  xAOD::ElectronContainer* electrons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(electrons_nominal, "STCalibElectrons"));

  xAOD::PhotonContainer* photons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(photons_nominal, "STCalibPhotons"));



  int nBJets = 0;

  for (const auto& jet : *jets_nominal) {
    if ((int)jet->auxdata<char>("baseline") == 0) continue;
    if ((int)jet->auxdata<char>("passOR") != 1) continue;
    if ((int)jet->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful jet
    ATH_MSG_VERBOSE( "jet pt : " << jet->pt() );

    selectedJets.first->push_back(jet  );

    if ((int)jet->auxdata<char>("bjet") == 1) nBJets++;

  }

  for (const auto& mu : *muons_nominal) {
    if ((int)mu->auxdata<char>("baseline") == 0) continue;
    if ((int)mu->auxdata<char>("passOR") != 1) continue;

    selectedBaselineMuons.first->push_back( mu );

    if ((int)mu->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful muon
    ATH_MSG_VERBOSE( "mu pt : " << mu->pt() );

    selectedMuons.first->push_back( mu );
  }

  for (const auto& el : *electrons_nominal) {
    if ((int)el->auxdata<char>("baseline") == 0) continue;
    if ((int)el->auxdata<char>("passOR") != 1) continue;

    selectedBaselineElectrons.first->push_back( el );

    if ((int)el->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful el
    ATH_MSG_VERBOSE( "el pt : " << el->pt() );

    selectedElectrons.first->push_back( el );
  }


  for (const auto& ph : *photons_nominal) {
    if ((int)ph->auxdata<char>("baseline") == 0) continue;
    if ((int)ph->auxdata<char>("passOR") != 1) continue;
    if ((int)ph->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful el
    ATH_MSG_VERBOSE( "ph pt : " << ph->pt() );

    selectedPhotons.first->push_back( ph );
  }

  int const nBaselineMuons = selectedBaselineMuons.first->size();
  int const nBaselineElectrons = selectedBaselineElectrons.first->size();
  int const nMuons = selectedMuons.first->size();
  int const nElectrons = selectedElectrons.first->size();
  //int const nLeptons = nMuons + nElectrons;
  //int const nBaselineLeptons = nBaselineMuons + nBaselineElectrons;
  int const nPhotons = selectedPhotons.first->size();

  ATH_MSG_DEBUG("Number of Selected Baseline Muons: " << nBaselineMuons );
  ATH_MSG_DEBUG("Number of Selected Baseline Electrons: " << nBaselineElectrons );
  ATH_MSG_DEBUG("Number of Selected Muons: " << nMuons  );
  ATH_MSG_DEBUG("Number of Selected Electrons: " << nElectrons  );
  ATH_MSG_DEBUG("Number of Selected Photons: " << nPhotons  );

  // Trigger ///////////////////

  /*
  bool passTM = false;
  for (auto muon: *selectedMuons.first){
    if(muon->auxdecor< int >( "passTM" ) ) passTM = true;
  }
  for (auto electron: *selectedElectrons.first){
    if(electron->auxdecor< int >( "passTM" ) ) passTM = true;
  }
  */

  auto trigORFromString = [](std::vector< std::string > passTrigs, std::string trigString){
    boost::replace_all(trigString, "_OR_", ":");
    std::vector<std::string> trigVect;
    boost::split(trigVect,trigString,boost::is_any_of(":") );
    bool trigDecision = 0;
    for(auto iTrig : trigVect){
      trigDecision |= std::find(passTrigs.begin(), passTrigs.end(), iTrig ) != passTrigs.end();
    }
    return trigDecision;
  };



  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::map<std::string,int> passedTriggers;
  passedTriggers["HLT_g140_loose"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_g140_loose") != passTrigs.end();
  passedTriggers["HLT_mu60_0eta105_msonly"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu60_0eta105_msonly") != passTrigs.end();
  // for cosmic run
  passedTriggers["HLT_mu4_msonly_cosmic_L1MU11_EMPTY"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_msonly_cosmic_L1MU11_EMPTY") != passTrigs.end();
  passedTriggers["HLT_mu4_msonly_cosmic_L1MU4_EMPTY"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu4_msonly_cosmic_L1MU4_EMPTY") != passTrigs.end();

  if(eventInfo->auxdecor<float>("year")==2015){
    passedTriggers["MET"] = passedTriggers["HLT_xe70"];
    passedTriggers["Electron"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("elTrig2015") );
    passedTriggers["Muon"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("muTrig2015") );
    passedTriggers["MSOnly"] = passedTriggers["HLT_mu60_0eta105_msonly"];
    passedTriggers["Photon"] = passedTriggers["HLT_g140_loose"];
  } else {
    passedTriggers["MET"] = trigORFromString(passTrigs,"HLT_xe100_mht_L1XE50_OR_HLT_xe110_mht_L1XE50");
    passedTriggers["Electron"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("elTrig2016") );
    passedTriggers["Muon"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("muTrig2016") );
    passedTriggers["MSOnly"] = passedTriggers["HLT_mu60_0eta105_msonly"];
    passedTriggers["Photon"] = passedTriggers["HLT_g140_loose"];
  }

  passedTriggers["MET"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
  eventInfo->auxdecor<bool>("passHLTmsonly") = passedTriggers["MSOnly"];
  eventInfo->auxdecor<bool>("passHLTmu4msonly_cosmic_L1MU11") = passedTriggers["MSOnly"];
  eventInfo->auxdecor<bool>("passHLTmu4msonly_cosmic_L1MU4")  = passedTriggers["MSOnly"];

  // DV ////////////////////////


  //bool isMC = eventInfo->auxdata<float>("isMC");

  if(nBJets){
    eventInfo->auxdecor< std::string >("regionName") = "BJets";
  } else {
    eventInfo->auxdecor< std::string >("regionName") = "";
  }

  ATH_MSG_DEBUG("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   );

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SelectBJetEvents :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectBJetEvents :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectBJetEvents :: histFinalize () {return EL::StatusCode::SUCCESS;}
