#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODJet/Jet.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"



#include "FactoryTools/RegionVarCalculator_zl.h"
#include "FactoryTools/strongErrorCheck.h"

#include <xAODAnaHelpers/HelperFunctions.h>
#include "TLorentzVector.h"
//from ROOT
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"
#include "TVectorD.h"


// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_zl)

EL::StatusCode RegionVarCalculator_zl::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	return EL::StatusCode::SUCCESS;
}

EL::StatusCode RegionVarCalculator_zl::doCalculate(std::map<std::string, anytype>& vars ) {
  
  //xAOD::TStore * store = m_worker->xaodStore();//grab the store from the worker
  xAOD::TEvent* event = m_worker->xaodEvent();

  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

  if      ( regionName.empty() ) {return EL::StatusCode::SUCCESS;}
  // If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
  else if ( regionName == "SR" )  {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS &&
                                                         doSRCalculations  (vars) == EL::StatusCode::SUCCESS);}

  else if ( regionName == "CR1L") {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS &&
                                                         doCR1LCalculations(vars) == EL::StatusCode::SUCCESS);}

  else if ( regionName == "CR2L") {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS &&
                                                         doCR2LCalculations(vars) == EL::StatusCode::SUCCESS);}

  else if ( regionName == "CRY") {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS &&
                                                        doCRYCalculations(vars) == EL::StatusCode::SUCCESS);}

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_zl::doAllCalculations(std::map<std::string, anytype>& vars )
{/*todo*/
  xAOD::TStore * store = m_worker->xaodStore();
  xAOD::TEvent * event = m_worker->xaodEvent();


  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  doGeneralCalculations(vars);

  // Get relevant info from the vertex container //////////////////////
  //

  const xAOD::VertexContainer* vertices = nullptr;
  STRONG_CHECK(event->retrieve( vertices, "PrimaryVertices"));
  vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);

  //
  /////////////////////////////////////////////////////////////////////

  auto toGeV = [](double a){return a*.001;};

  xAOD::MissingETContainer * metcont = nullptr;
  STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

  vars["MET"]      = toGeV((*metcont)["Final"]->met());
  vars["METPhi"]   = (*metcont)["Final"]->phi();

  if (eventInfo->auxdecor< bool >("writeDebugVars") ){
    vars[ "MET_RefEle"       ] = toGeV( (*metcont)["RefEle"]->met() );
    vars[ "MET_RefGamma"     ] = toGeV( (*metcont)["RefGamma"]->met() );
    vars[ "MET_Muons"        ] = toGeV( (*metcont)["Muons"]->met() );
    vars[ "MET_RefJet"       ] = toGeV( (*metcont)["RefJet"]->met() );
    // vars[ "MET_SoftClus"  ] = toGeV( (*metcont)["SoftClus"]->met() );
    vars[ "MET_PVSoftTrk"    ] = toGeV( (*metcont)["PVSoftTrk"]->met() );
    // vars[ "MET_FinalClus" ] = toGeV( (*metcont)["FinalClus"]->met() );

    const  xAOD::MissingETContainer * truthmetcont = nullptr;
    STRONG_CHECK(event->retrieve(truthmetcont, "MET_Truth"));
    vars[  "MET_Truth" ] = toGeV(  (*truthmetcont)["NonInt"]->met() );
  }


  xAOD::IParticleContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));

  // Sort by pt
  std::vector<xAOD::IParticle*> jetVec = sortByPt(jets_nominal);
  double Aplanarity = -1;

  float emfrac, hecf, LArQuality, HECQuality, Timing,  fracSamplingMax, NegativeE, AverageLArQF;
  double tmpJetBTagWeight;

  for( const auto& jet : jetVec) {
    addToVectorBranch( vars, "jetPt",   toGeV(jet->pt()));
    addToVectorBranch( vars, "jetEta",  jet->p4().Eta() );
    addToVectorBranch( vars, "jetPhi",  jet->p4().Phi() );
    addToVectorBranch( vars, "jetM",    toGeV(jet->m()) );
    addToVectorBranch( vars, "jetBTag", jet->auxdata<char>("bjet") == 1 );

    if (eventInfo->auxdecor< bool >("writeDebugVars") ){
      
      auto* j = dynamic_cast<xAOD::Jet*>( jet );
      
      j->getAttribute(xAOD::JetAttribute::EMFrac          , emfrac);
      j->getAttribute(xAOD::JetAttribute::HECFrac         , hecf);
      j->getAttribute(xAOD::JetAttribute::LArQuality      , LArQuality);
      j->getAttribute(xAOD::JetAttribute::HECQuality      , HECQuality);
      j->getAttribute(xAOD::JetAttribute::Timing          , Timing);
      j->getAttribute(xAOD::JetAttribute::FracSamplingMax , fracSamplingMax);
      j->getAttribute(xAOD::JetAttribute::NegativeE       , NegativeE);
      j->getAttribute(xAOD::JetAttribute::AverageLArQF    , AverageLArQF);
      
      j->btagging()->MVx_discriminant("MV2c20", tmpJetBTagWeight);

      addToVectorBranch( vars,  "jetemfrac"          , emfrac    );
      addToVectorBranch( vars,  "jethecf"            , hecf    );
      addToVectorBranch( vars,  "jetLArQuality"      , LArQuality    );
      addToVectorBranch( vars,  "jetHECQuality"      , HECQuality    );
      addToVectorBranch( vars,  "jetTiming"          , Timing    );
      addToVectorBranch( vars,  "jetfracSamplingMax" , fracSamplingMax    );
      addToVectorBranch( vars,  "jetNegativeE"       , NegativeE    );
      addToVectorBranch( vars,  "jetAverageLArQF"    , AverageLArQF    );
      addToVectorBranch( vars,  "jetBTagWeight"      , tmpJetBTagWeight     );

    }

  }


  xAOD::IParticleContainer* leptons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(leptons_nominal, "selectedLeptons"));

  // Sort by pt
  std::vector<xAOD::IParticle*> lepVec = sortByPt(leptons_nominal);

  for( xAOD::IParticle * lep : lepVec) {
    addToVectorBranch( vars, "lepPt"   ,   toGeV(lep->pt()));
    addToVectorBranch( vars, "lepEta"  ,    lep->p4().Eta() );
    addToVectorBranch( vars, "lepPhi"  ,    lep->p4().Phi() );
    addToVectorBranch( vars, "lepM"    ,  toGeV(lep->m()) );
    if(xAOD::Electron* myelectron = static_cast<xAOD::Electron*>(lep)) 		addToVectorBranch( vars, "lepSign"    ,  myelectron->charge() * 11. );
    else if(xAOD::Muon* mymuon = dynamic_cast<xAOD::Muon*>(lep)) addToVectorBranch( vars, "lepSign"    ,  mymuon->charge() * 11. );
    else addToVectorBranch( vars, "lepSign"    ,  0.);
  }

  vars["muSF"] = eventInfo->auxdecor<float>("muSF");

  for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("muSF_systs")){
    vars["muSF_"+systName+""] = eventInfo->auxdecor<float>("muSF_"+systName);
  }

  vars["elSF"] = eventInfo->auxdecor<float>("elSF");

  for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("elSF_systs")){
    vars["elSF_"+systName+""] = eventInfo->auxdecor<float>("elSF_"+systName);
  }

  vars["btagSF"] = eventInfo->auxdecor<float>("btagSF");

  for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("btagSF_systs")){
    vars["btagSF_"+systName+""] = eventInfo->auxdecor<float>("btagSF_"+systName);
  }

  vars["phSF"] = eventInfo->auxdecor<float>("phSF");

  for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("phSF_systs")){
    vars["phSF_"+systName+""] = eventInfo->auxdecor<float>("phSF_"+systName);
  }



  double MEff = 0;
  double HT = 0;

  for( const auto& jet : jetVec) {
    HT += toGeV(jet->pt());
  }

  MEff = HT + toGeV((*metcont)["Final"]->met());

  vars["MEff"] = MEff;
  vars["Aplanarity"] = CalcAplanarity(jetVec,Aplanarity);
  vars["dPhi"]   = calcdPhi(jetVec,(*metcont)["Final"],0,2);
  vars["dPhi3p"] = calcdPhi(jetVec,(*metcont)["Final"],3,-1);

  vars["WZweight"] = eventInfo->auxdecor<float>("WZweight");

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_zl::doSRCalculations(std::map<std::string, anytype>& vars )
{

  auto toGeV = [](double a){return a*.001;};


  xAOD::TStore * store = m_worker->xaodStore();
  xAOD::TEvent * event = m_worker->xaodEvent();

  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  if (eventInfo->auxdecor< bool >("writeDebugVars") ){
    xAOD::IParticleContainer* photons_nominal(nullptr);
    STRONG_CHECK(store->retrieve(photons_nominal, "selectedPhotons"));

    // Sort by pt
    std::vector<xAOD::IParticle*> phVec = sortByPt(photons_nominal);

    for( const auto& ph : phVec) {
      addToVectorBranch( vars, "phPt" , toGeV(ph->pt()));
      addToVectorBranch( vars, "phEta",  ph->p4().Eta() );
      addToVectorBranch( vars, "phPhi",  ph->p4().Phi() );
    }

    //////////////////////////////////////////////////////////////////////


    xAOD::MuonContainer* muons_nominal(nullptr);
    STRONG_CHECK(store->retrieve(muons_nominal, "STCalibMuons"));

    xAOD::ElectronContainer* electrons_nominal(nullptr);
    STRONG_CHECK(store->retrieve(electrons_nominal, "STCalibElectrons"));

    for( const auto& lep : *muons_nominal) {
      addToVectorBranch( vars, "muPt"      , toGeV(lep->pt()));
      addToVectorBranch( vars, "muEta"     ,  lep->p4().Eta() );
      addToVectorBranch( vars, "muPhi"     ,  lep->p4().Phi() );
      addToVectorBranch( vars, "muSign"    ,   lep->charge() * 13. );
      addToVectorBranch( vars, "muBaseline",    (int)lep->auxdata<char>("baseline") );
      addToVectorBranch( vars, "muSignal"  ,    (int)lep->auxdata<char>("signal") );
      addToVectorBranch( vars, "muPassOR"  ,    (int)lep->auxdata<char>("passOR") );
    }

    for( const auto& lep : *electrons_nominal) {
      addToVectorBranch( vars, "elPt"        ,  toGeV(lep->pt()));
      addToVectorBranch( vars, "elEta"       ,   lep->p4().Eta() );
      addToVectorBranch( vars, "elPhi"       ,   lep->p4().Phi() );
      addToVectorBranch( vars, "elSign"      ,    lep->charge() * 11. );
      addToVectorBranch( vars, "elBaseline"  ,     (int)lep->auxdata<char>("baseline") );
      addToVectorBranch( vars, "elSignal"    ,     (int)lep->auxdata<char>("signal") );
      addToVectorBranch( vars, "elPassOR"    ,      (int)lep->auxdata<char>("passOR") );
    }

    const xAOD::TruthParticleContainer* electrons_truth(nullptr);
    STRONG_CHECK(event->retrieve(electrons_truth, "TruthElectrons"));

    for( const auto& lep : *electrons_truth) {
      addToVectorBranch( vars, "truthElPt" , toGeV(lep->pt()));
      addToVectorBranch( vars, "truthElEta",  lep->p4().Eta() );
      addToVectorBranch( vars, "truthElPhi",   lep->p4().Phi() );
    }


    const xAOD::TruthParticleContainer* particles_truth(nullptr);
    STRONG_CHECK(event->retrieve(particles_truth, "TruthParticles"));

    for( const auto& particle : *particles_truth) {
      if (particle->pt()==0) continue;
      if ( abs(particle->pdgId() ) > 30 ) continue;
      // if (!particle->isLepton() && !particle->isW() && !particle->isZ() ) continue;
      addToVectorBranch( vars, "truthPt"    , toGeV(particle->pt()));
      addToVectorBranch( vars, "truthEta"   ,  particle->p4().Eta() );
      addToVectorBranch( vars, "truthPhi"   ,  particle->p4().Phi() );
      addToVectorBranch( vars, "truthPdgid" ,    particle->pdgId() );
      if (particle->nParents ()){
        addToVectorBranch( vars, "truthParentPdgid", particle->parent()->pdgId() );
      } else 	addToVectorBranch( vars, "truthParentPdgid",    0 );
    }
  }

  return EL::StatusCode::SUCCESS;

}


EL::StatusCode RegionVarCalculator_zl::doCR1LCalculations(std::map<std::string, anytype>& vars)
{
  auto toGeV = [](double a){return a*.001;};


  xAOD::TStore * store = m_worker->xaodStore();
  //xAOD::TEvent * event = m_worker->xaodEvent();


  xAOD::MissingETContainer * metcont = nullptr;
  STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

  xAOD::IParticleContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));

  xAOD::IParticleContainer* leptons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(leptons_nominal, "selectedLeptons"));

  std::vector<xAOD::IParticle*> jetVec = sortByPt(jets_nominal);
  std::vector<xAOD::IParticle*> lepVec = sortByPt(leptons_nominal);


  // Adding the leading lepton to the jet collection
  jetVec.push_back(lepVec.at(0));

  // Sort the "jet" collection before filling branches
  jetVec = sortByPt( &jetVec );

  // Fill up the "jet" branches
  double Aplanarity = -1;

  for( const auto& jet : jetVec) {
    addToVectorBranch( vars, "jetPt"   ,   toGeV(jet->pt()));
    addToVectorBranch( vars, "jetEta"  ,    jet->p4().Eta() );
    addToVectorBranch( vars, "jetPhi"  ,    jet->p4().Phi() );
    addToVectorBranch( vars, "jetM"    ,  toGeV(jet->m()) );
    addToVectorBranch( vars, "jetBTag" , jet->auxdata<char>("bjet") == 1 );
  }


  double MEff = 0;
  double HT = 0;

  for( const auto& jet : jetVec) {
    HT += toGeV(jet->pt());
  }

  for( const auto& lepton : lepVec) {
    HT += toGeV(lepton->pt());
  }

  MEff = HT + toGeV((*metcont)["Final"]->met());

  vars["MEff"] = MEff;

  TLorentzVector tmpLep = lepVec.at(0)->p4();
  tmpLep.SetZ(0); tmpLep.SetT(tmpLep.Perp() );
  TLorentzVector tmpNu((*metcont)["Final"]->mpx(),
                       (*metcont)["Final"]->mpy(),
                       0,
                       (*metcont)["Final"]->met() );

  double mT = (tmpLep+tmpNu).M();
  vars["mT"] = toGeV(mT);
  vars["Aplanarity"] = CalcAplanarity(jetVec,Aplanarity);
  vars["dPhi"]   = calcdPhi(jetVec,(*metcont)["Final"],0,2);
  vars["dPhi3p"] = calcdPhi(jetVec,(*metcont)["Final"],3,-1);


  // Here - what happens if the lepton is included in the MET instead of the Jets?
  // Needs to be completed for VRWT regions... Do we want to store a separate tree or just some key variables in this one?

  // double tmpLeptonPt = lepVec.at(0)->p4();
  // double tmpLeptonPx = (*(*leptons_nominal)[0]).p4().Px();
  // double tmpLeptonPy = (*(*leptons_nominal)[0]).p4().Py();
  // double tmpMET = (*metcont)["Final"]->met();
  // double tmpMEx = (*metcont)["Final"]->mpx();
  // double tmpMEy = (*metcont)["Final"]->mpy();

  // TVector2 METPrimeVec2( tmpLeptonPx+tmpMEx, tmpLeptonPy+tmpMEy );
  // double METPrimePhi = TMath::ATan2(METPrimeVec2.Y(),METPrimeVec2.X()); // return [-pi, pi]


  // vars["MET_Lep"]    = toGeV(METPrimeVec2.Mod());
  // vars["METPhi_Lep"] = METPrimePhi;




  return EL::StatusCode::SUCCESS;

}


EL::StatusCode RegionVarCalculator_zl::doCR2LCalculations(std::map<std::string, anytype>& vars )
{
  auto toGeV = [](double a){return a*.001;};


  xAOD::TStore * store = m_worker->xaodStore();
  //xAOD::TEvent * event = m_worker->xaodEvent();

  xAOD::MissingETContainer * metcont = nullptr;
  STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

  xAOD::IParticleContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));

  xAOD::IParticleContainer* leptons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(leptons_nominal, "selectedLeptons"));


  std::vector<xAOD::IParticle*> jetVec = sortByPt(jets_nominal);

  double MEff = 0;
  double HT = 0;
  double MET = 0;

  xAOD::MissingET METVec(*(*metcont)["Final"]);


  std::vector<xAOD::IParticle*> lepVec = sortByPt(leptons_nominal);
  METVec.add(lepVec.at(0) );

  for( const auto& jet : *jets_nominal) {
    HT += toGeV(jet->pt());
  }

  MET = toGeV(  METVec.met() );
  MEff = HT + MET;

  vars["MEff"] = MEff;
  vars["MET"] = MET;
  vars["METPhi"] =  METVec.phi();

  vars["dPhi"]   = calcdPhi(jetVec,(*metcont)["Final"],0,2);
  vars["dPhi3p"] = calcdPhi(jetVec,(*metcont)["Final"],3,-1);

  return EL::StatusCode::SUCCESS;

}



EL::StatusCode RegionVarCalculator_zl::doCRYCalculations(std::map<std::string, anytype>& vars)
{
  auto toGeV = [](double a){return a*.001;};

  xAOD::TStore * store = m_worker->xaodStore();
  //xAOD::TEvent * event = m_worker->xaodEvent();

  xAOD::MissingETContainer * metcont = nullptr;
  STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

  xAOD::IParticleContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));

  xAOD::IParticleContainer* photons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(photons_nominal, "selectedPhotons"));

  // Sort by pt
  std::vector<xAOD::IParticle*> jetVec = sortByPt(jets_nominal);
  std::vector<xAOD::IParticle*> phVec = sortByPt(photons_nominal);

  for( const auto& ph : phVec) {
    addToVectorBranch( vars, "phPt" ,  toGeV(ph->pt()));
    addToVectorBranch( vars, "phEta",   ph->p4().Eta() );
    addToVectorBranch( vars, "phPhi",   ph->p4().Phi() );
  }

  double MEff = 0;
  double MET = 0;
  double HT = 0;


  for( const auto& jet : *jets_nominal) {
    HT += toGeV(jet->pt());
  }


  xAOD::MissingET METVec(*(*metcont)["Final"]);

  METVec.add(phVec.at(0));

  MET = toGeV(  METVec.met() );
  MEff = HT + MET;

  vars["MEff"] = MEff;
  vars["MET"] = MET;
  vars["METPhi"] = METVec.phi();

  vars["dPhi"]   = calcdPhi(jetVec,(*metcont)["Final"],0,2);
  vars["dPhi3p"] = calcdPhi(jetVec,(*metcont)["Final"],3,-1);

  return EL::StatusCode::SUCCESS;

}










float RegionVarCalculator_zl::calcdPhi(std::vector<xAOD::IParticle*> jetVec, xAOD::MissingET * met, int startJetIndex, int endJetIndex){
	float dPhi    = 1e6;
	float dPhiTmp = 1e6;
	if (endJetIndex<0) endJetIndex = 1e6;
	for( int j=startJetIndex; j<endJetIndex+1; j++) {
			if (j > (int) jetVec.size()-1 ) break;
			dPhiTmp = fabs(jetVec.at(j)->p4().DeltaPhi(TLorentzVector(met->mpx(), met->mpy(), 0, met->met() ) ));
			if (dPhiTmp<dPhi) dPhi = dPhiTmp;
	}
	return dPhi;
}


double RegionVarCalculator_zl::CalcAplanarity(const std::vector<xAOD::IParticle*> jetVec, double Aplanarity){
  TMatrixDSym m_M(3); TVectorD m_eigen; std::vector<float> lambda;

  TVector3 momentum;

  for(size_t ijet=0; ijet<jetVec.size(); ijet++) {

    TLorentzVector v = jetVec.at(ijet)->p4();
    momentum = v.Vect();

    for (int i = 0; i < 3; i++){
      for (int j = 0; j < 3; j++){
        m_M[i][j] += momentum[i]*momentum[j];
      }
    }
  }
  TMatrixDSymEigen EM(m_M);
  Int_t m_size = 3;

  m_eigen.ResizeTo(m_size);
  m_eigen = EM.GetEigenValues();

  for(int i =0; i < m_eigen.GetNoElements(); i++){
    lambda.push_back(m_eigen[0]);
    lambda.push_back(m_eigen[1]);
    lambda.push_back(m_eigen[2]);
  }

  double scale = lambda[0]+lambda[1]+lambda[2];
  lambda[0]/=scale;
  lambda[1]/=scale;
  lambda[2]/=scale;


  sort(lambda.begin(),lambda.end());
  Aplanarity = 1.5*lambda[0];


  return Aplanarity;
}

