#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <FactoryTools/SelectSPEvents.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"


#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"

#include <boost/algorithm/string.hpp>


// this is needed to distribute the algorithm to the workers
ClassImp(SelectSPEvents)


SelectSPEvents :: SelectSPEvents () {}
EL::StatusCode SelectSPEvents :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectSPEvents :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectSPEvents :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectSPEvents :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectSPEvents :: initialize () {return EL::StatusCode::SUCCESS;}


EL::StatusCode SelectSPEvents :: execute ()
{

  //xAOD::TStore * store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  // If the event didn't pass the preselection alg, don't bother doing anything with it...
  if(eventInfo->isAvailable<std::string>("regionName")){
    std::string preselectedRegionName =  eventInfo->auxdecor< std::string >("regionName");
    ATH_MSG_DEBUG("EventNumber: " << eventInfo->eventNumber()  );
    ATH_MSG_DEBUG("Preselected?: " << preselectedRegionName  );
    if( preselectedRegionName == "" ) return EL::StatusCode::SUCCESS;
  } else { // but maybe there was no preselection alg run, so ignore this check.
    eventInfo->auxdecor< std::string >("regionName") = "";
  }

  // Trigger ///////////////////

  /*auto trigORFromString =*/ [](std::vector< std::string > passTrigs, std::string trigString){
    boost::replace_all(trigString, "_OR_", ":");
    std::vector<std::string> trigVect;
    boost::split(trigVect,trigString,boost::is_any_of(":") );
    bool trigDecision = 0;
    for(auto iTrig : trigVect){
      trigDecision |= std::find(passTrigs.begin(), passTrigs.end(), iTrig ) != passTrigs.end();
    }
    return trigDecision;
  };



  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::map<std::string,int> passedTriggers;
  passedTriggers["HLT_noalg_L1J12"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_noalg_L1J12") != passTrigs.end();

  // bool isMC = eventInfo->auxdata<float>("isMC");

  auto  getRegionName = []( std::map<std::string,int>  passedTriggers )
  {
  std::string regionName = "SRSP";
  if(passedTriggers["HLT_noalg_L1J12"])	std::string regionName = "SRSP";
	return regionName;
  };

  eventInfo->auxdecor< std::string >("regionName") = getRegionName( passedTriggers ) ;
  ATH_MSG_DEBUG("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   );

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectSPEvents :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectSPEvents :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectSPEvents :: histFinalize () {return EL::StatusCode::SUCCESS;}
