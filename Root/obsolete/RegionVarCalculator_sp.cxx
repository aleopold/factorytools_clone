#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"


#include "FactoryTools/RegionVarCalculator_sp.h"
#include "FactoryTools/strongErrorCheck.h"

#include <xAODAnaHelpers/HelperFunctions.h>

// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_sp)

EL::StatusCode RegionVarCalculator_sp::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	return EL::StatusCode::SUCCESS;
}

EL::StatusCode RegionVarCalculator_sp::doCalculate(std::map<std::string, anytype>& vars )
{
        //xAOD::TStore * store = m_worker->xaodStore();//grab the store from the worker
	xAOD::TEvent* event = m_worker->xaodEvent();

	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

	if      ( regionName.empty() ) {return EL::StatusCode::SUCCESS;}
	// If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
	else if ( regionName == "SRSP" ) {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); }

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_sp::doAllCalculations(std::map<std::string, anytype>& vars )
{
        //xAOD::TStore * store = m_worker->xaodStore();
	xAOD::TEvent * event = m_worker->xaodEvent();


	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	auto toGeV = [](double a){return a*.001;};

	// retrieve MET_LocHadTopo container
	const xAOD::MissingETContainer* met_lht_container = nullptr;
	STRONG_CHECK( event->retrieve(met_lht_container, "MET_LocHadTopo") );
	bool hasMET = met_lht_container->size() > 0;
	vars["MET_LHT"] = hasMET ? met_lht_container->at(0)->met() * 0.001 : -999.;
	vars["MET_LHT_phi"] = hasMET ? met_lht_container->at(0)->phi() : -999.;

	TVector2 hltMET;
	// retrieve HLT MET
	const xAOD::TrigMissingETContainer* met_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(met_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET") );
	hasMET = met_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(met_hlt_container->at(0)->ex(), met_hlt_container->at(0)->ey());
	vars["MET_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MET_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

	// retrieve HLT MHT
	const xAOD::TrigMissingETContainer* mht_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(mht_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht") );
	hasMET = mht_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(mht_hlt_container->at(0)->ex(), mht_hlt_container->at(0)->ey());
	vars["MHT_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MHT_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;


	// Jets

	const xAOD::JetContainer* uncalibjets_nominal(nullptr);
	STRONG_CHECK(event->retrieve(uncalibjets_nominal, "AntiKt4EMTopoJets"));

	for (const auto& jet : *uncalibjets_nominal)
	{
		addToVectorBranch(vars,"jetPt", toGeV(jet->pt() ) );
		addToVectorBranch(vars,"jetEta", jet->p4().Eta()  );
		addToVectorBranch(vars,"jetPhi", jet->p4().Phi()  );
		addToVectorBranch(vars,"jetM", jet->p4().M()  );
	}

	// Muons

	const xAOD::MuonContainer* uncalibmuons_nominal(nullptr);
	STRONG_CHECK(event->retrieve(uncalibmuons_nominal, "Muons"));

	for (const auto& mu : *uncalibmuons_nominal)
	{
		addToVectorBranch(vars,"muPt", toGeV(mu->pt() ) );
		addToVectorBranch(vars,"muEta", mu->p4().Eta()  );
		addToVectorBranch(vars,"muPhi", mu->p4().Phi()  );
	}


	return EL::StatusCode::SUCCESS;
}
