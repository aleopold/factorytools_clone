#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"


#include "FactoryTools/RegionVarCalculator_slbjets.h"
#include "FactoryTools/strongErrorCheck.h"

#include <xAODAnaHelpers/HelperFunctions.h>

// Determines if an id track is back to back with a muon
// bool cosmic_Tag( TLorentzVector p1, TLorentzVector p2){
 	
//  	double PI = 3.1415926535897; // already in fast jet?
// 	float absdphi = fabs(p1.DeltaPhi(p2));  
// 	float sumeta = p1.Eta()+p2.Eta();
// 	float dR_cos = sqrt( ( absdphi - PI )*( absdphi - PI ) + sumeta*sumeta );	
 					
// 	if (dR_cos < 0.1) return true;
// 	else return false;

// }


// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_slbjets)

EL::StatusCode RegionVarCalculator_slbjets::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	return EL::StatusCode::SUCCESS;
}

EL::StatusCode RegionVarCalculator_slbjets::doCalculate(std::map<std::string, anytype              >& vars )
{
        //xAOD::TStore * store = m_worker->xaodStore();//grab the store from the worker
	xAOD::TEvent* event = m_worker->xaodEvent();

	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

	if      ( regionName.empty() ) {return EL::StatusCode::SUCCESS;}
	// If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
	else if ( regionName == "BJets" ) {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); }


	return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_slbjets::doAllCalculations(std::map<std::string, anytype>& vars )
{/*todo*/
	xAOD::TStore * store = m_worker->xaodStore();
	xAOD::TEvent * event = m_worker->xaodEvent();


	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	doGeneralCalculations(vars);

	// Get relevant info from the vertex container //////////////////////
	//

	const xAOD::VertexContainer* vertices = nullptr;
	STRONG_CHECK(event->retrieve( vertices, "PrimaryVertices"));
	vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);

	//
	/////////////////////////////////////////////////////////////////////

	auto toGeV = [](double a){return a*.001;};

	xAOD::MissingETContainer * metcont = nullptr;
	STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

	//  std::cout << "MET : " << (*metcont)["Final"]->met() << std::endl;
	vars["passMETtrigger"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
	vars["MET"] = toGeV((*metcont)["Final"]->met());
	vars["MET_phi"] = ((*metcont)["Final"]->phi());

	bool hasMET = 0;

	// retrieve MET_LocHadTopo container
	// const xAOD::MissingETContainer* met_lht_container = nullptr;
	// STRONG_CHECK( event->retrieve(met_lht_container, "MET_LocHadTopo") );
	// bool hasMET = met_lht_container->size() > 0;
	// vars["MET_LHT"] = hasMET ? met_lht_container->at(0)->met() * 0.001 : -999.;
	// vars["MET_LHT_phi"] = hasMET ? met_lht_container->at(0)->phi() : -999.;

	TVector2 hltMET;
	// retrieve HLT MET
	const xAOD::TrigMissingETContainer* met_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(met_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET") );
	hasMET = met_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(met_hlt_container->at(0)->ex(), met_hlt_container->at(0)->ey());
	vars["MET_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MET_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

	// retrieve HLT MHT
	const xAOD::TrigMissingETContainer* mht_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(mht_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht") );
	hasMET = mht_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(mht_hlt_container->at(0)->ex(), mht_hlt_container->at(0)->ey());
	vars["MHT_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MHT_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;


	xAOD::IParticleContainer* jets_nominal(nullptr);
	STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));

	for( const auto& jet : *jets_nominal) {
		addToVectorBranch(vars,"jetPt", toGeV(jet->pt() ) );
		addToVectorBranch(vars,"jetEta", jet->p4().Eta()  );
		addToVectorBranch(vars,"jetPhi", jet->p4().Phi()  );
		addToVectorBranch(vars,"jetM", jet->p4().M()  );
		addToVectorBranch(vars,"jetBTag", jet->auxdata<char>("bjet") );
	}

	// Trackless jets

	// const xAOD::JetContainer* uncalibjets_nominal(nullptr);
	// STRONG_CHECK(event->retrieve(uncalibjets_nominal, "AntiKt4EMTopoJets"));

	// for (const auto& jet : *uncalibjets_nominal)
	// {
	//   // skip those with pT < 25 GeV or |eta| > 2.5
	//   if (jet->pt()*0.001 < 25 || fabs(jet->eta()) > 2.5) continue;

	//   // store some info for the ones with (uncalibrated) pT > 25 GeV
	//   std::vector<float> sumPtTrkvec;
	//   jet->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec);
	//   if (sumPtTrkvec.size() > 0) {
	// 	if (sumPtTrkvec.at(0) < 5000) {
	// 		addToVectorBranch(vars,"tracklessJetPt", toGeV(jet->pt() ) );
	// 		addToVectorBranch(vars,"tracklessJetEta", jet->p4().Eta()  );
	// 		addToVectorBranch(vars,"tracklessJetPhi", jet->p4().Phi()  );
	// 		addToVectorBranch(vars,"tracklessJetM", jet->p4().M()  );
	// 		addToVectorBranch(vars,"tracklessJetSumPtTrkPt500", toGeV(sumPtTrkvec.at(0))  );
	// 	}
	//   }
	// }

	vars["muSF"] = eventInfo->auxdecor<float>("muSF");

	for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("muSF_systs")){
			vars["muSF_"+systName+""] = eventInfo->auxdecor<float>("muSF_"+systName);
	}

	vars["elSF"] = eventInfo->auxdecor<float>("elSF");

	for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("elSF_systs")){
			vars["elSF_"+systName+""] = eventInfo->auxdecor<float>("elSF_"+systName);
	}

	double MEff = 0;
	double HT = 0;

	for( const auto& jet : *jets_nominal) {
		HT += toGeV(jet->pt());
	}

	MEff = HT + toGeV((*metcont)["Final"]->met());

	vars["MEff"] = MEff;
	vars["HT"] = HT;

	vars["WZweight"] = eventInfo->auxdecor<float>("WZweight");

	//////////////////////////////////////////////////
	// get the highest-sumpT primary vertex (one is needed for recalculating MET)
	// const xAOD::VertexContainer* pvc = nullptr;
	// STRONG_CHECK( store->retrieve(pvc, "selectedPV") );

	// auto pv = pvc->at(0);
	// vars["PV_x"] = pv->x();
	// vars["PV_y"] = pv->y();
	// vars["PV_z"] =  pv->z();
	// vars["PV_rxy"] =  TMath::Hypot(pv->x(),pv->y());
	// vars["PV_nTracks"] = pv->nTrackParticles();
	// vars["PV_sumpT2"] = pv->auxdataConst<float>( "sumPt2" );
	// vars["PV_n"] = nPV;

	///////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////
	//to do, write out nominal leptons for SRMET & SR2L
	//xAOD::IParticleContainer* muons_nominal(nullptr);
	//STRONG_CHECK(store->retrieve(muons_nominal, "selectedElectrons"));
	//xAOD::IParticleContainer* electrons_nominal(nullptr);
	//STRONG_CHECK(store->retrieve(electrons_nominal, "selectedMuons"));

	vars["passHLTmsonly"] = eventInfo->auxdecor<bool>("passHLTmsonly");

	//maps to IS & MS tracks
	std::map<int,int> IDtrkToMuonMap;
	std::map<int,int> MStrkToMuonMap;

	// ID DV mapping :/

	xAOD::IParticleContainer* muons_baseline(nullptr);
	STRONG_CHECK(store->retrieve(muons_baseline, "selectedBaselineMuons"));

	//std::cout << "Looping through selectedBaselineMuons " 	<< std::endl;
	for( xAOD::IParticle * muon : *muons_baseline){

		xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

		addToVectorBranch( vars, "muon_index"  , mu->index());
		addToVectorBranch( vars, "muon_pt"     , toGeV(mu->pt()));
		addToVectorBranch( vars, "muon_eta"    , mu->p4().Eta());
		addToVectorBranch( vars, "muon_phi"    , mu->p4().Phi());
		addToVectorBranch( vars, "muon_charge" , mu->charge() );

		const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
		const xAOD::TrackParticle* metrack = mu->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
		const xAOD::TrackParticle* mstrack = mu->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
		const xAOD::TrackParticle* cbtrack = mu->trackParticle( xAOD::Muon::CombinedTrackParticle );

		if (idtrack) IDtrkToMuonMap[ (idtrack)->index() ] = mu->index();
		if (mstrack) MStrkToMuonMap[ (mstrack)->index() ] = mu->index();

		addToVectorBranch( vars, "muon_hasCBtrack" , (cbtrack) ? 1 : 0 );
		addToVectorBranch( vars, "muon_hasIDtrack" , (idtrack) ? 1 : 0 );
		addToVectorBranch( vars, "muon_hasMEtrack" , (metrack) ? 1 : 0 );
		addToVectorBranch( vars, "muon_hasMStrack" , (mstrack) ? 1 : 0 );

		addToVectorBranch( vars, "muon_d0"      , (idtrack) ? idtrack->d0() : -9999.);
		addToVectorBranch( vars, "muon_z0"      , (idtrack) ? idtrack->z0() : -9999.);
		addToVectorBranch( vars, "muon_isLRT"   , (idtrack) ? static_cast<int>(idtrack->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0)) : 0 );
		addToVectorBranch( vars, "muon_RadFirstHit" , (idtrack) ? idtrack->auxdata< float >("radiusOfFirstHit" ) : -999. );

		addToVectorBranch( vars, "muon_ptcone20"    , toGeV( mu->auxdata< float >("ptcone20")     ) );
		addToVectorBranch( vars, "muon_ptcone30"    , toGeV( mu->auxdata< float >("ptcone30")     ) );
		addToVectorBranch( vars, "muon_ptcone40"    , toGeV( mu->auxdata< float >("ptcone40")     ) );
		addToVectorBranch( vars, "muon_ptvarcone20" , toGeV( mu->auxdata< float >("ptvarcone20")  ) );
		addToVectorBranch( vars, "muon_ptvarcone30" , toGeV( mu->auxdata< float >("ptvarcone30")  ) );
		addToVectorBranch( vars, "muon_ptvarcone40" , toGeV( mu->auxdata< float >("ptvarcone40")  ) );
		addToVectorBranch( vars, "muon_topoetcone20", toGeV( mu->auxdata< float >("topoetcone20") ) );
		addToVectorBranch( vars, "muon_topoetcone30", toGeV( mu->auxdata< float >("topoetcone30") ) );
		addToVectorBranch( vars, "muon_topoetcone40", toGeV( mu->auxdata< float >("topoetcone40") ) );

		addToVectorBranch( vars, "muon_nPIX"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfPixelHits") : -999);
		addToVectorBranch( vars, "muon_nSCT"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfSCTHits")   : -999);
		addToVectorBranch( vars, "muon_nTRT"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfTRTHits")   : -999);

		addToVectorBranch( vars, "muon_nPres"      , mu->auxdata< unsigned char >("numberOfPrecisionLayers") );
		addToVectorBranch( vars, "muon_nPresGood"  , mu->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
	 	addToVectorBranch( vars, "muon_nPresHole"  , mu->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );

		addToVectorBranch( vars, "muon_CBchi2" , (cbtrack) ? cbtrack->auxdata< float >("chiSquared")/(cbtrack->auxdata< float >("numberDoF")) : -999. );


		// try to get info w.r.t. displaced vertices
		// turning off until susy derivations are available
		//std::cout << "muon d0 information" << std::endl;
		//std::vector<std::vector<float>> d0s_wrtDVs = mu->auxdata< std::vector<std::vector<float>> >("d0_wrtSVs");
		//std::vector<std::vector<float>> z0s_wrtDVs = mu->auxdata< std::vector<std::vector<float>> >("z0_wrtSVs");
		//for (unsigned int type=0; type<d0s_wrtDVs.size() ; type++ ){
		//	std::vector<float> d0s_wrtDVs_forType = d0s_wrtDVs.at(type);
		//	std::vector<float> z0s_wrtDVs_forType = z0s_wrtDVs.at(type);
		//	for (unsigned int dv=0; dv<d0s_wrtDVs_forType.size() ; dv++){

		//		addToVectorBranch( vars, "muDV_muIndex"   ,mu->index());
		//		addToVectorBranch( vars, "muDV_dvIndex"   ,dv);
		//		addToVectorBranch( vars, "muDV_typeIndex" ,type);
		//		addToVectorBranch( vars, "muDV_d0wrtDV" ,d0s_wrtDVs_forType.at(dv));
		//		addToVectorBranch( vars, "muDV_z0wrtDV" ,z0s_wrtDVs_forType.at(dv));

		//	}
		//}


		float qOverPsigma  = -999.;
		float qOverPsignif = -999.;
		float rho = -999.;
		if ( (idtrack) && (metrack) && (cbtrack) ){
			float cbPt = cbtrack->pt();
			float idPt = idtrack->pt();
			float mePt = metrack->pt();
			float meP  = 1.0 / ( sin(metrack->theta()) / mePt);
			float idP  = 1.0 / ( sin(idtrack->theta()) / idPt);

			rho = fabs( idPt - mePt ) / cbPt;
			qOverPsigma  = sqrt( idtrack->definingParametersCovMatrix()(4,4) + metrack->definingParametersCovMatrix()(4,4) );
			qOverPsignif  = fabs( (metrack->charge() / meP) - (idtrack->charge() / idP) ) / qOverPsigma;
		}
		addToVectorBranch( vars, "muon_rho"   , rho );
		addToVectorBranch( vars, "muon_QoverPsignif", qOverPsignif );

		addToVectorBranch( vars, "muon_author" ,     mu->auxdata< unsigned short >("author") );
		addToVectorBranch( vars, "muon_isCommonGood",mu->auxdata< char >("DFCommonGoodMuon") );
		addToVectorBranch( vars, "muon_isSignal",    mu->auxdata< char >("signal") );
		addToVectorBranch( vars, "muon_truthType",   mu->auxdata< int >("truthType") );
		addToVectorBranch( vars, "muon_truthOrigin", mu->auxdata< int >("truthOrigin") );

		// Retrieve DV Mapping info, 3 attempts
		//float dv_index = -99;
		// if ( idtrack && dvc->size() > 0) {
		// 	if ( TrkToVtxMap.find(idtrack->index()) != TrkToVtxMap.end() ) dv_index = TrkToVtxMap[idtrack->index()];
		// }

		// addToVectorBranch( vars, "muon_DVindex"  ,  dv_index );
		// addToVectorBranch( vars, "muon_TrigMatch"      ,      mu->auxdata< int >( "passTM" ) );
		// addToVectorBranch( vars, "muon_TrigMatchMSOnly",            mu->auxdata< int >( "passTMmsonly" ) );

	}

	const xAOD::TrackParticleContainer* ms_tracks = nullptr;
	STRONG_CHECK( event->retrieve(ms_tracks, "MuonSpectrometerTrackParticles") );
	const xAOD::MuonContainer *muons = nullptr;
	STRONG_CHECK( event->retrieve(muons, "Muons") );

	//std::cout << "Looping through standalone Muons" << std::endl;
	for ( auto muon : *muons ){

		const xAOD::TrackParticle* mst = muon->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
		if (mst) {

			//std::cout << "MS Track " << mst->index() << " : eta " << mst->eta() << " : phi " << mst->phi() << " : pt " << mst->pt() << std::endl;
			//if (MStrkToMuonMap.find(mst->index()) == MStrkToMuonMap.end()) std::cout << "found standalone" << std::endl;
			//else std::cout << "match cb muons" << std::endl;

			addToVectorBranch( vars, "mstrack_MuonIndex" ,  ( MStrkToMuonMap.find(mst->index()) == MStrkToMuonMap.end() ) ? -99 : MStrkToMuonMap[mst->index()] );
			addToVectorBranch( vars, "mstrack_eta" ,    mst->eta() );
			addToVectorBranch( vars, "mstrack_phi" ,    mst->phi() );
			addToVectorBranch( vars, "mstrack_pt"  ,    toGeV( mst->pt() )  );
			addToVectorBranch( vars, "mstrack_D0"  ,    mst->d0()  );
			addToVectorBranch( vars, "mstrack_Z0"  ,    mst->z0()  );
			addToVectorBranch( vars, "mstrack_ELoss" ,            toGeV(  muon->auxdata<float>("EnergyLoss")   	     ) );
			addToVectorBranch( vars, "mstrack_ELossSigma" ,       toGeV(  muon->auxdata<float>("EnergyLossSigma") 	     ) );
			addToVectorBranch( vars, "mstrack_MeasELoss" ,        toGeV(  muon->auxdata<float>("MeasEnergyLoss") 	     ) );
			addToVectorBranch( vars, "mstrack_MeasELossSigma" ,   toGeV(  muon->auxdata<float>("MeasEnergyLossSigma")      ) );
			addToVectorBranch( vars, "mstrack_ParamELoss" ,       toGeV(  muon->auxdata<float>("ParamEnergyLoss")          ) );
			addToVectorBranch( vars, "mstrack_ParamELossSigmaM" , toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaMinus")) );
			addToVectorBranch( vars, "mstrack_ParamELossSigmaP" , toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaPlus") ) );
			addToVectorBranch( vars, "mstrack_nPres" ,      muon->auxdata< unsigned char >("numberOfPrecisionLayers") );
			addToVectorBranch( vars, "mstrack_nPresGood" ,  muon->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
	 		addToVectorBranch( vars, "mstrack_nPresHole" ,  muon->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );
		}
	}

	///////////////////////////////////////////////////////////////
	//write out inner detector tracks
	//to do: only do this if they're opposite a CB muon?
	//or a right angle away? for bkg estimation

	if (m_saveIDtracks) {

		const xAOD::TrackParticleContainer* id_tracks = nullptr;
		STRONG_CHECK( event->retrieve(id_tracks, "InDetTrackParticles") );
	   	addToVectorBranch( vars, "n_idTracks" , id_tracks->size() );

		for (auto *id_track : *id_tracks){

			// Save ID tracks if back to back in dR cosmic with muons
			int cos_tag = 0;
			// for( xAOD::IParticle * muon : *muons_baseline){
			// 	xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);
			// 	if ( cosmic_Tag(id_track->p4(), mu->p4() ) ) cos_tag=1; 
			// }


 			if ( id_track->index()%m_id_prescale != 0   && !cos_tag) continue; //prescale!
			if ( fabs(id_track->d0())    < m_id_d0_min  && !cos_tag) continue; //d0 cut
			if ( toGeV( id_track->pt() ) < m_id_pt_min  && !cos_tag) continue; //pt cut

			addToVectorBranch( vars, "idTrack_index" , id_track->index() );
			addToVectorBranch( vars, "idTrack_cosTag", cos_tag );
			addToVectorBranch( vars, "idTrack_isLRT" , static_cast<int>( id_track->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) );


			// From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
			double id_pt = id_track->pt();
			double id_diff_qp = -id_pt / std::fabs(id_track->qOverP());
			double id_diff_theta = id_pt / tan(id_track->theta());
			const std::vector<float> &id_cov = id_track->definingParametersCovMatrixVec();
			double id_pt_err2 = id_diff_qp * (id_diff_qp * id_cov[14] + id_diff_theta * id_cov[13]) + id_diff_theta * id_diff_theta * id_cov[9];
			double id_errpT = toGeV( TMath::Sqrt(id_pt_err2) );

			addToVectorBranch( vars, "idTrack_errPt"   , id_errpT );
			addToVectorBranch( vars, "idTrack_errd0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(0, 0)) );
			addToVectorBranch( vars, "idTrack_errz0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(1, 1)) );

			addToVectorBranch( vars, "idTrack_theta"   ,id_track->theta() );
			addToVectorBranch( vars, "idTrack_eta"     ,id_track->eta() );
			addToVectorBranch( vars, "idTrack_phi"     ,id_track->phi() );
			addToVectorBranch( vars, "idTrack_pt"      ,toGeV( id_track->pt() ) );
			addToVectorBranch( vars, "idTrack_d0"      ,id_track->d0()  );
			addToVectorBranch( vars, "idTrack_z0"      ,id_track->z0()  );
			// addToVectorBranch( vars, "idTrack_z0WrtPV" ,id_track->z0() + id_track->vz() - pv->z() );
			addToVectorBranch( vars, "idTrack_charge"  ,id_track->charge() );
			addToVectorBranch( vars, "idTrack_chi2"    ,id_track->auxdata< float >("chiSquared")/(id_track->auxdata< float >("numberDoF")) );

			addToVectorBranch( vars, "idTrack_RadFirstHit"     ,id_track->auxdata< float >("radiusOfFirstHit"    ) );

			addToVectorBranch( vars, "idTrack_NPix_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfPixelHits"       ) );
			addToVectorBranch( vars, "idTrack_NSct_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfSCTHits"         ) );
			addToVectorBranch( vars, "idTrack_NTrt_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfTRTHits"         ) );
			addToVectorBranch( vars, "idTrack_NPix_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfPixelHoles"      ) );
			addToVectorBranch( vars, "idTrack_NSct_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfSCTHoles"        ) );
			addToVectorBranch( vars, "idTrack_NTrt_Outliers"   ,(int) id_track->auxdata< unsigned char >("numberOfTRTOutliers"     ) );
			addToVectorBranch( vars, "idTrack_NPix_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfPixelDeadSensors") );
			addToVectorBranch( vars, "idTrack_NPix_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfPixelSharedHits" ) );
			addToVectorBranch( vars, "idTrack_NSct_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) );
			addToVectorBranch( vars, "idTrack_NSct_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfSCTSharedHits"   ) );
		}

	}// end if save id tracks

	if (m_saveSegments){
    	const xAOD::MuonSegmentContainer* ms_segments = nullptr;
		STRONG_CHECK( event->retrieve(ms_segments, "MuonSegments") );

		for (auto *ms_segment : *ms_segments){

			addToVectorBranch( vars, "msSegment_x"            , ms_segment->x() );
			addToVectorBranch( vars, "msSegment_y"            , ms_segment->y() );
			addToVectorBranch( vars, "msSegment_z"            , ms_segment->z() );
			addToVectorBranch( vars, "msSegment_px"           , ms_segment->px() );
			addToVectorBranch( vars, "msSegment_py"           , ms_segment->py() );
			addToVectorBranch( vars, "msSegment_pz"           , ms_segment->pz() );
			addToVectorBranch( vars, "msSegment_t0"           , ms_segment->auxdataConst< float >( "t0" ) );
			addToVectorBranch( vars, "msSegment_t0Err"        , ms_segment->auxdataConst< float >( "t0error"      ) );
			addToVectorBranch( vars, "msSegment_clusTimeErr"  , ms_segment->auxdataConst< float >( "clusterTimeError" ) );
			addToVectorBranch( vars, "msSegment_clusTime"     , ms_segment->auxdataConst< float >( "clusterTime"      ) );
			addToVectorBranch( vars, "msSegment_chmbIndex"    , ms_segment->auxdataConst< int >( "chamberIndex" ) );
			addToVectorBranch( vars, "msSegment_tech"         , ms_segment->auxdataConst< int >( "technology"   ) );
			addToVectorBranch( vars, "msSegment_sector"       , ms_segment->auxdataConst< int >( "sector"       ) );
			addToVectorBranch( vars, "msSegment_etaIndex"     , ms_segment->auxdataConst< int >( "etaIndex"     ) );
			addToVectorBranch( vars, "msSegment_nTrigEtaLays" , ms_segment->auxdataConst< int >( "nTrigEtaLayers" ) );
			addToVectorBranch( vars, "msSegment_nPhiLays"     , ms_segment->auxdataConst< int >( "nPhiLayers"     ) );
			addToVectorBranch( vars, "msSegment_nPresHits"    , ms_segment->auxdataConst< int >( "nPrecisionHits" ) );

		}

	}//end doCosmic

	///////////////////////////////////////////////////////////////
	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

		// get the truth events to get the true signal vertex position
		const xAOD::TruthEventContainer* truthEvents = 0;
		STRONG_CHECK( event->retrieve( truthEvents, "TruthEvents" ));
		auto trueSignalVertex = *((*truthEvents->begin())->truthVertexLink(0));


		// now get the truth particles
		const xAOD::TruthParticleContainer* truthParticles = 0;
		STRONG_CHECK( event->retrieve( truthParticles, "TruthParticles" ) );
		for (const auto& p : *truthParticles){
			if(p->pdgId()>1e6 && p->hasDecayVtx()) {

				Int_t nCharged = 0;
				Int_t nCharged1GeV = 0;
				TLorentzVector chargedParticleFourVector;
				for(size_t ii = 0; ii < p->decayVtx()->nOutgoingParticles(); ++ii){
					if ( p->decayVtx()->outgoingParticle(ii)->charge() ){
						nCharged++;
						if( toGeV( p->decayVtx()->outgoingParticle(ii)->pt() ) > 1.  ) {
							nCharged1GeV++;
						}
						chargedParticleFourVector +=  p->decayVtx()->outgoingParticle(ii)->p4();
					}
				}
				float lifetimeLab = (p->decayVtx()->v4().Vect()-trueSignalVertex->v4().Vect()).Mag()/(p->p4().Vect().Mag()/(p->p4().Gamma()*p->p4().M())*TMath::C());

				addToVectorBranch( vars, "truthSparticlePdgId"               , (int) p->pdgId() );
				addToVectorBranch( vars, "truthSparticlePt"                  , toGeV(p->pt() )  );
				addToVectorBranch( vars, "truthSparticleEta"                 , p->eta()         );
				addToVectorBranch( vars, "truthSparticlePhi"                 , p->phi()         );
				addToVectorBranch( vars, "truthSparticleM"                   , toGeV(p->m())    );
				addToVectorBranch( vars, "truthSparticleBetaGamma"           , p->p4().Beta()*p->p4().Gamma()  );
				addToVectorBranch( vars, "truthSparticleProperDecayTime"     , lifetimeLab*p->p4().Gamma()  );
				addToVectorBranch( vars, "truthSparticleVtxX"                , p->decayVtx()->x()  );
				addToVectorBranch( vars, "truthSparticleVtxY"                , p->decayVtx()->y()  );
				addToVectorBranch( vars, "truthSparticleVtxZ"                , p->decayVtx()->z()  );
				addToVectorBranch( vars, "truthSparticleVtxNParticles"       , p->decayVtx()->nOutgoingParticles()  );
				addToVectorBranch( vars, "truthSparticleVtxNChParticles"     , nCharged  );
				addToVectorBranch( vars, "truthSparticleVtxNChParticles1GeV" , nCharged1GeV  );
				addToVectorBranch( vars, "truthSparticleVtxMChParticles"     , toGeV(chargedParticleFourVector.M() )  );
			}
		}
	}

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_slbjets::doSR0LCalculations(std::map<std::string, anytype>& /*vars*/ )
{
	return EL::StatusCode::SUCCESS;
}



EL::StatusCode RegionVarCalculator_slbjets::doSR2LCalculations(std::map<std::string, anytype>& /*vars*/ )
{

	return EL::StatusCode::SUCCESS;

}

