#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/SelectZeroLeptonEvents.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"

#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"

#include <boost/algorithm/string.hpp>
#include <algorithm>


// this is needed to distribute the algorithm to the workers
ClassImp(SelectZeroLeptonEvents)



SelectZeroLeptonEvents :: SelectZeroLeptonEvents () {}
EL::StatusCode SelectZeroLeptonEvents :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectZeroLeptonEvents :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectZeroLeptonEvents :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectZeroLeptonEvents :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectZeroLeptonEvents :: initialize () {return EL::StatusCode::SUCCESS;}

EL::StatusCode SelectZeroLeptonEvents :: execute ()
{
  xAOD::TStore * store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  // If the event didn't pass the preselection alg, don't bother doing anything with it...
  if(eventInfo->isAvailable<std::string>("regionName")){
    std::string preselectedRegionName =  eventInfo->auxdecor< std::string >("regionName");
    ATH_MSG_DEBUG("EventNumber: " << eventInfo->eventNumber()  );
    ATH_MSG_DEBUG("Preselected?: " << preselectedRegionName  );
    if( preselectedRegionName == "" ) return EL::StatusCode::SUCCESS;
  } else { // but maybe there was no preselection alg run, so ignore this check.
    eventInfo->auxdecor< std::string >("regionName") = "";
  }


  typedef std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer*> containerPair;

  auto createIParticleContainerInTStore = [](std::string name, xAOD::TStore* store)
  {
    containerPair outputContainers( new xAOD::IParticleContainer(SG::VIEW_ELEMENTS) , nullptr);
    outputContainers.first->setStore(outputContainers.second);
    store->record( outputContainers.first  , name );
    store->record( outputContainers.second , name+"Aux."  );//todo configurable if needed
    return outputContainers;
  };

  auto selectedLeptons          = createIParticleContainerInTStore("selectedLeptons",store);
  auto selectedBaselineLeptons  = createIParticleContainerInTStore("selectedBaselineLeptons",store);
  auto selectedPhotons          = createIParticleContainerInTStore("selectedPhotons",store);
  auto selectedJets             = createIParticleContainerInTStore("selectedJets",store);


  std::pair<xAOD::IParticleContainer* , xAOD::ParticleAuxContainer *> selectedRJigsawParticles(new xAOD::IParticleContainer(SG::VIEW_ELEMENTS), nullptr);
  selectedRJigsawParticles.first->setStore(selectedRJigsawParticles.second);
  STRONG_CHECK( store->record( selectedRJigsawParticles.first  , "myparticles"    ) );//todo configurable if needed
  STRONG_CHECK( store->record( selectedRJigsawParticles.second , "myparticlesaux."    ) );//todo configurable if needed


  xAOD::JetContainer* jets_nominal(nullptr);
  STRONG_CHECK(store->retrieve(jets_nominal, "STCalibAntiKt4EMTopoJets"));

  xAOD::MuonContainer* muons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(muons_nominal, "STCalibMuons"));

  xAOD::ElectronContainer* electrons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(electrons_nominal, "STCalibElectrons"));

  xAOD::PhotonContainer* photons_nominal(nullptr);
  STRONG_CHECK(store->retrieve(photons_nominal, "STCalibPhotons"));



  for (const auto& jet : *jets_nominal) {
    if ((int)jet->auxdata<char>("baseline") == 0) continue;
    if ((int)jet->auxdata<char>("passOR") != 1) continue;
    if ((int)jet->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful jet
    ATH_MSG_VERBOSE( "jet pt : " << jet->pt() );

    selectedJets.first->push_back(jet  );
  }

  for (const auto& mu : *muons_nominal) {
    if ((int)mu->auxdata<char>("baseline") == 0) continue;
    if ((int)mu->auxdata<char>("passOR") != 1) continue;

    selectedBaselineLeptons.first->push_back( mu );

    if ((int)mu->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful muon
    ATH_MSG_VERBOSE( "mu pt : " << mu->pt() );

    selectedLeptons.first->push_back( mu );
  }

  for (const auto& el : *electrons_nominal) {
    if ((int)el->auxdata<char>("baseline") == 0) continue;
    if ((int)el->auxdata<char>("passOR") != 1) continue;

    selectedBaselineLeptons.first->push_back( el );

    if ((int)el->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful el
    ATH_MSG_VERBOSE( "el pt : " << el->pt() );

    selectedLeptons.first->push_back( el );
  }


  for (const auto& ph : *photons_nominal) {
    if ((int)ph->auxdata<char>("baseline") == 0) continue;
    if ((int)ph->auxdata<char>("passOR") != 1) continue;
    if ((int)ph->auxdata<char>("signal") != 1) continue;
    // If I've gotten this far, I have a signal, isolated, beautiful el
    ATH_MSG_VERBOSE( "ph pt : " << ph->pt() );

    selectedPhotons.first->push_back( ph );
  }

  int const nLeptons = selectedLeptons.first->size();
  int const nBaselineLeptons = selectedBaselineLeptons.first->size();
  int const nPhotons = selectedPhotons.first->size();

  ATH_MSG_DEBUG("Number of Selected Leptons: " << nLeptons  );
  ATH_MSG_DEBUG("Number of Selected Baseline Leptons: " << nBaselineLeptons  );
  ATH_MSG_DEBUG("Number of Selected Photons: " << nPhotons  );

  bool passTM = false;
  for (auto lepton: *selectedLeptons.first){
    if(lepton->auxdecor< int >( "passTM" ) ) passTM = true;
  }


  auto trigORFromString = [](std::vector< std::string > passTrigs, std::string trigString){
    boost::replace_all(trigString, "_OR_", ":");
    std::vector<std::string> trigVect;
    boost::split(trigVect,trigString,boost::is_any_of(":") );
    bool trigDecision = 0;
    for(auto iTrig : trigVect){
      trigDecision |= std::find(passTrigs.begin(), passTrigs.end(), iTrig ) != passTrigs.end();
    }
    return trigDecision;
  };



  std::vector< std::string > const & passTrigs = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");

  std::map<std::string,int> passedTriggers;
  passedTriggers["HLT_xe70"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_xe70") != passTrigs.end();
  passedTriggers["HLT_xe100"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_xe100") != passTrigs.end();
  passedTriggers["HLT_xe100_mht_L1XE50"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_xe100_mht_L1XE50") != passTrigs.end();

  // passedTriggers["HLT_e24_lhmedium_L1EM18VH"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_e24_lhmedium_L1EM18VH") != passTrigs.end();
  // passedTriggers["HLT_e26_lhtight_nod0_ivarloose"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_e26_lhtight_nod0_ivarloose") != passTrigs.end();
  // passedTriggers["HLT_mu20_iloose_L1MU15"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu20_iloose_L1MU15") != passTrigs.end();
  // passedTriggers["HLT_mu26_ivarmedium"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_mu26_ivarmedium") != passTrigs.end();

  passedTriggers["HLT_g140_loose"] = std::find(passTrigs.begin(), passTrigs.end(), "HLT_g140_loose") != passTrigs.end();

  passedTriggers["MET"] = eventInfo->auxdecor<bool>("isMETTrigPassed");

  if(eventInfo->auxdecor<float>("year")==2015){
    //passedTriggers["MET"] = passedTriggers["HLT_xe70"];
    passedTriggers["Electron"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("elTrig2015") );
    passedTriggers["Muon"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("muTrig2015") );
    passedTriggers["Photon"] = passedTriggers["HLT_g140_loose"];
  } else {
    //passedTriggers["MET"] = trigORFromString(passTrigs,"HLT_xe100_mht_L1XE50_OR_HLT_xe110_mht_L1XE50");
    passedTriggers["Electron"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("elTrig2016") );
    passedTriggers["Muon"] = trigORFromString(passTrigs,eventInfo->auxdecor<std::string>("muTrig2016") );
    passedTriggers["Photon"] = passedTriggers["HLT_g140_loose"];
  }


  auto  getRegionName = [](int nLeptons, int nBaselineLeptons, int nPhotons, std::map<std::string,int>  passedTriggers, bool passTM){ 
	  std::string regionName = "";
    if( nPhotons > 0  && passedTriggers["Photon"] ){regionName = "CRY";}
    if( nLeptons == 1 && (passedTriggers["Electron"]||passedTriggers["Muon"]) && passTM){regionName = "CR1L";}
    if( nLeptons == 2 && (passedTriggers["Electron"]||passedTriggers["Muon"]) && passTM){regionName = "CR2L";}
	  if( nBaselineLeptons == 0 && passedTriggers["MET"] ) {regionName = "SR";}
	  return regionName;};

  ATH_MSG_DEBUG("Event falls in region: " << getRegionName( nLeptons, nBaselineLeptons, nPhotons, passedTriggers, passTM )  );

  ATH_MSG_DEBUG("Writing particle container for calculator to store");

  eventInfo->auxdecor< std::string >("regionName") = getRegionName( nLeptons, nBaselineLeptons, nPhotons, passedTriggers, passTM ) ;

  // // What happens if we add the jets into the calculation?
  for( const auto& myjet: *selectedJets.first){
    selectedRJigsawParticles.first->push_back( myjet );
  }

  if (eventInfo->auxdecor< std::string >("regionName") == "CR1L"){
    for( const auto& mylep: *selectedLeptons.first){
      selectedRJigsawParticles.first->push_back( mylep );
    }
  }


  ATH_MSG_DEBUG("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   );




  return EL::StatusCode::SUCCESS;
}



EL::StatusCode SelectZeroLeptonEvents :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectZeroLeptonEvents :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectZeroLeptonEvents :: histFinalize () {return EL::StatusCode::SUCCESS;}
