#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "xAODTrigger/MuonRoI.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/L2StandAloneMuon.h"

#include "FactoryTools/RegionVarCalculator_cosmics.h"
#include "FactoryTools/strongErrorCheck.h"

#include <xAODAnaHelpers/HelperFunctions.h>

// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_cosmics)

EL::StatusCode RegionVarCalculator_cosmics::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	return EL::StatusCode::SUCCESS;
}

EL::StatusCode RegionVarCalculator_cosmics::doCalculate(std::map<std::string, anytype>& vars )
{
        //xAOD::TStore * store = m_worker->xaodStore();//grab the store from the worker
	xAOD::TEvent* event = m_worker->xaodEvent();

	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

	if      ( regionName.empty() ) {return EL::StatusCode::SUCCESS;}
	// If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
	else if ( regionName == "SRCOS" ) {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); }


	return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_cosmics::doAllCalculations(std::map<std::string, anytype>& vars )
{/*todo*/
        //xAOD::TStore * store = m_worker->xaodStore();
	xAOD::TEvent * event = m_worker->xaodEvent();


	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	//doGeneralCalculations(vars);
	//don't have pileup or PVs so don't want to break anything

  	// Include variables for all samples ///////////////////////////////
  	//
  	vars["runNumber"]   = eventInfo->runNumber();
  	vars["lumiBlock"]   = eventInfo->lumiBlock();
  	vars["bcid"]        = eventInfo->bcid();
  	vars["eventNumber:longlong"] = eventInfo->eventNumber();

	auto toGeV = [](double a){return a*.001;};

	/////////////////////////////////////////////////////////////////////
	// Save all the trigger information we can! 

	// *****
	// Level 1 ROIs, eta, phi, and threshold
  	const xAOD::MuonRoIContainer* LVL1MuonRoIs = 0;
  	STRONG_CHECK(event->retrieve( LVL1MuonRoIs, "LVL1MuonRoIs" ));
  	for (auto l1_muon : (*LVL1MuonRoIs)){
  	   
  		addToVectorBranch( vars, "lvl1_muon_thres", toGeV(l1_muon->thrValue()) );
  		addToVectorBranch( vars, "lvl1_muon_eta"  , l1_muon->eta() );
  		addToVectorBranch( vars, "lvl1_muon_phi"  , l1_muon->phi() );
		
  	}


	// *****
	// L2 Standalone Muons
  	const xAOD::L2StandAloneMuonContainer* L2SAMuons = 0;
  	STRONG_CHECK(event->retrieve( L2SAMuons, "HLT_xAOD__L2StandAloneMuonContainer_MuonL2SAInfo" ));
  	for (auto lvl2_muon : (*L2SAMuons)){
  	   
  		addToVectorBranch( vars, "lvl2_muon_pt"  , toGeV(lvl2_muon->pt()) );
  		addToVectorBranch( vars, "lvl2_muon_eta" ,       lvl2_muon->eta() );
  		addToVectorBranch( vars, "lvl2_muon_phi" ,       lvl2_muon->phi() );
		
  	}

	// *****
	// Regular HLT Muons
  	const xAOD::MuonContainer* HLTMuons = 0;
  	STRONG_CHECK(event->retrieve( HLTMuons, "HLT_xAOD__MuonContainer_MuonEFInfo" ));
  	for (auto hlt_muon : (*HLTMuons)){
  	   
 		addToVectorBranch( vars, "hlt_muon_pt"    ,toGeV(hlt_muon->pt()) );
 		addToVectorBranch( vars, "hlt_muon_eta"   ,   hlt_muon->eta() );
 		addToVectorBranch( vars, "hlt_muon_phi"   ,   hlt_muon->phi() );
		
  	}

	// *****
	// Full scan HLT msonly??
  	const xAOD::MuonContainer* HLTmsonlyMuons = 0;
  	STRONG_CHECK(event->retrieve( HLTmsonlyMuons, "HLT_xAOD__MuonContainer_MuonEFInfo_MSonlyTrackParticles_FullScan" ));
  	for (auto hlt_muon : (*HLTmsonlyMuons)){
  	   
  		addToVectorBranch( vars , "hlt_msonly_pt"  , toGeV(hlt_muon->pt()) );
  		addToVectorBranch( vars , "hlt_msonly_eta" ,    hlt_muon->eta() );
  		addToVectorBranch( vars , "hlt_msonly_phi" ,    hlt_muon->phi() );
		
  	}
	
	// *****
	// Trigger decision information
  	vars["passHLT_mu60_0eta105_msonly"]            = eventInfo->auxdecor<bool>("passHLT_mu60_0eta105_msonly"            );
  	vars["passHLT_mu0_muoncalib_L1MU4_EMPTY"]      = eventInfo->auxdecor<bool>("passHLT_mu0_muoncalib_L1MU4_EMPTY"      );
  	vars["passHLT_mu4_cosmic_L1MU11_EMPTY"]        = eventInfo->auxdecor<bool>("passHLT_mu4_cosmic_L1MU11_EMPTY"        );
  	vars["passHLT_mu4_cosmic_L1MU4_EMPTY"]         = eventInfo->auxdecor<bool>("passHLT_mu4_cosmic_L1MU4_EMPTY"         );
  	vars["passHLT_mu4_msonly_cosmic_L1MU11_EMPTY"] = eventInfo->auxdecor<bool>("passHLT_mu4_msonly_cosmic_L1MU11_EMPTY" );
  	vars["passHLT_mu4_msonly_cosmic_L1MU4_EMPTY" ] = eventInfo->auxdecor<bool>("passHLT_mu4_msonly_cosmic_L1MU4_EMPTY"  );
  	vars["passHLT_noalg_cosmicmuons_L1MU4_EMPTY" ] = eventInfo->auxdecor<bool>("passHLT_noalg_cosmicmuons_L1MU4_EMPTY"  );
  	vars["passHLT_noalg_cosmicmuons_L1MU11_EMPTY"] = eventInfo->auxdecor<bool>("passHLT_noalg_cosmicmuons_L1MU11_EMPTY"  );

	// don't forget stopped sparticless
  	vars["passHLT_noalg_L1J12"] 			 = eventInfo->auxdecor<bool>("passHLT_noalg_L1J12"); 

	// Muon information
	const xAOD::MuonContainer* uncalibmuons_nominal(nullptr);
	STRONG_CHECK(event->retrieve(uncalibmuons_nominal, "Muons"));

	//std::cout << "Looping through selectedBaselineMuons " 	<< std::endl;
	for(const auto& mu : *uncalibmuons_nominal){
		
		//xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);
		
		addToVectorBranch( vars, "muon_index"  ,  mu->index()     );
		addToVectorBranch( vars, "muon_pt"     ,  toGeV(mu->pt()) );
		addToVectorBranch( vars, "muon_eta"    ,  mu->p4().Eta()  );
		addToVectorBranch( vars, "muon_phi"    ,  mu->p4().Phi()  );
		addToVectorBranch( vars, "muon_charge" ,  mu->charge()    );	


		const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
		const xAOD::TrackParticle* metrack = mu->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
		const xAOD::TrackParticle* mstrack = mu->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
		const xAOD::TrackParticle* cbtrack = mu->trackParticle( xAOD::Muon::CombinedTrackParticle );

		addToVectorBranch( vars, "muon_hasCBtrack", (cbtrack) ? 1 : 0 );	
		addToVectorBranch( vars, "muon_hasIDtrack", (idtrack) ? 1 : 0 );	
		addToVectorBranch( vars, "muon_hasMEtrack", (metrack) ? 1 : 0 );	
		addToVectorBranch( vars, "muon_hasMStrack", (mstrack) ? 1 : 0 );	
		
		addToVectorBranch( vars, "muon_d0" ,  (idtrack) ? idtrack->d0() : -9999.);
		addToVectorBranch( vars, "muon_z0" ,  (idtrack) ? idtrack->z0() : -9999.);
		addToVectorBranch( vars, "muon_isLRT" , (idtrack) ? static_cast<int>( idtrack->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) : 0 );
		addToVectorBranch( vars, "muon_RadFirstHit" , (idtrack) ? idtrack->auxdata< float >("radiusOfFirstHit" ) : -999. );
		
		addToVectorBranch( vars, "muon_ptcone20"    , toGeV( mu->auxdata< float >("ptcone20")     ));
		addToVectorBranch( vars, "muon_ptcone30"    , toGeV( mu->auxdata< float >("ptcone30")     ));
		addToVectorBranch( vars, "muon_ptcone40"    , toGeV( mu->auxdata< float >("ptcone40")     ));
		addToVectorBranch( vars, "muon_ptvarcone20" , toGeV( mu->auxdata< float >("ptvarcone20")  ));
		addToVectorBranch( vars, "muon_ptvarcone30" , toGeV( mu->auxdata< float >("ptvarcone30")  ));
		addToVectorBranch( vars, "muon_ptvarcone40" , toGeV( mu->auxdata< float >("ptvarcone40")  ));
		addToVectorBranch( vars, "muon_topoetcone20", toGeV( mu->auxdata< float >("topoetcone20") ));
		addToVectorBranch( vars, "muon_topoetcone30", toGeV( mu->auxdata< float >("topoetcone30") ));
		addToVectorBranch( vars, "muon_topoetcone40", toGeV( mu->auxdata< float >("topoetcone40") ));

		addToVectorBranch( vars, "muon_nPIX" , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfPixelHits") : -999);
		addToVectorBranch( vars, "muon_nSCT" , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfSCTHits")   : -999);
		addToVectorBranch( vars, "muon_nTRT" , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfTRTHits")   : -999);

		addToVectorBranch( vars, "muon_nPres"    , mu->auxdata< unsigned char >("numberOfPrecisionLayers") );
		addToVectorBranch( vars, "muon_nPresGood", mu->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
	 	addToVectorBranch( vars, "muon_nPresHole", mu->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );

		addToVectorBranch( vars, "muon_CBchi2" , (cbtrack) ? cbtrack->auxdata< float >("chiSquared")/(cbtrack->auxdata< float >("numberDoF")) : -999. );

		float qOverPsigma  = -999.;
		float qOverPsignif = -999.;
		float rho = -999.;
		if ( (idtrack) && (metrack) && (cbtrack) ){
			float cbPt = cbtrack->pt();
			float idPt = idtrack->pt();
			float mePt = metrack->pt();
			float meP  = 1.0 / ( sin(metrack->theta()) / mePt);
			float idP  = 1.0 / ( sin(idtrack->theta()) / idPt);
			
			rho = fabs( idPt - mePt ) / cbPt;
			qOverPsigma  = sqrt( idtrack->definingParametersCovMatrix()(4,4) + metrack->definingParametersCovMatrix()(4,4) ); 	
			qOverPsignif  = fabs( (metrack->charge() / meP) - (idtrack->charge() / idP) ) / qOverPsigma;
		}
		addToVectorBranch( vars, "muon_rho"   , rho );
		addToVectorBranch( vars, "muon_QoverPsignif", qOverPsignif );
		addToVectorBranch( vars, "muon_author" ,     mu->auxdata< unsigned short >("author") );
		addToVectorBranch( vars, "muon_medium" ,     mu->auxdata< int >("medium") );

 		addToVectorBranch( vars, "muon_match_L1MU4"      , mu->auxdata<int>("match_L1MU4_EMPTY" ) );
 		addToVectorBranch( vars, "muon_match_L1MU11"     , mu->auxdata<int>("match_L1MU11_EMPTY") );
 		addToVectorBranch( vars, "muon_match_mu4_L1MU4"  , mu->auxdata<int>("match_HLT_mu4_msonly_cosmic_L1MU4_EMPTY" )); 
 		addToVectorBranch( vars, "muon_match_mu4_L1MU11" , mu->auxdata<int>("match_HLT_mu4_msonly_cosmic_L1MU11_EMPTY")); 
 		addToVectorBranch( vars, "muon_match_mu60"       , mu->auxdata<int>("match_HLT_mu60_0eta105_msonly"           ));

	}
	
	///////////////////////////////////////////////////////////////

	const xAOD::TrackParticleContainer* ms_tracks = nullptr;
	STRONG_CHECK( event->retrieve(ms_tracks, "MuonSpectrometerTrackParticles") );
	const xAOD::MuonContainer *muons = nullptr;
	STRONG_CHECK( event->retrieve(muons, "Muons") );
	for ( auto muon : *muons ){
		
		const xAOD::TrackParticle* mst = muon->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
		if (mst) {

			addToVectorBranch( vars, "mstrack_eta" , mst->eta() );
			addToVectorBranch( vars, "mstrack_phi" , mst->phi() );
			addToVectorBranch( vars, "mstrack_pt"  , toGeV( mst->pt() )  ); 
			addToVectorBranch( vars, "mstrack_D0"  , mst->d0()  );
			addToVectorBranch( vars, "mstrack_Z0"  , mst->z0()  );

			addToVectorBranch( vars, "mstrack_ELoss"            ,toGeV(  muon->auxdata<float>("EnergyLoss")   	     ) );
			addToVectorBranch( vars, "mstrack_ELossSigma"       ,toGeV(  muon->auxdata<float>("EnergyLossSigma") 	     ) );
			addToVectorBranch( vars, "mstrack_MeasELoss"        ,toGeV(  muon->auxdata<float>("MeasEnergyLoss") 	     ) );
			addToVectorBranch( vars, "mstrack_MeasELossSigma"   ,toGeV(  muon->auxdata<float>("MeasEnergyLossSigma")      ) );
			addToVectorBranch( vars, "mstrack_ParamELoss"       ,toGeV(  muon->auxdata<float>("ParamEnergyLoss")          ) );
			addToVectorBranch( vars, "mstrack_ParamELossSigmaM" ,toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaMinus")) );
			addToVectorBranch( vars, "mstrack_ParamELossSigmaP" ,toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaPlus") ) );

			addToVectorBranch( vars, "mstrack_nPres"    , muon->auxdata< unsigned char >("numberOfPrecisionLayers") );
			addToVectorBranch( vars, "mstrack_nPresGood", muon->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
	 		addToVectorBranch( vars, "mstrack_nPresHole", muon->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );

		}
	}

	///////////////////////////////////////////////////////////////
	//write out inner detector tracks 
	const xAOD::TrackParticleContainer* id_tracks = nullptr;
	STRONG_CHECK( event->retrieve(id_tracks, "InDetTrackParticles") );

	for (auto *id_track : *id_tracks){
 
		if ( fabs(id_track->d0()) < m_id_d0_min ) continue;
		if ( toGeV( id_track->pt() ) < 1.       ) continue; 

		addToVectorBranch( vars, "idTrack_index" , id_track->index() );
		addToVectorBranch( vars, "idTrack_isLRT" , static_cast<int>( id_track->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) );

		// From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
		double id_pt = id_track->pt();
		double id_diff_qp = -id_pt / std::fabs(id_track->qOverP());
		double id_diff_theta = id_pt / tan(id_track->theta());
		const std::vector<float> &id_cov = id_track->definingParametersCovMatrixVec();
		double id_pt_err2 = id_diff_qp * (id_diff_qp * id_cov[14] + id_diff_theta * id_cov[13]) + id_diff_theta * id_diff_theta * id_cov[9];
		double id_errpT = toGeV( TMath::Sqrt(id_pt_err2) );

		addToVectorBranch( vars, "idTrack_errPt"   , id_errpT );
		addToVectorBranch( vars, "idTrack_errd0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(0, 0)) );
		addToVectorBranch( vars, "idTrack_errz0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(1, 1)) );

		addToVectorBranch( vars, "idTrack_theta"   ,id_track->theta() );
		addToVectorBranch( vars, "idTrack_eta"     ,id_track->eta() );
		addToVectorBranch( vars, "idTrack_phi"     ,id_track->phi() );
		addToVectorBranch( vars, "idTrack_pt"      ,toGeV( id_track->pt() ) );
		addToVectorBranch( vars, "idTrack_d0"      ,id_track->d0()  );
		addToVectorBranch( vars, "idTrack_z0"      ,id_track->z0()  );
		addToVectorBranch( vars, "idTrack_charge"  ,id_track->charge() );
		addToVectorBranch( vars, "idTrack_chi2"    ,id_track->auxdata< float >("chiSquared")/(id_track->auxdata< float >("numberDoF")) );

		addToVectorBranch( vars, "idTrack_RadFirstHit"     ,id_track->auxdata< float >("radiusOfFirstHit"    ) );

		addToVectorBranch( vars, "idTrack_NPix_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfPixelHits"       ) );
		addToVectorBranch( vars, "idTrack_NSct_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfSCTHits"         ) );
		addToVectorBranch( vars, "idTrack_NTrt_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfTRTHits"         ) );
		addToVectorBranch( vars, "idTrack_NPix_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfPixelHoles"      ) );
		addToVectorBranch( vars, "idTrack_NSct_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfSCTHoles"        ) );
		addToVectorBranch( vars, "idTrack_NTrt_Outliers"   ,(int) id_track->auxdata< unsigned char >("numberOfTRTOutliers"     ) );
		addToVectorBranch( vars, "idTrack_NPix_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfPixelDeadSensors") );
		addToVectorBranch( vars, "idTrack_NPix_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfPixelSharedHits" ) );
		addToVectorBranch( vars, "idTrack_NSct_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) );
		addToVectorBranch( vars, "idTrack_NSct_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfSCTSharedHits"   ) );

	}
	
    	const xAOD::MuonSegmentContainer* ms_segments = nullptr;
	STRONG_CHECK( event->retrieve(ms_segments, "MuonSegments") );

	for (auto *ms_segment : *ms_segments){

		addToVectorBranch( vars, "msSegment_x"            , ms_segment->x() );
		addToVectorBranch( vars, "msSegment_y"            , ms_segment->y() );
		addToVectorBranch( vars, "msSegment_z"            , ms_segment->z() );
		addToVectorBranch( vars, "msSegment_px"           , ms_segment->px() );
		addToVectorBranch( vars, "msSegment_py"           , ms_segment->py() );
		addToVectorBranch( vars, "msSegment_pz"           , ms_segment->pz() );
		addToVectorBranch( vars, "msSegment_t0"           , ms_segment->auxdataConst< float >( "t0" ) );
		addToVectorBranch( vars, "msSegment_t0Err"        , ms_segment->auxdataConst< float >( "t0error"      ) );
		addToVectorBranch( vars, "msSegment_clusTimeErr"  , ms_segment->auxdataConst< float >( "clusterTimeError" ) );
		addToVectorBranch( vars, "msSegment_clusTime"     , ms_segment->auxdataConst< float >( "clusterTime"      ) );
		addToVectorBranch( vars, "msSegment_chmbIndex"    , ms_segment->auxdataConst< int >( "chamberIndex" ) );
		addToVectorBranch( vars, "msSegment_tech"         , ms_segment->auxdataConst< int >( "technology"   ) );
		addToVectorBranch( vars, "msSegment_sector"       , ms_segment->auxdataConst< int >( "sector"       ) );
		addToVectorBranch( vars, "msSegment_etaIndex"     , ms_segment->auxdataConst< int >( "etaIndex"     ) );
		addToVectorBranch( vars, "msSegment_nTrigEtaLays" , ms_segment->auxdataConst< int >( "nTrigEtaLayers" ) );
		addToVectorBranch( vars, "msSegment_nPhiLays"     , ms_segment->auxdataConst< int >( "nPhiLayers"     ) );
		addToVectorBranch( vars, "msSegment_nPresHits"    , ms_segment->auxdataConst< int >( "nPrecisionHits" ) );

	}

	///////////////////////////////////////////////////////////////

	// ****
	// Jets for cross check w/ stopped particles 
	const xAOD::JetContainer* uncalibjets_nominal(nullptr);
	STRONG_CHECK(event->retrieve(uncalibjets_nominal, "AntiKt4EMTopoJets"));
	for (const auto& jet : *uncalibjets_nominal)
	{
		addToVectorBranch( vars, "jetPt" , toGeV(jet->pt() ) );
		addToVectorBranch( vars, "jetEta", jet->p4().Eta() );
		addToVectorBranch( vars, "jetPhi", jet->p4().Phi() );
		addToVectorBranch( vars, "jetM"  , toGeV(jet->p4().M() ) );

		addToVectorBranch( vars, "jetN90Constituents"   ,jet->auxdata<float>("N90Constituents"  ) );
		addToVectorBranch( vars, "jetOotFracClusters5"  ,jet->auxdata<float>("OotFracClusters5" ) );
		addToVectorBranch( vars, "jetOotFracClusters10" ,jet->auxdata<float>("OotFracClusters10") );
		addToVectorBranch( vars, "jetTiming"            ,jet->auxdata<float>("Timing"           ) );
		addToVectorBranch( vars, "jetWidth"             ,jet->auxdata<float>("Width"            ) );
		addToVectorBranch( vars, "jetNegativeE"         ,jet->auxdata<float>("NegativeE"        ) );
		addToVectorBranch( vars, "jetEMFrac"            ,jet->auxdata<float>("EMFrac"           ) );
		addToVectorBranch( vars, "jetHECFrac"           ,jet->auxdata<float>("HECFrac"          ) );
		addToVectorBranch( vars, "jetLArQuality"        ,jet->auxdata<float>("LArQuality"       ) ); 
		addToVectorBranch( vars, "jetAverageLArQF"      ,jet->auxdata<float>("AverageLArQF"     ) );
		addToVectorBranch( vars, "jetFracSamplingMax"   ,jet->auxdata<float>("FracSamplingMax"  ) );

	}


	///////////////////////////////////////////////////////////////
	
	// ****
	// MET
	
	// retrieve MET_LocHadTopo container
	const xAOD::MissingETContainer* met_lht_container = nullptr;
	STRONG_CHECK( event->retrieve(met_lht_container, "MET_LocHadTopo") );
	bool hasMET = met_lht_container->size() > 0;
	vars["MET_LHT"    ] = hasMET ? met_lht_container->at(0)->met() * 0.001 : -999.;
	vars["MET_LHT_phi"] = hasMET ? met_lht_container->at(0)->phi() : -999.;

	// retrieve HLT MET 
	TVector2 hltMET;
const xAOD::TrigMissingETContainer* met_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(met_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET") );
	hasMET = met_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(met_hlt_container->at(0)->ex(), met_hlt_container->at(0)->ey());
	vars["MET_HLT"    ] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MET_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

	// retrieve HLT MHT
	const xAOD::TrigMissingETContainer* mht_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(mht_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht") );
	hasMET = mht_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(mht_hlt_container->at(0)->ex(), mht_hlt_container->at(0)->ey());
	vars["MHT_HLT"    ] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MHT_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;


	///////////////////////////////////////////////////////////////
	
	// ****
	// Electrons bc why not
	
	const xAOD::ElectronContainer* uncalibelectrons_nominal(nullptr);
	STRONG_CHECK(event->retrieve(uncalibelectrons_nominal, "Electrons"));

	for(const auto& el : *uncalibelectrons_nominal){

		addToVectorBranch( vars, "electron_pt"  ,  toGeV(el->pt()) );
		addToVectorBranch( vars, "electron_eta" ,  el->eta() );
		addToVectorBranch( vars, "electron_phi" ,  el->phi() );
		
		const xAOD::TrackParticle *eltrk = el->trackParticle();
		float eld0 = -9999.;
		float elz0 = -9999.;
		if (eltrk){
			eld0 = eltrk->d0();
			elz0 = eltrk->z0();
		}
		addToVectorBranch( vars, "electron_d0", eld0 );
		addToVectorBranch( vars, "electron_z0", elz0 );
	}

	return EL::StatusCode::SUCCESS;
}

