#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>


#include "JetRec/PseudoJetGetter.h"
//#include "JetRecTools/TrackPseudoJetGetter.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetRec/JetFinder.h"
#include "JetRec/JetSplitter.h"
#include "JetRec/JetRecTool.h"
#include "JetRec/JetDumper.h"
#include "JetRec/JetToolRunner.h"

#include "FactoryTools/JetMakerTool.h"
#include <FactoryTools/HelperFunctions.h>
#include "AsgTools/Check.h"
#include "JetRec/JetFromPseudojet.h"
#include "JetInterface/IJetFromPseudojet.h"
#include "xAODJet/Jet.h"
#include "JetEDM/PseudoJetVector.h"
#include "JetEDM/JetConstituentFiller.h"
#include "JetEDM/FastJetLink.h"
#include "xAODJet/Jet_PseudoJet.icc"
#include "JetRec/JetFromPseudojet.h"
#include "JetEDM/IndexedConstituentUserInfo.h"
#include "JetEDM/LabelIndex.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "AthLinks/ElementLink.h"

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"

#include "fastjet/contrib/SoftDrop.hh"
#include "xAODAnaHelpers/HelperFunctions.h"
#include <xAODAnaHelpers/tools/ReturnCheck.h>


#include <sstream>
#include <string>

using namespace std;


typedef IJetFromPseudojet::NameList NameList;
typedef FactoryTools::HelperFunctions HF;

ClassImp(JetMakerTool)


JetMakerTool::JetMakerTool():
	Algorithm("JetMakerTool")
{
	m_name = "JetMakerTool";

  JetAlgorithm = "AntiKt";
  InputLabel = "Track";
  InputClusters = "InDetTrackParticles";

  PtMin =  0.001;
  JetRadius = 0.4;
  SkipNegativeEnergy = true;
  onlyPV = true;
  minNTracks = 0;
  minSumPtTracks = 0.0;
  
}

EL::StatusCode JetMakerTool :: setupJob (EL::Job& job){
  Info("setupJob()", "Calling setupJob");

  job.useXAOD ();
  xAOD::Init("JetMakerTool").ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetMakerTool :: histInitialize (){
  RETURN_CHECK("xAH::Algorithm::algInitialize()", xAH::Algorithm::algInitialize(), "");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerTool :: fileExecute (){
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerTool :: changeInput (bool /*firstFile*/){
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerTool::initialize(){

  ///////////////////////////////////////////////////////
  // Prepares input for jet finding. 
  // Translates inputs from xAOD format to fastjet format
  ///////////////////////////////////////////////////////

  plcget = new PseudoJetGetter(Form("m_pjgetter%s", (const char *) OutputContainer));
  // Below 2 lines are dummy - these will be set again in execute
  EL_RETURN_CHECK("initialize()", plcget->setProperty("InputContainer", (const char *) InputClusters ));
  EL_RETURN_CHECK("initialize()", plcget->setProperty("OutputContainer", (const char *) OutputContainer ) );
  EL_RETURN_CHECK("initialize()", plcget->setProperty("Label", (const char *) InputLabel) );
  EL_RETURN_CHECK("initialize()", plcget->setProperty("SkipNegativeEnergy", true) );
  EL_RETURN_CHECK("initialize()", plcget->setProperty("GhostScale", 0.0) );
  EL_RETURN_CHECK("initialize()", plcget->initialize() );
  ToolHandle<IPseudoJetGetter> hlcget(plcget);
  hgets.push_back(hlcget);

  //check jetrec.cxx for jvt and many more attributes to be added
  cout << "Creating jet builder." << endl;
  pbuild = new JetFromPseudojet(Form("jetbuild%s", (const char *) OutputContainer));
  ToolHandle<IJetFromPseudojet> hbuild(pbuild);
  vector<string> jetbuildatts;
  jetbuildatts.push_back("ActiveArea");
  jetbuildatts.push_back("ActiveAreaFourVector");
  EL_RETURN_CHECK("initialize()", pbuild->setProperty("Attributes", jetbuildatts) );
  EL_RETURN_CHECK("initialize()", pbuild->initialize() );

  cout << "Creating jet finder." << endl;
  pfind = new JetFinder(Form("jetfind%s", (const char *) OutputContainer));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("JetAlgorithm", (const char *) JetAlgorithm) ); //JetAlgorithm = AntiKt
  EL_RETURN_CHECK("initialize()", pfind->setProperty("JetRadius", JetRadius));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("PtMin", PtMin));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("GhostArea", 0.01));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("RandomOption", 1));
  EL_RETURN_CHECK("initialize()", pfind->setProperty("JetBuilder", hbuild)); //Initialized in previous chunk
  ToolHandle<IJetFinder> hfind(pfind);
  EL_RETURN_CHECK("initialize()",pfind->initialize() );
  cout << "Creating jetrec tool." << endl;
  // Calls fastjet 
  // Runs PseudoJetGetter, Jet Finder, records final JetContainer
  pjrf = new JetRecTool(Form("jrfind%s", (const char *) OutputContainer));
  EL_RETURN_CHECK("initialize()", pjrf->setProperty("OutputContainer", (const char *) OutputContainer));
  EL_RETURN_CHECK("initialize()", pjrf->setProperty("PseudoJetGetters", hgets));
  EL_RETURN_CHECK("initialize()", pjrf->setProperty("JetFinder", hfind)); // Initialized in previous chunk
  EL_RETURN_CHECK("initialize()", pjrf->initialize());
  ToolHandle<IJetExecuteTool> hjrf(pjrf);
  hrecs.push_back(pjrf);

  jrun = new JetToolRunner(Form("jetrunner%s", (const char *) OutputContainer));
  EL_RETURN_CHECK("initialize()", jrun->setProperty("Tools", hrecs) );
  cout << "Initializing tools." << endl;
  EL_RETURN_CHECK("initialize()", jrun->initialize() );
  jrun->print();


  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetMakerTool::execute() {

  xAOD::TEvent* event = wk()->xaodEvent();
  auto vertices = HF::grabFromEvent<xAOD::VertexContainer>("PrimaryVertices",event);

  for (size_t i=0; i<vertices->size(); i++){

    if (onlyPV && i!=0) continue;
         

    TString m_outcont_pseudo = OutputContainer + "PJ_" + std::to_string(i);
    TString m_outcont_jets = OutputContainer + "_" + std::to_string(i);
    TString inputName = InputClusters + "_" + std::to_string(i);

    auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);
    TString numTracks = InputClusters + "_" + std::to_string(i) + "_numTracks";
    TString sumPt = InputClusters + "_" + std::to_string(i) + "_scalarSumPt";
    if (eventInfo->auxdecor<int>((const char *) numTracks)<minNTracks) continue;
    if (eventInfo->auxdecor<float>((const char *) sumPt)<minSumPtTracks) continue;

    EL_RETURN_CHECK("execute()", plcget->setProperty("InputContainer", (const char *) inputName ));
    EL_RETURN_CHECK("execute()", plcget->setProperty("OutputContainer", (const char *) m_outcont_pseudo ) );
    EL_RETURN_CHECK("execute()", pjrf->setProperty("OutputContainer", (const char *) m_outcont_jets));

    jrun->execute();
  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetMakerTool :: postExecute (){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetMakerTool :: finalize (){
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode JetMakerTool :: histFinalize (){

  Info("histFinalize()", "Calling histFinalize");
  RETURN_CHECK("xAH::Algorithm::algFinalize()", xAH::Algorithm::algFinalize(), "");
  return EL::StatusCode::SUCCESS;
}






