#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "AsgTools/MessageCheck.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"

//#include "FactoryTools/Baseline_DVJETS_Selection.h"
//#include "FactoryTools/DRAW_DVJETS_filters.h"
//#include "FactoryTools/JetCleaning_DVJETS_custom.h"
#include "FactoryTools/RegionVarCalculator_rpvmultijet.h"
#include "FactoryTools/strongErrorCheck.h"

#include <FactoryTools/HelperFunctions.h>
#include <FactoryTools/VsiAnalysis/VsiTruthHelper.h>


#include <fstream>
// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_rpvmultijet)
using namespace asg::msgUserCode;


typedef FactoryTools::HelperFunctions HF;
typedef xAOD::JetAttribute JA;


//*************************
// D O I N I T I A L I Z E
//*************************

EL::StatusCode RegionVarCalculator_rpvmultijet::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	//Instantiate class to resolve OR link
	m_overlapLinkHelper = std::make_unique<ORUtils::OverlapLinkHelper>("overlapLinkHelper");

	return EL::StatusCode::SUCCESS;
}

//**************************
// D O   C A L C U L A T E
//**************************
EL::StatusCode RegionVarCalculator_rpvmultijet::doCalculate(std::map<std::string, anytype>& vars) {

	//Setup
	xAOD::TEvent* event = m_worker->xaodEvent();

	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

	if ( regionName.empty() ) {
		// If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
		ANA_MSG_DEBUG("No region name set, no calculations performed.");
		return EL::StatusCode::SUCCESS;
	}
	else if ( regionName == "SRRPV" ) {
		// other SR definitions can have other trees written out with other functions here
		ANA_MSG_DEBUG("Selection region set: SRRPV");
		return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS);
	}

	return EL::StatusCode::SUCCESS;
}

const xAOD::TruthParticle* getFinalParent(const xAOD::TruthParticle* p, int pdgid){
    /************
     * Recursive helper function to locate the final parent in a decay chain. Ex. Find the last W in the chain W -> gamma W -> gamma W -> gamma q qbar 
     * Input: the top level truth particle (first W) and the pdgid of the chain particle (W pdgid)
     *
     * Note, pdgid must match the ID of the truth particle, including the sign! 
     * **********/

    //std::cout<<"Calculator: "<<__LINE__<<std::endl;
    //std::cout<<p->pdgId()<<", "<< pdgid<<std::endl;
    if(!p){
        return 0;
    }
    else if (p->pdgId() == pdgid){
        //std::cout<<"Calculator: "<<__LINE__<<std::endl;
        for( size_t ic = 0; ic < p->nChildren(); ic++ ){
            //std::cout<<"Calculator: "<<__LINE__<<std::endl;
            if (p->child(ic)->pdgId() == pdgid) return getFinalParent(p->child(ic),pdgid);
        }
        //std::cout<<"Calculator: "<<__LINE__<<std::endl;
        return p;
    }
    else{
        //std::cout<<"Calculator: "<<__LINE__<<std::endl; 
        return 0;
    }
}

//**************************
// D O   A L L   C A L C
//**************************
EL::StatusCode RegionVarCalculator_rpvmultijet::doAllCalculations(std::map<std::string, anytype>& vars ) {
	xAOD::TStore * store = m_worker->xaodStore();
	xAOD::TEvent * event = m_worker->xaodEvent();

	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
	  vars["mcChannelNumber"] = eventInfo->mcChannelNumber();
	  vars["mcEventWeight"]   = eventInfo->auxdecor< float >("mcEventWeight");
	}

	doGeneralCalculations(vars);

	// Function defined to convert TeV units to GeV
	auto toGeV = [](double a){return a*.001;};

	//&&&&&&&&&&&&&&&&&&&&&&&&&&&
	// S E L E C T E D   J E T S
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&

	xAOD::IParticleContainer* jets_nominal(nullptr);
	STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));

	// Sort by pt
	std::vector<xAOD::IParticle*> jetVec = sortByPt(jets_nominal);

	// Attribute vectors
	std::vector<int> NumTrkPt1000, NumTrkPt500;
	std::vector<float> SumPtTrkPt1000, SumPtTrkPt500, TrackWidthPt1000, TrackWidthPt500;


	for( const auto& jet : jetVec) {
	  addToVectorBranch(vars,"jet_e", toGeV(jet->e() ) );
	  addToVectorBranch(vars,"jet_pt", toGeV(jet->pt() ) );
	  addToVectorBranch(vars,"jet_eta", jet->p4().Eta()  );
	  addToVectorBranch(vars,"jet_phi", jet->p4().Phi()  );
	  addToVectorBranch(vars,"jet_bTag", jet->auxdata<char>("bjet") );
	  addToVectorBranch(vars,"jet_isSig", jet->auxdata<char>("signal") );
	  addToVectorBranch(vars,"jet_passOR", (int)jet->auxdata<char>("passOR") ); //TODO isn't this redundant since sleected jets are required to have passOR == 1?
	  addToVectorBranch(vars,"jet_PartonTruthLabelID", (int)jet->auxdata<int>("PartonTruthLabelID") ); 

	  auto* j = dynamic_cast<xAOD::Jet*>( jet );
		
	  //j->getAttribute(xAOD::JetAttribute::PartonTruthLabelID, PartonTruthLabelID);
	  j->getAttribute(xAOD::JetAttribute::NumTrkPt1000,NumTrkPt1000);
	  j->getAttribute(xAOD::JetAttribute::NumTrkPt500,NumTrkPt500);
	  j->getAttribute(xAOD::JetAttribute::SumPtTrkPt1000,SumPtTrkPt1000);
	  j->getAttribute(xAOD::JetAttribute::SumPtTrkPt500,SumPtTrkPt500);
	  j->getAttribute(xAOD::JetAttribute::TrackWidthPt1000,TrackWidthPt1000);
	  j->getAttribute(xAOD::JetAttribute::TrackWidthPt500,TrackWidthPt500);
	  
	  //addToVectorBranch(vars,"jet_PartonTruthLabelID", PartonTruthLabelID);
	  addToVectorBranch(vars,"jet_NumTrkPt1000", NumTrkPt1000);
	  addToVectorBranch(vars,"jet_NumTrkPt500", NumTrkPt500);
	  addToVectorBranch(vars,"jet_SumPtTrkPt1000", SumPtTrkPt1000);
	  addToVectorBranch(vars,"jet_TrackWidthPt1000", TrackWidthPt1000);
	  addToVectorBranch(vars,"jet_SumPtTrkPt500", SumPtTrkPt500);
	  addToVectorBranch(vars,"jet_TrackWidthPt500", TrackWidthPt500);
	}


    float min_max_mass = CalcMassMinMax(10, 10); // Javier: in your ubs model you probably want to set the btagdiff limit to 1, so that if there are 2 b-jets they are not both assigned to the same half
    vars["minmax_jet_mass"] = min_max_mass;

	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        // S E L E C T E D   B A S E L I N E  L E P T O N S
        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	xAOD::ElectronContainer* electrons_nominal(nullptr);
	STRONG_CHECK(store->retrieve(electrons_nominal, "STCalibElectrons"));

	for( const auto& lep : *electrons_nominal) {
	  addToVectorBranch( vars, "el_e"        ,  toGeV(lep->e()));
	  addToVectorBranch( vars, "el_pt"        ,  toGeV(lep->pt()));
	  addToVectorBranch( vars, "el_eta"       ,  lep->p4().Eta() );
	  addToVectorBranch( vars, "el_phi"       ,  lep->p4().Phi() );
	  addToVectorBranch( vars, "el_charge"      ,  lep->charge());
	  addToVectorBranch( vars, "el_d0"        ,  lep->trackParticle(0)->d0() );
	  addToVectorBranch( vars, "el_baseline"  ,  (int)lep->auxdata<char>("baseline") );
	  addToVectorBranch( vars, "el_signal"    ,  (int)lep->auxdata<char>("signal") );
	  addToVectorBranch( vars, "el_passOR"    ,  (int)lep->auxdata<char>("passOR") );
	}


	// ID DV mapping :/
	xAOD::IParticleContainer* muons_baseline(nullptr);
	STRONG_CHECK(store->retrieve(muons_baseline, "STCalibMuons"));

	//std::cout << "Looping through selectedBaselineMuons " << std::endl;
	for( xAOD::IParticle * muon : *muons_baseline){

	  xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

	  addToVectorBranch( vars, "muon_index"  , mu->index());
	  addToVectorBranch( vars, "muon_e"      , toGeV(mu->e()));
	  addToVectorBranch( vars, "muon_pt"     , toGeV(mu->pt()));
	  addToVectorBranch( vars, "muon_eta"    , mu->p4().Eta());
	  addToVectorBranch( vars, "muon_phi"    , mu->p4().Phi());
	  addToVectorBranch( vars, "muon_charge" , mu->charge() );

	}

	
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& 
        // S E L E C T E D   Q/G  T A G G I N G  F E A T U R E S 
        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  


	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        // S E L E C T E D   L A R G E   R  ( F A T )  J E T S
        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	xAOD::IParticleContainer* jets_ST(nullptr);
	STRONG_CHECK(store->retrieve(jets_ST, "STCalibAntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"));

	// Sort by pt
	std::vector<xAOD::IParticle*> fatjetVec = sortByPt(jets_ST);

	for( const auto& fatjet : fatjetVec) {
	  addToVectorBranch(vars,"fatjet_e", toGeV(fatjet->e() ) );
	  addToVectorBranch(vars,"fatjet_pt", toGeV(fatjet->pt() ) );
	  addToVectorBranch(vars,"fatjet_eta", fatjet->p4().Eta()  );
	  addToVectorBranch(vars,"fatjet_phi", fatjet->p4().Phi()  );
	}

	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
        // T R U T H  I N F O 
        //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
	  // get the truth events to get the true signal vertex position
	  const xAOD::TruthEventContainer* truthEvents = 0;
	  STRONG_CHECK( event->retrieve( truthEvents, "TruthEvents" ));

	  // now get the truth particles
	  const xAOD::TruthParticleContainer* truthParticles = 0;
	  STRONG_CHECK( event->retrieve( truthParticles, "TruthParticles" ) );

	  // barcodes of the particles to be saved
	  std::vector<int> QuarkBarcodes;
	  QuarkBarcodes.clear();

	  // ttbar system pdgid
	  int topPDGID = 6;
	  int wPDGID = 24;
	  int bPDGID = 5;
	  int quarkPDGID = 6;
	  
	  // gluino-gluino system pdgid
	  int gluinoPDGID = 1000021;
	  // List of neutralino pdgid's to check against
	  int neutralinoOnePDGID = 1000022;
	  int neutralinoTwoPDGID = 1000023;
	  int charginoOnePDGID = 1000024;
	  int neutralinoThreePDGID = 1000025;
	  int neutralinoFourPDGID = 1000035;
	  int charginoTwoPDGID = 1000037;
	  std::vector<int> electroweakinoPDGID = {neutralinoOnePDGID,neutralinoTwoPDGID,charginoOnePDGID,neutralinoThreePDGID,neutralinoFourPDGID,charginoTwoPDGID};

	  for (const auto & p : *truthParticles){

        bool goodChildren = true;
        // look for gluinos/tops without gluino/tops in their children
        if(fabs(p->pdgId()) != gluinoPDGID && fabs(p->pdgId()) != topPDGID) continue;
        if(!p->hasDecayVtx()) continue;
        if(!(p->barcode() < 200000)) continue;

        // make sure the parent particle type is not in the children, i.e. particle actually decays 
        for(size_t ic = 0; ic < p->nChildren(); ic++){
            if(p->child(ic)->pdgId() == p->pdgId()) goodChildren = false;
        }

        if(!goodChildren) continue;
        // save the parent info
        addToVectorBranch( vars, "truth_parent_pdgId"               , (int) p->pdgId()                );
        addToVectorBranch( vars, "truth_parent_pt"                  , toGeV(p->pt() )                 );
        addToVectorBranch( vars, "truth_parent_eta"                 , p->eta()                        );
        addToVectorBranch( vars, "truth_parent_phi"                 , p->phi()                        );
        addToVectorBranch( vars, "truth_parent_m"                   , toGeV(p->m())                   );
        addToVectorBranch( vars, "truth_parent_e"                   , toGeV(p->e())                   );
        addToVectorBranch( vars, "truth_parent__charge"             , p->charge()                     );
        addToVectorBranch( vars, "truth_parent_barcode"             , p->barcode()                    );

        // for top decay 
        if( fabs(p->pdgId()) == topPDGID){


            // loop over children
            for( size_t ic = 0; ic < p->nChildren(); ic++){

                auto top_child = p->child(ic);

                // decide what to do with W bosons
                if(fabs(top_child->pdgId()) == wPDGID){


                    // Get the charge
                    int w_charge = static_cast<int>(top_child->charge());

                    // save the info about the first W
                    addToVectorBranch( vars, "truth_WFromTop_pdgId"        , top_child->pdgId()         );
                    addToVectorBranch( vars, "truth_WFromTop_eta"          , top_child->eta()           );
                    addToVectorBranch( vars, "truth_WFromTop_phi"          , top_child->phi()           );
                    addToVectorBranch( vars, "truth_WFromTop_pt"           , toGeV(top_child->pt())     );
                    addToVectorBranch( vars, "truth_WFromTop_e"            , toGeV(top_child->e())      );
                    addToVectorBranch( vars, "truth_WFromTop_charge"       , top_child->charge()        );
                    addToVectorBranch( vars, "truth_WFromTop_barcode"      , top_child->barcode()       );
                    addToVectorBranch( vars, "truth_WFromTop_ParentBarcode", p->barcode()        );


                    // W may also radiate a W, so walk down the decay chain until a vertex is found that doesn't have an outgoing W
                    int ID_search =  wPDGID*w_charge;
                    auto finalW = getFinalParent(top_child, ID_search); // * w_charge for W- boson
                    

                    int n_quarks_added(0);

                    // Loop over the final W's children and pick up the quarks
                    for( size_t icc = 0; icc < finalW->nChildren(); icc++){
                        auto W_boson_child = finalW->child(icc);
                        

                        // only pick out quarks
                        if(fabs(W_boson_child->pdgId()) <= quarkPDGID){
                            addToVectorBranch( vars, "truth_QuarkFromW_pdgID"        , W_boson_child->pdgId()          );
                            addToVectorBranch( vars, "truth_QuarkFromW_eta"          , W_boson_child->eta()            );
                            addToVectorBranch( vars, "truth_QuarkFromW_phi"          , W_boson_child->phi()            );
                            addToVectorBranch( vars, "truth_QuarkFromW_pt"           , toGeV(W_boson_child->pt())      );
                            addToVectorBranch( vars, "truth_QuarkFromW_e"            , toGeV(W_boson_child->e())       );
                            addToVectorBranch( vars, "truth_QuarkFromW_charge"       , W_boson_child->charge()         );
                            addToVectorBranch( vars, "truth_QuarkFromW_barcode"      , W_boson_child->barcode()        );
                            addToVectorBranch( vars, "truth_QuarkFromW_ParentBarcode", top_child->barcode()         );
                            QuarkBarcodes.push_back( W_boson_child->barcode() );
                            n_quarks_added++;
                        }

                    }
                }
                // decide what to do with b quarks
                else if(fabs(top_child->pdgId()) == bPDGID){
                    addToVectorBranch( vars, "truth_bFromTop_pdgID",         top_child->pdgId()     );
                    addToVectorBranch( vars, "truth_bFromTop_eta",           top_child->eta()       );
                    addToVectorBranch( vars, "truth_bFromTop_phi",           top_child->phi()       );
                    addToVectorBranch( vars, "truth_bFromTop_pt",            toGeV(top_child->pt()) );
                    addToVectorBranch( vars, "truth_bFromTop_e",             toGeV(top_child->e())  );
                    addToVectorBranch( vars, "truth_bFromTop_charge",        top_child->charge()    );
                    addToVectorBranch( vars, "truth_bFromTop_barcode",       top_child->barcode()   );
                    addToVectorBranch( vars, "truth_bFromTop_ParentBarcode", p->barcode()           );
                    QuarkBarcodes.push_back( top_child->barcode() );
                }
            }
        }

        // for gluino decay
        if( fabs(p->pdgId()) == gluinoPDGID ){
            // loop over children
            for( size_t ic = 0; ic < p->nChildren(); ic++){
                auto pc = p->child(ic);
                // decide what to do with quarks
                if( fabs(pc->pdgId()) <= quarkPDGID){
                    addToVectorBranch( vars, "truth_QuarkFromGluino_pdgID"        , pc->pdgId()         );
                    addToVectorBranch( vars, "truth_QuarkFromGluino_eta"          , pc->eta()           );
                    addToVectorBranch( vars, "truth_QuarkFromGluino_phi"          , pc->phi()           );
                    addToVectorBranch( vars, "truth_QuarkFromGluino_pt"           , toGeV(pc->pt())     );
                    addToVectorBranch( vars, "truth_QuarkFromGluino_e"            , toGeV(pc->e())      );
                    addToVectorBranch( vars, "truth_QuarkFromGluino_charge"       , pc->charge()        );
                    addToVectorBranch( vars, "truth_QuarkFromGluino_barcode"      , pc->barcode()       );
                    addToVectorBranch( vars, "truth_QuarkFromGluino_ParentBarcode", p->barcode()        );
                    QuarkBarcodes.push_back( pc->barcode() );
                }
                
		// decide what to do with neutralino
                if( std::find(electroweakinoPDGID.begin(), electroweakinoPDGID.end(), fabs(pc->pdgId())) != electroweakinoPDGID.end()){
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_pdgID"        , pc->pdgId()         );
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_eta"          , pc->eta()           );
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_phi"          , pc->phi()           );
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_pt"           , toGeV(pc->pt())     );
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_e"            , toGeV(pc->e())      );
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_charge"       , pc->charge()        );
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_barcode"      , pc->barcode()       );
                    addToVectorBranch( vars, "truth_NeutralinoFromGluino_ParentBarcode", p->barcode()        );
                    for( size_t icc = 0; icc < pc->nChildren(); icc++){
                        auto pcc = pc->child(icc);
                        if( fabs(pcc->pdgId()) <= quarkPDGID){
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_pdgID"        , pcc->pdgId()         );
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_eta"          , pcc->eta()           );
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_phi"          , pcc->phi()           );
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_pt"           , toGeV(pcc->pt())     );
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_e"            , toGeV(pcc->e())      );
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_charge"       , pcc->charge()        );
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_barcode"      , pcc->barcode()       );
                            addToVectorBranch( vars, "truth_QuarkFromNeutralino_ParentBarcode", pc->barcode()        );
                            QuarkBarcodes.push_back( pcc->barcode() );
                        }
                    }
                }
            }
        }
   
	  }

	  // Loop over the jets and quarks to find the closest quark within a delta R cut from the jet axis                                                                                                                         
	  xAOD::IParticleContainer* jets_nominal(nullptr);
	  STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));

	  // Sort by pt                                                                                                                                                                                                             
	  std::vector<xAOD::IParticle*> jetVec = sortByPt(jets_nominal);
	  // delta R cut                                                                                                                                                                                                            
	  double dRcut = 0.2;

	  for( const auto& jet : jetVec) {
	    double min_dR = 999.;
	    double matched_barcode = -1;
	    for( const auto& p : *truthParticles ){
	      // if we have decided to save this particle
	      if( std::find(QuarkBarcodes.begin(), QuarkBarcodes.end(), p->barcode()) != QuarkBarcodes.end() ) {
		double dR = sqrt(pow(jet->p4().Eta() - p->eta(),2) + pow(TVector2::Phi_mpi_pi(jet->p4().Phi() - p->phi()),2)); 
		// check if delta R is less than the cut and less than the current minimum delta R                                                                                                                              
		if( dR <= dRcut && dR < min_dR ){
		  matched_barcode = p->barcode();
		}
	      }     
	    }
	    // remove the matched quark from the list of particles to match
	    if( matched_barcode != -1){
	      QuarkBarcodes.erase( std::find(QuarkBarcodes.begin(), QuarkBarcodes.end(), matched_barcode) );
	    }
	    // Save the information for the matched particle
	    addToVectorBranch(vars,Form("jet_deltaR%.2f_matched_truth_particle_barcode",dRcut),matched_barcode);
 	  }
	    /*
 	      addToVectorBranch( vars, "truth_pdgId"                  , p->pdgId()       );
	      addToVectorBranch( vars, "truth_index"                  , p->index()       );
	      addToVectorBranch( vars, "truth_pt"                     , toGeV(p->pt())   );
	      addToVectorBranch( vars, "truth_eta"                    , p->eta()         );
	      addToVectorBranch( vars, "truth_phi"                    , p->phi()         );
	      addToVectorBranch( vars, "truth_M"                      , toGeV(p->m())    );
	      addToVectorBranch( vars, "truth_d0"                     , d0               );
	      addToVectorBranch( vars, "truth_barcode"                , p->barcode()     );
	      addToVectorBranch( vars, "truth_nParents"               , p->nParents()    );
	      addToVectorBranch( vars, "truth_parentBarcode"          , (p->nParents() > 0) ? p->parent(0)->barcode() : -999    );
	      addToVectorBranch( vars, "truth_parentPdgId"            , (p->nParents() > 0) ? p->parent(0)->pdgId()   : -999    );
	      addToVectorBranch( vars, "truth_VtxX"                   , p->prodVtx()->x()              );
	      addToVectorBranch( vars, "truth_VtxY"                   , p->prodVtx()->y()              );
	      addToVectorBranch( vars, "truth_VtxZ"                   , p->prodVtx()->z()              );
	      addToVectorBranch( vars, "truth_generatorParent_pdgId"  , getGeneratorParent(p)->pdgId()     );
	      addToVectorBranch( vars, "truth_generatorParent_barcode", getGeneratorParent(p)->barcode()     );
	      addToVectorBranch( vars, "truth_truthOrigin"            , p->isAvailable<int>("truthOrigin") ? p->auxdata<int>("truthOrigin") : -999 );
	    */
	  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	  // S E L E C T E D   T R U T H  J E T S
	  //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

	  auto truthJets = HF::grabFromEvent<xAOD::IParticleContainer>("AntiKt4TruthJets",event);
	  //STRONG_CHECK(event->retrieve(truthJets, "AntiKt4TruthJets"));

	  // Sort by pt
	  //std::vector<xAOD::IParticle*> truthJetVec = sortByPt(truthJets);

	  // Attribute vectors
	  std::vector<int> truth_NumTrkPt1000, truth_NumTrkPt500;
	  std::vector<float> truth_SumPtTrkPt1000, truth_SumPtTrkPt500, truth_TrackWidthPt1000, truth_TrackWidthPt500;

	  for( const auto& jet : *truthJets) {
	    addToVectorBranch(vars,"truth_jet_e", toGeV(jet->p4().E() ) );
	    addToVectorBranch(vars,"truth_jet_pt", toGeV(jet->pt() ) );
	    addToVectorBranch(vars,"truth_jet_eta", jet->p4().Eta()  );
	    addToVectorBranch(vars,"truth_jet_phi", jet->p4().Phi()  );
	  }
	}

	
	return EL::StatusCode::SUCCESS;
}


/* Function to get no of set bits in binary
 *  * representation of positive integer n */
unsigned int countSetBits(unsigned int n)
{
    unsigned int count = 0;
    while (n)
    {
            count += n & 1;
                n >>= 1;
    }
    return count;
}


// Adapted from https://gitlab.cern.ch/atlas-phys-susy-wg/RPVLL/RPV1L/stop1l-xaod/-/blob/ntupleProd/SWup/Root/CalcHiggsinoVariables.cxx#L338
// on the suggestion of Javier 
float RegionVarCalculator_rpvmultijet::CalcMassMinMax(int jetdiff, int btagdiff)
{
    ANA_MSG_DEBUG( Form("CalcMassMinMax(int jetdiff %d, int btagdiff %d )",  jetdiff, btagdiff ) );

    TLorentzVector sum1, sum2;

	// Get jets and sort by pT 
	xAOD::TStore * store = m_worker->xaodStore();
	xAOD::IParticleContainer* jets_nominal(nullptr);
	STRONG_CHECK(store->retrieve(jets_nominal, "selectedJets"));
	std::vector<xAOD::IParticle*> jetVec = sortByPt(jets_nominal);

    // Count number of signal jets 
    int n_signal_jets(0);
    for(const auto& jet : jetVec){
        if(jet->auxdata<char>("signal")) n_signal_jets++;
    }


    //bitwise representation of all jets and for which half they are selected
    unsigned int bitmax = 1 << n_signal_jets;
    int bitcount, btagcount1, btagcount2;
    float minmass = 999999999;


    for(unsigned int bit=0; bit<bitmax; bit++){
        bitcount = countSetBits(bit);
        if(abs(n_signal_jets - 2*bitcount) > jetdiff){ 
            ANA_MSG_DEBUG( Form("Fail jetdiff: %d %d %d %d",bit, n_signal_jets, bitcount, jetdiff) );
            continue;
        }
        if (btagdiff<10){
            btagcount1 = 0;
            btagcount2 = 0;

            int jet_index(0);
            for( const auto& jet : jetVec) {
                if(!(jet->auxdata<char>("signal"))) continue;
                if(jet->auxdata<char>("bjet")){
                    if(bit & (1 << jet_index)) btagcount1++;
                    else btagcount2++;
                }
                jet_index++;
            }
            if (abs(btagcount1 - btagcount2) > btagdiff){
                ANA_MSG_DEBUG( Form("Fail btagdiff: %d %d %d %d",bit, btagcount1, btagcount2,btagdiff) ); 
                continue;
            }
        }

        sum1.SetPxPyPzE(0,0,0,0);
        sum2.SetPxPyPzE(0,0,0,0);
        int jet_index(0);
        for(const auto& jet : jetVec){
            if(!(jet->auxdata<char>("signal"))) continue;
            if(bit & (1 << jet_index)) sum1 += jet->p4();
            else sum2 += jet->p4();
            jet_index++;
        }

        ANA_MSG_DEBUG( Form("Masses: %f %f %f",sum1.M(), sum2.M(), minmass) );
        if (sum1.M() > sum2.M() and sum1.M() < minmass) minmass = sum1.M();
        else if (sum2.M() > sum1.M() and sum2.M() < minmass) minmass = sum2.M();
    }

    return minmass;
}
