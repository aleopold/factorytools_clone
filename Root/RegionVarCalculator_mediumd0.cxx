#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "AsgTools/MessageCheck.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODBPhys/BPhysHypoHelper.h"

//%%%%%%%%
//includes needed for isolation 
#include "xAODPrimitives/IsolationType.h"
#include "IsolationSelection/IsolationWP.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "PathResolver/PathResolver.h"
//%%%%%%%%

#include <FactoryTools/HelperFunctions.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include "FactoryTools/RegionVarCalculator_mediumd0.h"
#include "FactoryTools/strongErrorCheck.h"

#include <fstream>

// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_mediumd0)

using namespace asg::msgUserCode;

typedef FactoryTools::HelperFunctions HF;
typedef ElementLink<xAOD::VertexContainer> VertexLink;
typedef std::vector<VertexLink> VertexLinkVector;

RegionVarCalculator_mediumd0::RegionVarCalculator_mediumd0()
  : m_iso("CP::IsolationSelectionTool")
{  
}

std::string RegionVarCalculator_mediumd0::sampleType(int &dsid) const{
  if(dsid != -999 && dsid < 399085 && dsid > 399004){
    return "signalSample";
  }
  
  if(dsid == 410470){return "topSample";}
  if(dsid != -999 && dsid < 361612 && dsid > 361599){return "diBosonSample";}
  if(dsid != -999 && dsid < 364114 && dsid > 364099){return "zmumuSample";}
  if(dsid != -999 && dsid < 364204 && dsid > 364197){return "zmmSample";}
  if(dsid != -999 && dsid < 364142 && dsid > 364127){return "ztautauSample";}
  if(dsid != -999 && dsid < 364216 && dsid > 364209){return "zttSample";} 
  if(dsid == 999999){return "bPhysSample";}//TODO update when we have offcial bbmc
  return std::string(); //TODO put in error message with unkown sample
}

std::string RegionVarCalculator_mediumd0::sampleTypeSignalORsm(int &dsid) const{
  if(sampleType(dsid) == "signalSample"){
    return "signalSample";
  }

  if(sampleType(dsid) == "topSample"    || sampleType(dsid) == "diBosonSample"||
     sampleType(dsid) == "zmumuSample"  || sampleType(dsid) == "zmmSample"    ||
     sampleType(dsid) == "ztautauSample"|| sampleType(dsid) == "zttSample"    ||
     sampleType(dsid) == "bPhysSample"  ){
    return "standardModelMCsample";
  }
 
  return std::string();//TODO put in error message with unkown sample
}

int RegionVarCalculator_mediumd0::hadronType(int pdgid) const{

  int case1(abs(pdgid%1000));
  int case2(abs(pdgid%10000));

  if ( case2 >= 5000 && case2 < 6000 ) return 5;
  if(  case1 >= 500 && case1 < 600 ) return 5;

  if ( case2 >= 4000 && case2 < 5000 ) return 4;
  if(  case1 >= 400 && case1 < 500 ) return 4;

  return 0;

}

//lifetime of particle
double RegionVarCalculator_mediumd0::ctau(const xAOD::TruthParticle* particle,
					  const xAOD::TruthVertex *firstvertex) noexcept
{
  using ROOT::Math::XYZVector;
  auto decayVertex = particle->decayVtx();
  XYZVector decay(decayVertex->x(), decayVertex->y(), decayVertex->z());
  XYZVector firstv(firstvertex->x(), firstvertex->y(), firstvertex->z());
  double length = std::sqrt((decay - firstv).Mag2());
  XYZVector p(particle->px(), particle->py(), particle->pz());
  return length * particle->m() / std::sqrt(p.Mag2());
}

double RegionVarCalculator_mediumd0::tau(const xAOD::TruthParticle* particle,
					 const xAOD::TruthVertex *firstvertex) noexcept {
  constexpr double c = 1000./299.792458;
  return c * ctau(particle, firstvertex);
}

double RegionVarCalculator_mediumd0::ctau(const xAOD::TruthParticle* particle) noexcept{
  return ctau(particle, particle->prodVtx());
}

double RegionVarCalculator_mediumd0::tau(const xAOD::TruthParticle* particle) noexcept{
  return tau(particle, particle->prodVtx());
}

double RegionVarCalculator_mediumd0::getPdgidTau(int pdgidHadron){
  double c = 0.299792458;
  if(abs(pdgidHadron)==511){
    return 0.4587/c;
  }
  if(abs(pdgidHadron)==521){
    return 0.4911/c;
  }
  if(abs(pdgidHadron)==531){
    return 0.439/c;
  }
  if(abs(pdgidHadron)==541){
    return 0.138/c;
  }
  if(abs(pdgidHadron)==5122){
    return 0.369/c;
  }
  if(abs(pdgidHadron)==5142){
    return 0.364/c;
  }
  if(abs(pdgidHadron)==5232){
    return 0.364/c;
  }
  if(abs(pdgidHadron)==411){
    return 0.3118/c;
  }
  if(abs(pdgidHadron)==421){
    return 0.1229/c;
  }
  if(abs(pdgidHadron)==431){
    return 0.1499/c;
  }
  if(abs(pdgidHadron)==4122){
    return 0.0599/c;
  }
  if(abs(pdgidHadron)==4132){
    return 0.0336/c;
  }
  if(abs(pdgidHadron)==4232){
    return 0.132/c;
  }
  if(abs(pdgidHadron)==4332){
    return 0.021/c;
  }
  return 0;
    
}

void RegionVarCalculator_mediumd0::findAllParents(std::vector<const xAOD::TruthParticle* > &parentVec,
						  const xAOD::TruthParticle* part ) const{

  for(size_t parent_itr = 0; parent_itr < part->nParents(); parent_itr++){
    if(!part->parent(parent_itr)) continue;
    auto parent = part->parent(parent_itr);
        
    //can add truth pdgid for other parent particles here if you so wish
    if(hadronType(parent->pdgId())==bquark_pdgid || hadronType(parent->pdgId())==cquark_pdgid ||
       abs(parent->pdgId())==bquark_pdgid        || abs(parent->pdgId())==cquark_pdgid        ||
       abs(parent->pdgId())==wboson_pdgid        || abs(parent->pdgId())==tau_pdgid           ||
       abs(parent->pdgId())==tquark_pdgid        || abs(parent->pdgId())==zboson_pdgid        ){

      //only want the parents from hard process
      //see http://home.thep.lu.se/~torbjorn/pythia81html/ParticleProperties.html
      if(parent->status()<30){
	parentVec.push_back(parent);	 
      }
      //find the parents of the parent...all the family history :)
      findAllParents(parentVec, parent);	
    }
  }
}


//*************************
// D O  I N I T I A L I Z E
//*************************
EL::StatusCode RegionVarCalculator_mediumd0::doInitialize(EL::IWorker * worker) {
  
  if(m_worker != nullptr){
    std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
    return EL::StatusCode::FAILURE;
  }
  m_worker = worker;

  //Instantiate class to resolve OR link
  m_overlapLinkHelper = std::make_unique<ORUtils::OverlapLinkHelper>("overlapLinkHelper");

  //Need isolation tool so we can set bool in ntuple to see if muons passed isolation WPs
  ANA_CHECK( m_iso.setProperty("MuonWP","FCLoose") );
  ANA_CHECK( m_iso.initialize() );
    
  ANA_MSG_DEBUG("Initializing Cross Section Tool");
  ASG_SET_ANA_TOOL_TYPE(m_PMGCrossSectionTool, PMGTools::PMGCrossSectionTool);
  m_PMGCrossSectionTool.setName("myCrossSectionTool");
  m_PMGCrossSectionTool.retrieve();
    
  //need to give the PMGTool a resolved path, as we can't give it an environmental variable,
  //which is needed to run on grid
  std::string resolvedPMGfile = PathResolverFindCalibDirectory(m_PMGToolFiles);
  m_PMGCrossSectionTool->readInfosFromDir(resolvedPMGfile);

  return EL::StatusCode::SUCCESS;
}


//**************************
// D O   C A L C U L A T E
//**************************
EL::StatusCode RegionVarCalculator_mediumd0::doCalculate(std::map<std::string, anytype>& vars) {

  //Setup
  xAOD::TEvent* event = m_worker->xaodEvent();
  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

  if(regionName.empty()){
    // If it hasn't been selected in any of the regions from any of the select algs,
    //don't bother calculating anything...
    ANA_MSG_DEBUG("No region name set, no calculations performed.");
    return EL::StatusCode::SUCCESS;
  }
  else if(regionName == "MEDIUMD0"){
    //other SR definitions can have other trees written out with other functions here
    ANA_MSG_DEBUG("Selection region set: MEDIUMD0");
    return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); 
  }
  return EL::StatusCode::SUCCESS;
}


//**************************
// D O   A L L   C A L C  
//**************************
EL::StatusCode RegionVarCalculator_mediumd0::doAllCalculations(std::map<std::string, anytype>& vars ) {
  xAOD::TStore * store = m_worker->xaodStore();
  xAOD::TEvent * event = m_worker->xaodEvent();

  auto toGeV = [](double a){return a*.001;};
    
  const xAOD::EventInfo* eventInfo = nullptr;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

  doGeneralCalculations(vars);

  bool isMC = false;
  if(eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; 
  }
  if(isMC){
    datasetid = eventInfo->mcChannelNumber();
  }
   
  //%%%%%%%%%%%%%%%%%%%%
  // P V 
  //%%%%%%%%%%%%%%%%%%%%

  const xAOD::VertexContainer* vertices = nullptr;
  STRONG_CHECK(event->retrieve( vertices, "PrimaryVertices"));
  vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);

  for ( const auto& vx : *vertices ) {
    if (vx->vertexType() == xAOD::VxType::PriVtx) {
      addToVectorBranch(vars,"PVx",       vx->x() );
      addToVectorBranch(vars,"PVy",       vx->y() );
      addToVectorBranch(vars,"PVz",       vx->z() );
      addToVectorBranch(vars,"PVrxy",     TMath::Hypot(vx->x(),vx->y() ));	
      addToVectorBranch(vars,"PVnTracks", vx->nTrackParticles() );

      int VertexType = vx->vertexType(); //addToVectorBranch won't save type xAOD vertex
      addToVectorBranch(vars,"PVvertexType", VertexType);      
    }
  }

    
  //%%%%%%%%%%%%%%%%%%%%
  // T R I G G E R
  //%%%%%%%%%%%%%%%%%%%%

  //TODO can put this in region options in select alg if we need to reduce size of ntuple
  vars["passHLT_2mu14"]= eventInfo->auxdecor<bool>( "passHLT_2mu14" );

  
  //%%%%%%%%%%%%%%%%%%%%%%%%%
  // C R O S S  S E C T I O N
  //%%%%%%%%%%%%%%%%%%%%%%%%%
    
  //cross section and correction factors
  if(isMC){
    vars["xSection"]           = m_PMGCrossSectionTool->getAMIXsection(datasetid);
    vars["filterEff"]          = m_PMGCrossSectionTool->getFilterEff(datasetid);
    vars["kFactor"]            = m_PMGCrossSectionTool->getKfactor(datasetid);
    vars["xSectionUncert"]     = m_PMGCrossSectionTool->getXsectionUncertainty(datasetid); 
    vars["xSectionUncertUP"]   = m_PMGCrossSectionTool->getXsectionUncertaintyUP(datasetid); 
    vars["xSectionUncertDOWN"] = m_PMGCrossSectionTool->getXsectionUncertaintyUP(datasetid);   
    //TODO there is also a getXsectionUncertainty function which adds the up and down and divides by 2
    //May as well just do that further on than adding a new branch?!
  }
      
        
  //%%%%%%%%%%%%%%%%%%%%
  // M E T 
  //%%%%%%%%%%%%%%%%%%%%
    
  xAOD::MissingETContainer * metcont = nullptr;
  STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));
    
  vars["passMETtrigger"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
  vars["MET"]            = toGeV((*metcont)["Final"]->met());
  vars["MET_px"]         = toGeV((*metcont)["Final"]->mpx());
  vars["MET_py"]         = toGeV((*metcont)["Final"]->mpy());
  vars["MET_phi"]        = ((*metcont)["Final"]->phi());
  
  //%%%%%%%%%%%%%%%%%%%%
  // T R U T H   L E P T O N S + F A M I L Y
  //%%%%%%%%%%%%%%%%%%%%

  // %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%%
  
  TruthMuonsFromSlepBarcodeVec.clear();
  TruthMuonsFromStauBarcodeVec.clear();
  
  if(sampleTypeSignalORsm(datasetid)=="signalSample"){
 
    //get truth particles containter
    const xAOD::TruthParticleContainer* truthParticlesCont = 0;
    STRONG_CHECK(event->retrieve(truthParticlesCont, "TruthParticles"));
    
    //loop over all truth particles and store only the ones we want
    for (const auto & truthParticle : *truthParticlesCont){
      //only want the last slepton in the chain of MC sleptons
      //otherwise you end up with a loop of sleptons -> sleptons etc etc..
      if(truthParticle->status() != 1)continue;

      int fabsTruthParticlePdgid = fabs(truthParticle->pdgId());
      
      if(fabsTruthParticlePdgid==LHselectron_pdgid || fabsTruthParticlePdgid==RHselectron_pdgid ||
	 fabsTruthParticlePdgid==LHsmuon_pdgid     || fabsTruthParticlePdgid==RHsmuon_pdgid     ||
	 fabsTruthParticlePdgid==LHstau_pdgid      || fabsTruthParticlePdgid ==RHstau_pdgid     ){

	addToVectorBranch( vars, "TruthSlepPt",      toGeV(truthParticle->pt())  );
	addToVectorBranch( vars, "TruthSlepE",       toGeV(truthParticle->e() )  );
	addToVectorBranch( vars, "TruthSlepEta",     truthParticle->eta()  );
	addToVectorBranch( vars, "TruthSlepPhi",     truthParticle->phi()  );
	addToVectorBranch( vars, "TruthSlepCharge",  truthParticle->charge() );    
	addToVectorBranch( vars, "TruthSlepBarcode", truthParticle->barcode());
	addToVectorBranch( vars, "TruthSlepIndex",   truthParticle->index());
	addToVectorBranch( vars, "TruthSlepPdgid",   truthParticle->pdgId()); 
    
	for(size_t child_itr = 0; child_itr < truthParticle->nChildren(); child_itr++){
	  if( !truthParticle->child(child_itr)) continue;	
	  auto slep_child = truthParticle->child(child_itr);	

	  if(slep_child->pdgId()==gravitino_pdgid){
	    addToVectorBranch( vars, "TruthGravPt",       toGeV(slep_child->pt() ) );
	    addToVectorBranch( vars, "TruthGravE",        toGeV(slep_child->e() ) );
	    addToVectorBranch( vars, "TruthGravEta",      slep_child->eta() );
	    addToVectorBranch( vars, "TruthGravPhi",      slep_child->phi() );
	    addToVectorBranch( vars, "TruthGravBarcode",  slep_child->barcode() );
	    addToVectorBranch( vars, "TruthGravIndex",    slep_child->index() );
	    addToVectorBranch( vars, "TruthGravPdgid",    slep_child->pdgId() ); 
	  }
	  
	  if(abs(slep_child->pdgId())==muon_pdgid){
	    //store the barcode of the muon from the slepton so that we can
	    //match the reco muon to it
	    TruthMuonsFromSlepBarcodeVec.push_back(slep_child->barcode());
 
	    addToVectorBranch( vars, "TruthMuonFromSlepPt",       toGeV(slep_child->pt() ) );
	    addToVectorBranch( vars, "TruthMuonFromSlepE",        toGeV(slep_child->e() ) );
	    addToVectorBranch( vars, "TruthMuonFromSlepEta",      slep_child->eta() );
	    addToVectorBranch( vars, "TruthMuonFromSlepPhi",      slep_child->phi() );
	    addToVectorBranch( vars, "TruthMuonFromSlepCharge",   slep_child->charge() );    
	    addToVectorBranch( vars, "TruthMuonFromSlepBarcode",  slep_child->barcode() );
	    addToVectorBranch( vars, "TruthMuonFromSlepIndex",    slep_child->index() );
	  }
	  
	  //if stau get children of tau
	  if(abs(slep_child->pdgId())==tau_pdgid){
	    for(size_t tau_itr = 0; tau_itr < slep_child->nChildren(); tau_itr++){
	      if( !slep_child->child(tau_itr)) continue;
	      auto tau_child = slep_child->child(tau_itr);

	      if(abs(tau_child->pdgId())==muon_pdgid){
		//store the barcode of the muon from the stau->tau->mu so that we can
		//match the reco muon to it
		TruthMuonsFromStauBarcodeVec.push_back(tau_child->barcode());

		addToVectorBranch( vars, "TruthMuonFromTauFromStauPt",       toGeV(tau_child->pt() ) );
		addToVectorBranch( vars, "TruthMuonFromTauFromStauE",        toGeV(tau_child->e() ) );
		addToVectorBranch( vars, "TruthMuonFromTauFromStauEta",      tau_child->eta() );
		addToVectorBranch( vars, "TruthMuonFromTauFromStauPhi",      tau_child->phi() );
		addToVectorBranch( vars, "TruthMuonFromTauFromStauCharge",   tau_child->charge() );    
		addToVectorBranch( vars, "TruthMuonFromTauFromStauBarcode",  tau_child->barcode() );  
	      }
	    }//end of loop over taus children
	  }//end of if child of slepton is a tau
	}//end of loop over children of slepton
      }//end of if truth particle is a slepton
    }//end of loop over truth particles
  }//end of if signal sample


  // %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%% %%%%%%%%
  //truth info for SM MC

  truthMuonBarcodeVec.clear();

  if(sampleTypeSignalORsm(datasetid)=="standardModelMCsample"){

    const xAOD::TruthParticleContainer* truthParticlesCont = 0;
    STRONG_CHECK(event->retrieve(truthParticlesCont, "TruthParticles"));
    
    //loop over all truth particles, find a muon, pass to function to find its parents
    for (const auto & truthParticle : *truthParticlesCont){

      if(fabs(truthParticle->pdgId())==muon_pdgid && truthParticle->status() == 1){
	std::vector<const xAOD::TruthParticle* > parentVec;

	//don't look at muons that "come from muons", we just want the final one
	for(size_t parent_itr = 0; parent_itr < truthParticle->nParents(); parent_itr++){
	  if(!truthParticle->parent(parent_itr)) continue;
	  auto parent = truthParticle->parent(parent_itr);	  
	  if(abs(parent->pdgId())==muon_pdgid){
	    goto endOfLoop;
	  }
	}
	//store the barcode of the *nice* truth muon so that we can
	//match the reco muon to it
        truthMuonBarcodeVec.push_back(truthParticle->barcode() );
	//DO ANY TRUTH STUFF FOR ALL TRUTH PARTICLES HERE
	//later is just for truth muons that have been matched to reco muons
	//for example if you need to look at ALL truth muons in the event
	//not just the ones that got reconstructed.
      }
    endOfLoop: ;
    }//end of loop over truth particles
  }//end of if SM sample

  
  //%%%%%%%%%%%%%%%%%%%%
  // R E C O   L E P T O N S
  //%%%%%%%%%%%%%%%%%%%%

  xAOD::IParticleContainer* muons_baseline(nullptr);
  STRONG_CHECK(store->retrieve(muons_baseline, "selectedBaselineMuons"));

  std::vector<xAOD::IParticle*> muons_baseline_sorted = sortByPt(muons_baseline);

  //need to create a vec of muons in order to calc. inv mass of first two muons
  //outside of muon loop
  std::vector<const xAOD::Muon*> muons_vec;

  //TODO can just loop over first two muons if we need to save space
  for( xAOD::IParticle * muon : muons_baseline_sorted){
    xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

    float d0err = -999;
    float z0err = -999;
    int passFCLooseIsolation = m_iso->accept(*mu);

    muons_vec.push_back(mu);
      
    //link the ID track to the muon
    const xAOD::TrackParticle* muIdTrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
    
    if(muIdTrack){	
      //get the d0 and z0 errors
      auto &CovariantMatrix =  muIdTrack->definingParametersCovMatrix();
      d0err=std::sqrt(CovariantMatrix(0,0));
      z0err=std::sqrt(CovariantMatrix(1,1));
    }

    ANA_MSG_INFO("reco muon index: " << mu->index() << ", pt: " << mu->pt() );
    addToVectorBranch( vars, "muonPt",      toGeV(mu->pt()));
    addToVectorBranch( vars, "muonE",       toGeV(mu->e()));
    addToVectorBranch( vars, "muonEta",     mu->eta());
    addToVectorBranch( vars, "muonPhi",     mu->phi());
    addToVectorBranch( vars, "muonCharge",  mu->charge());     
    addToVectorBranch( vars, "muonIndex",   mu->index());
    addToVectorBranch( vars, "muonD0sig",   (float)mu->auxdata<float>("d0sig"));
    addToVectorBranch( vars, "muonPassOR" , mu->auxdata<char>("passOR") ? 1 : 0); //TODO selected muons should pass or anyway
    addToVectorBranch( vars, "muonPassFCLooseIso", passFCLooseIsolation);
    addToVectorBranch( vars, "muonTrigMatched",    (mu)->auxdecor< int >( "PassTM"));
    addToVectorBranch( vars, "muonPtvarcone30_TightTTVA_pt1000", toGeV( mu->isolation(xAOD::Iso::ptvarcone30_TightTTVA_pt1000) ));
    addToVectorBranch( vars, "muonTopoetcone20", toGeV( mu->isolation(xAOD::Iso::topoetcone20) ));    
    addToVectorBranch( vars, "muonIdTrackPt",    (muIdTrack) ?  toGeV(muIdTrack->pt())  : -999 );
    addToVectorBranch( vars, "muonIdTrackEta",   (muIdTrack) ?  muIdTrack->eta()  : -999 );
    addToVectorBranch( vars, "muonIdTrackPhi",   (muIdTrack) ?  muIdTrack->phi()  : -999 );
    addToVectorBranch( vars, "muonIdTrackD0",    (muIdTrack) ?  muIdTrack->d0()  : -999 );
    addToVectorBranch( vars, "muonIdTrackZ0",    (muIdTrack) ?  muIdTrack->z0()  : -999 );
    addToVectorBranch( vars, "muonIdTrackD0err", (muIdTrack) ? d0err : -999 );
    addToVectorBranch( vars, "muonIdTrackZ0err", (muIdTrack) ? z0err : -999 );
    addToVectorBranch( vars, "muonIdTrackChi2",  (muIdTrack && muIdTrack->isAvailable<float>("chiSquared") ) ?  muIdTrack->auxdata< float >("chiSquared")  : -999 );
    addToVectorBranch( vars, "muonIdTrackDOF",   (muIdTrack && muIdTrack->isAvailable<float>("numberDoF") ) ? muIdTrack->auxdata< float >("numberDoF")  : -999 );

    
    //look at truth info
    if(isMC){    
      int muonIsMatchedToSlepMuon = 0;
      int muonIsMatchedToTauMuon = 0;

      //link the reco muon to it's truth particle
      const ElementLink< xAOD::TruthParticleContainer >& truthLink = mu->auxdata< ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");
      
      if(sampleTypeSignalORsm(datasetid)=="signalSample"){
	
	if(truthLink.isValid()){
	  const xAOD::TruthParticle* truth = *truthLink;
	  
	  if( find(TruthMuonsFromSlepBarcodeVec.begin(),TruthMuonsFromSlepBarcodeVec.end(), truth->barcode()) != TruthMuonsFromSlepBarcodeVec.end()){
	    muonIsMatchedToSlepMuon = 1;
	  }
	  if( find(TruthMuonsFromStauBarcodeVec.begin(),TruthMuonsFromStauBarcodeVec.end(), truth->barcode()) != TruthMuonsFromStauBarcodeVec.end()){
	    muonIsMatchedToTauMuon = 1;
	  }

	  addToVectorBranch( vars, "muonIsMatchedToSlepMuon",    muonIsMatchedToSlepMuon );
	  addToVectorBranch( vars, "muonIsMatchedToStauTauMuon", muonIsMatchedToTauMuon );
	}//end if truth link exists
	else{
	  addToVectorBranch( vars, "muonIsMatchedToSlepMuon",    0 );
	  addToVectorBranch( vars, "muonIsMatchedToStauTauMuon", 0 );
	}
      }//end if signal sample

      
      if(sampleTypeSignalORsm(datasetid)=="standardModelMCsample"){
	if(truthLink.isValid()){
	  const xAOD::TruthParticle* truth = *truthLink;
	  if( find(truthMuonBarcodeVec.begin(),truthMuonBarcodeVec.end(), truth->barcode()) != truthMuonBarcodeVec.end()){
	    std::vector<const xAOD::TruthParticle* > muonParentVec;

	    //add the truth particles in order of "youngest" first
	    //i.e. muon, then parent of muon, then grandparent
	    
	    float bphysWeight=1;
	    addToVectorBranch(vars,"BlifetimeWeight", bphysWeight);
	    addToVectorBranch(vars,"TruthParticlePt",         toGeV(truth->pt() ) );
	    addToVectorBranch(vars,"TruthParticleE",          toGeV( truth->e() )  );
	    addToVectorBranch(vars,"TruthParticleEta",        truth->eta());
	    addToVectorBranch(vars,"TruthParticlePhi",        truth->phi() );
	    addToVectorBranch(vars,"TruthParticleHadronType", hadronType( truth->pdgId() ) );
	    addToVectorBranch(vars,"TruthParticlePdgid",      truth->pdgId() );
	    addToVectorBranch(vars,"TruthParticleStatus",     truth->status() );
	    addToVectorBranch(vars,"TruthParticleBarcode",    truth->barcode() );
	    addToVectorBranch(vars,"TruthParticleCharge",     truth->charge() );
	    addToVectorBranch(vars,"TruthParticleLevel",      0);
	    //TODO can use primary vertex to calculate below if needed 
	    addToVectorBranch(vars,"TruthParticleTau",   0);

	    findAllParents(muonParentVec,truth);

	    for(unsigned int i=0; i < muonParentVec.size(); i++){
	      float weightedTau = 0;
	      float pdgTau=0;
	      
	      if(sampleType(datasetid)=="bPhysSample"){
		//TODO if doesn't have prod vertex...do
		if(muonParentVec[i]->hasProdVtx() ){

		  pdgTau = getPdgidTau(muonParentVec[i]->pdgId() );
		  weightedTau = tau(muonParentVec[i]);
		  if(pdgTau!=0){
		    bphysWeight =  exp (- 0.9  *  weightedTau /pdgTau);
		  }		
		}
	      }
	      
	      addToVectorBranch(vars,"BlifetimeWeight",         bphysWeight );
	      addToVectorBranch(vars,"TrueParticleTau",         weightedTau );
	      addToVectorBranch(vars,"TruthParticlePt",         toGeV( muonParentVec[i]->pt() )  );
	      addToVectorBranch(vars,"TruthParticleE",          toGeV( muonParentVec[i]->e() )  );
	      addToVectorBranch(vars,"TruthParticleEta",        muonParentVec[i]->eta() );
	      addToVectorBranch(vars,"TruthParticlePhi",        muonParentVec[i]->phi() );
	      addToVectorBranch(vars,"TruthParticleHadronType", hadronType( muonParentVec[i]->pdgId() ) );
	      addToVectorBranch(vars,"TruthParticlePdgid",      muonParentVec[i]->pdgId() );
	      addToVectorBranch(vars,"TruthParticleStatus",     muonParentVec[i]->status() );
	      addToVectorBranch(vars,"TruthParticleBarcode",    muonParentVec[i]->barcode() );
	      addToVectorBranch(vars,"TruthParticleCharge",     muonParentVec[i]->charge() );
	      int level = i+1;
	      addToVectorBranch(vars,"TruthParticleLevel",      level );
	    }//end loop over parents	    
	  }
	}//end if truth link	  
	else{
	  addToVectorBranch(vars,"BlifetimeWeight",         dummy_f);
	  addToVectorBranch(vars,"TrueParticleTau",         dummy_f);
	  addToVectorBranch(vars,"TruthParticlePt",         dummy_d);
	  addToVectorBranch(vars,"TruthParticleE",          dummy_d);
	  addToVectorBranch(vars,"TruthParticleEta",        dummy_d);
	  addToVectorBranch(vars,"TruthParticlePhi",        dummy_d);
	  addToVectorBranch(vars,"TruthParticleHadronType", dummy_i);
	  addToVectorBranch(vars,"TruthParticlePdgid",      dummy_i);
	  addToVectorBranch(vars,"TruthParticleStatus",     dummy_i);
	  addToVectorBranch(vars,"TruthParticleBarcode",    -999);
	  addToVectorBranch(vars,"TruthParticleCharge",     dummy_d);
	  addToVectorBranch(vars,"TruthParticleLevel",      dummy_i);
	}
      }//end if SM MC
	
    }//end if MC sample    
  }//end of loop over muons

  
  //calc the invariant mass, and delta R/phi of first two muons (highest in pt)
  float invMass =     -999;
  float deltaRmus =   -999;
  float deltaPhimus = -999;
  
  if(muons_vec.size()>1){
    TLorentzVector lep1, lep2;

    lep1.SetPtEtaPhiE(toGeV(muons_vec[0]->pt()),muons_vec[0]->eta(),muons_vec[0]->phi(),toGeV(muons_vec[0]->e()));
    lep2.SetPtEtaPhiE(toGeV(muons_vec[1]->pt()),muons_vec[1]->eta(),muons_vec[1]->phi(),toGeV(muons_vec[1]->e()));
    
    invMass=(lep1+lep2).M();
    deltaRmus=lep1.DeltaR(lep2);
    deltaPhimus=lep1.DeltaPhi(lep2);
  }
 
  vars["invMassTwoMuons"] =  muons_vec.size()>1 ? invMass :     -999 ;
  vars["deltaRTwoMuons"]  =  muons_vec.size()>1 ? deltaRmus :   -999 ;
  vars["deltaPhiTwoMuons"]=  muons_vec.size()>1 ? deltaPhimus : -999 ;
  
  //%%%%%%%%%%%%%%%%%%%%
  // S F s
  //%%%%%%%%%%%%%%%%%%%%


  //TODO  currently sfs are for signal, not baseline muons, we are running over baseline muons and
  //then require them to be isolated later down the line, only difference should be that signal are isolated 
  //and baseline aren’t…will want to update this down the line…
  vars["muSF"] = eventInfo->auxdecor<float>("muSF");

  //TODO make sure we have correct SF when we decide on isolation
  for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("muSF_systs")) {
    vars["muSF_"+systName+""] = eventInfo->auxdecor<float>("muSF_"+systName);
  }

  vars["btagSF"] = eventInfo->auxdecor<float>("btagSF");

  //TODO make sure we have correct SF when we decide on isolation
  for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("btagSF_systs")) {
    vars["btagSF_"+systName+""] = eventInfo->auxdecor<float>("btagSF_"+systName);
  }
  
    
  return EL::StatusCode::SUCCESS;
}






