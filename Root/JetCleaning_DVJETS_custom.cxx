/*###############################################
 name: JetCleaning_DVJETS_custom
 type: C++ namespace
 responsible: rcarney@lbl.gov

 description:
    Contains a struct, JetCleaningFlags, used to store
    DRAWFilter decisions. Contains flag-setting
    functions and jet cleaning code copied out of
    standard JetSelectorTools package,
    commit: 39cf36fea45c5677a531fd95c1615fa2964a4f94

 last updated: Jul. 2018
###############################################*/

#include "FactoryTools/JetCleaning_DVJETS_custom.h"
#include "EventLoop/StatusCode.h"

//==================================
// C H E C K   C L E A N   J E T S
//==================================
EL::StatusCode JetCleaning_DVJETS::checkCleanJets( const xAOD::Jet_v1& jet, bool doUgly, bool doLLP, JetCleaning_DVJETS::JetCleaningFlags& flags ){

    //**********************
    // S E T U P
    //**********************
    flags.reset();

    //Jet pT
    float jetPt = jet.pt();
    if( jetPt<DBL_MIN ){
        flags.fail_jetpT = 1;
    }
    float eta = jet.eta();

    double sumTrkPt;
    double chf;
    int FracSamplingMaxIndex;
    float fmax; //FracSamplingMax
    float emf;  //EMFrac
    float hecf; //HECFrac
    float larq; //LArQuality
    float hecq; //HECQuality
    float negE; //NegativeE
    float AverageLArQF;

    if ( getJetVars( jet, doUgly, sumTrkPt, chf, FracSamplingMaxIndex, fmax, emf, hecf, larq, hecq, negE, AverageLArQF, jetPt) == EL::StatusCode::FAILURE ){
        std::cout<<"GetJetVars failed: exiting" <<std::endl;
        return EL::StatusCode::FAILURE;
    }

    //*************************
    // J E T   C L E A N I N G
    //*************************

    //Do UGLY
    if( doUgly && FracSamplingMaxIndex==17){
        flags.fail_ugly = 1;
    }

    //Non-collision background & cosmics
    bool useLLP = doLLP;
    if (!useLLP) {
        if( emf<0.05 && chf<0.05 && std::fabs(eta)<2){
            flags.fail_NCB_lowEta = 1;
        }
        if(emf<0.05 && std::fabs(eta)>=2){
            flags.fail_NCB_highEta = 1;
        }
    }

    //fmax cut
    if((fmax>0.99) && (std::fabs(eta)<2) ){
       flags.fail_fmaxCut = 1;
    }

    //LAr QF cut
    if(hecf>0.5 && std::fabs(hecq)>0.5 && AverageLArQF/65535>0.8){
        flags.fail_QF = 1;
    }

    //EM calo noise
    if(emf>0.95 && std::fabs(larq)>0.8 && std::fabs(eta)<2.8 && AverageLArQF/65535>0.8){
        flags.fail_EMCaloNoise = 1;
    }

    // LLP cleaning uses negative energy cut
    // (https://indico.cern.ch/event/472320/contribution/8/attachments/1220731/1784456/JetTriggerMeeting_20160102.pdf)
    if (useLLP && std::fabs(negE*0.001)>4 && fmax >0.85){
        flags.fail_LLPNegE = 1;
    }

    //OR all of the flags to determine fail state:
    flags.fail = ( flags.fail_jetpT ^ flags.fail_ugly ^ flags.fail_NCB_lowEta ^ flags.fail_NCB_highEta ^ flags.fail_fmaxCut ^ flags.fail_QF ^ flags.fail_EMCaloNoise ^ flags.fail_LLPNegE ^ flags.fail_fmaxMin ^ flags.fail_NCB_monoJet );
    //TODO: If LooseBad, return with pass at this point
/*
    if( fmax<DBL_MIN ){
        flags.fail_fmaxMin = 1;
    }

    // NCB monojet-style cut in central
    if(std::fabs(eta)<2.4 && chf/fmax<0.1){
        flags.fail_NCB_monoJet = 1;
    }

    //TODO: If TightBad, return with pass at this point
*/

    return EL::StatusCode::SUCCESS;
}

//==========================
// G E T   J E T   V A R S
//==========================
EL::StatusCode JetCleaning_DVJETS::getJetVars( const xAOD::Jet_v1& jet, bool doUgly, double& sumTrkPt, double& chf, int& FracSamplingMaxIndex, float& FracSamplingMax, float& EMFrac, float& HECFrac, float& LArQuality, float& HECQuality, float& NegativeE, float& AverageLArQF, float& jetPt){

    //Get sumTrkPt and calculate chf
    std::vector<float> sumPtTrkvec;
    jet.getAttribute( xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec );
    sumTrkPt = 0;
    if( !sumPtTrkvec.empty() ) sumTrkPt = sumPtTrkvec[0];
    chf=sumTrkPt/jetPt;

    //Get fmax index
    FracSamplingMaxIndex = -1;
    if (doUgly){
        if( !jet.getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex, FracSamplingMaxIndex) ){
            std::cout<<"ERROR: FracSamplingMaxIndex not available as jet attribute!";
            return EL::StatusCode::FAILURE;
        }
    }

    //Get fmax
    FracSamplingMax = 0;
    if( !jet.getAttribute(xAOD::JetAttribute::FracSamplingMax,FracSamplingMax) ){
        std::cout<<"ERROR: FracSamplingMax not available as jet attribute!";
        return EL::StatusCode::FAILURE;
    }

    //Get EMFrac
    EMFrac = 0;
    if( !jet.getAttribute(xAOD::JetAttribute::EMFrac,EMFrac) ){
        std::cout<<"ERROR: EMFrac not available as jet attribute!";
        return EL::StatusCode::FAILURE;
    }

    //Get HECFrac
    HECFrac = 0;
//    if( !jet.getAttribute(xAOD::JetAttribute::HECFrac,HECFrac)){
//        std::cout<<"ERROR: HECFrac  not available as jet attribute!";
//        return EL::StatusCode::FAILURE;
//    }

    //Get LArQ
    LArQuality = 0;
//    if( !jet.getAttribute(xAOD::JetAttribute::LArQuality,LArQuality) ){
//        std::cout<<"ERROR: LArQuality not available as jet attribute!";
//        return EL::StatusCode::FAILURE;
//    }

    //Get HECQ
    HECQuality = 0;
//    if (!jet.getAttribute(xAOD::JetAttribute::HECQuality,HECQuality)){
//        std::cout<<"ERROR: HECQuality not available as jet attribute!";
//        return EL::StatusCode::FAILURE;
//    }

    //Get negE
    NegativeE = 0;
//    if (!jet.getAttribute(xAOD::JetAttribute::NegativeE,NegativeE)){
//        std::cout<<"ERROR: NegativeE not available as jet attribute!";
//        return EL::StatusCode::FAILURE;
//    }

    //Get avgLArQF
    AverageLArQF = 0;
//    if (!jet.getAttribute(xAOD::JetAttribute::AverageLArQF,AverageLArQF)){
//        std::cout<<"ERROR: averageLArQF not available as jet attribute!";
//        return EL::StatusCode::FAILURE;
//    }

    return EL::StatusCode::SUCCESS;
}

