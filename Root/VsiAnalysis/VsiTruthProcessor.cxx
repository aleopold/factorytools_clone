#include "FactoryTools/VsiAnalysis/VsiTruthProcessor.h"
#include "FactoryTools/VsiAnalysis/VsiTruthHelper.h"
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthVertex.h>


VsiTruthProcessor::VsiTruthProcessor( std::string name )
  : ProcessorBase( name )
{}



VsiTruthProcessor::~VsiTruthProcessor()
{}


void VsiTruthProcessor::registerVariables() {

  m_vars["vtx_r"]               = std::vector<float> {};
  m_vars["vtx_z"]               = std::vector<float> {};
  m_vars["vtx_phi"]             = std::vector<float> {};
  m_vars["vtx_parent_pt"]       = std::vector<float> {};
  m_vars["vtx_parent_eta"]      = std::vector<float> {};
  m_vars["vtx_parent_phi"]      = std::vector<float> {};
  m_vars["vtx_parent_pid"]      = std::vector<int> {};
  m_vars["vtx_parent_prod_x"]   = std::vector<float> {};
  m_vars["vtx_parent_prod_y"]   = std::vector<float> {};
  m_vars["vtx_parent_prod_z"]   = std::vector<float> {};
  m_vars["vtx_parent_prod_r"]   = std::vector<float> {};
  m_vars["vtx_parent_prod_phi"] = std::vector<float> {};
  m_vars["vtx_outP_pt"]         = std::vector<std::vector<float>> {};
  m_vars["vtx_outP_eta"]        = std::vector<std::vector<float>> {};
  m_vars["vtx_outP_phi"]        = std::vector<std::vector<float>> {};
  m_vars["vtx_outP_pid"]        = std::vector<std::vector<int>> {};
  m_vars["vtx_outP_isReco"]     = std::vector<std::vector<uint8_t>> {};

}


EL::StatusCode VsiTruthProcessor::processDetail( xAOD::TEvent* event, xAOD::TStore* /*store*/ ) {

  if( !isMC() ) return EL::StatusCode::SUCCESS;

  const xAOD::TruthParticleContainer *truthParticles   ( nullptr );
  const xAOD::TruthVertexContainer   *truthVertices    ( nullptr );
  const xAOD::TrackParticleContainer *recoTracks       ( nullptr );

  STRONG_CHECK( event->retrieve(recoTracks,       "InDetTrackParticles") );
  STRONG_CHECK( event->retrieve(truthVertices,    "TruthVertices") );
  STRONG_CHECK( event->retrieve(truthParticles,   "TruthParticles") );

  REFVAR( vtx_r,       std::vector<float> );
  REFVAR( vtx_z,       std::vector<float> );
  REFVAR( vtx_phi,     std::vector<float> );

  REFVAR( vtx_parent_pt, std::vector<float> );
  REFVAR( vtx_parent_eta, std::vector<float> );
  REFVAR( vtx_parent_phi, std::vector<float> );
  REFVAR( vtx_parent_pid, std::vector<int> );
  REFVAR( vtx_parent_prod_x, std::vector<float> );
  REFVAR( vtx_parent_prod_y, std::vector<float> );
  REFVAR( vtx_parent_prod_z, std::vector<float> );
  REFVAR( vtx_parent_prod_r, std::vector<float> );
  REFVAR( vtx_parent_prod_phi, std::vector<float> );

  REFVAR( vtx_outP_pt,     std::vector< std::vector<float> > );
  REFVAR( vtx_outP_eta,    std::vector< std::vector<float> > );
  REFVAR( vtx_outP_phi,    std::vector< std::vector<float> > );
  REFVAR( vtx_outP_pid,    std::vector< std::vector<int> > );
  REFVAR( vtx_outP_isReco, std::vector< std::vector<uint8_t> > );



  static std::map<std::string, bool (*)(const xAOD::TruthVertex*)> pidFuncs;
  if( 0 == pidFuncs.size() ) {
    pidFuncs["Rhadron"]    = VsiTruthHelper::selectRhadron;
    pidFuncs["DarkPhoton"] = VsiTruthHelper::selectDarkPhoton;
    pidFuncs["Bmeson"]     = VsiTruthHelper::selectBmeson;
    pidFuncs["HadInt"]     = VsiTruthHelper::selectHadInt;
    pidFuncs["Kshort"]     = VsiTruthHelper::selectKshort;
  }



  for( const auto *truthVertex : *truthVertices ) {

    if( !pidFuncs[m_probeTruth]( truthVertex ) ) continue;

    r_vtx_r    .emplace_back( truthVertex->perp() );
    r_vtx_z    .emplace_back( truthVertex->z() );
    r_vtx_phi  .emplace_back( truthVertex->phi() );

    const auto* parent = truthVertex->incomingParticle(0);
    if( parent ) {
      r_vtx_parent_pt.emplace_back( parent->pt() );
      r_vtx_parent_eta.emplace_back( parent->eta() );
      r_vtx_parent_phi.emplace_back( parent->phi() );
      r_vtx_parent_pid.emplace_back( parent->pdgId() );

      const auto* prodVtx = parent->prodVtx();
      r_vtx_parent_prod_x.emplace_back( prodVtx? prodVtx->x() : AlgConsts::invalidFloat );
      r_vtx_parent_prod_y.emplace_back( prodVtx? prodVtx->y() : AlgConsts::invalidFloat );
      r_vtx_parent_prod_z.emplace_back( prodVtx? prodVtx->z() : AlgConsts::invalidFloat );
      r_vtx_parent_prod_r.emplace_back( prodVtx? prodVtx->perp() : AlgConsts::invalidFloat );
      r_vtx_parent_prod_phi.emplace_back( prodVtx? prodVtx->phi() : AlgConsts::invalidFloat );

    } else {
      r_vtx_parent_pt       .emplace_back ( AlgConsts::invalidFloat );
      r_vtx_parent_eta      .emplace_back( AlgConsts::invalidFloat );
      r_vtx_parent_phi      .emplace_back( AlgConsts::invalidFloat );
      r_vtx_parent_pid      .emplace_back( AlgConsts::invalidInt );
      r_vtx_parent_prod_x   .emplace_back( AlgConsts::invalidFloat );
      r_vtx_parent_prod_y   .emplace_back( AlgConsts::invalidFloat );
      r_vtx_parent_prod_z   .emplace_back( AlgConsts::invalidFloat );
      r_vtx_parent_prod_r   .emplace_back( AlgConsts::invalidFloat );
      r_vtx_parent_prod_phi .emplace_back( AlgConsts::invalidFloat );
    }

    std::vector<float> pt;
    std::vector<float> eta;
    std::vector<float> phi;
    std::vector<int> pid;
    std::vector<uint8_t> isReco;

    for( unsigned ip = 0; ip < truthVertex->nOutgoingParticles(); ip++ ) {
      auto* p = truthVertex->outgoingParticle( ip );

      if(  fabs( p->charge() ) < 1.0 ) continue;

      pt .emplace_back( p->pt()  );
      eta.emplace_back( p->eta() );
      phi.emplace_back( p->phi() );
      pid.emplace_back( p->pdgId() );

      bool reco = VsiTruthHelper::isReconstructed( p, recoTracks );

      isReco.emplace_back( reco );
    }

    r_vtx_outP_pt     .emplace_back( pt  );
    r_vtx_outP_eta    .emplace_back( eta );
    r_vtx_outP_phi    .emplace_back( phi );
    r_vtx_outP_pid    .emplace_back( pid );
    r_vtx_outP_isReco .emplace_back( isReco );

  }

  return EL::StatusCode::SUCCESS;
}
