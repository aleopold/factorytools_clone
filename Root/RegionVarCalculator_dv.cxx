#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "AsgTools/MessageCheck.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"

#include "FactoryTools/Baseline_DVJETS_Selection.h"
#include "FactoryTools/DRAW_DVJETS_filters.h"
#include "FactoryTools/JetCleaning_DVJETS_custom.h"
#include "FactoryTools/RegionVarCalculator_dv.h"
#include "FactoryTools/strongErrorCheck.h"

#include <FactoryTools/HelperFunctions.h>
#include <FactoryTools/VsiAnalysis/VsiTruthHelper.h>


#include <fstream>
// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_dv)
using namespace asg::msgUserCode;


typedef FactoryTools::HelperFunctions HF;
typedef xAOD::JetAttribute JA;


//**************************
// C O S M I C   T A G
//**************************
// Determines if an id track is back to back with a muon
bool cosmic_Tag( TLorentzVector p1, TLorentzVector p2) {

	double PI = 3.1415926535897; // already in fast jet?
	float absdphi = fabs(p1.DeltaPhi(p2));
	float sumeta = p1.Eta()+p2.Eta();
	float dR_cos = sqrt( ( absdphi - PI )*( absdphi - PI ) + sumeta*sumeta );

	if (dR_cos < 0.1) return true;
	else return false;
}

//************************************************
// F I N D  F I N A L  S T A T E  C H I L D R E N
//************************************************
// Find all status 1 children of a sparticle. Save the vector of pointers to the children together with a pointer to the the parent in a pair, i.e. (parent, children). Then save this pair in a map with the parent (pdgId, barcode) as key. 
// This is a complicated structure, but allows us to keep the original method to match ID tracks to
// truthParticles while adding the possibility to retrieve more LLP properties.
//  std::map< std::pair<int,int>, std::pair< const xAOD::TruthParticle*, std::vector< const xAOD::TruthParticle*> > > m_llpChildren;
EL::StatusCode RegionVarCalculator_dv::findLLPChildren(const xAOD::TruthParticle* parent){

    //First, fill a map of final-state, charged children    //First, fill a map of final-state, charged children
    //ing it this way ensures no duplicates from 'loops' in the decay tree
    std::map< std::pair<int,int>, const xAOD::TruthParticle* > childMap;
    RegionVarCalculator_dv::fillChildMap( &childMap, parent );

    //Dump the map values to a vector
    std::vector< const xAOD::TruthParticle* > uniqueChildren;
    typedef std::map< std::pair<int,int>, const xAOD::TruthParticle_v1* >::const_iterator MapIterator;
    for (MapIterator iter = childMap.begin(); iter != childMap.end(); iter++){
        uniqueChildren.push_back( iter->second );
    }

    //Copy that vector into the parent map
    m_llpChildren.insert( std::pair< std::pair<int,int>, std::pair< const xAOD::TruthParticle*, std::vector<const xAOD::TruthParticle*> > >( std::make_pair( parent->pdgId(), parent->barcode() ), std::make_pair( parent, uniqueChildren ) ) );

    return EL::StatusCode::SUCCESS;
}

//****************************
// F I L L  C H I L D  M A P
//****************************
void RegionVarCalculator_dv::fillChildMap( std::map< std::pair<int,int>, const xAOD::TruthParticle* >* childMap, const xAOD::TruthParticle* parent ){

    if (!parent->decayVtx()) {} // do nothing
    else {
        for( size_t i = 0; i<parent->decayVtx()->nOutgoingParticles(); i++ ){

            const xAOD::TruthParticle* child = parent->decayVtx()->outgoingParticle(i);

            if(child){
                if (child->status()==1 && child->charge() ){
                    childMap->insert( std::pair< std::pair<int,int>, const xAOD::TruthParticle* >( std::make_pair( child->pdgId(), child->barcode() ), child )); //This will only work if child's pdgID and barcode are not already in the map
                }else {  // if it's not a final state particle we need to loop through it's children
                    fillChildMap(childMap, child);
                }
            }
        }
    }
    return;
}

//*********************************
// S O R T  C O M P A R A T O R S
//*********************************
//Compares pointers numerically, not what they're pointing to
// Used to make iterating through ID tracks and comparing them to
// truthParticles - very quick!
struct sortPair {
    bool operator()(const std::pair<const xAOD::TruthParticle*,const xAOD::TrackParticle*> &left, const std::pair<const xAOD::TruthParticle*,const xAOD::TrackParticle*> &right) {
        return left.first < right.first;
    }
};
struct sortXAODTruthParticlePointers {
    bool operator()(const xAOD::TruthParticle_v1*  &left, const xAOD::TruthParticle_v1* &right) {
        return left < right;
    }
};

//*************************
// D O I N I T I A L I Z E
//*************************

EL::StatusCode RegionVarCalculator_dv::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	//Instantiate class to resolve OR link
	m_overlapLinkHelper = std::make_unique<ORUtils::OverlapLinkHelper>("overlapLinkHelper");

	return EL::StatusCode::SUCCESS;
}

//**************************
// D O   C A L C U L A T E
//**************************
EL::StatusCode RegionVarCalculator_dv::doCalculate(std::map<std::string, anytype>& vars) {

	//Setup
	xAOD::TEvent* event = m_worker->xaodEvent();

	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

	if ( regionName.empty() ) {
		// If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
		ANA_MSG_DEBUG("No region name set, no calculations performed.");
		return EL::StatusCode::SUCCESS;
	}
	else if ( regionName == "SRDV" ) {
		// other SR definitions can have other trees written out with other functions here
		ANA_MSG_DEBUG("Selection region set: SRDV");
		return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS);
	}

	return EL::StatusCode::SUCCESS;
}


//**************************
// D O   A L L   C A L C
//**************************
EL::StatusCode RegionVarCalculator_dv::doAllCalculations(std::map<std::string, anytype>& vars ) {
	xAOD::TStore * store = m_worker->xaodStore();
	xAOD::TEvent * event = m_worker->xaodEvent();

	auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

	doGeneralCalculations(vars);

	auto toGeV = [](double a){return a*.001;};

    //&&&&&&&&&&&&&&&&&&&&&
    // P V
    //&&&&&&&&&&&&&&&&&&&&&
	//////////////////////////////////////////////////
	// get the highest-sumpT primary vertex (one is needed for recalculating MET)
	auto pvc = HF::grabFromStore<xAOD::VertexContainer>("selectedPV",store);

	auto pv = pvc->at(0);
	vars["PV_x"] = pv->x();
	vars["PV_y"] = pv->y();
	vars["PV_z"] =  pv->z();
	vars["PV_rxy"] =  TMath::Hypot(pv->x(),pv->y());
	vars["PV_nTracks"] = pv->nTrackParticles();
	vars["PV_sumpT2"] = pv->auxdataConst<float>( "sumPt2" );

	///////////////////////////////////////////////////////////////

	//&&&&&&&&&&&&&
	// M E T
	//&&&&&&&&&&&&&
	auto metcont = HF::grabFromStore<xAOD::MissingETContainer>("STCalibMET",store);
	vars["passMETtrigger"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
	vars["MET"] = toGeV((*metcont)["Final"]->met());
	vars["MET_phi"] = ((*metcont)["Final"]->phi());


	auto met_lht_container = HF::grabFromEvent<xAOD::MissingETContainer>("MET_LocHadTopo",event);
	bool hasMET = met_lht_container->size() > 0;
	vars["MET_LHT"] = hasMET ? met_lht_container->at(0)->met() * 0.001 : -999.;
	vars["MET_LHT_phi"] = hasMET ? met_lht_container->at(0)->phi() : -999.;


	auto met_hlt_container = HF::grabFromEvent<xAOD::TrigMissingETContainer>("HLT_xAOD__TrigMissingETContainer_TrigEFMissingET",event);
	TVector2 hltMET;
	hasMET = met_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(met_hlt_container->at(0)->ex(), met_hlt_container->at(0)->ey());
	vars["MET_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MET_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;


	// retrieve HLT MHT
	auto mht_hlt_container = HF::grabFromEvent<xAOD::TrigMissingETContainer>("HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht",event);
	hasMET = mht_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(mht_hlt_container->at(0)->ex(), mht_hlt_container->at(0)->ey());
	vars["MHT_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MHT_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

        // photon trigger for DV background studies
        vars["pass_HLT_g140_loose"] = eventInfo->auxdecor<bool>("passHLTg140loose");

	//&&&&&&&&&&&&&&&&&&&&&&&&&&&
	// S E L E C T E D   J E T S
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&
   	auto selectedJets = HF::grabFromStore<xAOD::IParticleContainer>("selectedJets",store);
	if(inListOfDetailedObjects("selectedJets")){
	  for( const auto& jet : *selectedJets) {
		addToVectorBranch(vars,"jet_Pt", toGeV(jet->pt() ) );
		addToVectorBranch(vars,"jet_Eta", jet->p4().Eta()  );
		addToVectorBranch(vars,"jet_Phi", jet->p4().Phi()  );
		addToVectorBranch(vars,"jet_M", toGeV(jet->p4().M() ) );
		addToVectorBranch(vars,"jet_BTag", jet->auxdata<char>("bjet") );
		addToVectorBranch(vars,"jet_PassOR", (int)jet->auxdata<char>("passOR") ); //TODO isn't this redundant since sleected jets are required to have passOR == 1?
	  }
        }

	//&&&&&&&&&&&&&&&&&&&
	//Uncalibrated jets
	//&&&&&&&&&&&&&&&&&&&
	if(inListOfDetailedObjects("uncalibJets")){

		auto uncalibjets_nominal = HF::grabFromEvent<xAOD::JetContainer>("AntiKt4EMTopoJets",event);

		for (const auto& jet : *uncalibjets_nominal) {

			addToVectorBranch(vars,"uncalib_JetPt", toGeV(jet->pt() ) );
			addToVectorBranch(vars,"uncalib_JetEta", jet->p4().Eta()  );
			addToVectorBranch(vars,"uncalib_JetPhi", jet->p4().Phi()  );
			addToVectorBranch(vars,"uncalib_JetM", toGeV(jet->p4().M() ) );

			//Store relevant info for trackless jets
			std::vector<float> sumPtTrkvec;
			jet->getAttribute(JA::SumPtTrkPt500, sumPtTrkvec);

			//Keep track of if this branch exists to make sure not mixing up entries in flattened branches.
			if( sumPtTrkvec.size() > 0){
				addToVectorBranch(vars,"SumPtTrkPt500", toGeV(sumPtTrkvec[0]) );
			} else {
				addToVectorBranch(vars,"SumPtTrkPt500", -1. );
			}

			//Jet cleaning variables
		        if( inListOfDetailedObjects("jetCleaningFlags") ){
			  addToVectorBranch(vars,"uncalibJet_EMFrac", HF::getJetAttribute(jet,JA::EMFrac) );
			  addToVectorBranch(vars,"uncalibJet_HECFrac", HF::getJetAttribute(jet,JA::HECFrac) );
			  addToVectorBranch(vars,"uncalibJet_LArQuality", HF::getJetAttribute(jet,JA::LArQuality) );
			  addToVectorBranch(vars,"uncalibJet_HECQuality", HF::getJetAttribute(jet,JA::HECQuality) );
			  addToVectorBranch(vars,"uncalibJet_FracSamplingMax", HF::getJetAttribute(jet,JA::FracSamplingMax) );
			  addToVectorBranch(vars,"uncalibJet_NegativeE", HF::getJetAttribute(jet,JA::NegativeE) );
			  addToVectorBranch(vars,"uncalibJet_AverageLArQF", HF::getJetAttribute(jet,JA::AverageLArQF) );
			  //addToVectorBranch(vars,"uncalibJet_FracSamplingMaxIndex", HF::getJetAttribute(jet,JA::FracSamplingMaxIndex) );
			}
		}
	}

	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	// T R U T H  J E T S
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	if(inListOfDetailedObjects("truthJets")){
		if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

			// Truth jets matched to neutralino, corrected to point to the PV
			auto truthNeutralinoJets = HF::grabFromEvent<xAOD::IParticleContainer>("AntiKt4LLP_NeutralinoTruthJets",event);

			// Uncorrected (standard) truth jets
			auto truthJets = HF::grabFromEvent<xAOD::IParticleContainer>("AntiKt4TruthJets",event);
			for (unsigned int j=0; j<truthJets->size(); j++){
				addToVectorBranch(vars,"truthJet_E", toGeV(truthJets->at(j)->p4().E() ) );
				addToVectorBranch(vars,"truthJet_Pt", toGeV(truthJets->at(j)->pt() ) );
				addToVectorBranch(vars,"truthJet_Eta", truthJets->at(j)->p4().Eta()  );
				addToVectorBranch(vars,"truthJet_Phi", truthJets->at(j)->p4().Phi()  );
				addToVectorBranch(vars,"truthJet_M", toGeV(truthJets->at(j)->p4().M() ) );

				// Find matching BSM particle and store decay position and pdg Id
				TLorentzVector truthJetTLV(0,0,0,0);
				truthJetTLV.SetPtEtaPhiE(truthJets->at(j)->pt(), truthJets->at(j)->eta(), truthJets->at(j)->phi(), truthJets->at(j)->e());

				float matchX = -9999.;
				float matchY = -9999.;
				float matchZ = -9999.;
				int matchPdgId = 0;
				bool isMatched = false;
				double corrEta = truthJets->at(j)->p4().Eta();
				double corrPhi = truthJets->at(j)->p4().Phi();

				const xAOD::TruthParticle* matchedLLP = nullptr;

				auto truthParts = HF::grabFromEvent<xAOD::TruthParticleContainer>("TruthParticles",event);
				float dRMinTruth = 0.3; 
				for(auto tp: *truthParts){

					// Want to check if it's a BSM particle that decays
					// PDG Id between 1e6 and 3e6: SUSY
					// PDG Id 35 and 70: long-lived scalars from Higgs decays
					// PDG Id 4900211: dark pions
					bool isBSMparticle = false;
					int tpPDGId = std::abs(tp->pdgId());
					if( ( ( (tpPDGId > 1e6) && (tpPDGId < 3e6) ) || tpPDGId == 35 || tpPDGId == 70 || tpPDGId == 4900211 ) && tp->hasDecayVtx() ) {
						isBSMparticle = true;
					}

					if( !isBSMparticle ) continue;

					// Want to check if the BSM particle is long-lived
					bool isLLP = false;
					const xAOD::TruthVertex* vert = tp->decayVtx();
					float decayX = pv->position().x() - vert->x();
					float decayY = pv->position().y() - vert->y();
					float decayR = sqrt( decayX*decayX + decayY*decayY);
					if(decayR > 1) isLLP = true;
					
					if( !isLLP ) continue;

					// Want to make sure that it's an actual decay and that the LLP is not decaying into itself
					bool isRealDecay = true;

					if( vert->nOutgoingParticles() < 2 ) isRealDecay = false;

					if( !isRealDecay ) continue;

					for(unsigned int j=0; j<vert->nOutgoingParticles(); j++){
						if(!vert->outgoingParticle(j)) continue;
						if( std::abs(vert->outgoingParticle(j)->pdgId()) == std::abs(tp->pdgId()) ) {
							isRealDecay = false;
						}
					}

					if( !isRealDecay ) continue;

					// Loop through the children of the BSM particle
					for(unsigned int i=0; i<vert->nOutgoingParticles(); i++){
						if(!vert->outgoingParticle(i)) continue;
						const xAOD::TruthParticle* outgoing = vert->outgoingParticle(i);
						TLorentzVector outgoingTLV(0,0,0,0);
						outgoingTLV.SetPtEtaPhiE( outgoing->pt(), outgoing->eta(), outgoing->phi(), outgoing->e());

						// Find truth outgoing particle nearest to truth jet (within dR < 0.3)
						if( truthJetTLV.DeltaR( outgoingTLV) < dRMinTruth ){
							dRMinTruth = truthJetTLV.DeltaR( outgoingTLV);
							matchedLLP = tp;
						}
					}
				}

				// Fill all truth jet branches
				if (matchedLLP){
					isMatched = true;
					const xAOD::TruthVertex* matchedVert = matchedLLP->decayVtx();
					matchX = matchedVert->x();
					matchY = matchedVert->y();
					matchZ = matchedVert->z();
					matchPdgId = matchedLLP->pdgId();

					//If there jet is matched to a neutralino we want to use the corrected eta and phi
					if( std::abs(matchPdgId) == 1000022 ) {
						corrEta = truthNeutralinoJets->at(j)->p4().Eta();
						corrPhi = truthNeutralinoJets->at(j)->p4().Phi();
					}
				}

				addToVectorBranch(vars,"truthJet_matchedLLP_x", matchX );
				addToVectorBranch(vars,"truthJet_matchedLLP_y", matchY );
				addToVectorBranch(vars,"truthJet_matchedLLP_z", matchZ );
				addToVectorBranch(vars,"truthJet_matchedLLP_pdgId", matchPdgId );
				addToVectorBranch(vars,"truthJet_hasMatchedLLP", (int)isMatched );
				addToVectorBranch(vars,"truthJet_corrEta", corrEta );  // These branches are filled with the uncorrected eta and phi if there is no matching neutralino
				addToVectorBranch(vars,"truthJet_corrPhi", corrPhi );

			}
		}
	}
	



	//********************************
	// DRAW DV+JETS filter flags
	//********************************
	if(inListOfDetailedObjects("DVJETS_DRAW_flags")){

          auto uncalibjets_nominal = HF::grabFromEvent<xAOD::JetContainer>("AntiKt4EMTopoJets",event);

  	  DRAW_DVJETS_filters::FilterFlags filterFlags; //Struct to store filter decisions
	  if( DRAW_DVJETS_filters::filterEvents( uncalibjets_nominal, m_event_triggerDecisions, filterFlags ) == EL::StatusCode::FAILURE ){
		return EL::StatusCode::FAILURE;
	  } else {
		//TODO store other filter decisions, like multiplicity flag
		vars["DRAW_pass_DVJETS"]                =  filterFlags.pass_DRAW;
		vars["DRAW_pass_earlyRun2Flag"]         =  filterFlags.pass_earlyRun2;
		vars["DRAW_pass_highPtJetFlag"]         =  filterFlags.pass_highPtJetMult;
		vars["DRAW_pass_triggerFlags"]          =  filterFlags.pass_triggerFlags;
		vars["DRAW_pass_singleTracklessJetFlag"]=  filterFlags.pass_singleTracklessFlag;
		vars["DRAW_pass_doubleTracklessJetFlag"]=  filterFlags.pass_doubleTracklessFlag;
	  }
        }


	//&&&&&&&&&&&&&&&&&&&&&
	// Baseline selections
	//&&&&&&&&&&&&&&&&&&&&&
	auto jets_nominal = HF::grabFromStore<const xAOD::JetContainer>("STCalibAntiKt4EMTopoJets",store);
	Baseline_DVJETS_Selection::Flags baselineFlags; //Struct to store decision
	if( Baseline_DVJETS_Selection::filterEvents( jets_nominal, baselineFlags ) == EL::StatusCode::FAILURE ){

		ANA_MSG_ERROR("Baseline_DVJETS_Selection::filterEvents returned a failure code. Exiting.");
		return EL::StatusCode::FAILURE;

	} else {

		vars["BaselineSel_HighPtSR"]        =  baselineFlags.pass_jetHighPtSelection;
		vars["BaselineSel_TracklessSR"]     =  baselineFlags.pass_SRTracklessSelection;
		vars["BaselineSel_LowPtJetSel"]     =  baselineFlags.pass_jetLowPtSelection;
		vars["BaselineSel_SingleTrackless"] =  baselineFlags.pass_SingleTracklessFlag;
		vars["BaselineSel_DoubleTrackless"] =  baselineFlags.pass_DoubleTracklessFlag;
	}

	//&&&&&&&&&&&&&&&&&
	//Calibrated jets
	//&&&&&&&&&&&&&&&&&
	for( const auto& jet : *jets_nominal) {
		addToVectorBranch(vars,"calibJet_Pt", toGeV(jet->pt() ) );
		addToVectorBranch(vars,"calibJet_Eta", jet->p4().Eta()  );
		addToVectorBranch(vars,"calibJet_Phi", jet->p4().Phi()  );
		addToVectorBranch(vars,"calibJet_M", toGeV(jet->p4().M() ) );
		addToVectorBranch(vars,"calibJet_BTag", jet->auxdata<char>("bjet") );
		addToVectorBranch(vars,"calibJet_sPassOR", (int)jet->auxdata<char>("passOR") );
		addToVectorBranch(vars,"calibJet_isFlaggedBad", jet->auxdata<char>("bad") );
		addToVectorBranch(vars,"calibJet_Jvt", jet->auxdata<float>("Jvt") );

		//*******************************
		// JET CLEANING FLAGS
		//*******************************
		//Jet cleaning variables
		if( inListOfDetailedObjects("jetCleaningFlags") ){
  	          addToVectorBranch(vars,"calibJet_EMFrac", HF::getJetAttribute(jet,JA::EMFrac) );
		  addToVectorBranch(vars,"calibJet_HECFrac", HF::getJetAttribute(jet,JA::HECFrac) );
		  addToVectorBranch(vars,"calibJet_LArQuality", HF::getJetAttribute(jet,JA::LArQuality) );
		  addToVectorBranch(vars,"calibJet_HECQuality", HF::getJetAttribute(jet,JA::HECQuality) );
		  addToVectorBranch(vars,"calibJet_FracSamplingMax", HF::getJetAttribute(jet,JA::FracSamplingMax) );
		  addToVectorBranch(vars,"calibJet_NegativeE", HF::getJetAttribute(jet,JA::NegativeE) );
		  addToVectorBranch(vars,"calibJet_AverageLArQF", HF::getJetAttribute(jet,JA::AverageLArQF) );
		  //addToVectorBranch(vars,"calibJet_FracSamplingMaxIndex", HF::getJetAttribute(jet,JA::FracSamplingMaxIndex) );
                }

		//&&&&&&&&&&&&&&&&&&&&&&
		// setup OR link helper
		//&&&&&&&&&&&&&&&&&&&&&&
		//If the jet failed OR, a decoration is set describing what container didn't pass.
		//However, so far the overlapLinkHelper->getObjectLink always returns a null pointer.
		//TODO: why does this not work?
		if( inListOfDetailedObjects("detailedOR") ){

			const xAOD::IParticle* ipart = static_cast<const xAOD::IParticle*>( jet );
			if( (int)jet->auxdata<char>("passOR") != 1 ){
				if( ipart ){
					const xAOD::IParticle* linkParticle = m_overlapLinkHelper->getObjectLink( *ipart );
					if( linkParticle ){


						ANA_MSG_DEBUG("## RVC_dv ##:linkParticle->type: "<<linkParticle->type());
						//TODO: if this particle link ever resolves, this may be a way to do it.
					} else {
						//std::cout<<"Particle link is NULL."<<std::endl;
					}
				} else {
					//std::cout<<"ipart pointer is NULL"<<std::endl;
				}
			}//endif passOr
		}//endif detailedOr
	}//endif jetsNominal
 
        //&&&&&&&&&&&&&&&&&&&&&&&&
        // Track jets
        //&&&&&&&&&&&&&&&&&&&&&&&&
        auto primaryvertices = HF::grabFromEvent<xAOD::VertexContainer>("PrimaryVertices",event);
        for (size_t i=0; i<primaryvertices->size(); i++){

          // Limit only to PV
          if (onlyPVtrackjets && i!=0) continue;

          if (eventInfo->auxdecor<int>("InDetTrackParticles_noLRT_"+std::to_string(i)+"_numTracks")<minNTracks) continue;
          if (eventInfo->auxdecor<float>("InDetTrackParticles_noLRT_"+std::to_string(i)+"_scalarSumPt")<minSumPtTracks) continue;

          auto jets_track = HF::grabFromEvent<xAOD::JetContainer>("MyAntiKt4TrackJets_"+std::to_string(i),event);

          for (const auto& jet: *jets_track){
            if (jet->getConstituents().size()<2) continue;
            addToVectorBranch(vars,"AntiKt4Track_pt", toGeV(jet->p4().Pt()));
            addToVectorBranch(vars,"AntiKt4Track_eta", jet->p4().Eta() );
            addToVectorBranch(vars,"AntiKt4Track_phi", jet->p4().Phi() );
            addToVectorBranch(vars,"AntiKt4Track_m", toGeV(jet->p4().M()));
            addToVectorBranch(vars,"AntiKt4Track_nTracks", jet->getConstituents().size() );
            addToVectorBranch(vars,"AntiKt4Track_PV", i );
          }
        }


        //&&&&&&&&&&&&&&&&&&&&&
	// L E P T O N  S F
	//&&&&&&&&&&&&&&&&&&&&&
	vars["muSF"] = eventInfo->auxdecor<float>("muSF");

	for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("muSF_systs")) {
		vars["muSF_"+systName+""] = eventInfo->auxdecor<float>("muSF_"+systName);
	}

	vars["elSF"] = eventInfo->auxdecor<float>("elSF");

	for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("elSF_systs")){
		vars["elSF_"+systName+""] = eventInfo->auxdecor<float>("elSF_"+systName);
	}

	double MEff = 0;
	double HT = 0;

	for( const auto& jet : *selectedJets) {
		HT += toGeV(jet->pt());
	}

	MEff = HT + toGeV((*metcont)["Final"]->met());

	vars["MEff"] = MEff;
	vars["HT"] = HT;


    //&&&&&&&&&&&
    // T R U T H
    //&&&&&&&&&&&
    //Needed to match to truth sparticles
       auto id_tracks = HF::grabFromEvent<xAOD::TrackParticleContainer>("InDetTrackParticles",event);

    if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

        //**********************************************
        //Fill a vector with pairs of <TruthParticles,TrackParticles> to check against LLP children
        //**********************************************
        std::vector< std::pair< const xAOD::TruthParticle*, const xAOD::TrackParticle*> > id_track_pairs;
        if(inListOfDetailedObjects("LLP_truth")){

            id_track_pairs.reserve( id_tracks->size() );
            for ( const xAOD::TrackParticle* id_track : *id_tracks){
                const xAOD::TruthParticle* truth = VsiTruthHelper::getTruthParticle( id_track );
                if( truth ){
                    id_track_pairs.push_back( std::make_pair( truth, id_track ));
                }
            }
            //Sort
            std::sort( id_track_pairs.begin(), id_track_pairs.end(), sortPair());
        }


        //**********************************************
        // Get the truth events to get the true signal vertex position
        //**********************************************
        auto truthEvents = HF::grabFromEvent<xAOD::TruthEventContainer>("TruthEvents",event);
        auto trueSignalVertex = *((*truthEvents->begin())->truthVertexLink(0));


        //**********************************************
        // Truth Particles
        //**********************************************
        if(inListOfDetailedObjects("LLP_truth")){
            auto truthParticles = HF::grabFromEvent<xAOD::TruthParticleContainer>("TruthParticles",event);

            m_llpChildren.clear();
            ANA_MSG_DEBUG("Looping truth particles...");
            for (const auto& p : *truthParticles){

                //Keep a list of charged, final state particles for later
                if( p->status()==1 && p->charge() && toGeV( p->pt() )>1. ){
                    m_goodTrack_truthParticles.push_back( p );
                }
                //Only looking at SUSY particles that decay
                if( (fabs(p->pdgId()) > 1e6) && (fabs(p->pdgId()) < 3e6) && p->hasDecayVtx() ) {
                    float lifetimeLab = (p->decayVtx()->v4().Vect()-trueSignalVertex->v4().Vect()).Mag()/( p->p4().Beta()*p->p4().Gamma()*TMath::C()  );

                    //Don't save all the intermediate prompt SUSY particles
                    if( lifetimeLab > 0. && p->decayVtx()->nOutgoingParticles() > 1){

                        //Set up counters
                        Int_t nCharged_LLP_directDescendants = 0;
                        Int_t nCharged1GeV_LLP_directDescendants = 0;
                        TLorentzVector chargedParticleFourVector;
                        bool incompleteDecayProducts = false;

                        //**********************************************
                        // Loop over decay products
                        //**********************************************
                        //NB these are only the first decay products in the chain, not the final-state particles!
                        for(size_t ii = 0; ii < p->decayVtx()->nOutgoingParticles(); ++ii){

                            if ( p->decayVtx()->outgoingParticle(ii) ){ //check for broken particle links
                                const xAOD::TruthParticle* q = p->decayVtx()->outgoingParticle(ii);

                                if ( q->charge() ){
                                    nCharged_LLP_directDescendants++;
                                    if( toGeV( q->pt() ) > 1.  ) {
                                        nCharged1GeV_LLP_directDescendants++;
                                    }
                                    chargedParticleFourVector +=  q->p4();
                                }
                            } else {
                                incompleteDecayProducts = true;
                            }
                        }

                        //Is the LLP decaying far enough away from the PV?
                        bool truth_PVCut = true;
                        for(const auto & pv: *pvc){
			                float diff_x = pv->position().x() - p->decayVtx()->x();
			                float diff_y = pv->position().y() - p->decayVtx()->y();
                            float diff_perp = std::sqrt( diff_x*diff_x + diff_y*diff_y );
                            if( diff_perp < 4.0 ) truth_PVCut = false;
		                }

                        addToVectorBranch( vars, "truthSparticle_Barcode"             , p->barcode()     ); //Necessary for truth-track matching
                        addToVectorBranch( vars, "truthSparticle_PdgId"               , (int) p->pdgId() );
                        addToVectorBranch( vars, "truthSparticle_Pt"                  , toGeV(p->pt() )  );
                        addToVectorBranch( vars, "truthSparticle_Eta"                 , p->eta()         );
                        addToVectorBranch( vars, "truthSparticle_Phi"                 , p->phi()         );
                        addToVectorBranch( vars, "truthSparticle_M"                   , toGeV(p->m())    );
                        addToVectorBranch( vars, "truthSparticle_BetaGamma"           , p->p4().Beta()*p->p4().Gamma()  );
                        addToVectorBranch( vars, "truthSparticle_Beta"           , p->p4().Beta() );
                        addToVectorBranch( vars, "truthSparticle_ProperDecayTime"     , lifetimeLab  );
                        addToVectorBranch( vars, "truthSparticle_PVCut"               , (int)truth_PVCut );
                        addToVectorBranch( vars, "truthSparticle_VtxX"                , p->decayVtx()->x()  );
                        addToVectorBranch( vars, "truthSparticle_VtxY"                , p->decayVtx()->y()  );
                        addToVectorBranch( vars, "truthSparticle_VtxZ"                , p->decayVtx()->z()  );
                        addToVectorBranch( vars, "truthSparticle_VtxNParticles"       , p->decayVtx()->nOutgoingParticles()  );
                        addToVectorBranch( vars, "truthSparticle_VtxNChParticles"     , nCharged_LLP_directDescendants  );
                        addToVectorBranch( vars, "truthSparticle_VtxNChParticles1GeV" , nCharged1GeV_LLP_directDescendants  );
                        addToVectorBranch( vars, "truthSparticle_VtxMChParticles"     , toGeV(chargedParticleFourVector.M() )  );
                        if( !incompleteDecayProducts ){
                            addToVectorBranch( vars, "truthSparticle_VtxMChParticles"     , toGeV(chargedParticleFourVector.M() )  );
                        } else {
                            addToVectorBranch( vars, "truthSparticle_VtxMChParticles"     , -1.  );
                        }

                        //*************************************************
                        // LLP truth track info - via final state children
                        //*************************************************
                        //More counters
                        Int_t nCharged = 0;
                        Int_t nCharged1GeV = 0;
                        Int_t nCharged1GeVd0 = 0;
                        Int_t nCharged1GeVd0dist = 0;
                        Int_t nCharged1GeVd0distReco = 0;
                        TLorentzVector chargedParticleFourVector_reco;
                        TLorentzVector chargedParticleFourVector1GeV;
                        TLorentzVector chargedParticleFourVector1GeV_reco;
                        TLorentzVector chargedParticleFourVector1GeVd0;
                        TLorentzVector chargedParticleFourVector1GeVd0dist;
                        TLorentzVector chargedParticleFourVector1GeVd0distReco;
                        TVector3 truthVtxPos(p->decayVtx()->x(), p->decayVtx()->y(), p->decayVtx()->z());


                        ANA_MSG_DEBUG("Find LLP children for parent: "<<p->pdgId()<<"....");
                        findLLPChildren(p);

                        ANA_MSG_DEBUG("Iterating through map of children, checking if reconstructed...");
                        ANA_MSG_DEBUG("There are: "<<m_llpChildren.size()<<" keys in this map.");

                        //Set up iterators
                        std::map< std::pair<int,int>, std::pair< const xAOD::TruthParticle_v1*, std::vector< const xAOD::TruthParticle_v1* > > >::iterator iter = m_llpChildren.find( std::pair<int,int>( p->pdgId(), p->barcode() ) );
                        ANA_MSG_DEBUG("There are: "<<iter->second.second.size()<<" truthParticle children for this LLP");
                        std::vector< std::pair< const xAOD::TruthParticle_v1*, const xAOD::TrackParticle* > >::iterator id_It = id_track_pairs.begin();
                        std::vector< const xAOD::TruthParticle_v1* >::iterator child_It = iter->second.second.begin();

                        //Sort the children, otherwise the looping below won't match
                        std::sort( iter->second.second.begin(), iter->second.second.end(), sortXAODTruthParticlePointers() );

                        //&&&&&&&&&&&&&&&&&&
                        // L O O P  I D
                        //&&&&&&&&&&&&&&&&&&
                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        //All of the while-loop and pointer logic below allows us to loop
                        //through and match two very large collections.
                        //If logic is confusing, please see this function for stripped-down
                        //version: https://gitlab.cern.ch/rcarney/comparingcontainers/blob/master/runTest.cxx#L178
                        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        bool comparePointers = false;
                        while( child_It != iter->second.second.end() ){

                            //If the ID track pointer is > LLP child pointer
                            //WE can either enter this looop if we have finished iterating through longer list (ID tracks),
                            //in which case we still want to save remaining child info.
                            if( id_It == id_track_pairs.end() || ((*id_It).first > *child_It) ){

                                const xAOD::TruthParticle_v1* child = *child_It;
                                bool isReco = false;
                                const xAOD::TrackParticle* recoTrk = NULL;

                                //If the ID track piinter in previous it position == LLP pointer, then the LLP child was reconsturucted!
                                if( comparePointers && ( (*(id_It-1)).first == *child_It) ){
                                    ANA_MSG_VERBOSE("IS RECO!!!! sIt: "<<(*child_It)<<", lIt: "<<(*(id_It-1)).first);
                                    isReco = true;
                                    recoTrk = (*(id_It-1)).second;

                                    if( id_It == id_track_pairs.end() ){
                                        //From hereonin we can safely assume the LLP child wasn't reconstructed and can just save remaining info
                                        comparePointers = false;
                                    }
                                }

                                //+++++++++++++++++++++++++++++++++++++++++++
                                // get an approximate true d0 = R*sin(dPhi),
                                // where dPhi is between the PV-to-DV vector
                                // and the track from the DV
                                //+++++++++++++++++++++++++++++++++++++++++++
                                TVector3 q;
                                q.SetMagThetaPhi(1.0, child->p4().Theta(), child->phi());
                                float dPhi = q.DeltaPhi(truthVtxPos);
                                Double_t d0 = truthVtxPos.Perp()*sin(fabs(dPhi));
                                addToVectorBranch( vars, "truth_LLPChild_d0"                  , d0 );

                                TVector3 childVtxPos;
                                childVtxPos.SetXYZ(child->prodVtx()->x(), child->prodVtx()->y(), child->prodVtx()->z() );
                                float dist = (childVtxPos - truthVtxPos).Mag();

                                //++++++++++++++++++
                                // Update counters
                                //++++++++++++++++++
                                nCharged++;
                                chargedParticleFourVector += child->p4();
                                if( isReco ){
                                    chargedParticleFourVector_reco += child->p4();
                                }
                                if( toGeV( child->pt() ) > 1. ) {
                                    nCharged1GeV++;
                                    chargedParticleFourVector1GeV += child->p4();
                                    if( isReco ){
                                        chargedParticleFourVector1GeV_reco += child->p4();
                                    }
                                    if( d0>2.0 ) {
                                        nCharged1GeVd0++;
                                        chargedParticleFourVector1GeVd0 += child->p4();
                                        if( fabs(dist) < 5 ) {
                                            nCharged1GeVd0dist++;
                                            chargedParticleFourVector1GeVd0dist += child->p4();
                                            if( isReco ) {
                                                nCharged1GeVd0distReco++;
                                                chargedParticleFourVector1GeVd0distReco += child->p4();
                                            } //end if
                                        }//end if < 5mm
                                    }//end d0>2
                                }//end pT cut

                                addToVectorBranch( vars, "truth_LLPChild_Status"              , child->status() );
                                addToVectorBranch( vars, "truth_LLPChild_Charge"              , child->charge() );
                                addToVectorBranch( vars, "truth_LLPChild_PdgId"               , child->pdgId() );
                                addToVectorBranch( vars, "truth_LLPChild_Pt"                  , toGeV(child->pt() )  );
                                addToVectorBranch( vars, "truth_LLPChild_Eta"                 , child->eta()   );
                                addToVectorBranch( vars, "truth_LLPChild_Phi"                 , child->phi()   );
                                addToVectorBranch( vars, "truth_LLPChild_Parent_pdgID"        , p->pdgId()    );
                                addToVectorBranch( vars, "truth_LLPChild_Parent_barcode"      , p->barcode()    );
                                addToVectorBranch( vars, "truth_LLPChild_p4_px"               , child->p4().Px() );
                                addToVectorBranch( vars, "truth_LLPChild_p4_py"               , child->p4().Py() );
                                addToVectorBranch( vars, "truth_LLPChild_p4_pz"               , child->p4().Pz() );
                                addToVectorBranch( vars, "truth_LLPChild_p4_E"               , child->p4().E() );
                                addToVectorBranch( vars, "truth_LLPChild_isReconstructed"     ,(int)isReco );

                                if(inListOfDetailedObjects("LLP_recoTracks")){
                                  if( !recoTrk ){
                                    addToVectorBranch( vars, "truth_LLPChild_recoTrkIndex"     , (unsigned long)0 );
                                  } else {

                                    addToVectorBranch( vars, "truth_LLPChild_recoTrkIndex"     ,recoTrk->index() );
                                    addToVectorBranch( vars, "idTrack_index" , recoTrk->index() );
                                    addToVectorBranch( vars, "idTrack_isLRT" , static_cast<int>( recoTrk->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) );

                                    // From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
                                    double id_pt = recoTrk->pt();
                                    double id_diff_qp = -id_pt / std::fabs(recoTrk->qOverP());
                                    double id_diff_theta = id_pt / tan(recoTrk->theta());
                                    const std::vector<float> &id_cov = recoTrk->definingParametersCovMatrixVec();
                                    double id_pt_err2 = id_diff_qp * (id_diff_qp * id_cov[14] + id_diff_theta * id_cov[13]) + id_diff_theta * id_diff_theta * id_cov[9];
                                    double id_errpT = toGeV( TMath::Sqrt(id_pt_err2) );

                                    addToVectorBranch( vars, "idTrack_errPt"   , id_errpT );
                                    addToVectorBranch( vars, "idTrack_errd0"   , TMath::Sqrt(recoTrk->definingParametersCovMatrix()(0, 0)) );
                                    addToVectorBranch( vars, "idTrack_errz0"   , TMath::Sqrt(recoTrk->definingParametersCovMatrix()(1, 1)) );

                                    addToVectorBranch( vars, "idTrack_theta"   ,recoTrk->theta() );
                                    addToVectorBranch( vars, "idTrack_eta"     ,recoTrk->eta() );
                                    addToVectorBranch( vars, "idTrack_phi"     ,recoTrk->phi() );
                                    addToVectorBranch( vars, "idTrack_pt"      ,toGeV( recoTrk->pt() ) );
                                    addToVectorBranch( vars, "idTrack_d0"      ,recoTrk->d0()  );
                                    addToVectorBranch( vars, "idTrack_z0"      ,recoTrk->z0()  );
                                    addToVectorBranch( vars, "idTrack_z0WrtPV" ,recoTrk->z0() + recoTrk->vz() - pv->z() );
                                    addToVectorBranch( vars, "idTrack_charge"  ,recoTrk->charge() );
                                    addToVectorBranch( vars, "idTrack_chi2"    ,recoTrk->auxdata< float >("chiSquared")/(recoTrk->auxdata< float >("numberDoF")) );

                                    addToVectorBranch( vars, "idTrack_RadFirstHit"     ,recoTrk->auxdata< float >("radiusOfFirstHit"    ) );

                                    addToVectorBranch( vars, "idTrack_NPix_Hits"       ,(int) recoTrk->auxdata< unsigned char >("numberOfPixelHits"       ) );
                                    addToVectorBranch( vars, "idTrack_NSct_Hits"       ,(int) recoTrk->auxdata< unsigned char >("numberOfSCTHits"         ) );
                                    addToVectorBranch( vars, "idTrack_NTrt_Hits"       ,(int) recoTrk->auxdata< unsigned char >("numberOfTRTHits"         ) );
                                    addToVectorBranch( vars, "idTrack_NPix_Holes"      ,(int) recoTrk->auxdata< unsigned char >("numberOfPixelHoles"      ) );
                                    addToVectorBranch( vars, "idTrack_NSct_Holes"      ,(int) recoTrk->auxdata< unsigned char >("numberOfSCTHoles"        ) );
                                    addToVectorBranch( vars, "idTrack_NTrt_Outliers"   ,(int) recoTrk->auxdata< unsigned char >("numberOfTRTOutliers"     ) );
                                    addToVectorBranch( vars, "idTrack_NPix_DeadSens"   ,(int) recoTrk->auxdata< unsigned char >("numberOfPixelDeadSensors") );
                                    addToVectorBranch( vars, "idTrack_NPix_ShrHits"    ,(int) recoTrk->auxdata< unsigned char >("numberOfPixelSharedHits" ) );
                                    addToVectorBranch( vars, "idTrack_NSct_DeadSens"   ,(int) recoTrk->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) );
                                    addToVectorBranch( vars, "idTrack_NSct_ShrHits"    ,(int) recoTrk->auxdata< unsigned char >("numberOfSCTSharedHits"   ) );
                                    addToVectorBranch( vars, "idTrack_hitPattern"        , recoTrk->auxdata< unsigned int >("hitPattern"   ) );

                                  }
                                }

                                //Incrememnt child it
                                child_It++;

                            }//if id_It > child_It
                            else {
                                //Keep iterating through and comparing ID track and LLP children track pointers
                                // until you either reach the end of either of the collections.
                                id_It++;
                                comparePointers = true;
                            }
                        }//end while id_It != end of vector

                        //Save counters started above
                        addToVectorBranch( vars, "truthSparticle_VtxNChParticles_final" , nCharged  );
                        addToVectorBranch( vars, "truthSparticle_VtxNChParticles1GeV_final" , nCharged1GeV  );
                        addToVectorBranch( vars, "truthSparticle_VtxNChParticles1GeVd0" , nCharged1GeVd0  );
                        addToVectorBranch( vars, "truthSparticle_VtxNChParticles1GeVd0dist" , nCharged1GeVd0dist  );
                        addToVectorBranch( vars, "truthSparticle_VtxNChParticles1GeVd0distReco" , nCharged1GeVd0distReco  );

                        addToVectorBranch( vars, "truthSparticle_VtxMChParticles1GeV"   , toGeV(chargedParticleFourVector1GeV.M() )  );
                        addToVectorBranch( vars, "truthSparticle_VtxMChParticles1GeV_reco", toGeV( chargedParticleFourVector1GeV_reco.M() ));
                        addToVectorBranch( vars, "truthSparticle_VtxMChParticles_reco", toGeV( chargedParticleFourVector_reco.M() ));

                        addToVectorBranch( vars, "truthSparticle_VtxMChParticles1GeVd0" , toGeV(chargedParticleFourVector1GeVd0.M() )  );
                        addToVectorBranch( vars, "truthSparticle_VtxMChParticles1GeVd0dist" , toGeV(chargedParticleFourVector1GeVd0dist.M() )  );
                        addToVectorBranch( vars, "truthSparticle_VtxMChParticles1GeVd0distReco" , toGeV(chargedParticleFourVector1GeVd0distReco.M() )  );

                    }//end if LLP
                }//end if( decays && susy )
            }//end for truth particles
        }//if inDetailedList LLP
    }//end if sim


    //&&&&&&&&
    //  D V
    //&&&&&&&&
    //m_dvContainerSuffix = "fixedExtrapolator"; // needs to be hardcoded for now, CI won't pass now if committed with this line active though /CO
    std::string dvContainerName = "VrtSecInclusive_SecondaryVertices" + m_dvContainerSuffix;
    auto dvc = HF::grabFromEvent<xAOD::VertexContainer>(dvContainerName, event);
 
    vars["VSI_status"] = eventInfo->auxdataConst<int>(dvContainerName+"_status");

    std::map<int,int> TrkToVtxMap;

    ANA_MSG_DEBUG("Looping DVs");
    int nDV = 0;
    for (const auto& dv: *dvc){

        addToVectorBranch( vars, "DV_index"      , dv->index()) ;
        addToVectorBranch( vars, "DV_x"          , dv->x()) ;
        addToVectorBranch( vars, "DV_y"          , dv->y()) ;
        addToVectorBranch( vars, "DV_z"          , dv->z()) ;
        addToVectorBranch( vars, "DV_rxy"        , dv->position().perp()) ;
        addToVectorBranch( vars, "DV_covariance0"    , dv->covariance()[0]) ;
        addToVectorBranch( vars, "DV_covariance1"    , dv->covariance()[1]) ;
        addToVectorBranch( vars, "DV_covariance2"    , dv->covariance()[2]) ;
        addToVectorBranch( vars, "DV_covariance3"    , dv->covariance()[3]) ;
        addToVectorBranch( vars, "DV_covariance4"    , dv->covariance()[4]) ;
        addToVectorBranch( vars, "DV_covariance5"    , dv->covariance()[5]) ;
        addToVectorBranch( vars, "DV_m"          , toGeV( dv->auxdataConst<float>("mass_cleaned") ) ) ;
        addToVectorBranch( vars, "DV_nTracks"    , dv->auxdataConst<int>("nTracks_cleaned") ) ;
        addToVectorBranch( vars, "DV_nTracksSel"    , dv->auxdataConst<int>("nTracksSel_cleaned") ) ;
        addToVectorBranch( vars, "DV_nLRT"       , dv->auxdataConst<int>("nTracksLRT_cleaned") ) ;
        addToVectorBranch( vars, "DV_chisqPerDoF", dv->chiSquared() / dv->numberDoF()  ) ;
        addToVectorBranch( vars, "DV_maxPtOverSumPt", dv->auxdataConst<float>("maxPtOverSumPt") );
        // Below are DV properties coming directly from VSI (no analysis-specific track cleaning). Commented out to avoid confusion.
        //addToVectorBranch( vars, "DV_mass"          , toGeV(dv->auxdataConst<float>("mass") ) ) ;
        //addToVectorBranch( vars, "DV_mass_selected" , toGeV(dv->auxdataConst<float>("mass_selectedTracks") ) ) ;
        //addToVectorBranch( vars, "DV_nTracks_selected" , dv->auxdataConst<int>("num_selectedTracks") ) ;

        //Selections
        addToVectorBranch( vars, "DV_passFiducialCut",(int)dv->auxdataConst<bool>("passFiducialCuts") );
        addToVectorBranch( vars, "DV_passChiSqCut", (int)dv->auxdataConst<bool>("passChisqCut") );
        addToVectorBranch( vars, "DV_passDistCut", (int)dv->auxdataConst<bool>("passDistCut") );
        addToVectorBranch( vars, "DV_passMassCut", (int)dv->auxdataConst<bool>("passMassCut") );
        addToVectorBranch( vars, "DV_passNTrkCut", (int)dv->auxdataConst<bool>("passNtrkCut") );
        addToVectorBranch( vars, "DV_passMaterialVeto", (dv->auxdataConst<bool>("passMaterialVeto")) ? 1 : 0 );
        addToVectorBranch( vars, "DV_passMaterialVeto_strict", (dv->auxdataConst<bool>("passMaterialVeto_strict")) ? 1 : 0 );

        //&&&&&&&&&&&&&&&&&&&&&
        // D V   T R A C K S
        //&&&&&&&&&&&&&&&&&&&&&

        //TM = TruthMatched
        TLorentzVector TM_p4;
        TLorentzVector TM_LLP_p4;
        TLorentzVector TM_LLP_N1_p4;

        auto tpLinks = dv->trackParticleLinks();
        for (auto link : tpLinks) {
            const xAOD::TrackParticle *trk = (*link);

            // Variables necessary to adjust DV-track cleaning

            // Track flags and index used to match to DV
            addToVectorBranch( vars, "dvtrack_DVIndex" , dv->index()  );
            addToVectorBranch( vars, "dvtrack_failedExtrapolation", trk->auxdataConst<int>("failedExtrapolation") );
            addToVectorBranch( vars, "dvtrack_isBackwardsTrack", trk->auxdataConst<int>("isBackwardsTrack") );
            addToVectorBranch( vars, "dvtrack_isAssociated", (trk->isAvailable<char>("is_associated"+ m_dvContainerSuffix)) ? trk->auxdataConst<char>("is_associated"+ m_dvContainerSuffix) : false  );
            addToVectorBranch( vars, "dvtrack_passpatternCheck", trk->auxdataConst<int>("pass_patternCheck") );
            addToVectorBranch( vars, "dvtrack_hitPattern" , trk->auxdata< unsigned int >("hitPattern"   ) );
            addToVectorBranch( vars, "dvtrack_isLRT"   , static_cast<int>(trk->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) );
            addToVectorBranch( vars, "dvtrack_charge"      , trk->charge() );

            // Relevant IP with respect to DV
            addToVectorBranch( vars, "dvtrack_etaWrtDV"   , trk->auxdataConst<float>("eta_wrtSV"+ m_dvContainerSuffix ) );
            addToVectorBranch( vars, "dvtrack_phiWrtDV"   , trk->auxdataConst<float>("phi_wrtSV"+ m_dvContainerSuffix ) );
            addToVectorBranch( vars, "dvtrack_ptWrtDV"    , toGeV( trk->auxdataConst<float>("pt_wrtSV"+ m_dvContainerSuffix)  ) );
            addToVectorBranch( vars, "dvtrack_d0WrtDV"    , trk->auxdataConst<float>("d0_wrtSV" + m_dvContainerSuffix )  );
            addToVectorBranch( vars, "dvtrack_z0WrtDV"    , trk->auxdataConst<float>("z0_wrtSV" + m_dvContainerSuffix )  );
            addToVectorBranch( vars, "dvtrack_errd0WrtDV" , trk->auxdataConst<float>("errd0_wrtSV" + m_dvContainerSuffix) );
            addToVectorBranch( vars, "dvtrack_errz0WrtDV" , trk->auxdataConst<float>("errz0_wrtSV" + m_dvContainerSuffix) );
            addToVectorBranch( vars, "dvtrack_errPWrtDV" , trk->auxdataConst<float>("errP_wrtSV" + m_dvContainerSuffix) );
            addToVectorBranch( vars, "dvtrack_chi2toDV" , trk->auxdataConst<float>("chi2_toSV" + m_dvContainerSuffix) );

            // Relevant IP and errors with respect to PV
            addToVectorBranch( vars, "dvtrack_d0"      , trk->d0() );
            addToVectorBranch( vars, "dvtrack_z0"      , trk->z0() );
            addToVectorBranch( vars, "dvtrack_errd0"   , TMath::Sqrt(trk->definingParametersCovMatrix()(0, 0)) );
            addToVectorBranch( vars, "dvtrack_errz0"   , TMath::Sqrt(trk->definingParametersCovMatrix()(1, 1)) );
            addToVectorBranch( vars, "dvtrack_eta"     , trk->eta() );
            addToVectorBranch( vars, "dvtrack_phi"     , trk->phi() );
            addToVectorBranch( vars, "dvtrack_pt"      , toGeV( trk->pt() ) );

            // From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
            double pt = trk->pt();
            double diff_qp = -pt / std::fabs(trk->qOverP());
            double diff_theta = pt / tan(trk->theta());
            const std::vector<float> &cov = trk->definingParametersCovMatrixVec();
            double pt_err2 = diff_qp * (diff_qp * cov[14] + diff_theta * cov[13]) + diff_theta * diff_theta * cov[9];
            double errpT = toGeV( TMath::Sqrt(pt_err2) );

            addToVectorBranch( vars, "dvtrack_errPt"   , errpT );

            if(inListOfDetailedObjects("dvTracks")){

                addToVectorBranch( vars, "dvtrack_index"   , trk->index() );
                addToVectorBranch( vars, "dvtrack_DVIndexOld" ,  ( TrkToVtxMap.find(trk->index()) == TrkToVtxMap.end() ) ? -99 : TrkToVtxMap[trk->index()] );
                addToVectorBranch( vars, "dvtrack_m"       , toGeV( trk->m() ) );

                addToVectorBranch( vars, "dvtrack_RadFirstHit",   trk->auxdataConst<float>("radiusOfFirstHit") );
                addToVectorBranch( vars, "dvtrack_PassTrackCleaning", (unsigned char)trk->auxdata<bool>("pass_cleaning") );
                addToVectorBranch( vars, "dvtrack_NPixHits"       , trk->auxdata< unsigned char >("numberOfPixelHits"       ) );
                addToVectorBranch( vars, "dvtrack_NSctHits"       , trk->auxdata< unsigned char >("numberOfSCTHits"         ) );
                addToVectorBranch( vars, "dvtrack_NTrtHits"       , trk->auxdata< unsigned char >("numberOfTRTHits"         ) );
                addToVectorBranch( vars, "dvtrack_NPixHoles"      , trk->auxdata< unsigned char >("numberOfPixelHoles"      ) );
                addToVectorBranch( vars, "dvtrack_NSctHoles"      , trk->auxdata< unsigned char >("numberOfSCTHoles"        ) );
                addToVectorBranch( vars, "dvtrack_NTrtOutliers"   , trk->auxdata< unsigned char >("numberOfTRTOutliers"     ) );
                addToVectorBranch( vars, "dvtrack_NPixDeadSens"   , trk->auxdata< unsigned char >("numberOfPixelDeadSensors") );
                addToVectorBranch( vars, "dvtrack_NPixSharedHits" , trk->auxdata< unsigned char >("numberOfPixelSharedHits" ) );
                addToVectorBranch( vars, "dvtrack_NSctDeadSens"   , trk->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) );
                addToVectorBranch( vars, "dvtrack_NSctSharedHits" , trk->auxdata< unsigned char >("numberOfSCTSharedHits"   ) );

            }//end detailedObjects DVtracks

            if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
            {
                    addToVectorBranch( vars, "dvtrack_truthMatchProb" , trk->auxdata< float >("truthMatchProbability"   ) );
                    addToVectorBranch( vars, "dvtrack_truthType"      , trk->auxdata<  int  >("truthType"   ) );
                    addToVectorBranch( vars, "dvtrack_truthOrigin"    , trk->auxdata<  int  >("truthOrigin"   ) );


                    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // TRACK TRUTH MATCHING FOR HADRONIC INTERACTIONS ESTIMATE
                    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    if(inListOfDetailedObjects("hadInt")){

                        bool hasValidTruthLink = false;
                        bool hasValidTruthVertex = false;
                        bool hasValidParentInfo = false;
                        int parentBarcode = 0;
                        int parentPdgId = 0;
                        int trackBarcode = 0;
                        int trackPdgId = 0;
                        float truthVertX = -9999.;
                        float truthVertY = -9999.;
                        float truthVertZ = -9999.;

                        // Check if the truth link exists and works
                        typedef ElementLink<xAOD::TruthParticleContainer> ElementTruthLink_t;
                        const xAOD::TruthParticle* truthPart(nullptr);
                        const ElementTruthLink_t truthPartLink = trk->auxdata<ElementTruthLink_t>("truthParticleLink");

                        if (truthPartLink.isValid()) {
                            truthPart = *truthPartLink;
                        }

                        // Retrieve truth information if truth link is valid
                        if( truthPart ) {
                            hasValidTruthLink = true;
                            trackBarcode = truthPart->barcode();
                            trackPdgId = truthPart->pdgId();
                            const xAOD::TruthVertex* truthVert = truthPart->prodVtx();
                            if (truthVert) {
                                hasValidTruthVertex = true;
                                truthVertX = truthVert->x();
                                truthVertY = truthVert->y();
                                truthVertZ = truthVert->z();
                                if( truthVert->nIncomingParticles()==1 && truthVert->incomingParticle(0)){
                                    hasValidParentInfo = true;
                                    parentPdgId = truthVert->incomingParticle(0)->pdgId();
                                    parentBarcode = truthVert->incomingParticle(0)->barcode();
                                }
                            }
                        }

                        addToVectorBranch( vars, "dvtrack_hasValidTruthLink", (int)hasValidTruthLink);
                        addToVectorBranch( vars, "dvtrack_hasValidTruthVertex", (int)hasValidTruthVertex);
                        addToVectorBranch( vars, "dvtrack_hasValidTruthParentInfo", (int)hasValidParentInfo);
                        addToVectorBranch( vars, "dvtrack_truthParentBarcode", parentBarcode);
                        addToVectorBranch( vars, "dvtrack_truthParentPdgId", parentPdgId);
                        addToVectorBranch( vars, "dvtrack_truthBarcode", trackBarcode);
                        addToVectorBranch( vars, "dvtrack_truthPdgId", trackPdgId);
                        addToVectorBranch( vars, "dvtrack_truthVtxX", truthVertX);
                        addToVectorBranch( vars, "dvtrack_truthVtxY", truthVertY);
                        addToVectorBranch( vars, "dvtrack_truthVtxZ", truthVertZ);
                    }


                    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    // TRACK TRUTH MATCHING TO LLP CHILDREN
                    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    if(inListOfDetailedObjects("LLP_truth")){

                      const xAOD::TruthParticle* track_truthParticle = VsiTruthHelper::getTruthParticle( trk );

                      //TM = TruthMatched
                      bool isTM = false;
                      bool isTM_toLLP = false;
                      int llpBarcode = 0;
                      int llpPdgID = 0;

                      float llpVertX = -9999.;
                      float llpVertY = -9999.;
                      float llpVertZ = -9999.;

                      //First, check if truth_trackParticle can be matched to one of the LLP particles
                      typedef std::map< std::pair<int,int>, std::pair< const xAOD::TruthParticle_v1*, std::vector< const xAOD::TruthParticle_v1* > > >::const_iterator MapIterator;
                      typedef std::vector< const xAOD::TruthParticle_v1* >::const_iterator VectorIterator;
                      for (MapIterator iter = m_llpChildren.begin(); iter != m_llpChildren.end(); iter++){
                          for (VectorIterator vec_iter = iter->second.second.begin(); vec_iter != iter->second.second.end(); vec_iter++){
                              if( track_truthParticle == *vec_iter ){
                                  isTM = true;
                                  isTM_toLLP = true;
                                  llpPdgID = iter->first.first;
                                  llpBarcode = iter->first.second;
                                  llpVertX = iter->second.first->decayVtx()->x();
                                  llpVertY = iter->second.first->decayVtx()->y();
                                  llpVertZ = iter->second.first->decayVtx()->z();
                              }
                          }
                      }

                      //If not TM to LLP, is it TM to a different charged final state particle?
                      if( !isTM_toLLP ){
                          for( unsigned k=0; k<m_goodTrack_truthParticles.size(); k++ ){
                              if( track_truthParticle == m_goodTrack_truthParticles[k] ){
                                  isTM = true;
                              }
                          }
                      }


                      //Claculate mass of tracks that are truth-matched for purity study
                      if( isTM ){
                          TLorentzVector p4_track;
                          p4_track.SetPtEtaPhiM(  trk->auxdataConst<float>( "pt_wrtSV"+ m_dvContainerSuffix  ),
                                  trk->auxdataConst<float>( "eta_wrtSV"+ m_dvContainerSuffix ),
                                  trk->auxdataConst<float>( "phi_wrtSV"+ m_dvContainerSuffix ),
                                  trk->m() );
                          TM_p4 += p4_track;
                          if( isTM_toLLP )
                              TM_LLP_p4 += p4_track;
                      }


                      addToVectorBranch( vars, "dvTrack_isTM", (int)isTM  );
                      addToVectorBranch( vars, "dvTrack_isTM_toLLP",  (int)isTM_toLLP );
                      addToVectorBranch( vars, "dvTrack_TM_LLP_Barcode", llpBarcode );
                      addToVectorBranch( vars, "dvTrack_TM_LLP_PdgID",  llpPdgID );

                      addToVectorBranch( vars, "dvtrack_TM_LLP_VtxX", llpVertX);
                      addToVectorBranch( vars, "dvtrack_TM_LLP_VtxY", llpVertY);
                      addToVectorBranch( vars, "dvtrack_TM_LLP_VtxZ", llpVertZ);

                    }//end LLP_truth detailed objects block 

            }//end is_simulated

        }//end for DV track particles

        if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) && inListOfDetailedObjects("LLP_truth") )
        {
           addToVectorBranch( vars, "DV_visibleMass_TM", toGeV(TM_p4.M()) );
           addToVectorBranch( vars, "DV_visibleMass_TM_LLP", toGeV(TM_LLP_p4.M()) );
        }

        nDV++;

        if (!dv->auxdataConst<bool>("passFiducialCuts")    ){ addToVectorBranch(vars,"DV_lastCut",0); continue;}
        if (!dv->auxdataConst<bool>("passChisqCut")        ){ addToVectorBranch(vars,"DV_lastCut",1); continue;}
        if (!dv->auxdataConst<bool>("passDistCut")         ){ addToVectorBranch(vars,"DV_lastCut",2); continue;}
        if (!dv->auxdataConst<bool>("passMaterialVeto")    ){ addToVectorBranch(vars,"DV_lastCut",3); continue;}
        if (!dv->auxdataConst<bool>("passMassCut")         ){ addToVectorBranch(vars,"DV_lastCut",4); continue;}
        if (!dv->auxdataConst<bool>("passNtrkCut")         ){ addToVectorBranch(vars,"DV_lastCut",5); continue;}
        addToVectorBranch(vars,"DV_lastCut",-1);
    }//end DV loop

    vars["DV_n"] = nDV;

    //&&&&&&&&&&&&&&&
    // L E P T O N S
    //&&&&&&&&&&&&&&&

    vars["passHLTmsonly"] = eventInfo->auxdecor<bool>("passHLTmsonly");

    //maps to IS & MS tracks
    std::map<int,int> IDtrkToMuonMap;
    std::map<int,int> MStrkToMuonMap;

    //&&&&&&&&&&&&&&&&&&&
    // E L E C T R O N S
    //&&&&&&&&&&&&&&&&&&&
    if(inListOfDetailedObjects("electrons")){
	auto electrons_nominal = HF::grabFromStore<xAOD::ElectronContainer>("STCalibElectrons",store);

	for( const auto& lep : *electrons_nominal) {
		addToVectorBranch( vars, "el_Pt"        ,  toGeV(lep->pt()));
		addToVectorBranch( vars, "el_Eta"       ,  lep->p4().Eta() );
		addToVectorBranch( vars, "el_Phi"       ,  lep->p4().Phi() );
		// addToVectorBranch( vars, "el_Sign"      ,  lep->charge() * 11. );
		addToVectorBranch( vars, "el_d0"        ,  lep->trackParticle(0)->d0() );
		addToVectorBranch( vars, "el_Baseline"  ,  (int)lep->auxdata<char>("baseline") );
		addToVectorBranch( vars, "el_Signal"    ,  (int)lep->auxdata<char>("signal") );
		// addToVectorBranch( vars, "elPassOR"    ,  (int)lep->auxdata<char>("passOR") );
	}
    }

    //&&&&&&&&&&&&&&&&&&&
    // P H O T O N S
    //&&&&&&&&&&&&&&&&&&&
    auto photons_nominal = HF::grabFromStore<xAOD::PhotonContainer>("STCalibPhotons",store);

    for( const auto& photon : *photons_nominal) {
		addToVectorBranch( vars, "ph_Pt"        ,  toGeV(photon->pt()));
		addToVectorBranch( vars, "ph_Eta"       ,  photon->p4().Eta() );
		addToVectorBranch( vars, "ph_Phi"       ,  photon->p4().Phi() );
		addToVectorBranch( vars, "ph_Baseline"  ,  (int)photon->auxdata<char>("baseline") );
		addToVectorBranch( vars, "ph_Signal"    ,  (int)photon->auxdata<char>("signal") );
	        addToVectorBranch( vars, "ph_PassOR"    ,  (int)photon->auxdata<char>("passOR") );
    }

    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    // M U O N S,  B A S E L I N E
    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	auto muons_baseline = HF::grabFromStore<xAOD::IParticleContainer>("selectedBaselineMuons",store);

    if( inListOfDetailedObjects("muons_baseline") ){
	for( xAOD::IParticle * muon : *muons_baseline){

		xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

		addToVectorBranch( vars, "muon_index"  , mu->index());
		addToVectorBranch( vars, "muon_pt"     , toGeV(mu->pt()));
		addToVectorBranch( vars, "muon_eta"    , mu->p4().Eta());
		addToVectorBranch( vars, "muon_phi"    , mu->p4().Phi());
		addToVectorBranch( vars, "muon_charge" , mu->charge() );

		if(inListOfDetailedObjects("muons")){

			const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
			const xAOD::TrackParticle* metrack = mu->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
			const xAOD::TrackParticle* mstrack = mu->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
			const xAOD::TrackParticle* cbtrack = mu->trackParticle( xAOD::Muon::CombinedTrackParticle );

			if (idtrack) IDtrkToMuonMap[ (idtrack)->index() ] = mu->index();
			if (mstrack) MStrkToMuonMap[ (mstrack)->index() ] = mu->index();

			addToVectorBranch( vars, "muon_hasCBtrack" , (cbtrack) ? 1 : 0 );
			addToVectorBranch( vars, "muon_hasIDtrack" , (idtrack) ? 1 : 0 );
			addToVectorBranch( vars, "muon_hasMEtrack" , (metrack) ? 1 : 0 );
			addToVectorBranch( vars, "muon_hasMStrack" , (mstrack) ? 1 : 0 );

			addToVectorBranch( vars, "muon_d0"      , (idtrack) ? idtrack->d0() : -9999.);
			addToVectorBranch( vars, "muon_z0"      , (idtrack) ? idtrack->z0() : -9999.);
			addToVectorBranch( vars, "muon_errd0"   , (idtrack) ? TMath::Sqrt(idtrack->definingParametersCovMatrix()(0, 0)) : -9999 );
			addToVectorBranch( vars, "muon_errz0"   , (idtrack) ? TMath::Sqrt(idtrack->definingParametersCovMatrix()(1, 1)) : -9999 );
			addToVectorBranch( vars, "muon_isLRT"   , (idtrack && idtrack->isAvailable<unsigned long>("patternRecoInfo") ) ? static_cast<int>(idtrack->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0)) : -1 );
			addToVectorBranch( vars, "muon_RadFirstHit" , (idtrack&& idtrack->isAvailable<float>("radiusOfFirstHit") ) ? idtrack->auxdata< float >("radiusOfFirstHit" ) : -999. );

			addToVectorBranch( vars, "muon_ptcone20"    , toGeV( mu->auxdata< float >("ptcone20")     ) );
			addToVectorBranch( vars, "muon_ptcone30"    , toGeV( mu->auxdata< float >("ptcone30")     ) );
			addToVectorBranch( vars, "muon_ptcone40"    , toGeV( mu->auxdata< float >("ptcone40")     ) );
			addToVectorBranch( vars, "muon_ptvarcone20" , toGeV( mu->auxdata< float >("ptvarcone20")  ) );
			addToVectorBranch( vars, "muon_ptvarcone30" , toGeV( mu->auxdata< float >("ptvarcone30")  ) );
			addToVectorBranch( vars, "muon_ptvarcone40" , toGeV( mu->auxdata< float >("ptvarcone40")  ) );
			addToVectorBranch( vars, "muon_topoetcone20", toGeV( mu->auxdata< float >("topoetcone20") ) );
			addToVectorBranch( vars, "muon_topoetcone30", toGeV( mu->auxdata< float >("topoetcone30") ) );
			addToVectorBranch( vars, "muon_topoetcone40", toGeV( mu->auxdata< float >("topoetcone40") ) );

			addToVectorBranch( vars, "muon_nPIX"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfPixelHits") : -999);
			addToVectorBranch( vars, "muon_nSCT"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfSCTHits")   : -999);
			addToVectorBranch( vars, "muon_nTRT"      , (idtrack) ? idtrack->auxdata< unsigned char >("numberOfTRTHits")   : -999);

			addToVectorBranch( vars, "muon_nPres"      , mu->auxdata< unsigned char >("numberOfPrecisionLayers") );
			addToVectorBranch( vars, "muon_nPresGood"  , mu->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
			addToVectorBranch( vars, "muon_nPresHole"  , mu->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );

			addToVectorBranch( vars, "muon_CBchi2" , (cbtrack) ? cbtrack->auxdata< float >("chiSquared")/(cbtrack->auxdata< float >("numberDoF")) : -999. );

			float qOverPsigma  = -999.;
			float qOverPsignif = -999.;
			float rho = -999.;
			if ( (idtrack) && (metrack) && (cbtrack) ){
				float cbPt = cbtrack->pt();
				float idPt = idtrack->pt();
				float mePt = metrack->pt();
				float meP  = 1.0 / ( sin(metrack->theta()) / mePt);
				float idP  = 1.0 / ( sin(idtrack->theta()) / idPt);

				rho = fabs( idPt - mePt ) / cbPt;
				qOverPsigma  = sqrt( idtrack->definingParametersCovMatrix()(4,4) + metrack->definingParametersCovMatrix()(4,4) );
				qOverPsignif  = fabs( (metrack->charge() / meP) - (idtrack->charge() / idP) ) / qOverPsigma;
			}
			addToVectorBranch( vars, "muon_rho"   , rho );
			addToVectorBranch( vars, "muon_QoverPsignif", qOverPsignif );

			addToVectorBranch( vars, "muon_author" ,     mu->auxdata< unsigned short >("author") );
			addToVectorBranch( vars, "muon_isCommonGood",mu->auxdata< char >("DFCommonGoodMuon") );
			addToVectorBranch( vars, "muon_isSignal",    mu->auxdata< char >("signal") );
			addToVectorBranch( vars, "muon_truthType",   mu->auxdata< int >("truthType") );
			addToVectorBranch( vars, "muon_truthOrigin", mu->auxdata< int >("truthOrigin") );

			// Save cosmic veto information
			addToVectorBranch( vars, "muon_passSegmentMatch"     , mu->auxdataConst<bool>("passSegmentMatch"    ) ? 1 : 0 );
			addToVectorBranch( vars, "muon_passCosmicAcceptance" , mu->auxdataConst<bool>("passCosmicAcceptance") ? 1 : 0 );
			addToVectorBranch( vars, "muon_passCosmicVeto"       , mu->auxdataConst<bool>("passCosmicVeto"      ) ? 1 : 0 );

			addToVectorBranch( vars, "muon_passPreselection"  , mu->auxdataConst<bool>("passPreselection" ) ? 1 : 0) ;
			addToVectorBranch( vars, "muon_passFakeVeto"      , mu->auxdataConst<bool>("passFakeVeto"     ) ? 1 : 0) ;
			addToVectorBranch( vars, "muon_passIsolation"     , mu->auxdataConst<bool>("passIsolation"    ) ? 1 : 0) ;
			addToVectorBranch( vars, "muon_passFullSelection" , mu->auxdataConst<bool>("passFullSelection") ? 1 : 0) ;
			addToVectorBranch( vars, "muon_isLeading"         , mu->auxdataConst<bool>("isLeading"        ) ? 1 : 0) ;

			// Retrieve DV Mapping info, 3 attempts
			float dv_index = -99;
			if ( idtrack && dvc->size() > 0) {
				if ( TrkToVtxMap.find(idtrack->index()) != TrkToVtxMap.end() ) dv_index = TrkToVtxMap[idtrack->index()];
			}

			// try to get info w.r.t. displaced vertices
			// turning off until susy derivations are available
			// std::cout << "muon d0 information" << std::endl;
			// std::vector<std::vector<float>> d0s_wrtDVs = mu->auxdata< std::vector<std::vector<float>> >("d0_wrtSVs");
			// std::vector<std::vector<float>> z0s_wrtDVs = mu->auxdata< std::vector<std::vector<float>> >("z0_wrtSVs");
			//for (unsigned int type=0; type<d0s_wrtDVs.size() ; type++ ){
			//  std::vector<float> d0s_wrtDVs_forType = d0s_wrtDVs.at(type);
			//  std::vector<float> z0s_wrtDVs_forType = z0s_wrtDVs.at(type);
			//  for (unsigned int dv=0; dv<d0s_wrtDVs_forType.size() ; dv++){
			//
			//    addToVectorBranch( vars, "muDV_muIndex"   ,mu->index());
			//    addToVectorBranch( vars, "muDV_dvIndex"   ,dv);
			//    addToVectorBranch( vars, "muDV_typeIndex" ,type);
			//    addToVectorBranch( vars, "muDV_d0wrtDV" ,d0s_wrtDVs_forType.at(dv));
			//    addToVectorBranch( vars, "muDV_z0wrtDV" ,z0s_wrtDVs_forType.at(dv));
			//  }
			//}

			addToVectorBranch( vars, "muon_DVindex"       ,  dv_index );
			addToVectorBranch( vars, "muon_TrigMatch"      ,mu->auxdata< int >( "passTM" ) );
			addToVectorBranch( vars, "muon_TrigMatchMSOnly",mu->auxdata< int >( "passTMmsonly" ) );
		}
	}
    }
	// save if event passes Z->mumu for easy skimming
	if(inListOfDetailedObjects("muons")){
		vars["passZmumu"] = eventInfo->auxdata<bool>( "passZselection" );
		vars["zMass"]     = eventInfo->auxdata<float>( "zMass" );
	}

    //&&&&&&&&&&&&&&&&&&&&&&
    // M S  T R A C K S
    //&&&&&&&&&&&&&&&&&&&&&&
	if(inListOfDetailedObjects("msTracks")){

		auto muons = HF::grabFromEvent<xAOD::MuonContainer>("Muons",event);

		for ( auto muon : *muons ){

			const xAOD::TrackParticle* mst = muon->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
			if (mst) {
				addToVectorBranch( vars, "mstrack_MuonIndex" ,  ( MStrkToMuonMap.find(mst->index()) == MStrkToMuonMap.end() ) ? -99 : MStrkToMuonMap[mst->index()] );
				addToVectorBranch( vars, "mstrack_eta" ,    mst->eta() );
				addToVectorBranch( vars, "mstrack_phi" ,    mst->phi() );
				addToVectorBranch( vars, "mstrack_pt"  ,    toGeV( mst->pt() )  );
				addToVectorBranch( vars, "mstrack_D0"  ,    mst->d0()  );
				addToVectorBranch( vars, "mstrack_Z0"  ,    mst->z0()  );
				addToVectorBranch( vars, "mstrack_ELoss" ,            toGeV(  muon->auxdata<float>("EnergyLoss")   	     ) );
				addToVectorBranch( vars, "mstrack_ELossSigma" ,       toGeV(  muon->auxdata<float>("EnergyLossSigma") 	     ) );
				addToVectorBranch( vars, "mstrack_MeasELoss" ,        toGeV(  muon->auxdata<float>("MeasEnergyLoss") 	     ) );
				addToVectorBranch( vars, "mstrack_MeasELossSigma" ,   toGeV(  muon->auxdata<float>("MeasEnergyLossSigma")      ) );
				addToVectorBranch( vars, "mstrack_ParamELoss" ,       toGeV(  muon->auxdata<float>("ParamEnergyLoss")          ) );
				addToVectorBranch( vars, "mstrack_ParamELossSigmaM" , toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaMinus")) );
				addToVectorBranch( vars, "mstrack_ParamELossSigmaP" , toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaPlus") ) );
				addToVectorBranch( vars, "mstrack_nPres" ,      muon->auxdata< unsigned char >("numberOfPrecisionLayers") );
				addToVectorBranch( vars, "mstrack_nPresGood" ,  muon->auxdata< unsigned char >("numberOfGoodPrecisionLayers") );
				addToVectorBranch( vars, "mstrack_nPresHole" ,  muon->auxdata< unsigned char >("numberOfPrecisionHoleLayers") );
			}
		}

	}

    ANA_MSG_DEBUG("Saving ID tracks...");
    //&&&&&&&&&&&&&&&&&&
    // I D  T R A C K S
    //&&&&&&&&&&&&&&&&&&
	//write out inner detector tracks
	//to do: only do this if they're opposite a CB muon?
	//or a right angle away? for bkg estimation
	if(inListOfDetailedObjects("prescaled_idTracks")){

		addToVectorBranch( vars, "n_idTracks" , id_tracks->size() );

		for (auto *id_track : *id_tracks){

			// Save ID tracks if back to back in dR cosmic with muons
			int cos_tag = 0;
			for( xAOD::IParticle * muon : *muons_baseline){
				xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);
				if ( cosmic_Tag(id_track->p4(), mu->p4() ) ) cos_tag=1;
			}

			if ( id_track->index()%m_id_prescale != 0   && !cos_tag) continue; //prescale!
			if ( fabs(id_track->d0())    < m_id_d0_min  && !cos_tag) continue; //d0 cut
			if ( toGeV( id_track->pt() ) < m_id_pt_min  && !cos_tag) continue; //pt cut

			addToVectorBranch( vars, "idTrack_index" , id_track->index() );
			addToVectorBranch( vars, "idTrack_cosTag", cos_tag );
			addToVectorBranch( vars, "idTrack_isLRT" , static_cast<int>( id_track->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) );


			// From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
			double id_pt = id_track->pt();
			double id_diff_qp = -id_pt / std::fabs(id_track->qOverP());
			double id_diff_theta = id_pt / tan(id_track->theta());
			const std::vector<float> &id_cov = id_track->definingParametersCovMatrixVec();
			double id_pt_err2 = id_diff_qp * (id_diff_qp * id_cov[14] + id_diff_theta * id_cov[13]) + id_diff_theta * id_diff_theta * id_cov[9];
			double id_errpT = toGeV( TMath::Sqrt(id_pt_err2) );

			addToVectorBranch( vars, "idTrack_errPt"   , id_errpT );
			addToVectorBranch( vars, "idTrack_errd0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(0, 0)) );
			addToVectorBranch( vars, "idTrack_errz0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(1, 1)) );

			addToVectorBranch( vars, "idTrack_theta"   ,id_track->theta() );
			addToVectorBranch( vars, "idTrack_eta"     ,id_track->eta() );
			addToVectorBranch( vars, "idTrack_phi"     ,id_track->phi() );
			addToVectorBranch( vars, "idTrack_pt"      ,toGeV( id_track->pt() ) );
			addToVectorBranch( vars, "idTrack_d0"      ,id_track->d0()  );
			addToVectorBranch( vars, "idTrack_z0"      ,id_track->z0()  );
			addToVectorBranch( vars, "idTrack_z0WrtPV" ,id_track->z0() + id_track->vz() - pv->z() );
			addToVectorBranch( vars, "idTrack_charge"  ,id_track->charge() );
			addToVectorBranch( vars, "idTrack_chi2"    ,id_track->auxdata< float >("chiSquared")/(id_track->auxdata< float >("numberDoF")) );

			addToVectorBranch( vars, "idTrack_RadFirstHit"     ,id_track->auxdata< float >("radiusOfFirstHit"    ) );

			addToVectorBranch( vars, "idTrack_NPix_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfPixelHits"       ) );
			addToVectorBranch( vars, "idTrack_NSct_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfSCTHits"         ) );
			addToVectorBranch( vars, "idTrack_NTrt_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfTRTHits"         ) );
			addToVectorBranch( vars, "idTrack_NPix_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfPixelHoles"      ) );
			addToVectorBranch( vars, "idTrack_NSct_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfSCTHoles"        ) );
			addToVectorBranch( vars, "idTrack_NTrt_Outliers"   ,(int) id_track->auxdata< unsigned char >("numberOfTRTOutliers"     ) );
			addToVectorBranch( vars, "idTrack_NPix_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfPixelDeadSensors") );
			addToVectorBranch( vars, "idTrack_NPix_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfPixelSharedHits" ) );
			addToVectorBranch( vars, "idTrack_NSct_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) );
			addToVectorBranch( vars, "idTrack_NSct_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfSCTSharedHits"   ) );
            addToVectorBranch( vars, "idTrack_hitPattern"        , id_track->auxdata< unsigned int >("hitPattern"   ) );
		}

	}// end if save id tracks

    //&&&&&&&&&&&&&&&&&&&&&&
    // M S  S E G M E N T S
    //&&&&&&&&&&&&&&&&&&&&&&
	if(inListOfDetailedObjects("msSegments")){
		auto ms_segments = HF::grabFromEvent<xAOD::MuonSegmentContainer>("MuonSegments",event);

		for (auto *ms_segment : *ms_segments){

			addToVectorBranch( vars, "msSegment_x"            , ms_segment->x() );
			addToVectorBranch( vars, "msSegment_y"            , ms_segment->y() );
			addToVectorBranch( vars, "msSegment_z"            , ms_segment->z() );
			//addToVectorBranch( vars, "msSegment_px"           , ms_segment->px() );
			//addToVectorBranch( vars, "msSegment_py"           , ms_segment->py() );
			//addToVectorBranch( vars, "msSegment_pz"           , ms_segment->pz() );
			addToVectorBranch( vars, "msSegment_t0"           , ms_segment->auxdataConst< float >( "t0" ) );
			//addToVectorBranch( vars, "msSegment_t0Err"        , ms_segment->auxdataConst< float >( "t0error"      ) );
			//addToVectorBranch( vars, "msSegment_clusTimeErr"  , ms_segment->auxdataConst< float >( "clusterTimeError" ) );
			addToVectorBranch( vars, "msSegment_clusTime"     , ms_segment->auxdataConst< float >( "clusterTime"      ) );
			addToVectorBranch( vars, "msSegment_chmbIndex"    , ms_segment->auxdataConst< int >( "chamberIndex" ) );
			//addToVectorBranch( vars, "msSegment_tech"         , ms_segment->auxdataConst< int >( "technology"   ) );
			addToVectorBranch( vars, "msSegment_sector"       , ms_segment->auxdataConst< int >( "sector"       ) );
			addToVectorBranch( vars, "msSegment_etaIndex"     , ms_segment->auxdataConst< int >( "etaIndex"     ) );
			addToVectorBranch( vars, "msSegment_nTrigEtaLays" , ms_segment->auxdataConst< int >( "nTrigEtaLayers" ) );
			addToVectorBranch( vars, "msSegment_nPhiLays"     , ms_segment->auxdataConst< int >( "nPhiLayers"     ) );
			addToVectorBranch( vars, "msSegment_nPresHits"    , ms_segment->auxdataConst< int >( "nPrecisionHits" ) );

		}

	}//end doCosmic


	return EL::StatusCode::SUCCESS;
}
