/*###############################################
 name: Baseline_DVJETS_Selection
 type: C++ namespace
 responsible: rcarney@lbl.gov

 description:
    Contains a struct, Flags, used to store
     decisions. Contains flag-setting
    functions and logic functions.

 last updated: Mar. 2018
###############################################*/
#include "FactoryTools/Baseline_DVJETS_Selection.h"

//*********************************
// F L A G   C H E C K
//*********************************
//=======================================
//Multiplicity flags - calibrated jet pT
//=======================================
bool Baseline_DVJETS_Selection::DV_PtFlags( float jetPt, float threshold){

    if( jetPt >= threshold ) return true;
    else return false;
}
bool Baseline_DVJETS_Selection::DV_TracklessFlags( float jetPt, float threshold, float jetEta, float sumTrkPt){

    if( jetPt >= threshold && fabs(jetEta)<=2.5 && sumTrkPt <= 5.) return true;
    else return false;
}


//*********************************
// S E T   F I L T E R   F L A G S
//*********************************
void Baseline_DVJETS_Selection::setFlags( const xAOD::JetContainer* STCalibJets, Flags& flags ){

    TH1F* h_highpt_filters  = new TH1F("HighptFilters", "HighptFilters; Jet Multiplicity; Entries;", 10, 0, 10);
    TH1F* h_lowpt_filters   = new TH1F("LowptFilters", "LowptFilters; Jet Multiplicity; Entries;", 10, 0, 10);
    TH1F* h_trkless_filters = new TH1F("TracklFilters", "TracklFilters; Jet Multiplicity; Entries;", 10, 0, 10);

    // Set pT thresholds here: { jet multiplicity, jet pT threshold [GeV] }
    std::map< int, float> highPtThresholds ={{ 2, 551. },
                                             { 3, 206. },
                                             { 4, 250. },
                                             { 5, 195. },
                                             { 6, 116. },
                                             { 7, 90.  }};

    std::map< int, float> lowPtThresholds = {{ 2, 307. },
                                             { 3, 161. },
                                             { 4, 137. },
                                             { 5, 101.  },
                                             { 6, 83.  },
                                             { 7, 55.  }};

    std::map< int, float> tracklessPtThresholds = {{ 1, 78. },
                                                   { 2, 56. }};

    // Determine which pT thresholds each jet passes
    for (const auto& jet : *STCalibJets ){

        // Apply jet cleaning
	if ((int)jet->auxdata<char>("passOR")==0) continue;
        if ((int)jet->auxdata<char>("bad")==1) continue;

        //Get jet attributes (convert to GeV where necessary)
        float jetPt = jet->pt()*0.001;
        float jetEta = jet->p4().Eta();
        std::vector<float> sumPtTrkvec;
        jet->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec);
        float sumTrkPt = sumPtTrkvec[0]*0.001;

        // Determine if jet passes high/low pT thresholds
        for (int i=2; i<8; i++){
          if( Baseline_DVJETS_Selection::DV_PtFlags( jetPt, highPtThresholds[i] ) ) h_highpt_filters->AddBinContent(i);
          if( Baseline_DVJETS_Selection::DV_PtFlags( jetPt, lowPtThresholds[i] ) )  h_lowpt_filters->AddBinContent(i);
        }

        // Determine if jet passes trackless requirements
        if( Baseline_DVJETS_Selection::DV_TracklessFlags( jetPt, tracklessPtThresholds[1], jetEta, sumTrkPt ) ) h_trkless_filters->AddBinContent(1);
        if( Baseline_DVJETS_Selection::DV_TracklessFlags( jetPt, tracklessPtThresholds[2], jetEta, sumTrkPt ) ) h_trkless_filters->AddBinContent(2);
    }

    //Set filter flags
    flags.reset();

    // Count jets passing each low/high pt threshold
    for (int i=2; i<8; i++){
      if( h_highpt_filters->GetBinContent(i) >= i){
          if (i==2)      flags.pass_2_jetHighPtFlag = 1;
          else if (i==3) flags.pass_3_jetHighPtFlag = 1;
          else if (i==4) flags.pass_4_jetHighPtFlag = 1;
          else if (i==5) flags.pass_5_jetHighPtFlag = 1;
          else if (i==6) flags.pass_6_jetHighPtFlag = 1;
          else if (i==7) flags.pass_7_jetHighPtFlag = 1;
      }
      if( h_lowpt_filters->GetBinContent(i) >= i){
          if (i==2)      flags.pass_2_jetLowPtFlag = 1;
          else if (i==3) flags.pass_3_jetLowPtFlag = 1;
          else if (i==4) flags.pass_4_jetLowPtFlag = 1;
          else if (i==5) flags.pass_5_jetLowPtFlag = 1;
          else if (i==6) flags.pass_6_jetLowPtFlag = 1;
          else if (i==7) flags.pass_7_jetLowPtFlag = 1;
      }
    }

    // Count jets passing trackless requirements
    if( h_trkless_filters->GetBinContent(1) >= 1 ){
        flags.pass_SingleTracklessFlag = 1;
    }
    if( h_trkless_filters->GetBinContent(2) >= 2 ){
        flags.pass_DoubleTracklessFlag = 1;
    }

    delete h_highpt_filters;
    delete h_lowpt_filters;
    delete h_trkless_filters;
    return;
}

//*********************************
// F I L T E R   E V E N T S
//*********************************
EL::StatusCode Baseline_DVJETS_Selection::filterEvents(const xAOD::JetContainer* STCalibJets, Flags& flags){

    if( STCalibJets == NULL ){
        std::cout<<"ERROR! Calibrated jets pointer is null. Exiting."<<std::endl;
        return EL::StatusCode::FAILURE;
    }

    setFlags( STCalibJets, flags );

    //High-pt SR baseline jet selection
    if(     flags.pass_2_jetHighPtFlag==1 &&
            flags.pass_3_jetHighPtFlag==1 ){
        flags.pass_2AND3_jetHighPtFlag = 1;
    }

    // NB: 2AND3 jet selection is not used
    if(     flags.pass_4_jetHighPtFlag==1      ||
            flags.pass_5_jetHighPtFlag==1      ||
            flags.pass_6_jetHighPtFlag==1      ||
            flags.pass_7_jetHighPtFlag==1 ){
        flags.pass_jetHighPtSelection = 1;
    }

    //Trackless SR baseline jet selection
    if(     flags.pass_2_jetLowPtFlag==1 &&
            flags.pass_3_jetLowPtFlag==1 ){
        flags.pass_2AND3_jetLowPtFlag = 1;
    }

    // NB: 2AND3 jet selection is not used
    if(     flags.pass_4_jetLowPtFlag==1      ||
            flags.pass_5_jetLowPtFlag==1      ||
            flags.pass_6_jetLowPtFlag==1      ||
            flags.pass_7_jetLowPtFlag==1 ){
        flags.pass_jetLowPtSelection = 1;
    }

    if (    flags.pass_SingleTracklessFlag==1 ||
            flags.pass_DoubleTracklessFlag==1 ){
        flags.pass_jetTracklessSelection = 1;
    }

    if (    flags.pass_jetLowPtSelection==1 &&
            flags.pass_jetTracklessSelection==1 &&
            flags.pass_jetHighPtSelection==0 ){
        flags.pass_SRTracklessSelection = 1;
    }

    return EL::StatusCode::SUCCESS;
}

