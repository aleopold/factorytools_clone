//=================================
// name: Select_DRAWFilterEvents
// type: Selection tool (body)
// responsible: rcarney@lbl.gov
// description:
// 	Applies no special selections,
// 	simply a placeholder for now.
// 	Necessary for WriteNtupleTool
// 	to retrieve RegionName from aux store.
//
//  last updated: Feb. 2018
//=================================
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>
#include <FactoryTools/Select_DRAWFilterEvents.h>

#include <AsgTools/MsgStream.h>
#include <AsgTools/MsgStreamMacros.h>

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"


#include "xAODParticleEvent/ParticleContainer.h"
#include "xAODParticleEvent/ParticleAuxContainer.h"

#include <boost/algorithm/string.hpp>


// this is needed to distribute the algorithm to the workers
ClassImp(Select_DRAWFilterEvents)


//**************************
// E X E C U T E
//**************************
EL::StatusCode Select_DRAWFilterEvents :: execute (){

  //xAOD::TStore * store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

//TODO: what constitutes preselection?
// If the event didn't pass the preselection alg, don't bother doing anything with it...
//  std::string preselectedRegionName =  eventInfo->auxdecor< std::string >("regionName");
//  ATH_MSG_DEBUG("EventNumber: " << eventInfo->eventNumber()  );
//  ATH_MSG_DEBUG("Preselected?: " << preselectedRegionName  );
//
//  if( preselectedRegionName == "" ) return EL::StatusCode::SUCCESS;

  auto  getRegionName = [](){
	// for now, letting all events pass into n-tuples 
	std::string regionName = "earlyRun2";	
	return regionName;
  };

  eventInfo->auxdecor< std::string >("regionName") = getRegionName() ;
  ATH_MSG_DEBUG("Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName")   );

  return EL::StatusCode::SUCCESS;
}


//************************************
// Empty functions inherited from base
//************************************
Select_DRAWFilterEvents :: Select_DRAWFilterEvents (){}

EL::StatusCode Select_DRAWFilterEvents :: setupJob (EL::Job& /*job*/){return EL::StatusCode::SUCCESS;}

EL::StatusCode Select_DRAWFilterEvents :: histInitialize (){return EL::StatusCode::SUCCESS;}

EL::StatusCode Select_DRAWFilterEvents :: fileExecute (){return EL::StatusCode::SUCCESS;}

EL::StatusCode Select_DRAWFilterEvents :: changeInput (bool /*firstFile*/){return EL::StatusCode::SUCCESS;}

EL::StatusCode Select_DRAWFilterEvents :: initialize (){return EL::StatusCode::SUCCESS;}

EL::StatusCode Select_DRAWFilterEvents :: postExecute (){return EL::StatusCode::SUCCESS;}

EL::StatusCode Select_DRAWFilterEvents :: finalize (){return EL::StatusCode::SUCCESS;}

EL::StatusCode Select_DRAWFilterEvents :: histFinalize (){return EL::StatusCode::SUCCESS;}

