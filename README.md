# FactoryTools

For installation instructions, see below to the "Getting Started" section. For information about contributing, see `CONTRIBUTING.md`.

## Structure and Philosophy

`FactoryTools` is a framework based on `EventLoop` algorithms whose goal is to go from xAODs (or DxAODs) to a flat output ROOT ntuple for use in analysis.

A fundamental philosophy is to allow analyzers to focus purely on the physics at hand and not have to worry about:

* Dataset handling and grid/batch running
* Memory handling of output ntuple
* Calibration of physics objects
* Any selection that is common to all analyses (GRL, cleaning, etc)

and the user can focus on their analysis-specific selection, and their custom output ntuple format.

The primary structure is as follows: A python running script that schedules a series of algorithms to run for these events. These modular algorithms communicate by handing objects and collections to the TStore for use in downstream algorithms.

(For example, if you have a uncalibrated jet collection from your TEvent, a calibration algorithm can grab it, calibrate the jets, and write out a new collection to the TStore. Then a selection algorithm can categorize the event based on this calibrated jet collection, and then an output ntuple algorithm can write out various properties to the output ntuple.)

We outsource some tasks to other projects that have taken care of many problems for us. For things like trigger decisions, GRL, event cleaning, etc, we use the `BasicEventSelection` algorithm from [`xAODAnaHelpers`](https://github.com/UCATLAS/xAODAnaHelpers).

So an example job involves:

1. BasicEventSelection -- from xAH to take care of those things common to every analysis
2. CalibrateST -- Using SUSYTools to calibrate the physics objects.
3. Selection Algorithm -- Use the objects from the TStore to categorize or throw away event
4. Calculators -- Use the containers in the TStore (from upstream algorithms) to calculate any interesting variable. Write it to maps (key: branch names, value) and store them in TStore
5. WriteOutputNtuple -- Grabs those maps from the TStore and automatically writes them out to output ntuples!

For most analyses, steps 1., 2., and 5. can be used out of the box and one only needs to write selection and calculator algorithms. This is just making cuts and calculating numbers from physics objects with no need to play with the rest of the overhead.


Some fundamental design philosophies to follow:

* The user should only have to focus on physics objects, calculation of variables, format of output
* Creating new branches in output should be basically one line. No duplication of information at implementation level.
* Transparently run from input locations (local, RSE, grid) using various CPU sources (local, grid, condor, slurm, LSF) in any combination

Enjoy, and please subscribe to and contact [`atlas-phys-gen-factorytools@cern.ch`](mailto:atlas-phys-gen-factorytools@cern.ch) for major announcements, questions, discussions. Questions, bug fixes, feature requests should go into the gitlab repo which will be centralized [here](https://gitlab.cern.ch/atlas-phys-susy-wg/Common/FactoryTools).

There's a mattermost team devoted to discussing central `FactoryTools` usage and development which you can sign up for [here](https://mattermost.web.cern.ch/signup_user_complete/?id=wxujfwrwp3g7imjgdj9e6ekkpe).

## Getting Started

### First-time Setup and Installation

(See shortcut below. To save time, you can now just grab `INSTALL.sh` and source it.)

Make sure you have a directory structure that works for a modern CMake setup.

```
mkdir WorkArea; cd WorkArea; #!
mkdir build; #!
mkdir src; #!
mkdir run; #!
cd src; #!
```

Get FactoryTools with:

```
# The dependencies will automatically be cloned because of the --recursive option

setupATLAS; #!
lsetup git; #!
git clone --recursive https://:@gitlab.cern.ch:8443/atlas-phys-susy-wg/Common/FactoryTools.git #!

cd FactoryTools; #!
```

You'll still need a few dependencies that aren't in AnalysisBase, but now they're shipped with FT as the submodule! (`xAODAnaHelpers`).

Then let's turn off the modules we don't care about to reduce compilation time.

```
# Still in directory FactoryTools. Do just once...
source util/dependencyHacks.sh; #!!
```

And then setup the environment for compilation:

```
cp util/CMakeLists.txt.TopLevel ../CMakeLists.txt; #!!
source util/setup.sh; #!!
```

And compile

```
cd ../../build/; #!!
cmake ../src/; #!
make; #!
```

Then make sure you setup the new environment:

```
source x*-*gcc*-opt/setup.sh  #!!  # (wildcards since os and gcc versions may differ)
```

#### Shortcut (if you know what you're doing)

There's now a commit hook that automatically creates a script `INSTALL.sh` (based on the hash-bangs in this `README.md`) and throws it in the root directory. So now the easiest way to install from scratch is to grab this `INSTALL.sh` and run

```
source INSTALL.sh
```

wherever you want to throw `FactoryTools`.

### Future Sessions w/ Same Install

Future sessions, you'll just need to source `FactoryTools/util/setup.sh` from anywhere. The `setup.sh` in `build/` will automatically be sourced for you.
Of course if you make changes to the actual C++ algorithms, you'll have to go back into your `build` directory and `make`.

### Running

Go to your run directory

```
cd ../run/
```

Then you can run a test with e.g. (or use whatever run_* script you want!):

```
run_dv.py --doOverwrite --nevents 10 --verbosity warning --inputDS /eos/atlas/atlascerngroupdisk/phys-susy/DVjets_ANA-SUSY-2018-13/data
18_13TeV.00360373.physics_Main.deriv.DAOD_SUSY15.f969_m1831_r10799_p3651_p4296/
```

(This points to a public directory so this should run for anyone on afs. Note that `run_dv.py` requires that you run over a recent `DAOD_SUSY15` file.)

The python scripts in the `util` folder are put into your scripts path, which is why you can directly call `run_*.py` at the command line.

The inputDS option is smart.  You can give supply a local directory, txt file with a list of grid datasets, or a pattern which matches a grid pattern.  If you use the grid options, obviously you need panda stuff setup, which is setup by the setup script.

PLEASE NOTE : The output will be in
```
submit_dir/data-TEST_OUTPUTNAME/*.root
```
by default when doing a test, where TEST_OUTPUTNAME is set in your submission script.  The file in submit_dir does not have your output trees!!!


To run on the grid, you can call

```
run_dv.py --doOverwrite --driver grid --inputDS FactoryTools/data/data16_SUSY15_debug.ds
```

This default assumes your system username is the same as your grid/NICE name. If not, you can change it with --gridUser, and the --gridTag defaults to just the date.

If you hand `--inputDS` grid datasets and request the local driver, it'll automatically FAX down the samples assuming they're available.


## Merging Step

Once you've run on the grid, download the tree output and the metadata output to some directory. Then you can use the new merging script to combine them with a new branch added that incorporates the cross section weighting. Simply run

```
mergeOutput.py --inDir [path to datasets from grid]
```

And it will produce combined files containing the trees. One just needs to weight by the branch normweight. The assignment of dataset types is done in discoverInput.py where tags are added to samples based on their names. Then all of one kind of process are combined.

This will default to using 4 CPUs, but this is configurable with the `--nproc` option.

This can also be used with the `--inRucioDS` option instead of `--inDir` to point to a file with rucio DS names. This will then build a list of locally-accessible files stored on an RSE system. Try with:

```
mergeOutput.py --inRucioDS datasets --rse AUSTRALIA-ATLAS_LOCALGROUPDISK
```

where the `--rse` option points to the disk that contains the replicas of the dids contained in the file `datasets`. The default disk is `CERN-PROD_LOCALGROUPDISK`.

## RECAST (Experimental)

RECAST is now tied closely with the `FactoryTools` CI setup. If there is an implemented CI test for an analysis, it is basically setup for RECAST. The docker images are automatically built for tags and master (the latter is labeled `latest`). Given you have the dependencies set up, it is simple to run the full chain and get an output ntuple and yields summary right from your docker host.

You'll need the following:
* Docker installed
* `yadage` installed (`pip install yadage`)
* `recast` installed (`pip install https://github.com/recast-hep/recast-atlas/archive/master.zip`)
* `eval $(recast auth setup)` The eval wrapper is so your password isn't printed to the console.
* `eval $(recast auth write --basedir here)`

The `eval $(recast auth setup)` will ask you for a CERN login and an optional personal gitlab token. The latter will allow you to stream the config directly from the repo, which is nice. You can make this gitlab token in your gitlab profile settings. (https://gitlab.cern.ch/profile/personal_access_tokens) With this, you should be able run:

```
yadage-run testDir workflow.yml -t gitlab-cern:atlas-phys-susy-wg/Common/FactoryTools:recast/ test.yml 
```

where `test.yml` is local and contains input params like:

```
input_file: root://eosuser.cern.ch//eos/user/l/leejr/factoryToolsSample/DVPlusMuSignal_DAOD_RPVLL/
run_script: dv.py
```

This will run the script `scripts/dv.py` over the samples in `root://eosuser.cern.ch//eos/user/l/leejr/factoryToolsSample/DVPlusMuSignal_DAOD_RPVLL/` using the `latest`-tagged docker image (i.e. from the HEAD of `master`).
